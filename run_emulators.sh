#!/usr/bin/env bash

cd tango_server
export PYTHONPATH=..
pwd

#./tests/run_peripheral_emulator.sh &
#./tests/run_ecg_emulator.sh &
python tests/model/equipment/subsystem_device_manager/scsc_emulator/reconstruction_emulator/reconstruction_server.py &
sleep 3
python tests/model/equipment/subsystem_device_manager/scsc_emulator/tango/scsc_tango_server.py