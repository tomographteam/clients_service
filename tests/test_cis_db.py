import pytest
from src.db_manager import DataBaseManager
import os

from src.model import Protocol, ProtocolStep

os.environ.setdefault('PATHS', '')


@pytest.fixture
def manager():
    return DataBaseManager()


PROTOCOL_NAME = "Test protocol"
PROTOCOL_STEP_NAME = "Test protocol step"
protocol_id = None
step_id = None


def test_save_protocol(manager):
    global protocol_id
    p = Protocol(name=PROTOCOL_NAME)
    protocol_id = manager.save_protocol(p)
    assert protocol_id is not None


def test_get_protocols(manager):
    protocols = manager.get_protocols()
    names = [p.name for p in protocols]
    assert PROTOCOL_NAME in names
    ids = [s.id for s in protocols]
    assert protocol_id in ids


def test_save_protocol_step(manager):
    global step_id
    s = ProtocolStep(name=PROTOCOL_STEP_NAME, protocol_id = protocol_id)
    step_id = manager.save_protocol_step(s)
    assert step_id is not None


def test_get_protocol_steps(manager):
    global protocol_id, step_id
    steps = manager.get_protocol_steps(protocol_id)
    names = [s.name for s in steps]
    assert PROTOCOL_STEP_NAME in names
    ids = [s.id for s in steps]
    assert step_id in ids


def test_delete_protocol_step(manager):
    global step_id
    manager.delete_protocol_step(step_id)


def test_get_protocol_steps_after_delete(manager):
    global protocol_id, step_id
    steps = manager.get_protocol_steps(protocol_id)
    ids = [s.id for s in steps]
    assert step_id not in ids


def test_delete_protocol(manager):
    global protocol_id
    manager.delete_protocol(protocol_id)


def test_get_protocols_after_delete(manager):
    global protocol_id
    protocols = manager.get_protocols()
    ids = [s.id for s in protocols]
    assert protocol_id not in ids


def test_get_study_steps(manager):
    pass


def test_get_all_study_steps(manager):
    pass


def test_get_study_by_id(manager):
    pass


def test_get_operators(manager):
    pass


def test_get_operator_by_id(manager):
    pass


def test_save_operator(manager):
    pass


def test_delete_operator(manager):
    pass


def test_get_patients(manager):
    pass


def test_get_patient_by_id(manager):
    pass


def test_save_patient(manager):
    pass


def test_get_hw_tests(manager):
    pass


def test_get_calib_reports(manager):
    pass


def test_fill_out_calib_table(manager):
    pass


def test_clear_out_calib_table(manager):
    pass
