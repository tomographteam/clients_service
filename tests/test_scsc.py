import pytest
import asyncio
import aiohttp
import json

from asyncio import CancelledError

from zmq.asyncio import Context
import zmq


class TomographController:

    def __init__(self):
        self.url = 'tcp://10.10.10.100:5563'
        self._zmq_ctx = Context.instance()
        self.socket = self._zmq_ctx.socket(zmq.SUB)
        self.socket.connect(self.url)
        self.socket.set(zmq.SUBSCRIBE, b'DSEBCT')

        self.start_data = {"none"}
        self.finish_data = {"none"}
        self.result_data = {"result"}


    async def send_scsc_command(self, device: str, command_name: str, payload: dict):
        """
        command from client to SCSC
        :param device: scanner subdivide
        :param command_name: see API specification
        :param payload: command parameters. see API specification
        :return:
        """

        # "10.10.10.100" # "127.0.0.1" #
        url = 'http://10.10.10.100:8080/tango/rest/v11/commands'
        #url = 'http://127.0.0.1:10001/tango/rest/v11/commands'
        _auth = aiohttp.BasicAuth('tango-cs', 'tango')

        url_payload = {'host': '10.10.10.100:10000', 'device': device, 'name': command_name}
        if payload:
            url_payload['input'] = json.dumps(payload)
        url_payload = [url_payload, ]

        print(url_payload)
        async with aiohttp.ClientSession() as session:
            print("send")
            async with session.put(url,
                                   auth=_auth,
                                   json=url_payload,
                                   timeout=2
                                   ) as response:
                if response.status == 200:
                    try:
                        result_str = await response.read()
                        if result_str:
                            result = json.loads(result_str)
                            print("result", result)
                            return result
                        else:
                            print("result none")
                            return "none"
                    except Exception as exception:
                        print(f'scsc client error={exception} result_str={result_str}')
                else:
                    response_data = await response.read()
                    return f'Tango error status={response.status} url={url} payload={payload} response={response_data}'


    async def server_events_loop(self):


        print("Listening")
        while True:
            try:

                await asyncio.sleep(0.1)

                topic, msg_bytes = await self.socket.recv_multipart()
                print("Received")
                if msg_bytes:
                    data_str = msg_bytes.decode('utf-8')
                    evant = json.loads(data_str)
                    print(evant)
                    handler = getattr(self, evant['event_type'] + "_handler", None)
                    if handler:
                        await handler(evant["data"])
                    else:
                        print("Invalid event", data_str)
            except CancelledError as e:
                return
            except Exception as e:
                print(e)


class TestSCSC:

    tango_rest_server = "10.10.10.100" # "127.0.0.1" #
    tango_server = "10.10.10.100" # "127.0.0.1" #
    tango_rest_port = 8080
    request_timeout = 2
    user = "tango-cs"
    password = "tango"

    auth = aiohttp.BasicAuth(user, password)

    devices = [
        "test/pes/1",
        "test/ptable/1",
        "test/ptable/1",
    ]

    url_template = f'http://{tango_rest_server}:{tango_rest_port}/tango/rest/v11/hosts/{tango_server}/devices/'

    async def start(self, data):
        print("start", data)
        self.tc.start_data = data

    async def result(self, data):
        print("result", data)
        self.tc.result_data = data

    async def finish(self, data):
        print("finish", data)
        self.tc.finish_data = data


    @pytest.mark.asyncio
    async def test_attr(self):

        async with aiohttp.ClientSession() as session:

            for device in self.devices:
                async with session.get(self.url_template + f"{device}/attributes",
                                       auth=self.auth,
                                       timeout=self.request_timeout) as resp:
                    result = await resp.json()
                    assert len(result) > 0

    @pytest.mark.asyncio
    async def test_values(self):

        async with aiohttp.ClientSession() as session:

            for device in self.devices:
                async with session.get(self.url_template + f"{device}/attributes/value?attr=state",
                                       auth=self.auth,
                                       timeout=self.request_timeout) as resp:
                    result = await resp.json()
                    for r in result:
                        assert "value" in r
                        assert r["name"] == "State"
                    assert len(result) > 0

    @pytest.mark.asyncio
    async def test_start_test_equipment(self):

        self.tc = TomographController()
        print()
        task = asyncio.create_task(self.tc.server_events_loop())

        self.tc.test_equipment_start_handler = self.start
        self.tc.test_equipment_result_handler = self.result
        self.tc.test_equipment_finished_handler = self.finish

        result = await self.tc.send_scsc_command('test/dsebct/1', 'start_test_equipment', None)
        await asyncio.sleep(3)
        task.cancel()

        assert result[0]["output"]
        output = result[0]["output"]
        assert output == True
        assert self.tc.start_data["info"] == ''
        assert self.tc.finish_data["info"] == ''
        assert self.tc.result_data["info"]["result"]  == 'OK'

    async def test_prepare_scan(self):


        payload = {'scan_id': {'study_id': '2b25c82f579e404e88e7422ca829bc7d', 'series_number': None},
                   'scan': {
                       'SeriesType': 1,
                       'table_motion': 'moving',
                       'number_of_volume_images': 0,
                       'fluoro_scan_number_of_views': 0,
                       'TableStartPos': 0,
                       'TableStopPos': 500,
                       'TableSpeed': 24.0,
                       'table_height': 600,
                       'Kv': 100,
                       'Ma': 765,
                       'CollWidth': 1.2,
                       'Pitch': 0.5
                   },
                   'recon': {
                       'ScoutWinWidth': 513,
                       'ScoutWinLength': 514,
                       'ReconKernelIndex': 0,
                       'ReconNoiseReduced': 1,
                       'ReconMetalCorr': 1,
                       'patient position': 'FFDR',
                       'method': 'FBP + 5 SART',
                       'das_mode': 'DAS1+2'
                   },
                   'trigger': {
                       'TriggerW': 0,
                       'TriggerX': 0,
                       'TriggerY': 0,
                       'TriggerScanPeriod': 0,
                       'TriggerScanMaxCount': 20,
                       'TriggerDensityThreshold': 3
                   }
                   }

        self.tc = TomographController()
        print()
        task = asyncio.create_task(self.tc.server_events_loop())

        self.tc.work_start_handler = self.start
        self.tc.prepare_scan_finished_handler = self.result
        self.tc.work_complete_handler = self.finish

        result = await self.tc.send_scsc_command('test/dsebct/1', 'PrepareScanWithParams', payload=payload)
        await asyncio.sleep(3)
        task.cancel()

        assert result[0]["output"]
        output = result[0]["output"]
        assert output == True
        assert self.tc.start_data["info"] == ''
        assert self.tc.finish_data["info"] == ''
        assert self.tc.result_data["info"] == ''

    async def test_start_scan(self):

        self.tc = TomographController()
        print()
        task = asyncio.create_task(self.tc.server_events_loop())

        self.tc.work_start_handler = self.start
        self.tc.scan_report_handler = self.result
        self.tc.work_complete_handler = self.finish

        result = await self.tc.send_scsc_command('test/dsebct/1', 'StartScan', None)
        await asyncio.sleep(3)
        task.cancel()

        assert result[0]["output"]
        output = result[0]["output"]
        assert output == True
        assert self.tc.start_data["info"] == ''
        assert self.tc.finish_data["info"] == ''
        assert self.tc.result_data["passed"] == 500
