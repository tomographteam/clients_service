#!/usr/bin/env bash
sudo apt update

# Required for installing Python3.9
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa

# Install Python3.9
sudo apt install python3.9
sudo apt install virtualenv -y

# Install packages for Python3.9
virtualenv --python=python3.9 venv
source venv/bin/activate
python -m pip install -r requirements.txt
deactivate