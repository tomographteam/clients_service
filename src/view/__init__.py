from .main_window.global_tabs.dose_logs_tab import DoseLogsTab
from .main_window.global_tabs.global_tab_box import GlobalTabBox
from .main_window.global_tabs.maintenance_tab import MaintenanceGlobalTab
from .main_window.global_tabs.operators_tab import OperatorsTab
from .main_window.global_tabs.patients_tab import PatientsGlobalTab
from .main_window.global_tabs.protocol_designer_tab import ProtocolDesignerTab
from .main_window.global_tabs.reports_tab import ReportsTab
from .main_window.global_tabs.study_step_widgets.study_step_tab_box import StudyStepTabBox
from .main_window.global_ui.status_bar import StatusBar
from .main_window.global_ui.top_right_panel import TopRightWidget

