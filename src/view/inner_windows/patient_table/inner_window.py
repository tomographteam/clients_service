from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout, QLabel, QSpinBox, QVBoxLayout, QWidget
from src.device_status_manager import DeviceStatusManager
from src.model_.data.enums import ButtonTitlesEnum, ControlButtonsEnum
from src.model_manager import ModelManager
from src.view.elements.buttons.button import Button
from src.view.elements.buttons.control_button import ControlButton
from src.view.inner_windows.base_inner_window import BaseInnerWindow


class PatientTableInnerWindow(BaseInnerWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.setFixedSize(400, 400)

        self._main_gbox = QGridLayout(self)

        self._x_label = QLabel('X = ?', self)
        self._y_label = QLabel('Y = ?', self)

        self._x_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self._y_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self._move_forward_btn = ControlButton(self, ControlButtonsEnum.move_table_forward)
        self._move_backward_btn = ControlButton(self, ControlButtonsEnum.move_table_backward)
        self._move_up_btn = ControlButton(self, ControlButtonsEnum.move_table_up)
        self._move_down_btn = ControlButton(self, ControlButtonsEnum.move_table_down)
        self._stop_table_btn = ControlButton(self, ControlButtonsEnum.stop_table)

        self._set_x_btn = Button(self, ButtonTitlesEnum.move_table_to_x_pos)
        self._set_y_btn = Button(self, ButtonTitlesEnum.move_table_to_y_pos)

        # TODO: Set max value
        self._x_spin_box = QSpinBox(self)
        self._x_spin_box.setMaximum(999)
        self._y_spin_box = QSpinBox(self)
        self._y_spin_box.setMaximum(499)

        self._main_gbox.addWidget(self._move_forward_btn, 1, 0)
        self._main_gbox.addWidget(self._move_backward_btn, 1, 2)
        self._main_gbox.addWidget(self._move_up_btn, 0, 1)
        self._main_gbox.addWidget(self._move_down_btn, 2, 1)

        self._main_gbox.addWidget(self._x_spin_box, 4, 0, 1, 2)
        self._main_gbox.addWidget(self._y_spin_box, 5, 0, 1, 2)

        self._main_gbox.addWidget(self._set_x_btn, 4, 2, 1, 1)
        self._main_gbox.addWidget(self._set_y_btn, 5, 2, 1, 1)

        self._main_gbox.addWidget(self._stop_table_btn, 6, 0, 1, 3)

        label_vbox = QVBoxLayout()
        label_vbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        label_vbox.addWidget(self._x_label)
        label_vbox.addWidget(self._y_label)
        self._main_gbox.addLayout(label_vbox, 1, 1)

        self._set_x_btn.clicked.connect(self._set_x_table_pos)
        self._set_y_btn.clicked.connect(self._set_y_table_pos)

        DeviceStatusManager().set_callbacks(data_callback=self.update_axis_labels)

    def update_axis_labels(self, data):
        self._x_label.setText(f"X = {str(data['test/ptable/1']['h_pos']['value'])}")
        self._y_label.setText(f"Y = {str(data['test/ptable/1']['v_pos']['value'])}")

    def _set_x_table_pos(self):
        pos = self._x_spin_box.value()
        ModelManager().send_command_to_server(ControlButtonsEnum.move_table_to_x, x_coordinate=pos)

    def _set_y_table_pos(self):
        pos = self._y_spin_box.value()
        ModelManager().send_command_to_server(ControlButtonsEnum.move_table_to_y, x_coordinate=pos)
