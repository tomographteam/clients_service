from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QVBoxLayout, QWidget
from src.model_.managers.sensors_manager import SensorsManager
from src.view.inner_windows.base_inner_window import BaseInnerWindow
from src.view.inner_windows.sensors.breathe_monitor import ECGMonitor


class SensorsInnerWindow(BaseInnerWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.setFixedSize(1000, 200)

        # self.contrast_label = QLabel("Contrast = ?", self)
        # self.blood_label = QLabel("Blood = ?", self)
        # self.oxygen_label = QLabel("Oxygen = ?", self)
        self.ecg_monitor = ECGMonitor(self)

        # self.blood_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # self.oxygen_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        # self.contrast_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        main_vbox = QVBoxLayout(self)
        hbox = QHBoxLayout()

        #hbox.addWidget(self.blood_label)
        #hbox.addWidget(self.oxygen_label)
        hbox.addWidget(self.ecg_monitor)

        main_vbox.addLayout(hbox)
        #main_vbox.addWidget(self.contrast_label)

        # SensorsManager().contrast_updated.connect(self.update_contrast)
        # #SensorsManager().blood_updated.connect(self.update_blood)
        # SensorsManager().oxygen_updated.connect(self.update_oxygen)
        SensorsManager().blood_updated.connect(self.update_ecg)

    # def update_contrast(self, data: dict):
    #     self.contrast_label.setText("Contrast = {}".format(data))
    #
    # def update_blood(self, data: dict):
    #     color_good = '#93c47d'
    #     color_avg = '#ffa02f'
    #     color_bad = 'red'
    #
    #     syst = data['systolic']
    #     dyst = data['dystolic']
    #
    #     syst_color = color_good
    #     if syst < 110 or syst > 130:
    #         syst_color = color_avg
    #     if syst < 105 or syst > 135:
    #         syst_color = color_bad
    #
    #     dyst_color = color_good
    #     if dyst < 70 or dyst > 90:
    #         dyst_color = color_avg
    #     if dyst < 65 or dyst > 95:
    #         dyst_color = color_bad
    #
    #     html = f"""
    #         <html>
    #             <h3>Blood</h3>
    #             <p style='color: {syst_color}'>{syst}</p>
    #             <p style='color: {dyst_color}'>{dyst}</p>
    #         </html>
    #     """
    #     self.blood_label.setText(html)
    #
    # def update_oxygen(self, data: dict):
    #     color_good = '#93c47d'
    #     color_avg = '#ffa02f'
    #     color_bad = 'red'
    #
    #     syst = data['systolic']
    #     dyst = data['dystolic']
    #
    #     syst_color = color_good
    #     if syst < 95:
    #         syst_color = color_avg
    #     if syst < 90:
    #         syst_color = color_bad
    #
    #     dyst_color = color_good
    #     if dyst < 95:
    #         dyst_color = color_avg
    #     if dyst < 90:
    #         dyst_color = color_bad
    #
    #     html = f"""
    #         <html>
    #             <h3>Oxygen</h3>
    #             <p style='color: {syst_color}'>{syst}</p>
    #             <p style='color: {dyst_color}'>{dyst}</p>
    #         </html>
    #     """
    #     self.oxygen_label.setText(html)

    def update_ecg(self, data: list):
        self.ecg_monitor.update_data(data)
