import numpy as np

from PyQt5.QtCore import QPointF, Qt, QTimer, pyqtSignal, QLineF, QLine, QPoint
from PyQt5.QtGui import QColor, QFont, QPainter, QPainterPath, QPen
from PyQt5.QtWidgets import QFrame, QSizePolicy
from numpy import ndarray


def scale(min_value, max_value, value, start, end):
    if max_value == min_value:
        return 0
    return ((end - start) * (value - min_value) /
            (max_value - min_value) + start)


class ECGMonitor(QFrame):
    enabled_update = pyqtSignal(bool)

    def __init__(self, parent, *args, **kwargs) -> None:
        super().__init__(parent, *args, **kwargs)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)

        self._pens = [QPen(QColor('green'), 1),
                      QPen(QColor('red'), 1),
                      QPen(QColor('yellow'), 2),
                      ]

        self.setFixedHeight(200)
        self._data = None

    def update_data(self, event_data: ndarray):
        self._data = event_data
        self.repaint()

    def paint_array(self, array, painter):
        path = None
        v_factor = self.width() / array.size

        for x, y in enumerate(array):
            if np.isnan(y):
                if path is not None:
                    painter.drawPath(path)
                path = None
                continue
            if path is None:
                path = QPainterPath(QPoint(x * v_factor,y))
            else:
                path.lineTo(x * v_factor, y)
        if path is not None:
            painter.drawPath(path)

    def paintEvent(self, event):
        if self._data is None:
            return
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.RenderHint.Antialiasing, True)

        # trigger
        array = self._data[0] * 5
        array = array[:-1] + array[1:]
        array[array == 0] = np.nan

        painter.setPen(QPen(QColor('red'), 3))
        self.paint_array(array, painter)

        # xray
        array = self._data[1] * 10
        array[array == 0] = np.nan

        painter.setPen(QPen(QColor('green'), 3))
        self.paint_array(array, painter)

        # heart
        aligned = self._data[2] - np.nanmin(self._data[2])
        h_factor = (self.height() - 30) / np.nanmax(aligned)
        aligned = self.height() - 30 - aligned * h_factor
        painter.setPen(QPen(QColor('yellow'), 2))
        self.paint_array(aligned, painter)

        painter.end()
