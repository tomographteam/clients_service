from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QDialog, QWidget


class BaseInnerWindow(QDialog):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.hide()

    def keyPressEvent(self, e: QKeyEvent) -> None:
        if e.key() == Qt.Key_Escape:
            self.hide()
        return super().keyPressEvent(e)
