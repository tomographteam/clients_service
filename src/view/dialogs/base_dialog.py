from typing import Optional

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import (QDialog, QFrame, QHBoxLayout, QPushButton,
                             QVBoxLayout, QWidget, QLabel)


class BaseDialogWindow(QDialog):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TransparentForMouseEvents, True)
        self.setModal(True)

        self.main_vbox = QVBoxLayout(self)

        self._title_bar = QFrame(self)
        self._title_label = QLabel(self)
        self._close_btn = QPushButton('X', self)
        self._close_btn.setFixedSize(30, 30)
        self._close_btn.clicked.connect(self.hide)

        title_bar_hbox = QHBoxLayout(self._title_bar)
        title_bar_hbox.setContentsMargins(0, 0, 0, 0)
        title_bar_hbox.setAlignment(Qt.AlignmentFlag.AlignCenter | Qt.AlignmentFlag.AlignTop)

        title_bar_hbox.addStretch()
        title_bar_hbox.addWidget(self._title_label)
        title_bar_hbox.addStretch()
        title_bar_hbox.addWidget(self._close_btn)

        self.main_vbox.addWidget(self._title_bar)

        self._inner_widget: Optional[QWidget] = None
        self.hide()

    def set_inner_widget(self, inner_widget: QWidget):
        """ Used only once """
        self._inner_widget = inner_widget
        self.main_vbox.addWidget(inner_widget)

    def set_title(self, text: str):
        self._title_label.setText(text)

    def keyPressEvent(self, e: QKeyEvent) -> None:
        if e.key() == Qt.Key_Escape:
            self.hide()
        return super().keyPressEvent(e)
