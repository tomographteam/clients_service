from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout, QLabel, QWidget
from src.model_.data.enums import (ButtonTitlesEnum, ControlButtonsEnum,
                                   InfoLabelTitlesEnum)
from src.model_.managers.locale_manager import locale
from src.view.elements.buttons.button import Button
from src.view.elements.buttons.control_button import ControlButton
from src.view.elements.forms.labels import InfoLabel

if TYPE_CHECKING:
    from src.view.dialogs.calib.dialog import CalibDialog


class CalibControlWidget(QWidget):
    def __init__(self, parent: 'CalibDialog'):
        super().__init__(parent)

        self._main_gbox = QGridLayout(self)
        self._main_gbox.setContentsMargins(0, 0, 0, 0)

        self._send_to_hw_btn = Button(self, ButtonTitlesEnum.send_to_hw)
        self._start_btn = ControlButton(self, ControlButtonsEnum.start_scan)
        self._stop_btn = ControlButton(self, ControlButtonsEnum.stop_scan)

        self._send_to_hw_btn.hide()
        self._start_btn.hide()
        self._stop_btn.hide()
        self._main_gbox.setAlignment(Qt.AlignmentFlag.AlignLeft)

        self._kv_label = QLabel(self)
        self._ma_label = QLabel(self)
        self._calib_type_label = QLabel(self)
        self._last_time_done_label = QLabel(self)
        self._operator_label = QLabel(self)

        label_size = (350, 30)
        header_align = Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignVCenter
        value_align = Qt.AlignmentFlag.AlignLeft | Qt.AlignmentFlag.AlignVCenter
        self._kv_label.setFixedSize(*label_size)
        self._ma_label.setFixedSize(*label_size)
        self._last_time_done_label.setFixedSize(*label_size)
        self._operator_label.setFixedSize(*label_size)
        self._calib_type_label.setFixedSize(*label_size)

        # Header labels
        self._main_gbox.addWidget(
            InfoLabel(self, InfoLabelTitlesEnum.calib_type), 0, 0, header_align)
        self._main_gbox.addWidget(
            InfoLabel(self, InfoLabelTitlesEnum.kv), 1, 0, header_align)
        self._main_gbox.addWidget(
            InfoLabel(self, InfoLabelTitlesEnum.ma), 2, 0, header_align)
        self._main_gbox.addWidget(
            InfoLabel(self, InfoLabelTitlesEnum.operator), 3, 0, header_align)
        self._main_gbox.addWidget(
            InfoLabel(self, InfoLabelTitlesEnum.last_time_done), 4, 0, header_align)

        # Value labels
        self._main_gbox.addWidget(self._calib_type_label, 0, 1, value_align)
        self._main_gbox.addWidget(self._kv_label, 1, 1, value_align)
        self._main_gbox.addWidget(self._ma_label, 2, 1, value_align)
        self._main_gbox.addWidget(self._operator_label, 3, 1, value_align)
        self._main_gbox.addWidget(self._last_time_done_label, 4, 1, value_align)

        # Control buttons
        self._main_gbox.addWidget(self._send_to_hw_btn, 5, 0)
        self._main_gbox.addWidget(self._start_btn, 5, 2)
        self._main_gbox.addWidget(self._stop_btn, 5, 3)

    def set_data(self, data: tuple):
        """ Доки завезу после того, как доделаю
        """
        self._current_calib_type = data[0]
        self._current_kv = data[1]
        self._current_ma = data[2]
        self._current_operator = data[3]
        self._current_last_time_done = data[4]

        self._calib_type_label.setText(locale(data[0]))
        self._kv_label.setText(str(data[1]))
        self._ma_label.setText(str(data[2]))
        self._operator_label.setText(str(data[3].name))
        self._last_time_done_label.setText(str(data[4]))
