from datetime import datetime
from typing import List, Optional, Tuple

from PyQt5.QtCore import QModelIndex, pyqtSignal
from PyQt5.QtGui import QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QAbstractItemView, QTableView, QWidget
from src.db_manager import DataBaseManager
from src.model_.data.enums import CalibTypesEnum
from src.model_.managers.resource_manager import (KVMAListItemType,
                                                  ResourceManager)


class TableCore(QStandardItemModel):
    global_dt_format = '%d-%m-%Y %H:%M'

    kv_col = 0
    ma_col = 1
    air_calib_op_dt_col = 2
    multi_pin_calib_op_dt_col = 3
    water_calib_op_dt_col = 4
    dark_calib_op_dt_col = 5

    _column_count = 6

    calib_table_updated = pyqtSignal()

    def __init__(self, parent: 'CalibTable'):
        super().__init__(parent)

        self._kv_ma_list: List[Tuple[int, int]] = self._get_new_kv_ma_list()
        self.header_labels = {
            self.kv_col: 'kV',
            self.ma_col: 'mA',
            self.air_calib_op_dt_col: 'Air',
            self.multi_pin_calib_op_dt_col: 'MultiPin',
            self.water_calib_op_dt_col: 'Water',
            self.dark_calib_op_dt_col: 'Dark'
        }
        self.calib_type_map = {
            self.air_calib_op_dt_col: CalibTypesEnum.air,
            self.dark_calib_op_dt_col: CalibTypesEnum.dark,
            self.water_calib_op_dt_col: CalibTypesEnum.water,
            self.multi_pin_calib_op_dt_col: CalibTypesEnum.multi_pin
        }
        self.setHorizontalHeaderLabels(self.header_labels.values())
        self.setColumnCount(self._column_count)

        DataBaseManager().calib_table_updated.connect(self.update_table)
        self.update_table()

    def _op_dt_to_str(self, dt: datetime, op_id: int) -> str:
        if not dt:
            return "Hasn't been done yet."

        dt = dt.strftime(self.global_dt_format)
        op = DataBaseManager().get_operator_by_id(op_id).name
        return f'[{dt}] {op}'

    def update_table(self):
        return
        self.reports: List[CalibReport] = DataBaseManager().get_calib_reports()

        if not self.reports:
            return DataBaseManager().fill_out_calib_table(self._get_new_kv_ma_list())

        self.setRowCount(len(self.reports))

        for row, report in enumerate(list(self.reports)):
            # This item should not be clickable
            item = QStandardItem(str(report.kv))
            item.setEnabled(False)
            self.setItem(row, self.kv_col, item)

            # This item should not be clickable
            item = QStandardItem(str(report.ma))
            item.setEnabled(False)
            self.setItem(row, self.ma_col, item)

            # Air column
            self.setData(self.index(row, self.air_calib_op_dt_col),
                         self._op_dt_to_str(report.air_calib_time_done,
                                            report.air_calib_operator_id))
            # MultiPin column
            self.setData(self.index(row, self.multi_pin_calib_op_dt_col),
                         self._op_dt_to_str(report.multi_pin_calib_time_done,
                                            report.multi_pin_calib_operator_id))
            # Water column
            self.setData(self.index(row, self.water_calib_op_dt_col),
                         self._op_dt_to_str(report.water_calib_time_done,
                                            report.water_calib_operator_id))
            # Dark column
            self.setData(self.index(row, self.dark_calib_op_dt_col),
                         self._op_dt_to_str(report.dark_calib_time_done,
                                            report.dark_calib_operator_id))

    def _get_new_kv_ma_list(self) -> List[Tuple[int, int]]:
        kv_ma_dataset: List[KVMAListItemType] = ResourceManager().get_kv_ma_list()

        kv_ma_list: List[Tuple[str, str]] = []
        for kv_ma in kv_ma_dataset:
            kv = kv_ma['kv']
            if kv == 0:
                continue
            ma_values = kv_ma['ma']
            for ma in ma_values:
                kv_ma_list.append((kv, ma))
        return kv_ma_list

    def get_kv_ma_list(self) -> List[Tuple[int, int]]:
        return self._kv_ma_list


class CalibTable(QTableView):
    report_changed = pyqtSignal(tuple)

    def __init__(self, parent: Optional[QWidget] = ...) -> None:
        super().__init__(parent)

        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.verticalHeader().setVisible(False)

        self.horizontalHeader().setSectionsMovable(False)

        self._model = TableCore(self)
        self.setModel(self._model)

        self.clicked.connect(self._on_cell_changed)

    def _on_cell_changed(self, index: QModelIndex):
        row, col = index.row(), index.column()

        if col < 2:
            return

        try:
            report = self._model.reports[row]
        except IndexError:
            return

        self._current_report = report
        self._current_kv = report.kv
        self._current_ma = report.ma

        self._current_calib_type = self._model.calib_type_map[col]
        if self._current_calib_type is CalibTypesEnum.air:
            operator = DataBaseManager().get_operator_by_id(
                report.air_calib_operator_id)
            time_done = report.air_calib_time_done
        elif self._current_calib_type is CalibTypesEnum.dark:
            operator = DataBaseManager().get_operator_by_id(
                report.dark_calib_operator_id)
            time_done = report.dark_calib_time_done
        elif self._current_calib_type is CalibTypesEnum.water:
            operator = DataBaseManager().get_operator_by_id(
                report.water_calib_operator_id)
            time_done = report.water_calib_time_done
        elif self._current_calib_type is CalibTypesEnum.multi_pin:
            operator = DataBaseManager().get_operator_by_id(
                report.multi_pin_calib_operator_id)
            time_done = report.multi_pin_calib_time_done

        self._current_operator = operator
        self._current_time_done = time_done

        self.report_changed.emit((
            self._current_calib_type,
            self._current_kv,
            self._current_ma,
            self._current_operator,
            self._current_time_done
        ))

    def update_table(self):
        self._model.update_table()
