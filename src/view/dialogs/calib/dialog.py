from PyQt5.QtWidgets import QHBoxLayout, QWidget
from PyQt5.QtCore import Qt
from src.db_manager import DataBaseManager
from src.model_.data.enums import ButtonTitlesEnum
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.dialogs.calib.control_widget import CalibControlWidget
from src.view.dialogs.calib.table import CalibTable
from src.view.elements.buttons.button import Button


class CalibDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setFixedSize(1600, 900)

        self._table = CalibTable(self)
        self._control_widget = CalibControlWidget(self)

        self._update_btn = Button(self, ButtonTitlesEnum.update)
        self._clear_out_db_btn = Button(self, ButtonTitlesEnum.clear_out_db)  # Don't use in Prod.

        self._clear_out_db_btn.hide()
        self.main_vbox.setAlignment(Qt.AlignmentFlag.AlignLeft)

        btn_hbox = QHBoxLayout()
        btn_hbox.addWidget(self._update_btn)
        btn_hbox.addWidget(self._clear_out_db_btn)

        self.main_vbox.addWidget(self._table)
        self.main_vbox.addWidget(self._control_widget)
        self.main_vbox.addLayout(btn_hbox)

        self._update_btn.clicked.connect(self._table.update_table)
        self._clear_out_db_btn.clicked.connect(DataBaseManager().clear_out_calib_table)
        self._table.report_changed.connect(self._control_widget.set_data)
