from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from src.model_.data.enums import ButtonTitlesEnum, ErrorsEnum, InfoLabelTitlesEnum
from src.model_.managers.locale_manager import locale

from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.labels import InfoLabel


class DataBaseErrorDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self._close_btn.hide()
        self.set_title(locale(InfoLabelTitlesEnum.data_error_dialog))

        self.error_text = QTextBrowser(self)
        self.repeat_btn = Button(self, ButtonTitlesEnum.repeat)

        self.error_text.setFixedSize(400, 400)

        self.main_vbox.addWidget(self.error_text)
        self.main_vbox.addWidget(self.repeat_btn)

        self.repeat_btn.clicked.connect(self.close)

        # For force-major :)
        shortcut_open = QShortcut(QKeySequence('Ctrl+Shift+Del'), self)
        shortcut_open.activated.connect(quit)

    def keyPressEvent(self, e: QKeyEvent) -> None:
        return

    def set_error(self, error_key: ErrorsEnum):
        text = f"""
        <h3>{locale(InfoLabelTitlesEnum.error_desc)}:</h3>
        <ul><li style='color: wheat'>{locale(error_key)}</li></ul>
        <h3>{locale(InfoLabelTitlesEnum.data_error_may_be_solution)}:</h3>
        <ul><li style='color: wheat'>{locale(InfoLabelTitlesEnum.data_error_solution)}</li></ul>
        """
        self.error_text.setHtml(text)

    def on_repeat(self):
        pass
