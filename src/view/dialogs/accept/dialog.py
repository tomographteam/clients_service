from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from src.model_.data.enums import AcceptActionsEnum, ButtonTitlesEnum, DialogTitlesEnum
from src.model_.managers.locale_manager import locale

from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button


class AcceptDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.set_title(locale(DialogTitlesEnum.accept_dialog))
        self.setFixedSize(400, 300)
        self._close_btn.hide()
        self._is_accepted = False

        # TODO: Добавить возможность вывода
        #       доп. инфы об удаляемом объекте
        self.label = QLabel(self)
        self.ok_btn = Button(self, ButtonTitlesEnum.ok)
        self.cancel_btn = Button(self, ButtonTitlesEnum.cancel)

        self.label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.label.setWordWrap(True)

        hbox = QHBoxLayout()
        hbox.addWidget(self.ok_btn)
        hbox.addWidget(self.cancel_btn)

        self.main_vbox.addStretch()
        self.main_vbox.addWidget(self.label)
        self.main_vbox.addStretch()
        self.main_vbox.addLayout(hbox)

        self.ok_btn.clicked.connect(self._on_accepted)
        self.cancel_btn.clicked.connect(self.close)

    def _on_accepted(self):
        self._is_accepted = True
        self.close()

    def set_action_data(self, action_key: AcceptActionsEnum):
        self.label.setText(locale(action_key))

    def keyPressEvent(self, e: QKeyEvent) -> None:
        return

    def exec_(self) -> bool:
        """ Executes the dialog and returns `True`
            if the `OK` button was pressed, else `False` """
        super().exec_()
        return self._is_accepted
