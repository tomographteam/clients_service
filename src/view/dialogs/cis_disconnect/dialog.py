from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from src.model_.data.enums import ButtonTitlesEnum, DialogTitlesEnum, InfoLabelTitlesEnum

from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.labels import InfoLabel


class CISDisconnectedDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.label = InfoLabel(self, DialogTitlesEnum.cis_disconnected)
        self.continue_btn = Button(self, ButtonTitlesEnum.continue_work)

        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(self.label)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(self.continue_btn)

        self.continue_btn.clicked.connect(self.close)
