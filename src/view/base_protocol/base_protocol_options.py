from typing import Dict, Union

from PyQt5.QtWidgets import QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model import (
    AutoCalculatedEnum,
    ContrastDensityEnum,
    ContrastTypeEnum,
    DASMethodEnum,
    FBPMethodEnum,
    KernelEnum,
    MetalCorrectionEnum,
    ProtocolStep,
    ResolutionEnum,
)
from src.view.base_protocol.patient_pos_smart_label import PatientPosSmartLabel
from src.view.elements.forms.enum_combo_boxes import EnumComboBox, EnumComboBoxSync
from src.view.elements.forms.json_combo_boxes import (
    JsonKvComboBoxSync,
    JsonMaComboBoxSync,
    JsonPitchComboBoxSync,
)
from src.view.elements.forms.labels import ValueLabel
from src.view.elements.forms.line_edits import LineEditSync

FormDict = Dict[InstrumentedAttribute, Union[EnumComboBox, ValueLabel, LineEditSync]]


# Настройки одного шага сканирования
class BaseProtocolOptions(QWidget):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.form_dict: FormDict = {}

        # ==================
        # Шаг сканирования =====================================
        # ==================

        # Название шага
        # self.name_label = Label(self, ProtocolStep.name)

        # Тип сканирования
        # self.scan_type_label = Label(self, ProtocolStep.scan_type)

        # Положение пациента
        self.patient_pos_smart_label = PatientPosSmartLabel(
            self, ProtocolStep.patient_position
        )

        # Движение стола
        # self.table_motion_label = Label(self, ProtocolStep.table_motion)

        # Число объёмных изображений
        # self.number_of_volume_images_label = Label(self, ProtocolStep.number_of_volume_images)

        # Позиция стола (СТАРТ[мин, макс], СТОП[мин, макс])
        self.start_pos_line_edit = LineEditSync(self, ProtocolStep.start_position)
        # self.start_pos_max_label = Label(self, ProtocolStep.start_position_max)
        # self.start_pos_min_label = Label(self, ProtocolStep.start_position_min)
        self.stop_pos_line_edit = LineEditSync(self, ProtocolStep.stop_position)
        # self.stop_pos_max_label = Label(self, ProtocolStep.stop_position_max)
        # self.stop_pos_min_label = Label(self, ProtocolStep.stop_position_min)

        # Высота стола (ВЫСОТА[мин, макс])
        self.table_height_line_edit = LineEditSync(self, ProtocolStep.table_height)
        # self.table_height_max_label = Label(self, ProtocolStep.table_height_max)
        # self.table_height_min_label = Label(self, ProtocolStep.table_height_min)

        # Автовычисление
        self.auto_calculated_combo_box = EnumComboBoxSync(
            self, ProtocolStep.auto_calculated, AutoCalculatedEnum
        )

        # Питч (ПИТЧ[мин, макс])
        self.pitch_combo_box = JsonPitchComboBoxSync(
            self, ProtocolStep.pitch, start_index=1
        )
        # self.pitch_min_label = Label(self, ProtocolStep.pitch_min)
        # self.pitch_max_label = Label(self, ProtocolStep.pitch_max)

        # Ширина коллиматора, мм (ШИРИНА[мин, макс])
        self.collimator_width_line_edit = LineEditSync(
            self, ProtocolStep.collimator_width
        )
        # self.collimator_width_min_label = Label(self, ProtocolStep.collimator_width_min)
        # self.collimator_width_max_label = Label(self, ProtocolStep.collimator_width_max)

        # Скорость стола, мм/c (СКОРОСТЬ[мин, макс])
        self.table_speed_line_edit = LineEditSync(self, ProtocolStep.table_speed)
        # self.table_speed_min_label = Label(self, ProtocolStep.table_speed_max)
        # self.table_speed_max_label = Label(self, ProtocolStep.table_speed_max)

        # kV (kV[мин, макс])
        self.kv_combo_box = JsonKvComboBoxSync(self, ProtocolStep.kv, start_index=1)
        # self.kv_min_label = Label(self, ProtocolStep.kv_min)
        # self.kv_max_label = Label(self, ProtocolStep.kv_max)

        # Ток пучка, мА
        self.ma_combo_box = JsonMaComboBoxSync(self, ProtocolStep.ma, start_index=1)
        # self.ma_max_label = Label(self, ProtocolStep.ma_max)
        # self.ma_min_label = Label(self, ProtocolStep.ma_min)

        # Доза, мАс (ДОЗА[выбранная, макс.])
        # self.dose_label = Label(...)
        # self.dose_max_label = Label(self, ProtocolStep.dose_max)

        # Задержка старта, сек
        # self.start_delay_label = Label(self, ProtocolStep.start_delay)

        # Голосовая запись (СТАРТ и КОНЕЦ)
        # self.start_voice_cmd = ...
        # self.end_voice_cmd = ...

        # Автозапуск, сек (Хз, почему-то тут секунды,
        # хотя сам виджет является абстракцией типа bool)
        # и выполнен через QComboBox[Включен, выключен]
        # self.is_auto_start_label = Label(self, ProtocolStep.is_auto_start)

        # Старт с ЭКГ
        # self.is_cardio_start_label = Label(self, ProtocolStep.is_cardio_start)

        # Тип контраста
        self.contrast_type_combo_box = EnumComboBoxSync(
            self, ProtocolStep.contrast_type, ContrastTypeEnum
        )

        # Ручной старт (Тоже хз, как он работает
        # и не связан ли он с автозапуском)
        # self.manual_start = ...

        # =====================================================
        # =====================================================

        # Кто это и что это - я пока не знаю
        # self.fluoro_scan_number_of_views_label = Label(self, ProtocolStep.fluoro_scan_number_of_views)
        # self.number_label = Label(self, ProtocolStep.number)
        self.point_zero_label = ValueLabel(self, ProtocolStep.point_zero)

        # ===============
        # Реконструкция =================================
        # ===============

        # Ширина и высота
        self.recon_width_line_edit = LineEditSync(
            self, ProtocolStep.reconstruction_width
        )
        self.recon_height_line_edit = LineEditSync(
            self, ProtocolStep.reconstruction_height
        )

        # Разрешение
        self.recon_res_combo_box = EnumComboBoxSync(
            self, ProtocolStep.reconstruction_resolution, ResolutionEnum
        )

        # Осевое реконструирование(?)
        self.axial_dist_line_edit = LineEditSync(
            self, ProtocolStep.reconstruction_axial_distance
        )

        # Размер среза, мм
        self.slice_size_line_edit = LineEditSync(
            self, ProtocolStep.reconstruction_slice_size
        )

        # Ядро
        self.kernel_combo_box = EnumComboBoxSync(
            self, ProtocolStep.reconstruction_kernel, KernelEnum
        )

        # Подавление шума
        self.noise_reduct_line_edit = LineEditSync(
            self, ProtocolStep.reconstruction_noise_reduction
        )

        # Коррекция металла
        self.metal_corr_combo_box = EnumComboBoxSync(
            self, ProtocolStep.reconstruction_metal_correction, MetalCorrectionEnum
        )

        # FPB Метод
        self.fbp_method_combo_box = EnumComboBoxSync(
            self, ProtocolStep.fbp_method, FBPMethodEnum
        )

        # DAS Метод
        self.das_method_combo_box = EnumComboBoxSync(
            self, ProtocolStep.das_method, DASMethodEnum
        )

        # ===================================================================
        # ===================================================================

        # ====================
        # Инжектор контраста ================================================
        # ====================

        # Плотность контраста (ПЛОТНОСТЬ[мин, макс])
        self.contrast_density_combo_box = EnumComboBoxSync(
            self, ProtocolStep.contrast_density, None
        )
        # self.contrast_ = ...
        # self.contrast_ = ...

        # Объём контраста, мл (ОБЪЁМ[мин, макс])
        self.contrast_volume_line_edit = LineEditSync(
            self, ProtocolStep.contrast_volume
        )
        # self.contrast_ = ...
        # self.contrast_ = ...

        # Объём растворителя(?), мл (ОБЪЁМ[мин, макс])
        self.solvent_volume_line_edit = LineEditSync(
            self, ProtocolStep.contrast_solvent_volume
        )
        # self.solvent_ = ...
        # self.solvent_ = ...

        # Диаметр зоны, мм
        self.zone_diameter_line_edit = LineEditSync(self, ProtocolStep.zone_diameter)

        # Trigger start: время шага
        self.step_delay_line_edit = LineEditSync(self, ProtocolStep.step_delay)

        # Задержка старта, сек (Скорее всего для инжектора отдельная задержка)
        self.start_delay_line_edit = LineEditSync(self, ProtocolStep.start_delay)

        # Целевой ROI, HU
        self.target_roi_line_edit = LineEditSync(self, ProtocolStep.target_roi)

        # Макс. ожидание
        self.max_trigger_wait_line_edit = LineEditSync(
            self, ProtocolStep.max_trigger_wait
        )

        # Макс. проверок
        self.max_checks_line_edit = LineEditSync(self, ProtocolStep.max_checks)

        # =====================================================================
        # =====================================================================

        # ==================
        # Параметры кардио ====================================================
        # ==================
        # Там таблица идёт какая-то,
        # состоящая из трёх строк и четырёх столбцов
        # [bool | Кадр | Сканирований | Смещение ]
        # =====================================================================
        # =====================================================================

        # Описание
        # self.desc_ = ...

        # Иллюстрация
        # self.image_ = ...

    def from_model(self, protocol: ProtocolStep):
        for key, widget in self.form_dict.items():
            try:
                widget.set_value_from_model(getattr(protocol, key.key))
            except AttributeError as e:
                print("TODO:", e, key)
            except TypeError:
                pass
