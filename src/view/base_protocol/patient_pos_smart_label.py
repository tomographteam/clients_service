from typing import Dict, Optional

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QCursor, QMouseEvent, QPixmap
from PyQt5.QtWidgets import QHBoxLayout, QLabel, QWidget
from src.model import PatientPositionEnum, ProtocolStep
from src.model_.managers.resource_manager import (IResourceManager,
                                                  ResourceManager)
from src.model_manager import ModelManager
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.forms.labels import ValueLabel

resource_manager: IResourceManager = ResourceManager()


class PatientPosItem(QLabel):
    clicked = pyqtSignal(object)  # PatientPosItem

    def __init__(self, parent: "PatientPosListWidget",
                 img_key_name: str, img_path: str):
        super().__init__(parent)

        self._pixmap_size = (230, 150)
        self._patient_pos = PatientPositionEnum.get_by_img_name(img_key_name)

        self._img_key_name = img_key_name
        self._img_path = img_path

        self._pixmap = QPixmap(img_path)

        self.setPixmap(self._pixmap)
        self.setFixedSize(*self._pixmap_size)
        self.setScaledContents(True)

        # self.setStyleSheet('PatientPosItem:hover {background-color: rgb(70, 70, 70)}')

    def get_img_key_name(self) -> str:
        return self._img_key_name

    def mousePressEvent(self, _: QMouseEvent) -> None:
        self.clicked.emit(self)

    def get_pixmap_copy(self) -> QPixmap:
        return self._pixmap.copy()

    def get_patient_pos(self) -> PatientPositionEnum:
        return self._patient_pos


class PatientPosListWidget(BaseDialogWindow):
    pos_currented = pyqtSignal(object)  # PatientPosItem

    def __init__(self, parent: "PatientPosSmartLabel"):
        super().__init__(parent)
        # self.setStyleSheet('PatientPosListWidget {border-radius: 10px}')
        self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        self.setModal(True)

        self._img_dataset = resource_manager.get_patient_pos_dataset()

        # self._main_vbox = QVBoxLayout(self)

        # TODO
        i = 1
        self._max_items_on_line = 4
        self._items: Dict[PatientPositionEnum, PatientPosItem] = {}
        hbox = QHBoxLayout()
        hbox.setAlignment(Qt.AlignLeft)
        for key_name, img_path in self._img_dataset.items():
            item = PatientPosItem(self, key_name, img_path)
            item.clicked.connect(self._on_item_clicked)
            self._items[item.get_patient_pos()] = item

            hbox.addWidget(item)

            if i % self._max_items_on_line == 0:
                self.main_vbox.addLayout(hbox)
                hbox = QHBoxLayout()
                hbox.setAlignment(Qt.AlignLeft)

            i += 1
        self.main_vbox.addLayout(hbox)

    def _on_item_clicked(self, item: PatientPosItem):
        self.pos_currented.emit(item)
        self.hide()

    def show(self):
        self.move(QCursor.pos())
        return super().show()

    def mousePressEvent(self, a0: QMouseEvent) -> None:
        return super().mousePressEvent(a0)

    def get_item(self, patient_pos: PatientPositionEnum) -> PatientPosItem:
        return self._items[patient_pos]


class PatientPosSmartLabel(ValueLabel):
    def __init__(self, parent: QWidget, key_name: str) -> None:
        super().__init__(parent, key_name)

        self.setFixedHeight(250)
        self.setScaledContents(True)

        self._sync_available = True

        self._current_patient_pos: Optional[PatientPositionEnum] = None

        self._patient_pos_list_widget = PatientPosListWidget(self)
        self._patient_pos_list_widget.pos_currented.connect(self._commit)

    def _commit(self, item):
        ModelManager().send_form_data(
            ProtocolStep.patient_position.key, item._patient_pos.name)

    def _set_pixmap_from_item(self, item: PatientPosItem):
        self._current_patient_pos = item.get_patient_pos()
        self._pixmap = item.get_pixmap_copy()
        self.setPixmap(self._pixmap)

    def set_value_from_model(self, value: PatientPositionEnum):
        self.blockSignals(True)
        self._set_pixmap_from_item(self._patient_pos_list_widget.get_item(value))
        self.blockSignals(False)

    def mousePressEvent(self, ev: QMouseEvent) -> None:
        ev.accept()
        self._patient_pos_list_widget.show()

    def get_current_patient_pos(self) -> PatientPositionEnum:
        return self._current_patient_pos
