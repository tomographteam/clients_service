from PyQt5.QtCore import QSize
from PyQt5.QtGui import QResizeEvent, QKeySequence
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QWidget, QShortcut
from src.model import Operator
from src.model_.data.enums import MainWindowTabsEnum, OperatorAccessEnum
from src.model_.managers.access_manager import AccessManager
from src.model_.managers.locale_manager import LocaleManager, locale
from src.model_.managers.resource_manager import ResourceManager
from src.model_.managers.sensors_manager import SensorsManager
from src.model_.managers.style_manager import StyleManager
from src.model_manager import ModelManager
from src.utils.singleton import singleton
from src.view import DoseLogsTab, GlobalTabBox, MaintenanceGlobalTab, OperatorsTab
from src.view import PatientsGlobalTab, ProtocolDesignerTab
from src.view import StudyStepTabBox, ReportsTab, StatusBar, TopRightWidget
from src.view.dialogs.cis_disconnect.dialog import CISDisconnectedDialog


@singleton
class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()

        self.setWindowTitle("Client")

        SensorsManager()  # run it before dialog open

        self._central_widget = QWidget(self)
        self.setCentralWidget(self._central_widget)

        self._top_right_widget = TopRightWidget(self)
        self._main_tab_box = GlobalTabBox(self)
        self._status_bar = StatusBar(self)

        self.accessible_widgets = {
            MainWindowTabsEnum.scan: StudyStepTabBox(self),
            MainWindowTabsEnum.protocol_designer: ProtocolDesignerTab(self),
            MainWindowTabsEnum.reports: ReportsTab(self),
            MainWindowTabsEnum.dose_logs: DoseLogsTab(self),
            MainWindowTabsEnum.patients: PatientsGlobalTab(self),
            MainWindowTabsEnum.operators: OperatorsTab(self),
            MainWindowTabsEnum.maintenance: MaintenanceGlobalTab(self),
        }

        for key, widget in self.accessible_widgets.items():
            self._main_tab_box.addTab(widget, locale(key))

        self._main_vbox = QVBoxLayout(self._central_widget)
        self._main_vbox.setContentsMargins(0, 0, 0, 0)
        self._main_vbox.addWidget(self._main_tab_box)
        self._main_vbox.addWidget(self._status_bar)
        self._main_tab_box.setCurrentIndex(6)

        self.user_selected(OperatorAccessEnum.read_only)
        AccessManager().access_changed.connect(self.user_selected)
        LocaleManager().need_translate.connect(self.translate)
        AccessManager().set_access(OperatorAccessEnum.read_only)
        ModelManager().cis_status_updated.connect(self.try_show_dialog)

    def user_selected(self, access: OperatorAccessEnum):
        enabled = Operator.get_enabled_windows(access)
        for key, widget in self.accessible_widgets.items():
            index = self._main_tab_box.indexOf(widget)
            self._main_tab_box.widget(index).setEnabled(key in enabled)
        self._main_tab_box.setCurrentIndex(0)

    def resizeEvent(self, event: QResizeEvent) -> None:
        size: QSize = event.size() - self._top_right_widget.size()
        self._top_right_widget.move(size.width(), 0)
        return super().resizeEvent(event)

    def translate(self):
        for key, widget in self.accessible_widgets.items():
            index = self._main_tab_box.indexOf(widget)
            self._main_tab_box.setTabText(index, locale(key))

    def update_style(app: "QApplication"):
        app.setStyleSheet(StyleManager.render(
            ResourceManager().get_qss(),
            ResourceManager().get_palette_dataset()['dark']['palette']))

    def try_show_dialog(self, status: bool):
        if ModelManager().is_connected() and not status:
            ModelManager().set_connected(False)
            dialog = CISDisconnectedDialog(None)
            dialog.exec_()
