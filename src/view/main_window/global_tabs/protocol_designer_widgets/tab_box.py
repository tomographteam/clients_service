from typing import TYPE_CHECKING, Optional

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QFrame, QHBoxLayout, QTabWidget, QVBoxLayout
from src.db_manager import DataBaseManager
from src.model import Protocol, ProtocolStep
from src.model_.data.enums import ButtonTitlesEnum, TabTitlesEnum
from src.model_.managers.locale_manager import LocaleManager, locale
from src.view.elements.buttons.button import Button
from src.view.main_window.global_tabs.protocol_designer_widgets.dialogs import (
    ProtocolEditorDialog, StepEditorDialog)
from src.view.main_window.global_tabs.protocol_designer_widgets.table_widgets import (
    ProtocolsTable, StepsTable)

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_tab import \
        ProtocolDesignerTab


class ProtocolsAndStepsTabBox(QTabWidget):
    def __init__(self, parent: 'ProtocolDesignerTab') -> None:
        super().__init__(parent)

        # Будет использоваться Tab'ами
        # и коннектиться внутри них
        self.update_btn = Button(self, ButtonTitlesEnum.update)
        self.update_btn.setFixedWidth(150)

        self.addTab(Tab(self, True), locale(TabTitlesEnum.examples_of_protocols))
        self.addTab(Tab(self), locale(TabTitlesEnum.user_protocols))

        LocaleManager().need_translate.connect(self.translate)

    def translate(self):
        self.setTabText(0, locale(TabTitlesEnum.examples_of_protocols))
        self.setTabText(1, locale(TabTitlesEnum.user_protocols))

    def resizeEvent(self, e: QResizeEvent) -> None:
        size: QSize = e.size() - self.update_btn.size()
        self.update_btn.move(size.width(), 0)
        return super().resizeEvent(e)


class Tab(QFrame):
    def __init__(self, parent: ProtocolsAndStepsTabBox, is_examples: bool = False):
        super().__init__(parent)

        self._protocols_table = ProtocolsTable(self, is_examples)
        self._steps_table = StepsTable(self, is_examples)

        self._protocol_editor_dialog: Optional[ProtocolEditorDialog] = None
        self._step_editor_dialog: Optional[ProtocolEditorDialog] = None

        self._create_protocol_btn = Button(self, ButtonTitlesEnum.create_protocol)
        self._create_step_btn = Button(self, ButtonTitlesEnum.create_step)
        self._edit_protocol_btn = Button(self, ButtonTitlesEnum.edit)
        self._edit_step_btn = Button(self, ButtonTitlesEnum.edit)
        self._delete_protocol_btn = Button(self, ButtonTitlesEnum.delete)
        self._delete_step_btn = Button(self, ButtonTitlesEnum.delete)

        self._delete_protocol_btn.setEnabled(False)
        self._delete_step_btn.setEnabled(False)
        self._edit_protocol_btn.setEnabled(False)
        self._edit_step_btn.setEnabled(False)
        self._create_step_btn.setEnabled(False)

        # TODO
        main_hbox = QHBoxLayout(self)

        # PROTOCOL
        vbox = QVBoxLayout()
        main_hbox.addLayout(vbox)

        hbox = QHBoxLayout()
        hbox.addWidget(self._protocols_table)
        vbox.addLayout(hbox)

        hbox = QHBoxLayout()
        hbox.setAlignment(Qt.AlignmentFlag.AlignRight |
                          Qt.AlignmentFlag.AlignVCenter)
        hbox.addWidget(self._delete_protocol_btn)
        hbox.addStretch()
        hbox.addWidget(self._edit_protocol_btn)
        hbox.addWidget(self._create_protocol_btn)
        vbox.addLayout(hbox)

        # STEP
        vbox = QVBoxLayout()
        main_hbox.addLayout(vbox)

        hbox = QHBoxLayout()
        hbox.addWidget(self._steps_table)
        vbox.addLayout(hbox)

        hbox = QHBoxLayout()
        hbox.setAlignment(Qt.AlignmentFlag.AlignRight |
                          Qt.AlignmentFlag.AlignVCenter)
        hbox.addWidget(self._delete_step_btn)
        hbox.addStretch()
        hbox.addWidget(self._edit_step_btn)
        hbox.addWidget(self._create_step_btn)
        vbox.addLayout(hbox)

        self._protocols_table.protocol_selected.connect(
            self._steps_table.need_update_table.emit)

        self._protocols_table.protocol_edit_action_signal.connect(
            self._show_protocol_editor)

        # Protocol
        self._create_protocol_btn.clicked.connect(self._on_create_protocol_btn_clicked)
        self._edit_protocol_btn.clicked.connect(self._on_edit_protocol_btn_clicked)
        self._delete_protocol_btn.clicked.connect(self._on_delete_protocol_btn_clicked)

        # Step
        self._create_step_btn.clicked.connect(self._on_create_step_btn_clicked)
        self._delete_step_btn.clicked.connect(self._on_delete_step_btn_clicked)
        self._edit_step_btn.clicked.connect(self._on_edit_step_btn_clicked)

        # Other
        self._protocols_table.protocol_deselected.connect(
            lambda: (self._delete_protocol_btn.setEnabled(False),
                     self._edit_protocol_btn.setEnabled(False),
                     self._create_step_btn.setEnabled(False)))
        self._protocols_table.protocol_selected.connect(
            lambda: (self._delete_protocol_btn.setEnabled(True),
                     self._edit_protocol_btn.setEnabled(True),
                     self._create_step_btn.setEnabled(True)))
        self._steps_table.step_deselected.connect(
            lambda: (self._delete_step_btn.setEnabled(False),
                     self._edit_step_btn.setEnabled(False),
                     self._create_step_btn.setEnabled(False)))
        self._steps_table.step_selected.connect(
            lambda: (self._delete_step_btn.setEnabled(True),
                     self._edit_step_btn.setEnabled(True)))

        parent.update_btn.clicked.connect(
            self._protocols_table.update_table)

    def _show_protocol_editor(self, protocol: Protocol):
        if self._protocol_editor_dialog is None:
            self._protocol_editor_dialog = ProtocolEditorDialog(self)
        self._protocol_editor_dialog.set_protocol(protocol)
        self._protocol_editor_dialog.show()

    def _show_step_editor(self, protocol_step: ProtocolStep):
        if self._step_editor_dialog is None:
            self._step_editor_dialog = StepEditorDialog(self)
        if self._protocols_table.get_current_protocol():
            self._step_editor_dialog.set_protocol_step(
                self._protocols_table.get_current_protocol(), protocol_step)
            self._step_editor_dialog.show()

    def _on_create_protocol_btn_clicked(self):
        protocol = Protocol()
        self._show_protocol_editor(protocol)

    def _on_create_step_btn_clicked(self):
        protocol_step = ProtocolStep()
        self._show_step_editor(protocol_step)

    def _on_edit_protocol_btn_clicked(self):
        protocol = self._protocols_table.get_current_protocol()
        if protocol:
            self._show_protocol_editor(protocol)

    def _on_delete_protocol_btn_clicked(self):
        protocol = self._protocols_table.get_current_protocol()
        if protocol:
            DataBaseManager().delete_protocol(protocol.id)

    def _on_delete_step_btn_clicked(self):
        protocol_step = self._steps_table.get_current_step()
        if protocol_step:
            DataBaseManager().delete_protocol_step(protocol_step.id)

    def _on_edit_step_btn_clicked(self):
        protocol_step = self._steps_table.get_current_step()
        if protocol_step:
            self._show_step_editor(protocol_step)
