from typing import TYPE_CHECKING, List, Optional

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import Protocol, ProtocolStep
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, locale, multi_locale
from src.utils import const

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))
        self.setSortingEnabled(True)


class ProtocolsTable(_Table):
    protocol_selected = pyqtSignal(object)  # Protocol
    protocol_deselected = pyqtSignal()
    protocol_edit_action_signal = pyqtSignal(object)  # Protocol

    def __init__(self, parent: 'Tab', is_examples: bool = False):
        super().__init__(parent)
        self._is_examples = is_examples

        self.setColumnCount(3)

        self._protocols: Optional[List[Protocol]] = None
        self._current_protocol: Optional[Protocol] = None

        self.currentCellChanged.connect(self._on_current_cell_changed)
        DataBaseManager().protocol_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()
        self.update_table()

    def _on_current_cell_changed(self, row: int):
        if row >= 0:
            self.protocol_selected.emit(self._protocols[row])
        else:
            self.protocol_deselected.emit()

    def update_table(self):
        protocols: List[Protocol] = DataBaseManager().get_protocols()
        self._protocols = [protocol for protocol in protocols
                           if protocol.example_protocol == self._is_examples]

        self.setRowCount(len(self._protocols))

        for row, protocol in enumerate(self._protocols):
            self.setItem(row, 0, QTableWidgetItem(protocol.name))
            self.setItem(row, 1, QTableWidgetItem(locale(protocol.area_kind)))
            self.setItem(row, 2, QTableWidgetItem(locale(protocol.body_area)))

            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)

        try:
            self.protocol_selected.emit(self._protocols[self.currentRow()])
        except IndexError:
            pass

    def get_current_protocol(self) -> Optional[Protocol]:
        try:
            return self._protocols[self.currentRow()]
        except IndexError:
            return None

    def translate(self):
        self.update_table()
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.protocol,
                          TableHeadersEnum.area_kind,
                          TableHeadersEnum.body_area)))


class StepsTable(_Table):
    need_update_table = pyqtSignal(object)  # Protocol
    step_selected = pyqtSignal(ProtocolStep)  # ProtocolStep
    step_deselected = pyqtSignal()

    def __init__(self, parent: 'Tab', is_examples: bool = False):
        super().__init__(parent)
        self._is_examples = is_examples

        self.setColumnCount(2)
        self.setHorizontalHeaderLabels(('Step name', 'Scan type'))

        self._protocol: Optional[Protocol] = None
        self._steps: Optional[List[ProtocolStep]] = None

        self.need_update_table.connect(self.set_protocol)
        DataBaseManager().protocol_step_table_updated.connect(
            lambda: self.set_protocol(self._protocol))

        self.currentCellChanged.connect(self._on_current_cell_changed)
        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        self.set_protocol(self._protocol)
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.step_name,
                          TableHeadersEnum.scan_type)))

    def _on_current_cell_changed(self, row: int):
        if row >= 0:
            self.step_selected.emit(self._steps[row])
            self.selectRow(row)
        else:
            self.step_deselected.emit()

    def set_protocol(self, protocol: Optional[Protocol]):
        self._protocol = protocol

        if protocol is None:
            return self.clear()

        self._steps: List[ProtocolStep] = DataBaseManager().get_protocol_steps(protocol.id)

        self.setRowCount(len(self._steps))

        for row, step in enumerate(self._steps):
            self.setItem(row, 0, QTableWidgetItem(step.name))
            self.setItem(row, 1, QTableWidgetItem(locale(step.scan_type)))

            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)

    def get_current_step(self) -> Optional[ProtocolStep]:
        try:
            return self._steps[self.currentRow()]
        except IndexError:
            return None
