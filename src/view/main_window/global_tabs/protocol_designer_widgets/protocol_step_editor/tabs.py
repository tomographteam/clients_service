from enum import Enum
from typing import TYPE_CHECKING, Dict

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QFrame, QHBoxLayout, QVBoxLayout
from src.model import (AutoCalculatedEnum, BoolEnum, ContrastTypeEnum, DASMethodEnum, FBPMethodEnum,
                       KernelEnum, MetalCorrectionEnum, PatientPositionEnum,
                       ProtocolStep, ResolutionEnum, ScanTypeEnum,
                       TableMotionEnum)
from src.model_.data.enums import ProtocolTabsEnum
from src.view.elements.forms.enum_combo_boxes import EnumComboBox
from src.view.elements.forms.json_combo_boxes import (JsonKvComboBox,
                                                      JsonMaComboBox,
                                                      JsonPitchComboBox)
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit
from src.view.main_window.global_tabs.protocol_designer_widgets.protocol_step_editor.table_select_widget import \
    TableSelectWidget

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.protocol_step_editor.tab_box import \
        ProtocolStepEditorTabBox


class BaseProtocolStepEditTab(QFrame):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        self.form_dict: Dict[Enum, LineEdit] = {}

        self._main_vbox = QVBoxLayout(self)

        # TEMP
        # Так просто приятнее смотрится
        self.setObjectName('ProtocolDesigner_StepEditTab')
        # self.setStyleSheet(_temporary_style.style)

    def _place_widgets(self):
        for widget in self.form_dict.values():
            label = InfoLabel(self, widget.get_key_name())
            label.setFixedWidth(250)
            label.setAlignment(Qt.AlignmentFlag.AlignRight |
                               Qt.AlignmentFlag.AlignVCenter)

            widget.setFixedWidth(200)

            hbox = QHBoxLayout()
            hbox.addWidget(label)
            hbox.addWidget(widget)
            self._main_vbox.addLayout(hbox)

    def set_protocol_step(self, protocol_step: ProtocolStep):
        for key_name, widget in self.form_dict.items():
            if not isinstance(key_name, str):
                key_name = key_name.name

            try:
                value = getattr(protocol_step, key_name)
            except AttributeError as e:
                print(e)
                continue

            widget.set_value_from_model(value)


class ProtocolStepTab(BaseProtocolStepEditTab):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        self._step_name = LineEdit(self, ProtocolStep.name)

        self._scan_type = EnumComboBox(self, ProtocolStep.scan_type, ScanTypeEnum)
        self._patient_pos = EnumComboBox(self, ProtocolStep.patient_position, PatientPositionEnum)
        self._table_motion = EnumComboBox(self, ProtocolStep.table_motion, TableMotionEnum)

        self._number_of_volume_images = LineEdit(self, ProtocolStep.number_of_volume_images)

        self._start_pos = LineEdit(self, ProtocolStep.start_position)
        self._start_pos_min = LineEdit(self, ProtocolStep.start_position_min)
        self._start_pos_max = LineEdit(self, ProtocolStep.start_position_max)

        self._stop_pos = LineEdit(self, ProtocolStep.stop_position)
        self._stop_pos_min = LineEdit(self, ProtocolStep.stop_position_min)
        self._stop_pos_max = LineEdit(self, ProtocolStep.stop_position_max)

        self._table_height = LineEdit(self, ProtocolStep.table_height)
        self._table_height_min = LineEdit(self, ProtocolStep.table_height_min)
        self._table_height_max = LineEdit(self, ProtocolStep.table_height_max)

        self._auto_calculated = EnumComboBox(self, ProtocolStep.auto_calculated, AutoCalculatedEnum)

        self._pitch = JsonPitchComboBox(self, ProtocolStep.pitch)
        self._pitch_min = JsonPitchComboBox(self, ProtocolStep.pitch_min)
        self._pitch_max = JsonPitchComboBox(self, ProtocolStep.pitch_max)

        self._collimator_width = LineEdit(self, ProtocolStep.collimator_width)
        self._collimator_width_min = LineEdit(self, ProtocolStep.collimator_width_min)
        self._collimator_width_max = LineEdit(self, ProtocolStep.collimator_width_max)

        self._table_speed = LineEdit(self, ProtocolStep.table_speed)
        self._table_speed_min = LineEdit(self, ProtocolStep.table_speed_min)
        self._table_speed_max = LineEdit(self, ProtocolStep.table_speed_max)

        self._kv = JsonKvComboBox(self, ProtocolStep.kv)
        self._kv_min = JsonKvComboBox(self, ProtocolStep.kv_min)
        self._kv_max = JsonKvComboBox(self, ProtocolStep.kv_max)

        self._ma = JsonMaComboBox(self, ProtocolStep.ma)
        self._ma_min = JsonMaComboBox(self, ProtocolStep.ma_min)
        self._ma_max = JsonMaComboBox(self, ProtocolStep.ma_max)

        self._dose = LineEdit(self, ProtocolStep.dose)
        self._dose_max = LineEdit(self, ProtocolStep.dose_max)

        self._start_delay = LineEdit(self, ProtocolStep.start_delay)

        # TODO: commands
        self._start_voice_cmd = LineEdit(self, ProtocolStep.start_voice_cmd_uid)
        self._end_voice_cmd = LineEdit(self, ProtocolStep.end_voice_cmd_uid)

        self._is_auto_start = EnumComboBox(self, ProtocolStep.is_auto_start, BoolEnum)

        self._ecg_trigger = EnumComboBox(self, ProtocolStep.ecg_trigger, BoolEnum)
        self._contrast_type = EnumComboBox(self, ProtocolStep.contrast_type, ContrastTypeEnum)

        self._manual_start = EnumComboBox(self, ProtocolStep.manual_start, BoolEnum)

        self._table_widget = TableSelectWidget(self)

        self._place_widget_on_table_widget()
        self._table_widget.resizeColumnsToContents()

        name_vbox = QVBoxLayout()
        name_vbox.setAlignment(Qt.AlignmentFlag.AlignCenter)
        name_vbox.addWidget(InfoLabel(self, ProtocolStep.name))
        name_vbox.addWidget(self._step_name)

        self._step_name.setMaximumWidth(400)

        self._main_vbox.addLayout(name_vbox)
        self._main_vbox.addWidget(self._table_widget)

    def _place_widget_on_table_widget(self):
        self._table_widget.add_row(self._scan_type)
        self._table_widget.add_row(self._patient_pos)
        self._table_widget.add_row(self._table_motion)
        self._table_widget.add_row(self._number_of_volume_images)

        self._table_widget.add_row(self._start_pos,
                                   self._start_pos_min,
                                   self._start_pos_max)

        self._table_widget.add_row(self._stop_pos,
                                   self._stop_pos_min,
                                   self._stop_pos_max)

        self._table_widget.add_row(self._table_height,
                                   self._table_height_min,
                                   self._table_height_max)

        self._table_widget.add_row(self._auto_calculated)

        self._table_widget.add_row(self._pitch, self._pitch_min, self._pitch_max)

        self._table_widget.add_row(self._collimator_width,
                                   self._collimator_width_min,
                                   self._collimator_width_max)

        self._table_widget.add_row(self._table_speed,
                                   self._table_speed_min,
                                   self._table_speed_max)

        self._table_widget.add_row(self._kv, self._kv_min, self._kv_max)
        self._table_widget.add_row(self._ma, self._ma_min, self._ma_max)

        self._table_widget.add_row(self._dose, None, self._dose_max)

        self._table_widget.add_row(self._start_delay)
        self._table_widget.add_row(self._start_voice_cmd)
        self._table_widget.add_row(self._end_voice_cmd)
        self._table_widget.add_row(self._is_auto_start)
        self._table_widget.add_row(self._ecg_trigger)
        self._table_widget.add_row(self._contrast_type)
        self._table_widget.add_row(self._manual_start)


class ReconstructionOptionsTab(BaseProtocolStepEditTab):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        self._rec_width = LineEdit(self, ProtocolStep.reconstruction_width)
        self._rec_height = LineEdit(self, ProtocolStep.reconstruction_height)
        self._rec_res = EnumComboBox(self, ProtocolStep.reconstruction_resolution, ResolutionEnum)
        self._rec_axial_dist = LineEdit(self, ProtocolStep.reconstruction_axial_distance)
        self._slice_size = LineEdit(self, ProtocolStep.reconstruction_slice_size)
        self._kernel = EnumComboBox(self, ProtocolStep.reconstruction_kernel, KernelEnum)
        self._noise_reduction = LineEdit(self, ProtocolStep.reconstruction_noise_reduction)
        self._metal_correction = EnumComboBox(self, ProtocolStep.reconstruction_metal_correction, MetalCorrectionEnum)
        self._fbp_method = EnumComboBox(self, ProtocolStep.fbp_method, FBPMethodEnum)
        self._das_method = EnumComboBox(self, ProtocolStep.das_method, DASMethodEnum)

        # self._place_widgets()
        self._main_vbox.setAlignment(Qt.AlignmentFlag.AlignTop |
                                     Qt.AlignmentFlag.AlignLeft)
        self._place_widgets()


class ContrastInjectorTab(BaseProtocolStepEditTab):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        self._contrast_density = LineEdit(self, ProtocolStep.contrast_density)
        self._contrast_density_min = LineEdit(self, ProtocolStep.contrast_density_min)
        self._contrast_density_max = LineEdit(self, ProtocolStep.contrast_density_max)

        self._contrast_volume = LineEdit(self, ProtocolStep.contrast_volume)
        self._contrast_volume_min = LineEdit(self, ProtocolStep.contrast_volume_min)
        self._contrast_volume_max = LineEdit(self, ProtocolStep.contrast_volume_max)

        self._solvent_volume = LineEdit(self, ProtocolStep.contrast_solvent_volume)
        self._solvent_volume_min = LineEdit(self, ProtocolStep.contrast_solvent_volume_min)
        self._solvent_volume_max = LineEdit(self, ProtocolStep.contrast_solvent_volume_max)

        self._zone_diameter = LineEdit(self, ProtocolStep.zone_diameter)
        self._zone_diameter_min = LineEdit(self, ProtocolStep.zone_diameter_min)
        self._zone_diameter_max = LineEdit(self, ProtocolStep.zone_diameter_max)

        self._trigger_start_step_delay = LineEdit(self, ProtocolStep.contrast_step_delay)
        self._start_delay = LineEdit(self, ProtocolStep.contrast_start_delay)
        self._target_roi = LineEdit(self, ProtocolStep.target_roi)
        self._max_checks = LineEdit(self, ProtocolStep.max_checks)
        self._max_trigger_wait = LineEdit(self, ProtocolStep.max_trigger_wait)

        self._table_widget = TableSelectWidget(self)
        self._main_vbox.addWidget(self._table_widget)

        self._place_widget_on_table_widget()
        self._table_widget.resizeColumnsToContents()

    def _place_widget_on_table_widget(self):
        self._table_widget.add_row(self._contrast_density,
                                   self._contrast_density_min,
                                   self._contrast_density_max)

        self._table_widget.add_row(self._contrast_volume,
                                   self._contrast_volume_min,
                                   self._contrast_volume_max)

        self._table_widget.add_row(self._solvent_volume,
                                   self._solvent_volume_min,
                                   self._solvent_volume_max)

        self._table_widget.add_row(self._zone_diameter,
                                   self._zone_diameter_min,
                                   self._zone_diameter_max)

        self._table_widget.add_row(self._trigger_start_step_delay)
        self._table_widget.add_row(self._start_delay)
        self._table_widget.add_row(self._target_roi)
        self._table_widget.add_row(self._max_checks)
        self._table_widget.add_row(self._max_trigger_wait)


class DescriptionTab(BaseProtocolStepEditTab):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        self._description = LineEdit(self, ProtocolTabsEnum.desc)
        label = InfoLabel(self, self._description.get_key_name())
        label.setFixedWidth(130)

        hbox = QHBoxLayout()
        hbox.addWidget(label)
        hbox.addWidget(self._description)
        self._main_vbox.addLayout(hbox)


class CardioOptionsTab(BaseProtocolStepEditTab):
    def __init__(self, parent: 'ProtocolStepEditorTabBox'):
        super().__init__(parent)

        # TODO: Пока решили не трогать :)
