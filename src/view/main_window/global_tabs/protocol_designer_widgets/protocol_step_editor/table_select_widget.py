from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QAbstractItemView, QFrame, QHeaderView,
                             QTableWidget, QWidget)
from src.utils import const
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit


class TableSelectWidget(QTableWidget):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.NoSelection)

        self.setColumnCount(4)
        self.setHorizontalHeaderLabels(('Parameter', 'Value', 'Min', 'Max'))

    def _increment_row(self) -> int:
        row = self.rowCount()
        self.setRowCount(row + 1)
        return row

    def add_row(self, widget: LineEdit,
                widget_min: LineEdit = None,
                widget_max: LineEdit = None):

        if widget_min is None:
            widget_min = QFrame()
        if widget_max is None:
            widget_max = QFrame()

        info_label = InfoLabel(self, widget.get_key_name())
        info_label.setAlignment(Qt.AlignmentFlag.AlignRight |
                                Qt.AlignmentFlag.AlignVCenter)

        row = self._increment_row()
        self.setCellWidget(row, 0, info_label)
        self.setCellWidget(row, 1, widget)
        self.setCellWidget(row, 2, widget_min)
        self.setCellWidget(row, 3, widget_max)

        self.setRowHeight(row, const.TABLE_ROW_HEIGHT)
