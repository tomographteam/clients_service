from enum import Enum
from typing import Dict, Union

from PyQt5.QtWidgets import QTabWidget, QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model import ProtocolStep, ProtocolTabsEnum
from src.model_.managers.locale_manager import locale
from src.view.elements.forms.enum_combo_boxes import EnumComboBox
from src.view.elements.forms.line_edits import LineEdit
from src.view.main_window.global_tabs.protocol_designer_widgets.protocol_step_editor.tabs import (
    BaseProtocolStepEditTab, CardioOptionsTab, ContrastInjectorTab,
    DescriptionTab, ProtocolStepTab, ReconstructionOptionsTab)

FormDict = Dict[InstrumentedAttribute, Union[EnumComboBox, LineEdit]]


class ProtocolStepEditorTabBox(QTabWidget):
    def __init__(self, parent: QWidget) -> None:
        super().__init__(parent)

        self.form_dict: FormDict = {}

        self._protocol_step_tab = ProtocolStepTab(self)
        self._rec_opts_tab = ReconstructionOptionsTab(self)
        self._contrast_injector_tab = ContrastInjectorTab(self)
        self._cardio_opts_tab = CardioOptionsTab(self)
        self._desc_tab = DescriptionTab(self)

        self.addTab(self._protocol_step_tab, ProtocolTabsEnum.protocol)
        self.addTab(self._rec_opts_tab, ProtocolTabsEnum.recon)
        self.addTab(self._contrast_injector_tab, ProtocolTabsEnum.contrast)
        self.addTab(self._cardio_opts_tab, ProtocolTabsEnum.cardio)
        self.addTab(self._desc_tab, ProtocolTabsEnum.desc)

    def set_protocol_step(self, protocol_step: ProtocolStep):
        self._protocol_step_tab.set_protocol_step(protocol_step)
        self._rec_opts_tab.set_protocol_step(protocol_step)
        self._contrast_injector_tab.set_protocol_step(protocol_step)
        self._cardio_opts_tab.set_protocol_step(protocol_step)
        self._desc_tab.set_protocol_step(protocol_step)

    def addTab(self, tab: BaseProtocolStepEditTab, key_name: Enum):
        self.form_dict.update(tab.form_dict)
        return super().addTab(tab, locale(key_name))
