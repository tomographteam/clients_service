from typing import TYPE_CHECKING, Optional, Union

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QHBoxLayout
from src.db_manager import DataBaseManager
from src.model import Protocol, ProtocolStep
from src.model_.data.enums import (AreaKindEnum, BodyAreaEnum, BoolEnum,
                                   ButtonTitlesEnum)
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.enum_combo_boxes import EnumComboBox
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit
from src.view.elements.forms.spin_boxes import FloatSpinBox
from src.view.main_window.global_tabs.protocol_designer_widgets.protocol_step_editor.tab_box import \
    ProtocolStepEditorTabBox

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class ProtocolEditorDialog(BaseDialogWindow):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.setFixedWidth(400)
        self._close_btn.hide()

        self._protocol: Optional[Protocol] = None

        self.form_dict = {}

        # TODO
        self._name_line_edit = LineEdit(self, Protocol.name)
        self._kind_combo_box = EnumComboBox(self, Protocol.area_kind, AreaKindEnum)
        self._body_area_combo_box = EnumComboBox(self, Protocol.body_area, BodyAreaEnum)
        self._sievert_factor_fspin_box = FloatSpinBox(self, Protocol.sievert_factor)
        self._author_line_edit = LineEdit(self, Protocol.author)
        self._is_example_combo_box = EnumComboBox(self, Protocol.example_protocol, BoolEnum)

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self._sievert_factor_fspin_box.setSingleStep(0.01)

        # TODO: Сделать нормальную форму
        self._append_form_row(self._is_example_combo_box)
        self.main_vbox.addSpacing(20)
        self._append_form_row(self._name_line_edit)
        self._append_form_row(self._kind_combo_box)
        self._append_form_row(self._body_area_combo_box)
        self._append_form_row(self._sievert_factor_fspin_box)
        self.main_vbox.addSpacing(20)
        self._append_form_row(self._author_line_edit)

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._close_btn)

        self.main_vbox.addLayout(hbox)

        self._save_btn.clicked.connect(self._on_save_btn_clicked)
        self._close_btn.clicked.connect(self._on_close_btn_clicked)

    def _append_form_row(self, widget: Union[LineEdit, EnumComboBox]):
        hbox = QHBoxLayout()
        label = InfoLabel(self, widget.get_key_name())
        label.setAlignment(Qt.AlignmentFlag.AlignVCenter |
                           Qt.AlignmentFlag.AlignRight)
        label.setFixedWidth(150)
        label.setWordWrap(True)
        hbox.addWidget(label)
        hbox.addWidget(widget)
        self.main_vbox.addLayout(hbox)

    def set_protocol(self, protocol: Protocol):
        # TODO
        self._protocol = protocol

        self._name_line_edit.set_value_from_model(str(protocol.name))
        self._kind_combo_box.set_value_from_model(protocol.area_kind)
        self._body_area_combo_box.set_value_from_model(protocol.body_area)
        self._sievert_factor_fspin_box.set_value_form_model(protocol.sievert_factor or 0.0)
        self._author_line_edit.set_value_from_model(protocol.author)
        self._is_example_combo_box.set_value_from_model(protocol.example_protocol)

        self._save_btn.setEnabled(not protocol.example_protocol)

    def _on_save_btn_clicked(self):
        self._protocol.name = self._name_line_edit.text()
        self._protocol.area_kind = self._kind_combo_box.get_value()
        self._protocol.body_area = self._body_area_combo_box.get_value()
        self._protocol.sievert_factor = self._sievert_factor_fspin_box.value()
        self._protocol.author = self._author_line_edit.get_value()
        self._protocol.example_protocol = self._is_example_combo_box.get_value()

        DataBaseManager().save_protocol(self._protocol)
        self.hide()

    def _on_close_btn_clicked(self):
        self.hide()


class StepEditorDialog(BaseDialogWindow):
    def __init__(self, parent: 'Tab'):
        self._errors = set()
        super().__init__(parent)

        self.setMinimumSize(1500, 900)

        self._protocol: Optional[Protocol] = None
        self._step: Optional[ProtocolStep] = None

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._cancel_btn = Button(self, ButtonTitlesEnum.cancel)

        self._tab_box = ProtocolStepEditorTabBox(self)

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._cancel_btn)

        self.main_vbox.addWidget(self._tab_box)
        self.main_vbox.addLayout(hbox)

        self._cancel_btn.clicked.connect(self.hide)
        self._save_btn.clicked.connect(self._on_save_btn_clicked)

    def validate(self):

        for key_name, widget in self._tab_box.form_dict.items():
            try:
                value = widget.get_value()
                setattr(self._step, key_name.key, value)
            except AttributeError:
                pass

        type_errors = self._step.validate_types()
        for key_name, control in self._tab_box.form_dict.items():
            if key_name in type_errors:
                control.set_error_state()
            else:
                control.set_default_state()
        if type_errors:
            return

        for control, values in self._step.auto_lists().items():
            self._tab_box.form_dict[control]._sync_available = False
            self._tab_box.form_dict[control].clear()
            self._tab_box.form_dict[control].addItems((str(k) for k in values))
            self._tab_box.form_dict[control]._sync_available = True
        for control, value in self._step.auto_calculate().items():
            self._tab_box.form_dict[control]._sync_available = False
            self._tab_box.form_dict[control].clear()
            self._tab_box.form_dict[control].set_value_from_model(value)
            self._tab_box.form_dict[control]._sync_available = False

        self._errors = self._step.validate()
        for key_name, control in self._tab_box.form_dict.items():
            if key_name in self._errors:
                control.set_error_state()
            else:
                control.set_default_state()

        if self._errors:
            return False
        return True

    def _on_save_btn_clicked(self):

        if not self.validate():
            return

        self._step.number = len(self._protocol.steps)
        self._step.protocol_id = self._protocol.id
        DataBaseManager().save_protocol_step(self._step)
        self.hide()

    def set_protocol_step(self, protocol: Protocol, protocol_step: ProtocolStep):
        self._protocol = protocol
        self._step = protocol_step
        self._save_btn.setEnabled(not protocol.example_protocol)
        self._tab_box.set_protocol_step(protocol_step)
        self._tab_box.setCurrentIndex(0)

    # def hide(self):
    #     # Не даёт закрыть, если есть ошибки валидации,
    #     # чтобы протокол не остался с неверными значениями
    #     if not self._errors:
    #         return super().hide()

    # def keyPressEvent(self, e: QKeyEvent) -> None:
    #     if not self._errors:
    #         return super().keyPressEvent(e)
