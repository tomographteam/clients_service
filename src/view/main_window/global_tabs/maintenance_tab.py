import asyncio
from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget
from src.device_status_manager import DeviceStatusManager
from src.model import ControlButtonsEnum
from src.view.elements.buttons.control_button import ControlButton
from src.view.main_window.global_tabs.maintenance_tab_widgets.table import \
    MaintenanceTable
from src.view.main_window.global_tabs.maintenance_tab_widgets.tree_view import \
    HWTestTreeView

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class MaintenanceGlobalTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self._maintenance_table = MaintenanceTable(self)
        self._tree = HWTestTreeView(self)
        self._hw_test_btn = ControlButton(self, ControlButtonsEnum.run_hardware_test)
        self._morning_statup_btn = ControlButton(self, ControlButtonsEnum.morning_startup)
        self._evening_shutdown_btn = ControlButton(self, ControlButtonsEnum.evening_shutdown)

        hbox = QHBoxLayout(self)
        r_hbox = QHBoxLayout()
        l_vbox = QVBoxLayout()

        r_hbox.addWidget(self._maintenance_table)

        l_vbox.addWidget(self._tree)
        l_vbox.addWidget(self._morning_statup_btn)
        l_vbox.addWidget(self._evening_shutdown_btn)
        l_vbox.addSpacing(20)
        l_vbox.addWidget(self._hw_test_btn)

        hbox.addLayout(l_vbox)
        hbox.addLayout(r_hbox)

        self._hw_test_btn.clicked.connect(self._delayed_refresh)

        DeviceStatusManager().set_callbacks(
            tree_callback=self.tree_callback,
            data_callback=self.data_callback
        )

    def _delayed_refresh(self):
        async def doit():
            await asyncio.sleep(5)
            self._maintenance_table.update_table()

        loop = asyncio.get_event_loop()
        loop.create_task(doit())

    def tree_callback(self, tree):
        """
        Called on new parameters list from SCSC
        :param tree: dict
        :return:
        """

        # TODO: create/recreate QTreeWidget
        # pprint(tree)
        self._tree.recreate_tree(tree)
        pass

    def data_callback(self, data):
        """
        Called on new data from SCSC
        :param data:
        :return:
        """
        # TODO: fill QTreeWidget with data
        self._tree.update_tree(data)
        pass
