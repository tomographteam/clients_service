from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QVBoxLayout, QWidget
from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
    ProtocolsAndStepsTabBox

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class ProtocolDesignerTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        vbox = QVBoxLayout(self)
        vbox.addWidget(ProtocolsAndStepsTabBox(self))
