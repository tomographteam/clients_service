from typing import TYPE_CHECKING, Optional

from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget
from src.model import Patient
from src.model_.data.enums import ButtonTitlesEnum
from src.view.elements.buttons.button import Button
from src.view.main_window.global_tabs.patient_tab_widgets.dialog import \
    PatientEditorDialog
from src.view.main_window.global_tabs.patient_tab_widgets.table import \
    PatientsTable

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class PatientsGlobalTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self._patients_table = PatientsTable(self)
        self._patient_editor_dialog: Optional[PatientEditorDialog] = None

        self._update_btn = Button(self, ButtonTitlesEnum.update)
        self._create_btn = Button(self, ButtonTitlesEnum.create)
        self._edit_btn = Button(self, ButtonTitlesEnum.edit)

        vbox = QVBoxLayout(self)
        hbox = QHBoxLayout()

        hbox.addWidget(self._update_btn)
        hbox.addStretch()
        hbox.addWidget(self._edit_btn)
        hbox.addWidget(self._create_btn)

        vbox.addWidget(self._patients_table)
        vbox.addLayout(hbox)

        self._update_btn.clicked.connect(self._patients_table.update_table)
        self._edit_btn.clicked.connect(self._on_edit_btn_clicked)
        self._create_btn.clicked.connect(self._on_create_btn_clicked)

    def _show_editor_dialog(self, patient: Patient):
        if self._patient_editor_dialog is None:
            self._patient_editor_dialog = PatientEditorDialog(self)
        self._patient_editor_dialog.set_patient(patient)
        self._patient_editor_dialog.show()

    def _on_edit_btn_clicked(self):
        patient = self._patients_table.get_current_patient()
        if patient is not None:
            self._show_editor_dialog(patient)

    def _on_create_btn_clicked(self):
        patient = Patient()
        self._show_editor_dialog(patient)
