from typing import TYPE_CHECKING, Dict, List, Optional

from PyQt5.QtCore import QModelIndex
from PyQt5.QtGui import QColor, QFont, QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QTreeView
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import locale
from src.view.main_window.global_tabs.maintenance_tab_widgets.dialog import \
    ParameterEditorDialog

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.maintenance_tab_widgets.table import \
        MaintenanceTable

JsonTreeType = Dict[str, List[Dict[str, str]]]


class TreeItem(QStandardItem):
    def __init__(self, txt='', font_size=12, set_bold=False, color=QColor(0, 0, 0)):
        super().__init__()

        fnt = QFont('Open Sans', font_size)
        fnt.setBold(set_bold)

        self.setEditable(False)
        self.setForeground(color)
        self.setFont(fnt)
        self.setText(txt)


class HWTestTreeView(QTreeView):
    def __init__(self, parent: 'MaintenanceTable') -> None:
        super().__init__(parent)

        self.setFixedWidth(360)

        self._item_dataset: Dict[str, List[str]] = {}

        # TODO: Сделать локализацию, при смене языка
        self._model = QStandardItemModel()
        self._model.setHorizontalHeaderItem(0, QStandardItem(locale(TableHeadersEnum.parameter)))
        self._model.setHorizontalHeaderItem(1, QStandardItem(locale(TableHeadersEnum.value)))
        self.setModel(self._model)

        self._editor_dialog: Optional[ParameterEditorDialog] = None

        self.doubleClicked.connect(self._show_parameter_editor_dialog)

    def _show_parameter_editor_dialog(self, index: QModelIndex):
        if self._editor_dialog is None:
            self._editor_dialog = ParameterEditorDialog(self)

        device_row = index.parent().row()

        if device_row < 0:
            return

        data = self._item_dataset.get((device_row, index.row()))
        if data:
            self._editor_dialog.set_parameter_data(
                data['device'], data['name'], data['value'])
            self._editor_dialog.show()

    def update_tree(self, json_tree: JsonTreeType):
        self._item_dataset.clear()
        device_row = 0
        for device, device_data in json_tree.items():
            item = self._model.findItems(device)[0]
            for row, item_data in enumerate(device_data.values()):
                child_name = item.child(row, 0)
                child_value = item.child(row, 1)
                assert child_name.text() == item_data["name"]
                child_value.setText(str(item_data["value"]))

                data = item_data.copy()
                data.update({'device': device})
                self._item_dataset[(device_row, row)] = data
            device_row += 1

    def recreate_tree(self, json_tree: JsonTreeType):
        self._model.clear()
        self._model.setColumnCount(2)
        root = self._model.invisibleRootItem()

        # DeviceName: [{Name, Value, Info, History, Properties}, {...}]
        for device, item_data_list in json_tree.items():
            device_item = QStandardItem(device)
            device_item.setEditable(False)

            # Заглушка
            _item = QStandardItem()
            _item.setEditable(False)

            root.appendRow((device_item, _item))

            for item_data in item_data_list:
                item = QStandardItem(item_data)
                item.setEditable(False)
                item2 = QStandardItem("?")
                item2.setEditable(False)
                device_item.appendRow((item, item2))

        self.resizeColumnToContents(0)
