from typing import Union

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QWidget
from src.model_.data.enums import ButtonTitlesEnum, ParameterPropsEnum
from src.model_manager import ModelManager
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.labels import InfoLabel, ValueLabel
from src.view.elements.forms.line_edits import LineEdit


class ParameterEditorDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setFixedWidth(430)
        self.form_dict = None

        self._device_name = ValueLabel(self, ParameterPropsEnum.device_name)
        self._param_name = ValueLabel(self, ParameterPropsEnum.param_name)
        self._old_value = ValueLabel(self, ParameterPropsEnum.old_value)
        self._new_value = LineEdit(self, ParameterPropsEnum.new_value)

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self._add_form(self._device_name)
        self._add_form(self._param_name)
        self._add_form(self._old_value)
        self._add_form(self._new_value)

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._close_btn)
        self.main_vbox.addLayout(hbox)

        self._close_btn.clicked.connect(self.hide)
        self._save_btn.clicked.connect(self._save)

    def _add_form(self, widget: Union[LineEdit, ValueLabel]):
        hbox = QHBoxLayout()
        label = InfoLabel(self, widget.get_key_name())
        label.setAlignment(Qt.AlignmentFlag.AlignVCenter |
                           Qt.AlignmentFlag.AlignRight)
        label.setFixedWidth(200)
        hbox.addWidget(label)
        hbox.addWidget(widget)
        self.main_vbox.addLayout(hbox)

    def set_parameter_data(self, device: str, name: str, old_value: str):
        self._device_name.set_value_from_model(device)
        self._param_name.set_value_from_model(name)
        self._old_value.set_value_from_model(old_value)

    def _save(self):
        # TODO: Сделать правильно отправку форму на CIS
        device = self._device_name.get_value()
        name = self._param_name.get_value()
        value = self._new_value.get_value()
        ModelManager().send_form_data(..., {device: {name: value}})

        self.hide()
