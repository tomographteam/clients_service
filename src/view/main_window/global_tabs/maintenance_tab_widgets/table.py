import json
from datetime import datetime, timedelta, timezone
from typing import TYPE_CHECKING, List

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import HWTest
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, multi_locale

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Interactive)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))


class MaintenanceTable(_Table):

    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self._hw_tests: List[HWTest] = None

        header_labels = ('Time done', 'Report data')
        self.setColumnCount(len(header_labels))
        self.setHorizontalHeaderLabels(header_labels)

        DataBaseManager().patients_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()
        self.update_table()
        self.resizeColumnsToContents()

    def translate(self):
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.time_done,
                          TableHeadersEnum.report_data)))

    def update_table(self):
        self._hw_tests: List[HWTest] = DataBaseManager().get_hw_tests()

        self.setRowCount(len(self._hw_tests))
        local_tz = datetime.now(timezone(timedelta(0))).astimezone().tzinfo

        for row, hw_test in enumerate(self._hw_tests):
            data = json.loads(hw_test.report_json)

            if row == 0:
                self.setColumnCount(1 + len(data))
                self.setHorizontalHeaderLabels([HWTest.time_done.key] + list(data.keys()))

            time = hw_test.time_done.replace(tzinfo=timezone.utc)
            time = time.astimezone(local_tz).strftime("%Y-%m-%d %H:%M:%S")

            self.setItem(row, 0, QTableWidgetItem(time))
            for i, val in enumerate(data.values()):
                self.setItem(row, 1 + i, QTableWidgetItem(str(val)))

        self.sortByColumn(0, Qt.DescendingOrder)
