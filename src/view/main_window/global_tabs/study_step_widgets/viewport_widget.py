from typing import TYPE_CHECKING

import numpy as np
import pydicom
from PyQt5.QtCore import QRectF, Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QFrame, QGraphicsScene, QGraphicsView, QLabel
from src.view.elements.resizeble_rect import ResizableRect

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
        StudyStepTab
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab_box import \
        StudyStepTabBox


class ScoutViewportWidget(QFrame):

    def __init__(self, parent: 'StudyStepTabBox'):
        super().__init__(parent)

        self.rect = ResizableRect()
        scene = QGraphicsScene(0, 0, 512, 512)

        data_set = pydicom.dcmread(  # TODO temporary
            "resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm")
        height, width = data_set.pixel_array[20].shape
        qimage = QImage(data_set.pixel_array[20].astype(np.uint8),
                        height, width, QImage.Format_Grayscale8)
        pixmap = QPixmap(qimage)

        scene.addPixmap(pixmap.scaled(512, 512,
                        Qt.AspectRatioMode.KeepAspectRatio,
                        Qt.TransformationMode.SmoothTransformation))

        scene.addItem(self.rect)
        self.view = QGraphicsView(self)
        self.view.setFixedSize(512, 512)
        self.view.setScene(scene)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)

        self.setMinimumSize(512, 512)
        self.set_rect()

    def set_rect(self, min_x=10, max_x=90, min_y=10, max_y=90):
        """
        :param min_x:
        :param max_x:
        :param min_y: percents from head to legs
        :param max_y: percents from head to legs
        :return:
        """
        size = self.view.maximumViewportSize()

        new_rect = QRectF(size.width() / 100 * min_x, size.height() / 100 * min_y,
                          size.width() / 100 * (max_x - min_x),
                          size.height() / 100 * (max_y - min_y))
        self.rect.setRect(new_rect)


class ViewportWidget3d(QFrame):

    def __init__(self, parent: 'StudyStepTab'):
        super().__init__(parent)

        data_set = pydicom.dcmread(  # TODO temporary
            "resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm")
        self.pixel_array = data_set.pixel_array

        self.current_image = 0

        self.label = QLabel(self)

        self.set_image()
        self.setMinimumSize(512, 512)

    def wheelEvent(self, event):
        d = event.angleDelta().y()
        self.current_image += int(np.sign(d))
        self.current_image = max(0, min(self.current_image, self.pixel_array.shape[0]-1))
        self.set_image()

    def set_image(self):

        height, width = self.pixel_array[self.current_image].shape
        qimage = QImage(self.pixel_array[self.current_image].astype(np.uint8),
                        height, width, QImage.Format_Grayscale8)
        pixmap = QPixmap(qimage).scaled(512, 512,
                                        Qt.AspectRatioMode.KeepAspectRatio,
                                        Qt.TransformationMode.SmoothTransformation)

        self.label.setPixmap(pixmap)
