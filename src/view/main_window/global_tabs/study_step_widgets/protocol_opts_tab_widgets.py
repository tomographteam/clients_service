from enum import Enum
from typing import TYPE_CHECKING, Optional, Union

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QTabWidget, QVBoxLayout, QWidget
from src.model import ProtocolStep, ScanTypeEnum
from src.model_.data.enums import DialogTitlesEnum, TabTitlesEnum, TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, locale
from src.view.base_protocol.base_protocol_options import BaseProtocolOptions
from src.view.elements.forms.enum_combo_boxes import EnumComboBoxSync
from src.view.elements.forms.form_item import ProtocolOptFormItem
from src.view.elements.forms.json_combo_boxes import JsonKvComboBoxSync
from src.view.elements.forms.labels import InfoLabel, TitleLabel, ValueLabel
from src.view.elements.forms.line_edits import LineEditSync
from src.view.main_window.global_tabs.study_step_widgets.control_widgets import \
    StudyControlWidget

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
        StudyStepTab


class BaseProtocolOptsTab(QWidget):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__()
        self._base_protocol = base_protocol_opts_widget
        self._tab_title_key: Optional[TabTitlesEnum] = None

        self._main_vbox = QVBoxLayout(self)
        self._main_vbox.setAlignment(Qt.AlignTop | Qt.AlignHCenter)

    def _append_to_form(self, label: ValueLabel,
                        widget: Union[ValueLabel, Union[EnumComboBoxSync,
                                                        LineEditSync,
                                                        JsonKvComboBoxSync]]):
        self._main_vbox.addLayout(ProtocolOptFormItem(label, widget))

    def get_title(self) -> str:
        if self._tab_title_key:
            return locale(self._tab_title_key)
        return ''


class PatientOptionsTab(BaseProtocolOptsTab):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__(base_protocol_opts_widget)
        self._tab_title_key = TabTitlesEnum.patient

        self._main_vbox.addWidget(TitleLabel(self, locale(ProtocolStep.patient_position)))
        self._main_vbox.addWidget(self._base_protocol.patient_pos_smart_label)


class ScanOptionsTab(BaseProtocolOptsTab):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__(base_protocol_opts_widget)
        self._tab_title_key = TabTitlesEnum.scan

        self._main_vbox.addWidget(TitleLabel(self, locale(DialogTitlesEnum.scan_options)))

        self._append_to_form(InfoLabel(None, ProtocolStep.start_position),
                             self._base_protocol.start_pos_line_edit)
        self._append_to_form(QLabel(),
                             self._base_protocol.stop_pos_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.point_zero),
                             ValueLabel(self._base_protocol, ProtocolStep.point_zero))
        self._append_to_form(InfoLabel(None, ProtocolStep.table_height),
                             self._base_protocol.table_height_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.auto_calculated),
                             self._base_protocol.auto_calculated_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.pitch),
                             self._base_protocol.pitch_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.collimator_width),
                             self._base_protocol.collimator_width_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.table_speed),
                             self._base_protocol.table_speed_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.kv),
                             self._base_protocol.kv_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.ma),
                             self._base_protocol.ma_combo_box)


class ReconstructionOptionsTab(BaseProtocolOptsTab):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__(base_protocol_opts_widget)
        self._tab_title_key = TabTitlesEnum.recon

        self._main_vbox.addWidget(TitleLabel(self, locale(DialogTitlesEnum.reconstruction_options)))

        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_width),
                             self._base_protocol.recon_width_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_height),
                             self._base_protocol.recon_height_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_resolution),
                             self._base_protocol.recon_res_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_axial_distance),
                             self._base_protocol.axial_dist_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_slice_size),
                             self._base_protocol.slice_size_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_kernel),
                             self._base_protocol.kernel_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_noise_reduction),
                             self._base_protocol.noise_reduct_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.reconstruction_metal_correction),
                             self._base_protocol.metal_corr_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.fbp_method),
                             self._base_protocol.fbp_method_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.das_method),
                             self._base_protocol.das_method_combo_box)


class PlaningOptionsTab(BaseProtocolOptsTab):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__(base_protocol_opts_widget)
        self._tab_title_key = TabTitlesEnum.planning

        self._main_vbox.addWidget(TitleLabel(self, locale(DialogTitlesEnum.planing_options)))
        self._main_vbox.setAlignment(Qt.AlignTop | Qt.AlignHCenter)

        self._study_control_widget = StudyControlWidget(self)

        self._main_vbox.addWidget(self._study_control_widget)


class TriggerOptionsTab(BaseProtocolOptsTab):

    def __init__(self, base_protocol_opts_widget: BaseProtocolOptions):
        super().__init__(base_protocol_opts_widget)
        self._tab_title_key = TabTitlesEnum.trigger

        self._main_vbox.addWidget(TitleLabel(self, locale(DialogTitlesEnum.trigger_options)))

        self._append_to_form(InfoLabel(None, ProtocolStep.contrast_type),
                             self._base_protocol.contrast_type_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.contrast_density),
                             self._base_protocol.contrast_density_combo_box)
        self._append_to_form(InfoLabel(None, ProtocolStep.contrast_volume),
                             self._base_protocol.contrast_volume_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.contrast_solvent_volume),
                             self._base_protocol.solvent_volume_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.start_delay),
                             self._base_protocol.start_delay_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.target_roi),
                             self._base_protocol.target_roi_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.max_trigger_wait),
                             self._base_protocol.max_trigger_wait_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.max_checks),
                             self._base_protocol.max_checks_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.zone_diameter),
                             self._base_protocol.zone_diameter_line_edit)
        self._append_to_form(InfoLabel(None, ProtocolStep.step_delay),
                             self._base_protocol.step_delay_line_edit)


class StudyStepOptionsTabBox(QTabWidget):

    def __init__(self, parent: 'StudyStepTab'):
        super().__init__(parent)

        self.setTabPosition(QTabWidget.West)
        self.setMaximumWidth(510)

        self.setObjectName('StudyStepOptionsTabBox')

        self._base_protocol = BaseProtocolOptions(self)

        self._patient_tab = PatientOptionsTab(self._base_protocol)
        self._scan_opts_tab = ScanOptionsTab(self._base_protocol)
        self._recon_opts_tab = ReconstructionOptionsTab(self._base_protocol)
        self._planing_opts_tab = PlaningOptionsTab(self._base_protocol)
        self._trigger_opts_tab = TriggerOptionsTab(self._base_protocol)

        self.addTab(self._patient_tab, self._patient_tab.get_title())
        self.addTab(self._scan_opts_tab, self._scan_opts_tab.get_title())
        self.addTab(self._recon_opts_tab, self._recon_opts_tab.get_title())
        self.addTab(self._planing_opts_tab, self._planing_opts_tab.get_title())
        self.addTab(self._trigger_opts_tab, self._trigger_opts_tab.get_title())

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        for i in range(self.count()):
            self.setTabText(i, self.widget(i).get_title())

    def set_protocol(self, step: ProtocolStep):
        self._base_protocol.from_model(step)

        if step.scan_type is ScanTypeEnum.area_select:
            self.remove_tab(self._scan_opts_tab)
            self.remove_tab(self._recon_opts_tab)
            self.remove_tab(self._trigger_opts_tab)
            self.remove_tab(self._patient_tab)

    def remove_tab(self, tab: BaseProtocolOptsTab) -> None:
        return super().removeTab(self.indexOf(tab))

    def from_model(self, protocol: ProtocolStep):
        self._base_protocol.from_model(protocol)
