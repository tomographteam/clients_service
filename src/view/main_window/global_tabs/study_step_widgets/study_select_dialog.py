from asyncqt import QtCore
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import (QAbstractItemView, QHBoxLayout, QHeaderView,
                             QTableView, QVBoxLayout, QWidget)
from sqlalchemy.orm import Session
from src.db_manager import DataBaseManager
from src.model import ControlButtonsEnum, Patient, Protocol, Study
from src.model_.data.enums import ButtonTitlesEnum
from src.model_.managers.locale_manager import locale
from src.model_manager import ModelManager
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button


class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, data):
        super(TableModel, self).__init__()
        self._data = data

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self._data[index.row()][index.column()]

    def rowCount(self, index):
        return len(self._data)

    def columnCount(self, index):
        return len(self._data[0])


class StudySelectWidget(QTableView):
    study_selected = pyqtSignal(object)  # Study

    def __init__(self, parent):
        super().__init__(parent)

        studies = DataBaseManager().get_studies_with_patients()

        self._model = TableModel([[s.id, locale(s.body_area), p.family, p.name]
                                  for s, p in studies])
        self.setModel(self._model)
        self.verticalHeader().setVisible(False)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)


class SelectStudyDialog(BaseDialogWindow):
    step_added = pyqtSignal(object)  # Protocol

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self._title_bar.hide()

        inner_widget = QWidget(self)
        self._studies_selector = StudySelectWidget(self)

        main_vbox = QVBoxLayout(inner_widget)
        selectors_hbox = QHBoxLayout()
        btns_hbox = QHBoxLayout()

        self._close_btn = Button(self, ButtonTitlesEnum.close)
        self._select_btn = Button(self, ButtonTitlesEnum.select)

        selectors_hbox.addWidget(self._studies_selector)

        btns_hbox.addWidget(self._select_btn)
        btns_hbox.addWidget(self._close_btn)

        main_vbox.addLayout(selectors_hbox)
        main_vbox.addLayout(btns_hbox)

        self.set_inner_widget(inner_widget)

        self._close_btn.clicked.connect(self.hide)
        self._select_btn.clicked.connect(self._select)

        self.setFixedSize(600, 400)

    def _select(self):
        selected = self._studies_selector.selectedIndexes()
        if selected:
            study_id = selected[0].data()
            ModelManager().send_command_to_server(
                ControlButtonsEnum.select_study,
                study_id=study_id)
            self.close()


class ProtocolSelectWidget(QTableView):
    study_selected = pyqtSignal(object)  # Study

    def __init__(self, parent):
        super().__init__(parent)

        protocols = DataBaseManager().get_protocols()

        self._model = TableModel([[p.id, locale(p.body_area), p.name] for p in protocols])
        self.setModel(self._model)
        self.verticalHeader().setVisible(False)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)


class SelectProtocolDialog(BaseDialogWindow):
    step_added = pyqtSignal(object)  # Protocol

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self._title_bar.hide()

        inner_widget = QWidget(self)
        self._protocol_selector = ProtocolSelectWidget(self)

        main_vbox = QVBoxLayout(inner_widget)
        selectors_hbox = QHBoxLayout()
        btns_hbox = QHBoxLayout()

        self._close_btn = Button(self, ButtonTitlesEnum.close)
        self._select_btn = Button(self, ButtonTitlesEnum.select)

        selectors_hbox.addWidget(self._protocol_selector)

        btns_hbox.addWidget(self._select_btn)
        btns_hbox.addWidget(self._close_btn)

        main_vbox.addLayout(selectors_hbox)
        main_vbox.addLayout(btns_hbox)

        self.set_inner_widget(inner_widget)

        self._close_btn.clicked.connect(self.hide)
        self._select_btn.clicked.connect(self._select)

        self.setFixedSize(600, 400)

    def _select(self):
        selected = self._protocol_selector.selectedIndexes()
        if selected:
            protocol_id = selected[0].data()
            ModelManager().send_command_to_server(
                ControlButtonsEnum.import_protocol,
                protocol_id=protocol_id)
            self.close()
