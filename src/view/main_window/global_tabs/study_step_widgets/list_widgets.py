from PyQt5.QtWidgets import QListWidget, QWidget


class CurrentedStepsListWidget(QListWidget):
    def __init__(self, parent: QWidget, ):
        super().__init__(parent)

        self.setMinimumHeight(50)
        self.setMaximumHeight(300)
