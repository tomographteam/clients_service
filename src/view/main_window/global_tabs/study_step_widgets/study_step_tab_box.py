from PyQt5.QtWidgets import QTabWidget, QWidget
from src.model import Study
from src.model_manager import ModelManager
from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
    StudyStepTab


class StudyStepTabBox(QTabWidget):

    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.setTabPosition(QTabWidget.South)

        ModelManager().model_updates_signal.connect(self.update_tabs)

    def update_tabs(self, data):
        study = Study.from_dict(data)

        # check steps ids
        # if changed delete all and recreate
        new_ids = [s.id for s in study.steps]
        old_ids = {self.widget(i).id: self.widget(i) for i in range(self.count())}

        self.setUpdatesEnabled(False)

        if new_ids != list(old_ids.keys()):

            for id, widget in old_ids.items():
                if id not in new_ids:
                    self.removeTab(self.indexOf(widget))
                    widget.setParent(None)
                    widget.deleteLater()

            old_ids = [self.widget(i).id for i in range(self.count())]

            for i, step in enumerate(study.steps):
                if step.id not in old_ids:
                    self.add_study_step_tab(study.steps[i], i)

        self.setCurrentIndex(study.current_step)

        for i, step in enumerate(study.steps):
            tab: StudyStepTab = self.widget(i)
            tab.from_model(step, study)

            # Оставь пока, мне так лучше видно вкладки
            if i != self.currentIndex() and i != 0:
                self.setTabEnabled(i, False)
            else:
                self.setTabEnabled(i, True)

        self.setUpdatesEnabled(True)

    def add_study_step_tab(self, step, step_index: int):
        tab = StudyStepTab(self, step, step_index)
        self.insertTab(step_index, tab, tab.get_step_name())
