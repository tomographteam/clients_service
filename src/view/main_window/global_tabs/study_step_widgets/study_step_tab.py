from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget
from src.model import (BodyAreaEnum, ProtocolStep, ScanTypeEnum, Study,
                       StudyStep)
from src.model_.data.enums import ButtonTitlesEnum, StepStateEnum
from src.model_.managers.locale_manager import locale
from src.view.elements.buttons.button import Button
from src.view.elements.forms.enum_combo_boxes import EnumComboBox
from src.view.main_window.global_tabs.reports_tab_widgets.dialog import ReportDialog
from src.view.main_window.global_tabs.study_step_widgets.area_place_editor import \
    AreaPlaceEditor
from src.view.main_window.global_tabs.study_step_widgets.control_widgets import \
    StepControlWidget
from src.view.main_window.global_tabs.study_step_widgets.dose_eval_widget import \
    DoseEvalWidget
from src.view.main_window.global_tabs.study_step_widgets.protocol_opts_tab_widgets import \
    StudyStepOptionsTabBox
from src.view.main_window.global_tabs.study_step_widgets.study_select_dialog import (
    SelectProtocolDialog, SelectStudyDialog)
from src.view.main_window.global_tabs.study_step_widgets.viewport_widget import (
    ScoutViewportWidget, ViewportWidget3d)


class StudyStepTab(QWidget):

    def __init__(self, parent: QWidget, step: ProtocolStep, step_index: int):
        super().__init__(parent)

        self.id = step.id
        self.form_dict = {}

        self._protocol = step
        self._step_name = f'Step {step_index}'
        self._step_index = step_index

        self._main_vbox = QVBoxLayout(self)

        hbox = QHBoxLayout()
        self._main_vbox.addLayout(hbox)

        self._study_step_opts_tab_box = StudyStepOptionsTabBox(self)

        self._select_study_btn = Button(self, ButtonTitlesEnum.select_study)
        self._select_study_dialog = SelectStudyDialog(self)
        self._select_study_btn.clicked.connect(self._select_study_dialog.show)

        hbox2 = QHBoxLayout()
        hbox2.setAlignment(Qt.AlignmentFlag.AlignBottom)

        vbox = QVBoxLayout()

        if step.scan_type == ScanTypeEnum.area_select:
            study_hbox = QHBoxLayout()

            self._area_place_editor = AreaPlaceEditor(self)

            select_btns_vbox = QVBoxLayout()
            select_btns_vbox.setAlignment(Qt.AlignmentFlag.AlignRight | Qt.AlignmentFlag.AlignTop)

            self._select_protocol_btn = Button(self, ButtonTitlesEnum.select_protocol)
            self._body_area_combo = EnumComboBox(self, Study.body_area, BodyAreaEnum)
            self._body_area_combo.currentIndexChanged.connect(self._area_place_editor.rect_from_body_area)

            select_btns_vbox.addWidget(self._select_study_btn)
            select_btns_vbox.addWidget(self._select_protocol_btn)
            select_btns_vbox.addWidget(self._body_area_combo)

            self._select_protocol_dialog = SelectProtocolDialog(self)
            self._select_protocol_btn.clicked.connect(self._select_protocol_dialog.show)

            study_hbox.addWidget(self._area_place_editor)
            study_hbox.addLayout(select_btns_vbox)
            vbox.addLayout(study_hbox)

        elif step.scan_type == ScanTypeEnum.scout_two_side:
            viewport = QHBoxLayout()

            vbox.addLayout(viewport)

            self._dose_eval_widget = DoseEvalWidget(self)
            self._viewport_widget_left = ScoutViewportWidget(self)
            self._viewport_widget_front = ScoutViewportWidget(self)

            l_rect = self._viewport_widget_left.rect
            f_rect = self._viewport_widget_front.rect
            # cross-references for identical vertical coordinates
            f_rect.vertical_twin = l_rect
            l_rect.vertical_twin = f_rect

            hbox2.addWidget(self._dose_eval_widget)
            viewport.addWidget(self._viewport_widget_left)
            viewport.addWidget(self._viewport_widget_front)

        elif step.scan_type == ScanTypeEnum.helical:

            viewport = QHBoxLayout()

            vbox.addLayout(viewport)

            self._dose_eval_widget = DoseEvalWidget(self)
            self._viewport_widget = ViewportWidget3d(self)

            hbox2.addWidget(self._dose_eval_widget)
            viewport.addWidget(self._viewport_widget)

        self._step_control_widget = StepControlWidget(self)
        self._step_control_widget.need_show_report_dialog.connect(
            self._show_report_dialog)

        ct_vbox = QVBoxLayout()
        ct_vbox.setAlignment(Qt.AlignmentFlag.AlignBottom)
        ct_vbox.addWidget(self._step_control_widget)
        ct_vbox.addWidget(self._select_study_btn)

        hbox2.addStretch()
        hbox2.addLayout(ct_vbox)

        vbox.addLayout(hbox2)

        hbox.addWidget(self._study_step_opts_tab_box)
        hbox.addLayout(vbox)

        self.setMinimumSize(self.layout().sizeHint())

        self.set_protocol(step)

    def _show_report_dialog(self):

        report_dialog = ReportDialog(self)
        report_dialog.set_report(self._protocol)
        report_dialog.show()

    def set_protocol(self, step):
        self._step_name = f'{locale(step.scan_type)}\n{step.name}'
        self._study_step_opts_tab_box.set_protocol(step)

    def from_model(self, step: StudyStep, study):
        self._step_control_widget.update_buttons(step.get_control_buttons_enabled(study))
        self._study_step_opts_tab_box.from_model(step)
        if step.scan_type == ScanTypeEnum.scout_two_side:
            self._viewport_widget_left.setVisible(step.state == StepStateEnum.done)
            self._viewport_widget_front.setVisible(step.state == StepStateEnum.done)
        if step.scan_type == ScanTypeEnum.helical:
            self._viewport_widget.setVisible(step.state == StepStateEnum.done)

    def get_step_name(self) -> str:
        return self._step_name
