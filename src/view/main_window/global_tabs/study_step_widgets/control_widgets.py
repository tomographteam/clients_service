from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QWidget, QGridLayout, QVBoxLayout, QHBoxLayout
from src.model import ControlButtonsEnum, ProtocolStep
from src.model_manager import ModelManager
from src.view.elements.buttons.button import Button
from src.view.elements.buttons.control_button import ControlButton
from src.view.main_window.global_tabs.study_step_widgets.list_widgets import \
    CurrentedStepsListWidget
from src.view.main_window.global_tabs.study_step_widgets.protocol_select_dialog import \
    AddStepDialog

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
        StudyStepTab


class StepControlWidget(QWidget):
    need_show_report_dialog = pyqtSignal()

    def __init__(self, parent: 'StudyStepTab'):
        super().__init__(parent)

        self.setFixedSize(400, 150)

        self._send_to_hw_btn = ControlButton(self, ControlButtonsEnum.send_to_hw)
        self._back_step_btn = ControlButton(self, ControlButtonsEnum.prev)
        self._next_step_btn = ControlButton(self, ControlButtonsEnum.next)
        self._start_scan_btn = ControlButton(self, ControlButtonsEnum.start_scan)
        self._stop_scan_btn = ControlButton(self, ControlButtonsEnum.stop_scan)

        self._show_report_dialog_btn = Button(self, ControlButtonsEnum.show_report)

        gbox = QGridLayout(self)
        gbox.setContentsMargins(0, 0, 0, 0)
        gbox.addWidget(self._send_to_hw_btn, 0, 0, 1, 2)
        gbox.addWidget(self._back_step_btn, 1, 0)
        gbox.addWidget(self._next_step_btn, 1, 1)
        gbox.addWidget(self._start_scan_btn, 2, 0)
        gbox.addWidget(self._stop_scan_btn, 2, 1)
        gbox.addWidget(self._show_report_dialog_btn, 3, 0, 1, 2)

        self._show_report_dialog_btn.clicked.connect(self.need_show_report_dialog.emit)

    def update_buttons(self, state):
        self._back_step_btn.setEnabled(state[ControlButtonsEnum.prev])
        self._next_step_btn.setEnabled(state[ControlButtonsEnum.next])
        self._send_to_hw_btn.setEnabled(state[ControlButtonsEnum.send_to_hw])
        self._start_scan_btn.setEnabled(state[ControlButtonsEnum.start_scan])
        self._stop_scan_btn.setEnabled(state[ControlButtonsEnum.stop_scan])
        self._show_report_dialog_btn.setEnabled(state[ControlButtonsEnum.show_report])


class StudyControlWidget(QWidget):
    def __init__(self, parent: ...):
        super().__init__(parent)

        self._skip_button = ControlButton(self, ControlButtonsEnum.skip_step)
        self._add_button = Button(self, ControlButtonsEnum.add_step)

        self._add_step_dialog = AddStepDialog(self)
        self._add_step_dialog.set_inner_widget(AddStepDialog(self))

        self._main_vbox = QVBoxLayout(self)
        self._main_vbox.setAlignment(Qt.AlignTop | Qt.AlignHCenter)

        self._step_list_widget = CurrentedStepsListWidget(self)

        btn_hbox = QHBoxLayout()
        btn_hbox.addWidget(self._add_button)
        btn_hbox.addWidget(self._skip_button)

        self._main_vbox.addWidget(self._step_list_widget)
        self._main_vbox.addLayout(btn_hbox)

        self._add_button.clicked.connect(self._add_step_dialog.show)
        self._add_step_dialog.step_added.connect(self._on_step_added)

    def _on_step_added(self, step: ProtocolStep):
        ModelManager().send_command_to_server(ControlButtonsEnum.add_step, step_id=step.id)
