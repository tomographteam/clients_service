from typing import TYPE_CHECKING, List

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QFrame, QGridLayout, QLabel, QWidget
from src.model_.data.enums import DialogTitlesEnum
from src.model_.managers.locale_manager import locale

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
        StudyStepTab


# TODO
class DoseEvalWidget(QFrame):
    def __init__(self, parent: 'StudyStepTab'):
        super().__init__(parent)
        self.setMaximumSize(600, 200)
        self.setFont(QFont('', 17))

        self._main_gbox = QGridLayout(self)
        self._main_gbox.setSpacing(10)

        self._add_row((QLabel(locale(DialogTitlesEnum.this_scan)),
                       QLabel(locale(DialogTitlesEnum.all_scans)),
                       QLabel(locale(DialogTitlesEnum.evaluation))),
                      col=1)

        self._add_row((QLabel(locale(DialogTitlesEnum.gy)), QLabel('0'),
                       QLabel('0'), QLabel('0.15/3.0')))

        self._add_row((QLabel(locale(DialogTitlesEnum.sv_on_cm)), QLabel('0'),
                       QLabel('0'), QLabel('0.00')))

    def _add_row(self, widgets: List[QWidget], col: int = 0):
        row = self._main_gbox.rowCount()
        for widget in widgets:
            self._main_gbox.addWidget(widget, row, col, Qt.AlignRight)
            col += 1
