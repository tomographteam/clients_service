from typing import TYPE_CHECKING

from PyQt5.QtCore import QRectF, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsView, QVBoxLayout, QWidget
from src.model import BodyAreaEnum
from src.model_.managers.config_manager import ConfigManager, IConfigManager
from src.view.elements.resizeble_rect import ResizableRect

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.study_step_widgets.study_step_tab import \
        StudyStepTab

config_manager: IConfigManager = ConfigManager()


class AreaPlaceEditor(QWidget):
    def __init__(self, parent: 'StudyStepTab'):
        super().__init__(parent)

        self.setFixedSize(350, 650)

        self.rect = ResizableRect()
        scene = QGraphicsScene(0, 0, 300, 600)

        pixmap = QPixmap(config_manager.get_human_image_path())

        scene.addPixmap(pixmap.scaled(300, 600,
                        Qt.AspectRatioMode.KeepAspectRatio,
                        Qt.TransformationMode.SmoothTransformation))

        scene.addItem(self.rect)
        self.view = QGraphicsView(self)
        self.view.setFixedSize(300, 600)
        self.view.setScene(scene)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)

        main_vbox = QVBoxLayout(self)
        main_vbox.addWidget(self.view)

        self.rect_from_body_area(BodyAreaEnum.head)

    # TODO move this to server
    def rect_from_body_area(self, area: BodyAreaEnum):
        mapping = {
            BodyAreaEnum.head: {"min_y": 0, "max_y": 15},
            BodyAreaEnum.neck: {"min_y": 10, "max_y": 25},
            BodyAreaEnum.chest: {"min_y": 15, "max_y": 35},
            BodyAreaEnum.abdominal: {"min_y": 30, "max_y": 45},
            BodyAreaEnum.pelvis: {"min_y": 35, "max_y": 50},
            BodyAreaEnum.limbs: {"min_y": 55, "max_y": 100},
        }

        self.set_rect(**mapping[BodyAreaEnum(area)])

    def set_rect(self, min_y=10, max_y=30):
        """
        :param min_y: percents from head to legs
        :param max_y: percents from head to legs
        :return:
        """
        size = self.view.maximumViewportSize()

        new_rect = QRectF(0, size.height() / 100 * min_y,
                          size.width(), size.height() / 100 * (max_y - min_y))

        self.rect.setRect(new_rect)
