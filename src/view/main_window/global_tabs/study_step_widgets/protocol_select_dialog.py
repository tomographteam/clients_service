from typing import List, Optional

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QHBoxLayout, QListWidget, QVBoxLayout, QWidget
from src.db_manager import DataBaseManager
from src.model import Protocol, ProtocolStep
from src.model_.data.enums import ButtonTitlesEnum
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button


class StudyProtocolsSelectWidget(QListWidget):
    study_selected = pyqtSignal(object)  # Study

    def __init__(self, parent: 'AddStepDialog'):
        super().__init__(parent)

        self._studies: List[Protocol] = DataBaseManager().get_protocols()

        self.addItems([str(study.name) for study in self._studies])
        self.currentRowChanged.connect(self._on_current_row_changed)

    def _on_current_row_changed(self, row: int):
        self.study_selected.emit(self._studies[row])


class StepsSelectWidget(QListWidget):
    step_selected = pyqtSignal(object)  # Protocol
    step_deselected = pyqtSignal()

    def __init__(self, parent: 'AddStepDialog') -> None:
        super().__init__(parent)

        self._steps: Optional[List[ProtocolStep]] = None

        self.currentRowChanged.connect(self._on_current_row_changed)

    def update_steps(self, study: Protocol):
        self.clear()
        self._steps = DataBaseManager().get_protocol_steps(study)
        self.addItems([str(step.name) for step in self._steps])
        self.step_deselected.emit()

    def _on_current_row_changed(self, row: int):
        self.step_selected.emit(self._steps[row])

    def get_current_step(self) -> ProtocolStep:
        return self._steps[self.currentRow()]


class AddStepDialog(BaseDialogWindow):
    step_added = pyqtSignal(object)  # Protocol

    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self._title_bar.hide()

        inner_widget = QWidget(self)

        self._studies_selector = StudyProtocolsSelectWidget(self)
        self._steps_selector = StepsSelectWidget(self)

        main_vbox = QVBoxLayout(inner_widget)
        selectors_hbox = QHBoxLayout()
        btns_hbox = QHBoxLayout()

        self._close_btn = Button(self, ButtonTitlesEnum.close)
        self._add_btn = Button(self, ButtonTitlesEnum.add)

        selectors_hbox.addWidget(self._studies_selector)
        selectors_hbox.addWidget(self._steps_selector)

        btns_hbox.addWidget(self._add_btn)
        btns_hbox.addWidget(self._close_btn)

        main_vbox.addLayout(selectors_hbox)
        main_vbox.addLayout(btns_hbox)

        self.set_inner_widget(inner_widget)

        self._studies_selector.study_selected.connect(self._steps_selector.update_steps)
        self._steps_selector.step_selected.connect(self._on_step_selected)
        self._steps_selector.step_deselected.connect(self._on_step_deselected)

        self._close_btn.clicked.connect(self.hide)
        self._add_btn.clicked.connect(self._on_add_btn_clicked)

    def _on_step_selected(self):
        self._add_btn.setEnabled(True)

    def _on_step_deselected(self):
        self._add_btn.setEnabled(False)

    def _on_add_btn_clicked(self):
        self.step_added.emit(self._steps_selector.get_current_step())
        self.hide()
