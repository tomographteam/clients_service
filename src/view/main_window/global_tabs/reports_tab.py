from typing import TYPE_CHECKING, Optional

from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget
from src.model import StudyStep
from src.model_.data.enums import ButtonTitlesEnum
from src.view.elements.buttons.button import Button
from src.view.main_window.global_tabs.reports_tab_widgets.dialog import \
    ReportDialog
from src.view.main_window.global_tabs.reports_tab_widgets.table import \
    ReportsTable

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class ReportsTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self._update_btn = Button(self, ButtonTitlesEnum.update)
        self._view_report_btn = Button(self, ButtonTitlesEnum.view_report)

        self._reports_table = ReportsTable(self)
        self._report_dialog: Optional[None] = None

        vbox = QVBoxLayout(self)
        hbox = QHBoxLayout()

        hbox.addWidget(self._update_btn)
        hbox.addStretch()
        hbox.addWidget(self._view_report_btn)

        vbox.addWidget(self._reports_table)
        vbox.addLayout(hbox)

        self._update_btn.clicked.connect(self._reports_table.update_table)
        self._view_report_btn.clicked.connect(self._on_view_report_btn_clicked)

    def _show_report_dialog(self, report: StudyStep):
        if self._report_dialog is None:
            self._report_dialog = ReportDialog(self)
        self._report_dialog.set_report(report)
        self._report_dialog.show()

    def _on_view_report_btn_clicked(self):
        report = self._reports_table.get_current_report()
        if report is not None:
            self._show_report_dialog(report)
        pass
