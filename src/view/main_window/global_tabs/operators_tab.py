from typing import TYPE_CHECKING, Optional

from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QWidget
from src.db_manager import DataBaseManager
from src.model import Operator
from src.model_.data.enums import AcceptActionsEnum, ButtonTitlesEnum
from src.view.dialogs.accept.dialog import AcceptDialog
from src.view.elements.buttons.button import Button
from src.view.main_window.global_tabs.operators_tab_widgets.dialog import \
    OperatorEditorDialog
from src.view.main_window.global_tabs.operators_tab_widgets.table import \
    OperatorsTable

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class OperatorsTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self._update_btn = Button(self, ButtonTitlesEnum.update)
        self._edit_operator_btn = Button(self, ButtonTitlesEnum.edit)
        self._create_operator_btn = Button(self, ButtonTitlesEnum.create)
        self._delete_operator_btn = Button(self, ButtonTitlesEnum.delete)

        self._operators_table = OperatorsTable(self)
        self._operator_edit_dialog: Optional[OperatorEditorDialog] = None

        vbox = QVBoxLayout(self)
        vbox.addWidget(self._operators_table)

        btn_hbox = QHBoxLayout()
        btn_hbox.addWidget(self._delete_operator_btn)
        btn_hbox.addStretch()
        btn_hbox.addWidget(self._update_btn)
        btn_hbox.addStretch()
        btn_hbox.addWidget(self._edit_operator_btn)
        btn_hbox.addWidget(self._create_operator_btn)

        vbox.addLayout(btn_hbox)

        self._update_btn.clicked.connect(self._operators_table.update_table)
        self._edit_operator_btn.clicked.connect(self._on_edit_operator_btn_clicked)
        self._create_operator_btn.clicked.connect(self._on_create_operator_btn_clicked)
        self._delete_operator_btn.clicked.connect(self._on_delete_operator_btn_clicked)

    def _show_dialog_window(self, operator: Operator, show_password: bool):
        if self._operator_edit_dialog is None:
            self._operator_edit_dialog = OperatorEditorDialog(self)
        self._operator_edit_dialog.set_operator(operator, show_password)
        self._operator_edit_dialog.show()

    def _on_edit_operator_btn_clicked(self):
        operator = self._operators_table.get_current_operator()
        if operator:
            self._show_dialog_window(operator, False)

    def _on_create_operator_btn_clicked(self):
        self._show_dialog_window(Operator(), True)

    def _on_delete_operator_btn_clicked(self):
        operator = self._operators_table.get_current_operator()
        if operator:
            dialog = AcceptDialog(self)
            dialog.set_action_data(AcceptActionsEnum.delete_operator)
            if dialog.exec_():
                DataBaseManager().delete_operator(operator)
