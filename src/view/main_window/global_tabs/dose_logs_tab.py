from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QVBoxLayout, QWidget
from src.model_.data.enums import ButtonTitlesEnum
from src.view.elements.buttons.button import Button
from src.view.main_window.global_tabs.dose_logs_tab_widgets.table import \
    DoseLogsTable

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class DoseLogsTab(QWidget):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self._update_btn = Button(self, ButtonTitlesEnum.update)

        self._dose_logs_table = DoseLogsTable(self)

        vbox = QVBoxLayout(self)
        vbox.addWidget(self._dose_logs_table)
        vbox.addWidget(self._update_btn)

        self._update_btn.clicked.connect(self._dose_logs_table.update_table)
