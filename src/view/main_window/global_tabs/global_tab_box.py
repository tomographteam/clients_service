from typing import TYPE_CHECKING

from PyQt5.QtWidgets import QTabWidget

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class GlobalTabBox(QTabWidget):
    def __init__(self, parent: 'MainWindow') -> None:
        super().__init__(parent)
