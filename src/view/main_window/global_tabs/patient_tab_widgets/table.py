from typing import TYPE_CHECKING, List, Optional

from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import Patient
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, multi_locale
from src.utils import const

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))
        self.setSortingEnabled(True)


class PatientsTable(_Table):

    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        header_labels = ('Name', 'Family', 'Middle name',
                         'Date of birth', 'Gender')
        self.setColumnCount(len(header_labels))
        self.setHorizontalHeaderLabels(header_labels)

        self._patients: Optional[List[Patient]] = None
        self._current_patient: Optional[Patient] = None

        self.currentCellChanged.connect(self._on_current_cell_changed)
        DataBaseManager().patients_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

        self.update_table()

    def translate(self):
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.name,
                          TableHeadersEnum.family,
                          TableHeadersEnum.middle_name,
                          TableHeadersEnum.date_of_birth,
                          TableHeadersEnum.gender)))

    def _on_current_cell_changed(self, row: int):
        pass

    def update_table(self):
        self._patients: List[Patient] = DataBaseManager().get_patients()

        self.setRowCount(len(self._patients))

        for row, patient in enumerate(self._patients):
            self.setItem(row, 0, QTableWidgetItem(patient.name))
            self.setItem(row, 1, QTableWidgetItem(patient.family))
            self.setItem(row, 2, QTableWidgetItem(patient.middle_name))
            self.setItem(row, 3, QTableWidgetItem(patient.date_of_birth))
            self.setItem(row, 4, QTableWidgetItem(patient.sex))

            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)

    def get_current_patient(self) -> Optional[Patient]:
        try:
            return self._patients[self.currentRow()]
        except IndexError:
            return None
