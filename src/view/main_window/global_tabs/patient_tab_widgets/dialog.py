from typing import Optional

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QWidget
from src.db_manager import DataBaseManager
from src.model import Patient
from src.model_.data.enums import ButtonTitlesEnum
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit


class PatientEditorDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.form_dict = {}

        self._patient: Optional[Patient] = None

        self._name = LineEdit(self, Patient.name)
        self._family = LineEdit(self, Patient.family)
        self._middle_name = LineEdit(self, Patient.middle_name)
        self._date_of_birth = LineEdit(self, Patient.date_of_birth)
        self._sex = LineEdit(self, Patient.sex)
        self._phantom = LineEdit(self, Patient.phantom)
        self._height = LineEdit(self, Patient.height)
        self._weight = LineEdit(self, Patient.weight)

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self._close_btn.clicked.connect(self.hide)
        self._save_btn.clicked.connect(self._on_save_btn_clicked)

        [self._appent_to_form(widget) for widget in self.form_dict.values()]

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._close_btn)
        self.main_vbox.addSpacing(50)
        self.main_vbox.addLayout(hbox)

    def _on_save_btn_clicked(self):
        patient: Patient = self._patient

        patient.name = self._name.get_value()
        patient.family = self._family.get_value()
        patient.middle_name = self._middle_name.get_value()
        patient.date_of_birth = self._date_of_birth.get_value()
        patient.sex = self._sex.get_value()
        patient.phantom = self._phantom.get_value()
        patient.height = self._height.get_value()
        patient.weight = self._weight.get_value()

        DataBaseManager().save_patient(patient)
        self.hide()

    def _appent_to_form(self, input_widget: LineEdit):
        label = InfoLabel(self, input_widget.get_key_name())
        label.setFixedWidth(150)
        label.setWordWrap(True)
        label.setAlignment(Qt.AlignmentFlag.AlignVCenter |
                           Qt.AlignmentFlag.AlignRight)

        input_widget.setFixedWidth(300)

        hbox = QHBoxLayout()
        hbox.addWidget(label)
        hbox.addWidget(input_widget)
        self.main_vbox.addLayout(hbox)

    def set_patient(self, patient: Patient):
        self._patient = patient
        self._name.set_value_from_model(patient.name)
        self._family.set_value_from_model(patient.family)
        self._middle_name.set_value_from_model(patient.middle_name)
        self._date_of_birth.set_value_from_model(patient.date_of_birth)
        self._sex.set_value_from_model(patient.sex)
        self._phantom.set_value_from_model(patient.phantom)
        self._height.set_value_from_model(patient.height)
        self._weight.set_value_from_model(patient.weight)
