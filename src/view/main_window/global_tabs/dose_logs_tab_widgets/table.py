from datetime import datetime
from typing import TYPE_CHECKING, List, Optional

from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import Patient, Study, StudyStep
from src.model_.data.enums import ScanTypeEnum, TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, locale, multi_locale
from src.utils import const

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))
        self.setSortingEnabled(True)


class DoseLogsTable(_Table):

    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        header_labels = ('Date', 'Full name', 'Date of birth',
                         'Protocol', 'Scan type', 'Step name',
                         'Gy', 'Sv*cm')
        self.setColumnCount(len(header_labels))
        self.setHorizontalHeaderLabels(header_labels)

        self._study_steps: Optional[List[StudyStep]] = None
        self._current_study_step: Optional[StudyStep] = None

        self.currentCellChanged.connect(self._on_current_cell_changed)
        DataBaseManager().dose_logs_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

        self.update_table()

    def translate(self):
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.date,
                          TableHeadersEnum.full_name,
                          TableHeadersEnum.date_of_birth,
                          TableHeadersEnum.protocol,
                          TableHeadersEnum.scan_type,
                          TableHeadersEnum.step_name,
                          TableHeadersEnum.gy,
                          TableHeadersEnum.sv_on_cm)))

    def _on_current_cell_changed(self, row: int):
        pass

    def update_table(self):
        self._study_steps: List[StudyStep] = DataBaseManager().get_all_study_steps()

        # do not display select
        self._study_steps = [s for s in self._study_steps
                             if s.scan_type != ScanTypeEnum.area_select]

        self.setRowCount(len(self._study_steps))

        for row, study_step in enumerate(self._study_steps):
            study: Study = DataBaseManager().get_study_by_id(study_step.study_id)
            patient: Patient = DataBaseManager().get_patient_by_id(study.patient_id)
            if study_step.scan_timestamp:
                date = datetime.fromtimestamp(study_step.scan_timestamp).isoformat()
            else:
                date = "Not yet executed"
            self.setItem(row, 0, QTableWidgetItem(date))
            self.setItem(row, 1, QTableWidgetItem(f'{patient.family} {patient.name} {patient.middle_name}'))
            self.setItem(row, 2, QTableWidgetItem(str(patient.date_of_birth)))
            self.setItem(row, 3, QTableWidgetItem(study.protocol_name, QTableWidgetItem.ItemType.Type))
            self.setItem(row, 4, QTableWidgetItem(locale(study_step.scan_type)))
            self.setItem(row, 5, QTableWidgetItem(study_step.name))
            self.setItem(row, 6, QTableWidgetItem(str(study_step.dose_sievert)))
            self.setItem(row, 7, QTableWidgetItem(str(study.sievert_factor)))

            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)
