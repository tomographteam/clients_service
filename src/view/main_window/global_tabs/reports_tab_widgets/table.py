from datetime import datetime
from typing import TYPE_CHECKING, List, Optional

from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import Patient, ScanTypeEnum, Study, StudyStep
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, multi_locale
from src.utils import const

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))
        self.setSortingEnabled(True)


class ReportsTable(_Table):

    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        header_labels = ('Date&Time', 'Full name',
                         'Protocol', 'Step name', 'DICOM file')
        self.setColumnCount(len(header_labels))
        self.setHorizontalHeaderLabels(header_labels)

        self._study_steps: Optional[List[StudyStep]] = None
        self._current_study_step: Optional[StudyStep] = None

        self.currentCellChanged.connect(self._on_current_cell_changed)
        DataBaseManager().reports_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

        self.update_table()

    def translate(self):
        self.update_table()
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.date_time,
                          TableHeadersEnum.full_name,
                          TableHeadersEnum.protocol,
                          TableHeadersEnum.step_name,
                          TableHeadersEnum.dicom_file)))

    def _on_current_cell_changed(self, row: int):
        pass

    def update_table(self):
        self._study_steps: List[StudyStep] = DataBaseManager().get_all_study_steps()

        # do not display select
        self._study_steps = [s for s in self._study_steps
                             if s.scan_type != ScanTypeEnum.area_select]

        self.setRowCount(len(self._study_steps))

        for row, study_step in enumerate(self._study_steps):
            study: Study = DataBaseManager().get_study_by_id(study_step.study_id)
            patient: Patient = DataBaseManager().get_patient_by_id(study.patient_id)
            if study_step.scan_timestamp:
                date = datetime.fromtimestamp(study_step.scan_timestamp).isoformat()
            else:
                date = "Not yet executed"
            self.setItem(row, 0, QTableWidgetItem(date))
            self.setItem(row, 1, QTableWidgetItem(f'{patient.family} {patient.name} {patient.middle_name}'))
            self.setItem(row, 2, QTableWidgetItem(study.protocol_name))
            self.setItem(row, 3, QTableWidgetItem(study_step.name))
            self.setItem(row, 4, QTableWidgetItem(study_step.dicom_path))

            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)

    def get_current_report(self) -> Optional[StudyStep]:
        try:
            return self._study_steps[self.currentRow()]
        except IndexError:
            return None
