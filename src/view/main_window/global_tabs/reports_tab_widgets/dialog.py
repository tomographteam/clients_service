from PyQt5.QtWidgets import QHBoxLayout, QTextBrowser, QTextEdit, QWidget
from src.db_manager import DataBaseManager
from src.model import Patient, Study, StudyStep
from src.model_.data.enums import ButtonTitlesEnum
from src.model_.managers.locale_manager import locale
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button


class ReportDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self.setFixedSize(650, 800)

        # Такой виджет нужен для поддержки HTML-разметки
        self._text_browser = QTextBrowser(self)
        self._text_browser.setLineWrapMode(QTextEdit.LineWrapMode.NoWrap)

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._edit_btn = Button(self, ButtonTitlesEnum.edit)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self.main_vbox.addWidget(self._text_browser)

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._edit_btn)
        hbox.addWidget(self._close_btn)

        self.main_vbox.addLayout(hbox)

        self._close_btn.clicked.connect(self.hide)

    def set_report(self, report: StudyStep):
        # TODO
        study: Study = DataBaseManager().get_study_by_id(report.study_id)
        patient: Patient = DataBaseManager().get_patient_by_id(study.patient_id)
        text = (
            "<h3>Patient:</h3>"
            "<ul>"
            f"<li>Full name: {patient.family} {patient.name} {patient.middle_name}</li>"
            f"<li>Date of birth: {patient.date_of_birth}</li>"
            f"<li>Gender: {patient.sex}</li>"
            f"<li>Height, sm: {patient.height}</li>"
            f"<li>Weight, kg: {patient.weight}</li>"
            f"<li>Organ: {locale(study.body_area)}</li>"
            "</ul>"

            "<h3>Scan options:</h3>"
            "<ul>"
            f"<li>Scan time, s: {'<(????????)>'}</li>"
            f"<li>Mode: {locale(report.scan_type)}</li>"
            f"<li>Protocol kind: {locale(study.area_kind)}</li>"
            f"<li>Organ: Chest {locale(study.body_area)}</li>"
            f"<li>Patient position: {locale(report.patient_position)}</li>"
            f"<li>Pitch: {report.pitch}</li>"
            f"<li>Collimator width, mm: {report.collimator_width}</li>"
            f"<li>Table speed, mm/s: {report.table_speed}</li>"
            f"<li>Passed, mm: {'<(????????)>'}</li>"
            f"<li>kV: {report.kv}</li>"
            f"<li>Dose, mAs: {report.dose}</li>"
            f"<li>Beam current, mA: {report.ma}</li>"
            "</li>"
        )
        self._text_browser.setHtml(text)
