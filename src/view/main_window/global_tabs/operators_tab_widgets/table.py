from typing import TYPE_CHECKING, List, Optional

from PyQt5.QtWidgets import (QAbstractItemView, QHeaderView, QTableWidget,
                             QTableWidgetItem)
from src.db_manager import DataBaseManager
from src.model import Operator
from src.model_.data.enums import TableHeadersEnum
from src.model_.managers.locale_manager import LocaleManager, locale, multi_locale
from src.utils import const

if TYPE_CHECKING:
    from src.view.main_window.global_tabs.protocol_designer_widgets.tab_box import \
        Tab


class _Table(QTableWidget):
    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        self.verticalHeader().hide()
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.setSelectionMode(QTableWidget.SelectionMode.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.currentCellChanged.connect(lambda row: self.selectRow(row))
        self.setSortingEnabled(True)


class OperatorsTable(_Table):

    def __init__(self, parent: 'Tab'):
        super().__init__(parent)

        header_labels = ('Name', 'Access')
        self.setColumnCount(len(header_labels))
        self.setHorizontalHeaderLabels(header_labels)

        self._operators: Optional[List[Operator]] = None

        self.currentCellChanged.connect(self._on_current_cell_changed)
        DataBaseManager().operators_table_updated.connect(self.update_table)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

        self.update_table()

    def translate(self):
        self.update_table()
        self.setHorizontalHeaderLabels(
            multi_locale((TableHeadersEnum.name,
                          TableHeadersEnum.access)))

    def _on_current_cell_changed(self, row: int):
        pass

    def update_table(self):
        self._operators: List[Operator] = DataBaseManager().get_operators()

        self.setRowCount(len(self._operators))

        for row, operator in enumerate(self._operators):
            self.setItem(row, 0, QTableWidgetItem(operator.name))
            self.setItem(row, 1, QTableWidgetItem(locale(operator.access)))
            self.setRowHeight(row, const.TABLE_ROW_HEIGHT)

    def get_current_operator(self) -> Operator:
        """ Returns the `Operator` selected in the table

            Returns:
                `Operator` | `None`
        """
        try:
            return self._operators[self.currentRow()]
        except IndexError:
            return None
