from typing import Optional, Union

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QWidget
from src.db_manager import DataBaseManager
from src.model import Operator
from src.model_.data.enums import ButtonTitlesEnum, OperatorAccessEnum
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.elements.buttons.button import Button
from src.view.elements.forms.enum_combo_boxes import EnumComboBox
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit


class OperatorEditorDialog(BaseDialogWindow):
    """ Dialog window for editing `Operator` """

    def __init__(self, parent: QWidget):
        super().__init__(parent)

        self.form_dict = None

        self._operator: Optional[Operator] = None

        self._name = LineEdit(self, Operator.name)
        self._position = LineEdit(self, Operator.position)
        self._password = LineEdit(self, Operator.password_hash)
        self._access = EnumComboBox(self, Operator.access, OperatorAccessEnum)

        self._save_btn = Button(self, ButtonTitlesEnum.save)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self._password.setEchoMode(LineEdit.EchoMode.Password)

        self._add_form(self._name)
        self._add_form(self._position)
        self._add_form(self._password)
        self._add_form(self._access)

        hbox = QHBoxLayout()
        hbox.addWidget(self._save_btn)
        hbox.addWidget(self._close_btn)

        self.main_vbox.addSpacing(20)
        self.main_vbox.addLayout(hbox)

        self._close_btn.clicked.connect(self.hide)
        self._save_btn.clicked.connect(self._save)

    def _add_form(self, widget: Union[LineEdit, EnumComboBox]) -> None:
        """ Creates and adds `QHboxLayout`
            as `InfoLabel + Widget` to `self.main_vbox`.

            Args:
                widget (LineEdit | EnumComboBox): Any widget with key_name support.
        """

        label = InfoLabel(self, widget.get_key_name())
        label.setFixedWidth(150)
        label.setAlignment(Qt.AlignmentFlag.AlignVCenter |
                           Qt.AlignmentFlag.AlignRight)

        hbox = QHBoxLayout()
        hbox.addWidget(label)
        hbox.addWidget(widget)
        self.main_vbox.addLayout(hbox)

    def set_operator(self, operator: Operator, show_password: bool) -> None:
        """ Set operator data in dialog before show.

            Args:
                operator (Operator): The `Operator` instance.
                show_password (bool): If `True`, it shows the one-way password field
        """

        self._operator = operator

        self._name.set_value_from_model(operator.name)
        self._position.set_value_from_model(operator.position)
        self._access.set_value_from_model(operator.access)

        self._password.set_value_from_model(operator.password_hash)
        if show_password:
            self._password.setEnabled(True)
            self._access.setEnabled(True)
        else:
            self._password.setEnabled(False)
            self._access.setEnabled(False)

    def _save(self):
        self._operator.name = self._name.get_value()
        self._operator.position = self._position.get_value()
        self._operator.access = self._access.get_value()

        if self._password.isEnabled():
            self._operator.password_hash = self._password.get_value()

        DataBaseManager().save_operator(self._operator)
        self.hide()
