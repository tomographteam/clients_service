from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QFrame, QHBoxLayout, QPushButton
from src.model_.data.enums import ButtonTitlesEnum
from src.model_.managers.access_manager import AccessManager
from src.model_.managers.locale_manager import locale
from src.view.elements.buttons.button import Button
from src.view.main_window.global_ui.top_right_panel_widgets.dialog import \
    MainMenuDialog, OperatorSelectDialog

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


class TopRightWidget(QFrame):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self.setFixedSize(500, 50)

        self.menu_btn = Button(self, ButtonTitlesEnum.main_menu)
        self.operator_btn = QPushButton('READ-ONLY')
        self.dialog = MainMenuDialog(self)

        self.menu_btn.setObjectName('MainMenuBtn')
        self.operator_btn.setObjectName('OperatorBtn')

        main_hbox = QHBoxLayout(self)
        main_hbox.setAlignment(Qt.AlignmentFlag.AlignVCenter |
                               Qt.AlignmentFlag.AlignRight)
        main_hbox.setContentsMargins(0, 0, 0, 0)
        main_hbox.addWidget(self.operator_btn)
        main_hbox.addWidget(self.menu_btn)

        self.menu_btn.clicked.connect(
            lambda: self.dialog.move(
                self.menu_btn.mapToGlobal(self.menu_btn.pos())))
        self.menu_btn.clicked.connect(self.dialog.show)

        self.operator_btn.clicked.connect(self._show_operator_select_dialog)

    def _show_operator_select_dialog(self):
        dialog = OperatorSelectDialog(self)
        dialog.exec_()
        op = dialog.get_operator()
        if op is not None:
            self.operator_btn.setFont(QFont('', 10))
            self.operator_btn.setText(
                str(f'{op.name}\n{locale(op.position)}'))
