from typing import List
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from src.db_manager import DataBaseManager

from src.model_.managers.locale_manager import LocaleManager, locale
from src.model import Operator


class OperatorSelector(QListWidget):
    def __init__(self, parent: QWidget) -> None:
        super().__init__(parent)

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        self.clear()
        ops: List[Operator] = DataBaseManager().get_operators()
        for op in ops:
            item = QListWidgetItem(f'{op.name} [{locale(op.position)}]')
            item.setData(Qt.UserRole, op)
            self.addItem(item)
