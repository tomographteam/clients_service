from typing import Optional, Union
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QComboBox, QWidget, QHBoxLayout
from src.db_manager import DataBaseManager
from src.model import Operator
from src.model_.data.enums import (ButtonTitlesEnum, InfoLabelTitlesEnum, LanguagesEnum,
                                   OperatorAccessEnum)
from src.model_.managers.access_manager import AccessManager
from src.model_.managers.locale_manager import LocaleManager
from src.view.dialogs.base_dialog import BaseDialogWindow
from src.view.dialogs.calib.dialog import CalibDialog
from src.view.elements.buttons.button import Button
from src.view.elements.forms.labels import InfoLabel
from src.view.elements.forms.line_edits import LineEdit
from src.view.inner_windows.patient_table.inner_window import \
    PatientTableInnerWindow
from src.view.inner_windows.sensors.inner_window import SensorsInnerWindow
from src.view.main_window.global_ui.top_right_panel_widgets.operator_selector import OperatorSelector


class OperatorSelectDialog(BaseDialogWindow):
    def __init__(self, parent: QWidget):
        super().__init__(parent)
        self._title_bar.hide()
        self.form_dict = None
        self._operator: Optional[Operator] = None

        self.operator_selector = OperatorSelector(self)
        self.password = LineEdit(self, Operator.password_hash)

        self.select_btn = Button(self, ButtonTitlesEnum.select)
        self.cancel_btn = Button(self, ButtonTitlesEnum.cancel)

        label = InfoLabel(self, Operator.password_hash)
        label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.operator_selector.setMinimumSize(500, 400)

        self.main_vbox.addWidget(self.operator_selector)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(label)
        self.main_vbox.addWidget(self.password)

        hbox = QHBoxLayout()
        hbox.addWidget(self.select_btn)
        hbox.addWidget(self.cancel_btn)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addLayout(hbox)

        self.cancel_btn.clicked.connect(self._on_cancel_btn_clicked)
        self.select_btn.clicked.connect(self._on_select_btn_clicked)
        self.operator_selector.itemClicked.connect(self._on_item_selected)

    def _on_cancel_btn_clicked(self):
        self._operator = None
        self.close()

    def keyPressEvent(self, _) -> None:
        return

    def _on_item_selected(self, item):
        self._operator = item.data(Qt.UserRole)
        self.password.set_default_state()

    def _on_select_btn_clicked(self):
        if self._operator.password_hash != 0:
            if self.password.text() != str(self._operator.password_hash):
                self.password.set_error_state()
                return
        if self._operator:
            AccessManager().set_access(self._operator.access)
            self.close()

    def get_operator(self) -> Optional[Operator]:
        return self._operator


class MainMenuDialog(BaseDialogWindow):

    user_selected_signal = pyqtSignal(OperatorAccessEnum)

    def __init__(self, parent: QWidget):
        super().__init__(parent)

        # self.setFixedWidth(300)

        self._title_bar.hide()
        self.main_vbox.setAlignment(Qt.AlignmentFlag.AlignTop |
                                    Qt.AlignmentFlag.AlignHCenter)

        self._ru_btn = Button(self, ButtonTitlesEnum.ru)
        self._en_btn = Button(self, ButtonTitlesEnum.en)
        self._patient_table_btn = Button(self, ButtonTitlesEnum.patient_table)
        self._sensors_btn = Button(self, ButtonTitlesEnum.sensors)
        self._calib_btn = Button(self, ButtonTitlesEnum.calib)
        self._close_btn = Button(self, ButtonTitlesEnum.close)

        self.main_vbox.addWidget(self._patient_table_btn)
        self.main_vbox.addWidget(self._sensors_btn)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(self._calib_btn)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(self._en_btn)
        self.main_vbox.addWidget(self._ru_btn)
        self.main_vbox.addSpacing(20)
        self.main_vbox.addWidget(self._close_btn)

        self._connect_all()

    def _connect_all(self):
        self._ru_btn.clicked.connect(lambda: LocaleManager().lang_changed(LanguagesEnum.RU))
        self._en_btn.clicked.connect(lambda: LocaleManager().lang_changed(LanguagesEnum.EN))
        self._patient_table_btn.clicked.connect(self._show_patient_table_window)
        self._sensors_btn.clicked.connect(self._show_sensors_window)
        self._calib_btn.clicked.connect(self._show_calib_dialog)

        self._en_btn.clicked.connect(self.hide)
        self._ru_btn.clicked.connect(self.hide)
        self._close_btn.clicked.connect(self.hide)

    def _show_patient_table_window(self):
        self.hide()
        self._patient_table_window = PatientTableInnerWindow(self.parent())
        self._patient_table_window.show()

    def _show_sensors_window(self):
        self.hide()
        self._sensors_window = SensorsInnerWindow(self.parent())
        self._sensors_window.show()

    def _show_calib_dialog(self):
        self.hide()
        self._calib_dialog = CalibDialog(self.parent())
        self._calib_dialog.show()
