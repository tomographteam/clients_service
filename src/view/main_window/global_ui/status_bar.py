from datetime import datetime
from typing import TYPE_CHECKING

from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QFrame, QHBoxLayout, QLabel
from src.utils.singleton import singleton
from src.model_manager import ModelManager

if TYPE_CHECKING:
    from src.view.main_window.main_window import MainWindow


@singleton
class StatusBar(QFrame):
    def __init__(self, parent: 'MainWindow'):
        super().__init__(parent)

        self.setFixedHeight(50)

        self._main_hbox = QHBoxLayout(self)
        self._main_hbox.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self._cis_status = QLabel('CIS')
        self._tango_status = QLabel('TANGO')
        self._sensors_status = QLabel('SENSORS')

        self._tango_status.hide()
        self._sensors_status.hide()

        self._dt_label = QLabel(self)
        self._dt_label.setFont(QFont('', 20))

        self._timer = QTimer(self)
        self._timer.setInterval(500)
        self._timer.timeout.connect(
            lambda: self._dt_label.setText(
                datetime.now().strftime('%d-%m-%Y %H:%M:%S')))
        self._timer.start()

        self._main_hbox.addWidget(self._cis_status)
        self._main_hbox.addWidget(self._tango_status)
        self._main_hbox.addWidget(self._sensors_status)
        self._main_hbox.addStretch()
        self._main_hbox.addWidget(self._dt_label)

        self.update_cis_status(False)
        self.update_tango_status(False)
        self.update_sensors_status(False)

        ModelManager().cis_status_updated.connect(self.update_cis_status)

    def update_cis_status(self, status):
        if status:
            self._cis_status.setProperty('state', 'connected')
            self._cis_status.style().unpolish(self._cis_status)
            self._cis_status.style().polish(self._cis_status)
            self._cis_status.update()
        else:
            self._cis_status.setProperty('state', 'disconnected')
            self._cis_status.style().unpolish(self._cis_status)
            self._cis_status.style().polish(self._cis_status)
            self._cis_status.update()

    def update_tango_status(self, status):
        if status:
            self._tango_status.setProperty('state', 'connected')
            self._tango_status.style().unpolish(self._tango_status)
            self._tango_status.style().polish(self._tango_status)
            self._tango_status.update()
        else:
            self._tango_status.setProperty('state', 'disconnected')
            self._tango_status.style().unpolish(self._tango_status)
            self._tango_status.style().polish(self._tango_status)
            self._tango_status.update()

    def update_sensors_status(self, status):
        if status:
            self._sensors_status.setProperty('state', 'connected')
            self._sensors_status.style().unpolish(self._sensors_status)
            self._sensors_status.style().polish(self._sensors_status)
            self._sensors_status.update()
        else:
            self._sensors_status.setProperty('state', 'disconnected')
            self._sensors_status.style().unpolish(self._sensors_status)
            self._sensors_status.style().polish(self._sensors_status)
            self._sensors_status.update()
