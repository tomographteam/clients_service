from enum import Enum
from typing import Union

from PyQt5.QtWidgets import QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model_manager import ModelManager
from src.view.elements.buttons.button import Button


class ControlButton(Button):
    def __init__(self, parent: QWidget, key_name: Union[InstrumentedAttribute, Enum]):
        super().__init__(parent, key_name)
        self.clicked.connect(self._send_command)

    def _send_command(self):
        ModelManager().send_command_to_server(self._key_name)

    def from_model(self, model: ...):
        pass
