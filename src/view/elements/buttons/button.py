from enum import Enum
from typing import Union

from PyQt5.QtWidgets import QPushButton, QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model_.managers.locale_manager import locale, LocaleManager


class Button(QPushButton):
    def __init__(self, parent: QWidget, key_name: Union[InstrumentedAttribute, Enum]):
        super().__init__(parent)

        self._key_name = key_name

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        self.setText(locale(self._key_name))

    def get_key_name(self) -> Union[InstrumentedAttribute, Enum]:
        return self._key_name
