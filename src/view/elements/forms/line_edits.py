from typing import Any, Union

from PyQt5.QtWidgets import QLineEdit, QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model_manager import ModelManager
from src.utils.funcs import is_float


class LineEdit(QLineEdit):
    def __init__(self, parent: QWidget, key_name: InstrumentedAttribute) -> None:
        super().__init__(parent)

        self._key_name = key_name
        self._sync_available = True

        if parent.form_dict is not None:
            parent.form_dict.update({key_name: self})

        if hasattr(self.parent().parent().parent(), "validate"):
            self.editingFinished.connect(self._commit)
            self.returnPressed.connect(self._commit)

    def _commit(self):
        if self._sync_available and hasattr(self.parent().parent().parent().parent().parent().parent(), "validate"):
            self.parent().parent().parent().parent().parent().parent().validate()

    def get_key_name(self) -> InstrumentedAttribute:
        return self._key_name

    def get_value(self) -> str:
        text = self.text()
        if is_float(text):
            return float(self.text())
        if text.isdecimal():
            return int(self.text())
        return self.text()

    def set_value_from_model(self, value: Union[str, Any]):
        self._sync_available = False
        self.setText(str(value))
        self._sync_available = True

    def set_error_state(self):
        self.setProperty('state', 'error')
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()

    def set_default_state(self):
        self.setProperty('state', 'default')
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()


class LineEditSync(LineEdit):
    def __init__(self, parent: QWidget, key_name: InstrumentedAttribute):
        super(LineEditSync, self).__init__(parent, key_name)
        self.editingFinished.connect(self._commit)
        self.returnPressed.connect(self._commit)

    def _commit(self):
        if self._sync_available:
            ModelManager().send_form_data(self._key_name.key, self.text())
