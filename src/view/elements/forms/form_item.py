from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QHBoxLayout, QWidget
from src.view.elements.forms.labels import ValueLabel


class ProtocolOptFormItem(QHBoxLayout):
    def __init__(self, label: ValueLabel, widget: QWidget):
        super().__init__()
        label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

        label.setFixedWidth(200)
        label.setWordWrap(True)
        self.addWidget(label)
        self.addWidget(widget)
