from enum import Enum
from typing import Union

from PyQt5.QtWidgets import QDoubleSpinBox, QWidget
from sqlalchemy.orm import InstrumentedAttribute


class FloatSpinBox(QDoubleSpinBox):
    def __init__(self, parent: QWidget,
                 key_name: Union[InstrumentedAttribute, Enum]) -> None:
        super().__init__(parent)

        self._key_name = key_name

        if parent.form_dict:
            parent.form_dict.update({key_name: self})

    def get_key_name(self) -> Union[InstrumentedAttribute, Enum]:
        return self._key_name

    def set_value_form_model(self, value: float):
        self.setValue(value)

    def get_value(self) -> float:
        return self.value()
