from enum import Enum
from typing import Callable, Union

from PyQt5.QtWidgets import QComboBox, QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model_.managers.locale_manager import LocaleManager
from src.model_.managers.resource_manager import (IResourceManager,
                                                  ResourceManager)
from src.model_manager import ModelManager

resource_manager: IResourceManager = ResourceManager()


class JsonComboBox(QComboBox):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, lines: list,
                 start_index=0, convert: Callable = str) -> None:
        super().__init__(parent)

        self._lines = lines
        self._key_name = key_name
        self._start_index = start_index
        self._convert = convert
        self.setEditable(True)

        self._current_index = 0

        self._sync_available = True

        if parent.form_dict is not None:
            parent.form_dict.update({key_name: self})

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

        if hasattr(self.parent().parent().parent(), "validate"):
            self.currentTextChanged.connect(self._commit)

    def _commit(self, text: str):  # TODO здесь какая-то херня
        if self._sync_available and hasattr(self.parent().parent().parent().parent().parent().parent(), "validate"):
            self.parent().parent().parent().parent().parent().parent().validate()

    def translate(self):
        self.clear()
        self.addItems((str(self._convert(k)) for k in self._lines[self._start_index:]))

    def get_key_name(self) -> str:
        return self._key_name

    def get_enum(self) -> Enum:
        return self._enum

    def get_value(self) -> str:
        return self.currentText()

    def set_value_from_model(self, value: Union[int, str, float]):
        self._sync_available = False
        self.setCurrentText(str(value))
        self._sync_available = True

    def set_error_state(self):
        self.setProperty('state', 'error')
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()

    def set_default_state(self):
        self.setProperty('state', 'default')
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()


class JsonComboBoxSync(JsonComboBox):
    def __init__(self, parent: QWidget, key_name: InstrumentedAttribute,
                 lines: list, start_index=0, convert: Callable = str) -> None:
        super().__init__(parent, key_name, lines, start_index, convert)

        self.currentTextChanged.connect(self._commit)

    def _commit(self, text: str):
        if self._sync_available:
            ModelManager().send_form_data(self._key_name.key, self.currentIndex())


class JsonPitchComboBox(JsonComboBox):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_pitch_list(),
                         start_index, convert=float)


class JsonKvComboBox(JsonComboBox):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_kv_list(),
                         start_index, convert=float)


class JsonMaComboBox(JsonComboBox):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_ma_list(),
                         start_index, convert=float)


class JsonPitchComboBoxSync(JsonComboBoxSync):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_pitch_list(),
                         start_index, convert=float)


class JsonKvComboBoxSync(JsonComboBoxSync):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_kv_list(),
                         start_index, convert=float)


class JsonMaComboBoxSync(JsonComboBoxSync):
    def __init__(self, parent: QWidget,
                 key_name: InstrumentedAttribute, start_index=0) -> None:
        super().__init__(parent, key_name, resource_manager.get_ma_list(),
                         start_index, convert=float)
