from enum import Enum
from typing import Union

from PyQt5.QtWidgets import QComboBox, QWidget
from sqlalchemy.orm import InstrumentedAttribute
from src.model import BoolEnum
from src.model_.managers.locale_manager import LocaleManager, locale
from src.model_manager import ModelManager


class EnumComboBox(QComboBox):

    def __init__(self, parent: QWidget, key_name: InstrumentedAttribute, enum_: Enum) -> None:
        super().__init__(parent)

        self._enum = enum_
        self._current_index = 0
        self._key_name = key_name

        self._sync_available = True

        if parent.form_dict is not None:
            parent.form_dict.update({key_name: self})

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        if self._enum:
            self.blockSignals(True)
            self.clear()
            self.addItems((locale(x) for x in self._enum))
            self.blockSignals(False)

    def get_key_name(self) -> str:
        return self._key_name

    def get_enum(self) -> Enum:
        return self._enum

    def get_value(self) -> Enum:
        if self._enum is BoolEnum:
            return True if self._enum(self.currentIndex()) is BoolEnum.enable else False
        return self._enum(self.currentIndex())

    def set_value_from_model(self, value: Union[Enum, int, InterruptedError]):
        self._sync_available = False
        if isinstance(value, InterruptedError):
            self.setCurrentIndex(value.value)
        elif isinstance(value, float):
            self.setCurrentText(str(value))
        elif isinstance(value, Enum):
            self.setCurrentIndex(value.value)
        elif isinstance(value, bool):
            self.setCurrentIndex(BoolEnum.enable.value
                                 if value
                                 else BoolEnum.disable.value)
        self._sync_available = True

    def set_error_state(self):
        self.setProperty('state', 'error')

    def set_default_state(self):
        self.setProperty('state', 'default')


class EnumComboBoxSync(EnumComboBox):
    def __init__(self, parent: QWidget, key_name: InstrumentedAttribute, enum_: Enum) -> None:
        super().__init__(parent, key_name, enum_)

        self.currentTextChanged.connect(self._commit)

    def _commit(self):
        if self._sync_available:
            if self._enum:
                value = self._enum(self.currentIndex()).name
            else:
                value = self.currentIndex()
            ModelManager().send_form_data(self._key_name.key, value)
