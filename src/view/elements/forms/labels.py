import enum
from typing import Any, Union

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QLabel, QWidget
from src.model_.managers.locale_manager import LocaleManager, locale


class ValueLabel(QLabel):

    def __init__(self, parent: QWidget, key_name: str) -> None:
        super().__init__(parent)

        self._key_name = key_name

        if parent.form_dict is not None:
            parent.form_dict.update({key_name: self})

    def set_value_from_model(self, value: Union[str, Any]):
        self.setText(str(value) if not isinstance(value, str) else value)

    def get_key_name(self) -> enum.Enum:
        return self._key_name

    def get_value(self) -> str:
        return self.text()


class TitleLabel(QLabel):
    def __init__(self, parent: QWidget, text: str):
        super().__init__(parent)

        self.setFont(QFont('', 20))
        self.setAlignment(Qt.AlignCenter)
        self.setText(text)

        self.setMaximumHeight(50)


class InfoLabel(QLabel):
    def __init__(self, parent: QWidget, key_name: enum.Enum = None):
        super().__init__(parent)
        self._key_name = key_name

        LocaleManager().need_translate.connect(self.translate)
        self.translate()

    def translate(self):
        self.setText(locale(self._key_name))

    def get_key_name(self) -> enum.Enum:
        return self._key_name
