import json

from src.controller.controller import TomographController, DBController

import os
import asyncio
from aiohttp import web


tc = TomographController()
db = DBController()
loop = asyncio.get_event_loop()
lock = asyncio.Lock(loop=loop)


async def model_stat(request):
    timestamp = request.match_info['timestamp']
    async with lock:
        if float(timestamp) < tc.study.timestamp:
            return web.json_response(tc.dump())
        else:
            return web.json_response({"status": "OK"})


async def model_stat_data(request):
    attrib = request.match_info['attrib']
    value = request.match_info['value']
    async with lock:
        if tc.set_step_attrib(attrib, value):
            return web.Response(text='OK')
        return web.Response(text='Error')


async def model_control(request):
    params = await request.json()
    command = request.match_info['command']
    async with lock:
        if await tc.do_command(command, **params):
            return web.Response(text='OK')
        return web.Response(text='Error')


async def db_get(request):
    params = await request.json()
    command = request.match_info['command']
    async with lock:
        resp = web.json_response(await db.do_command(command, **params))
        return resp

def run():
    app = web.Application()
    app.add_routes([web.get('/tom_stat/{timestamp}', model_stat),
                    web.post('/tom_stat/{attrib}/{value}', model_stat_data),
                    web.post('/tom_control/{command}', model_control),
                    web.get('/db/{command}', db_get),
                    web.post('/db/{command}', db_get),
                    web.delete('/db/{command}', db_get)
                    ])

    async def run_other_task(_app):
        task = asyncio.create_task(tc.server_events_loop())

        yield

        task.cancel()
        # with suppress(asyncio.CancelledError):
        #     await task  # Ensure any exceptions etc. are raised.

    app.cleanup_ctx.append(run_other_task)
    web.run_app(app, loop=loop, port=os.environ.get('CIS_PORT', 8082))
