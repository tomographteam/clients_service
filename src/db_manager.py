import asyncio
import os
from datetime import datetime

import requests
from aiohttp import ClientConnectorError
from typing import Any, Callable, List, Tuple, Union

from PyQt5.QtCore import QObject, pyqtSignal
import aiohttp

from src.model import (HWTest, Operator, Patient, Protocol,
                       ProtocolStep, Study, StudyStep)
from src.model_.data.enums import ErrorsEnum
from src.utils.singleton import singleton



@singleton
class DataBaseManager(QObject):
    protocol_table_updated = pyqtSignal()
    protocol_step_table_updated = pyqtSignal()
    dose_logs_table_updated = pyqtSignal()
    reports_table_updated = pyqtSignal()
    operators_table_updated = pyqtSignal()
    patients_table_updated = pyqtSignal()
    calib_table_updated = pyqtSignal()

    host=os.environ.get('CIS_IP', "localhost")
    port=os.environ.get('CIS_PORT', 8082)

    def _get(self, cmd: str, **params) -> dict:
        try:
            return requests.get(f'http://{self.host}:{self.port}/db/{cmd}', json=params, timeout=1).json()
        except (requests.ConnectionError, requests.ReadTimeout, requests.JSONDecodeError) as e:
            # raise e
            return None

    def _post(self, cmd: str, **params):
        try:
            return requests.post(f'http://{self.host}:{self.port}/db/{cmd}', json=params).json()
        except Exception as e:
            # raise e
            return None

    def _delete(self, cmd: str, **params):
        try:
            return requests.delete(f'http://{self.host}:{self.port}/db/{cmd}', json=params).status_code
        except Exception as e:
            # raise e
            return None

    def _process(self, req_func: Callable, method: Callable, **req_params):
        """ Sends http-request to CIS to work with the database, in case of an error,
        displays a dialog until the data is received.

        Args:
            * req_func (`self._get` | `self._delete` | `self._post`): Request type
            * method (`self.api_method`): The function whose name will be used as the API method
            * req_params (`**kwargs`): Request parameters

        Example:
            >>> self._process(self._get, self.get_protocols)
            >>> self._process(self._get, self.get_protocol_by_id, protocol_id==protocol_id)
        """
        from src.view.dialogs.error.db_dialog import DataBaseErrorDialog
        while (resp := req_func(method.__name__, **req_params)) is None:
            dialog = DataBaseErrorDialog(None)
            dialog.set_error(ErrorsEnum[method.__name__])
            dialog.exec_()
        return resp

    def get_studies_with_patients(self) -> List:
        return [[Study.from_dict(s), Patient.from_dict(p)]
                for s, p in self._process(self._get, self.get_studies_with_patients)]

    def get_study_protocols(self) -> List[Study]:
        return [Study.from_dict(p) for p in
                self._process(self._get, self.get_study_protocols)]

    def get_protocols(self) -> List[Protocol]:
        return [Protocol.from_dict(p) for p in
                self._process(self._get, self.get_protocols)]

    def get_protocol_steps(self, protocol_id: int) -> List[ProtocolStep]:
        return [ProtocolStep.from_dict(p) for p in
                self._process(self._get, self.get_protocol_steps, protocol_id=protocol_id)]

    def get_study_steps(self, study: Study) -> List[StudyStep]:
        return [StudyStep.from_dict(p)for p in
                self._process(self._get, self.get_study_steps, study_id=study.id)]

    def save_protocol(self, protocol: Protocol):
        result = self._process(self._post, self.save_protocol, protocol=protocol.to_dict())
        self.protocol_table_updated.emit()
        return result

    def save_protocol_step(self, protocol_step: ProtocolStep):
        result = self._process(self._post, self.save_protocol_step, step=protocol_step.to_dict())
        self.protocol_step_table_updated.emit()
        return result

    def delete_protocol(self, protocol_id: int):
        self._process(self._delete, self.delete_protocol, protocol_id=protocol_id)
        self.protocol_table_updated.emit()

    def delete_protocol_step(self, protocol_step_id: int):
        self._process(self._delete, self.delete_protocol_step, step_id=protocol_step_id)
        self.protocol_step_table_updated.emit()

    def get_all_study_steps(self) -> List[StudyStep]:
        return [StudyStep.from_dict(p) for p in
                self._process(self._get, self.get_all_study_steps)]

    def get_study_by_id(self, study_id: int) -> Study:
        return Study.from_dict(self._process(self._get, self.get_study_by_id, study_id=study_id))

    def get_operators(self) -> List[Operator]:
        return [Operator.from_dict(p) for p in
                self._process(self._get, self.get_operators)]

    def get_operator_by_id(self, id: int) -> Operator:
        return Study.from_dict(self._process(self._get, self.get_operator_by_id, operator_id=id))

    def save_operator(self, operator: Operator):
        self._process(self._post, self.save_operator, operator=operator.to_dict())
        self.operators_table_updated.emit()

    def delete_operator(self, operator: Operator):
        self._process(self._delete, self.delete_operator, operator_id=operator.id)
        self.operators_table_updated.emit()

    def get_patients(self) -> List[Patient]:
        return [Patient.from_dict(p) for p in self._process(self._get, self.get_patients)]

    def get_patient_by_id(self, patient_id: int) -> Patient:
        return Patient.from_dict(self._process(self._get, self.get_patient_by_id, patient_id=patient_id))

    def save_patient(self, patient: Patient):
        self._process(self._post, self.save_patient, patient=patient.to_dict())
        self.patients_table_updated.emit()

    def get_hw_tests(self) -> List[HWTest]:
        return [HWTest.from_dict(p) for p in self._process(self._get, self.get_hw_tests)]

    def get_calib_reports(self) -> List[StudyStep]:
        return []  # [CalibReport.from_dict(p) for p in self._process(self._get, self.get_calib_reports)]

    def fill_out_calib_table(self, kv_ma_list: List[Tuple[int, int]]):
        self._post(self.fill_out_calib_table.__name__, kv_ma_list=kv_ma_list)
        # self.calib_table_updated.emit()

    def clear_out_calib_table(self):
        self._delete(self.clear_out_calib_table.__name__)
        # self.calib_table_updated.emit()
