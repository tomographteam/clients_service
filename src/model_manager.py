import asyncio
import os

import aiohttp
from PyQt5.QtCore import QObject, pyqtSignal
from src.utils.singleton import singleton


@singleton
class ModelManager(QObject):
    model_updates_signal = pyqtSignal(dict)
    cis_status_updated = pyqtSignal(bool)

    host=os.environ.get('CIS_IP', "localhost")
    port=os.environ.get('CIS_PORT', 8082)

    def __init__(self):
        super().__init__()
        self._connected = True

    def is_connected(self) -> bool:
        return self._connected

    def set_connected(self, value: bool):
        self._connected = value

    async def state_update(self):
        timestamp = 0
        while True:
            async with aiohttp.ClientSession() as session:
                try:
                    async with session.get(f'http://{self.host}:{self.port}/tom_stat/{timestamp}') as resp:
                        data = await resp.json()

                        if data != {"status": "OK"}:
                            self.model_updates_signal.emit(data)
                            timestamp = data["timestamp"]

                    self.cis_status_updated.emit(True)
                    self._connected = True

                except aiohttp.ClientConnectorError as e:
                    self.cis_status_updated.emit(False)

            await asyncio.sleep(0.1)

    def send_form_data(self, attrib, value):

        async def do_request():
            async with aiohttp.ClientSession() as session:
                async with session.post(f'http://{self.host}:{self.port}/tom_stat/{attrib}/{value}') as resp:
                    print(f'SEND_STATE: {attrib}/{value}')
                    pass

        asyncio.ensure_future(do_request())

    def send_command_to_server(self, command, **parameters):
        async def do_request():
            async with aiohttp.ClientSession() as session:
                async with session.post(f'http://{self.host}:{self.port}/tom_control/{command.name}',
                                        json=parameters) as resp:
                    print(f'SEND_CMD: {command} {parameters}')

        asyncio.ensure_future(do_request())


if __name__ == '__main__':
    ModelManager.get_study_recon_data()
