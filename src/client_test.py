import json

import aiohttp
import asyncio

import zmq
from zmq.asyncio import Context


async def send():

    await asyncio.sleep(5)

    url = 'http://127.0.0.1:10001/tango/rest/v11/commands'
    _auth = aiohttp.BasicAuth('tango-cs', 'tango')

    payload = {'scan_id': {'study_id': '2b25c82f579e404e88e7422ca829bc7d', 'series_number': None},
               'scan': {'SeriesType': 1, 'table_motion': 'moving', 'number_of_volume_images': 0,
                        'fluoro_scan_number_of_views': 0,
                        'TableStartPos': 0, 'TableStopPos': 500, 'TableSpeed': 24.0, 'table_height': 600, 'Kv': 100,
                        'Ma': 765,
                        'CollWidth': 1.2, 'Pitch': 0.5},
               'recon': {'ScoutWinWidth': 513, 'ScoutWinLength': 514, 'ReconKernelIndex': 0, 'ReconNoiseReduced': 1,
                         'ReconMetalCorr': 1, 'patient position': 'FFDR', 'method': 'FBP + 5 SART',
                         'das_mode': 'DAS1+2'},
               'trigger': {'TriggerW': 0, 'TriggerX': 0, 'TriggerY': 0, 'TriggerScanPeriod': 0,
                           'TriggerScanMaxCount': 20,
                           'TriggerDensityThreshold': 3}}

    url_payload = {'host': '127.0.0.1:10000', 'device': 'test/dsebct/1', 'name': 'PrepareScanWithParams'}
    url_payload['input'] = json.dumps(payload)
    url_payload = [url_payload, ]

    async with aiohttp.ClientSession() as session:
        print("send")
        async with session.put(url,
                               auth=_auth,
                               json=url_payload,
                               ) as response:
            if response.status == 200:
                try:
                    result_str = await response.read()
                    if result_str:
                        result = json.loads(result_str)
                        print(result)
                    else:
                        print(None)
                except Exception as e:
                   print(f'scsc client error={e} result_str={result_str}')
            else:
                response_data = await response.read()
                print(
                    f'Tango error status={response.status} url={url} payload={payload} response={response_data}')


async def send2():

    await asyncio.sleep(10)

    url = 'http://127.0.0.1:10001/tango/rest/v11/commands'
    _auth = aiohttp.BasicAuth('tango-cs', 'tango')


    url_payload = {'host': '127.0.0.1:10000', 'device': 'test/dsebct/1', 'name': 'StartScan'}
    url_payload = [url_payload, ]

    async with aiohttp.ClientSession() as session:
        print("send")
        async with session.put(url,
                               auth=_auth,
                               json=url_payload,
                               ) as response:
            if response.status == 200:
                try:
                    result_str = await response.read()
                    if result_str:
                        result = json.loads(result_str)
                        print(result)
                    else:
                        print(None)
                except Exception as e:
                   print(f'scsc client error={e} result_str={result_str}')
            else:
                response_data = await response.read()
                print(
                    f'Tango error status={response.status} url={url} payload={payload} response={response_data}')



async def read_zmq_event_stream():

    _zmq_ctx = Context.instance()
    url = 'tcp://127.0.0.1:5563'
    print(f'zmq url={url}')
    try:
        sub = _zmq_ctx.socket(zmq.SUB)
        print(f'zmq sub = {sub}')
        sub.connect(url)
    except Exception as e:
        print(f'ZMQError: {e} url={url}')
        return
    print(f'ZMQ connect ok:  url={url}')
    sub.setsockopt(zmq.SUBSCRIBE, b'DSEBCT')
    loop = asyncio.get_event_loop()
    while True:
        try:
            topic, msg_bytes = await sub.recv_multipart()
            event_str = msg_bytes.decode('utf-8')
            if event_str:
                print(json.loads(event_str))
        except:
            pass

loop = asyncio.get_event_loop()
task = loop.create_task(send())
task = loop.create_task(send2())
task = loop.create_task(read_zmq_event_stream())

loop.run_forever()