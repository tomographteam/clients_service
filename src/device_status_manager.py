import sys

import aiohttp
import asyncio
import os

from PyQt5.QtCore import QObject, pyqtSignal

from src.utils.singleton import singleton
from src.view.main_window.global_ui.status_bar import StatusBar


@singleton
class DeviceStatusManager(QObject):

    tree_updated = pyqtSignal(dict)
    data_updated = pyqtSignal(dict)

    host = os.environ.get('SCSC_IP', "127.0.0.1")
    port = int(os.environ.get('SCSC_PORT', 10001))

    #     url_template = f'http://{tango_rest_server}:{tango_rest_port}/tango/rest/v11/hosts/{tango_server}/devices/'
    url_template = f'http://{host}:{port}/tango/rest/v11/hosts/{host}'

    user = "tango-cs"
    password = "tango"

    auth = aiohttp.BasicAuth(user, password)

    devices = [
        "test/dsebct/1",
        "test/scanner/1",
        "test/pes/1",
        "test/ptable/1",
        #"test/hvps/nwl",
        # "hvps/ct/ct",
        # "PS_NWL| HVPS_CT",
        # "PS_NWL",
        # "test/bce/1",
        # "test/wwires/1",
        # "test/chiller/1",
        # "test/a4uhv/1",
        # "test/negpower/1",
        # "test/negmini/1",
        # "test/negmini/2",
        # "test/das/1",
        # "test/das/2",
        # "test/recon/1",
        # "test/ECG/1",
        # "test/electrodes_m/1",
        # "test/collimator/1",
        # "test/acm4826/left",
        # "test/acm4826/middle",
        # "test/acm4826/right",
    ]

    def __init__(self):

        super().__init__()

        self.attrib_tree = {}
        self.attrib_data = {}

        self.tree_callback = None
        self.data_callback = None

        loop = asyncio.get_event_loop()
        loop.create_task(self._refresh())

    def set_callbacks(self, tree_callback=None, data_callback=None):
        if tree_callback:
            self.tree_updated.connect(tree_callback)
        if data_callback:
            self.data_updated.connect(data_callback)

    async def _get_tree(self):
        self.attrib_tree = {}
        self.attrib_data = {}

        async with aiohttp.ClientSession() as session:
            # async with session.get(self.url_template.format(request='devices')) as resp:
            #     devices = await resp.json()

            for device in self.devices:
                async with session.get(self.url_template + f"/devices/{device}/attributes",
                                       auth=self.auth
                                       ) as resp:
                    resp = await resp.json()
                    data = [d["name"]  for d in resp]
                    self.attrib_tree[device] = data

            self.tree_updated.emit(self.attrib_tree)
            StatusBar().update_tango_status(True)

    async def _refresh(self):

        while True:

            try:
                await self._get_tree()

                while True:
                    async with aiohttp.ClientSession() as session:

                        for device, attribute in self.attrib_tree.items():
                            attributes_query = "&".join([f"attr={a}" for a in attribute])
                            url = self.url_template + f"/devices/{device}/attributes/value?{attributes_query}"
                            async with session.get(url,
                                    auth=self.auth
                            ) as resp:
                                try:
                                    r = await resp.json()
                                    self.attrib_data[device] = {p["name"]:p for p in r}
                                except Exception as e:
                                    r = await resp.text()
                                    print(e)
                                    print(device,  url)
                                    self.attrib_data[device] = {}

                        self.data_updated.emit(self.attrib_data)
                        await asyncio.sleep(1)

            except Exception as e:
                print("DeviceStatusManager", e)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                print(self.attrib_tree)
                print()
                print(self.attrib_data)
                print(r.text())

                StatusBar().update_tango_status(False)
                await asyncio.sleep(3)

    def set_param(self):
        pass
