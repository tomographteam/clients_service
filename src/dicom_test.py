import pydicom
import pandas as pd
import matplotlib.pyplot as plt

data_set = pydicom.dcmread("../resources/DICOM/1.2.826.0.1.3680043.8.498.12761213340114088162458482324012885000.dcm")
for im in data_set.pixel_array:
    plt.imshow(im, cmap=plt.cm.bone)
    plt.pause(0.5)
dataset = [[str(k), v.name, v.keyword, v.value] for k, v in dict(data_set).items()]
dataset[-1][-1] = ""

df = pd.DataFrame(dataset, dtype=str, columns=["tag", "name", "keyword", "value example"])
df["SCSC"] = ""
df["Client"] = ""
df.to_csv("DCOM_fields.csv")