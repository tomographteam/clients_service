from datetime import datetime

from src.model_.data.enums import *

from sqlalchemy.sql import func
from typing import List

from sqlalchemy import (Boolean, Column, Enum, Float, ForeignKey, Integer,
                        String, inspect, DateTime, TIMESTAMP)
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy_utils import force_instant_defaults

from src.utils.sievert import sievert_age_factor, sievert_factor

Base = declarative_base()
force_instant_defaults()


class AbstractSequence(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True)

    body_area = Column(Enum(BodyAreaEnum), default=BodyAreaEnum.chest)
    area_kind = Column(Enum(AreaKindEnum), default=AreaKindEnum.adult)

    name = Column(String, default='')
    icon = Column(String, default='')
    zero_line_position = 100
    description = Column(String, default='')
    description_img = Column(String, default='')
    example_protocol = Column(Boolean, default=False)
    author = Column(String, default='')
    sievert_factor = Column(Float, default=0.002)


class Protocol(AbstractSequence):
    __tablename__ = 'protocol_table'

    steps: List['ProtocolStep'] = relationship("ProtocolStep", order_by="asc(ProtocolStep.order)",
                                               cascade="all, delete, delete-orphan",
                                               lazy="joined",
                                               backref="protocol")


    @staticmethod
    def from_dict(d):
        steps = d.pop("steps")
        d = {k: v for k, v in d.items() if k in inspect(Protocol).attrs.keys()}
        s = Protocol(**d)
        s.body_area = BodyAreaEnum[s.body_area['name']]
        s.area_kind = AreaKindEnum[s.area_kind['name']]

        return s

    def to_dict(self, steps=False):
        d = {k: getattr(self, k) for k in inspect(Protocol).attrs.keys() if k is not Study.patient.key}
        d["body_area"] = {'name': d["body_area"].name}
        d["area_kind"] = {'name': d["area_kind"].name}

        d["steps"] = [p.to_dict() for p in d["steps"]] if steps else []

        return d

    @staticmethod
    def from_examples_dict(d):
        steps = d.pop("steps")
        d = {k: v for k, v in d.items() if k in inspect(Protocol).attrs.keys()}
        s = Protocol(**d)
        s.body_area = BodyAreaEnum[s.body_area['name']]
        s.area_kind = AreaKindEnum[s.area_kind['name']]

        for i, step in enumerate(steps):
            ps = ProtocolStep.from_examples_dict(step)
            ps.order = i
            s.steps.append(ps)

        return s


class Study(AbstractSequence):
    __tablename__ = 'study_table'

    steps: List['StudyStep'] = relationship("StudyStep", order_by="asc(StudyStep.order)",
                                            cascade="all, delete, delete-orphan",
                                            lazy="joined",
                                            backref="study")

    patient_id = Column(Integer, ForeignKey('patient_table.id'), nullable=False)
    current_step = Column(Integer, default=0)
    timestamp = Column(Float, default=0)
    protocol_name = Column(String, default="")

    def from_protocol(self, p: Protocol):
        """
        copy all except id
        :param p: protocol
        """
        for k, col in inspect(Protocol).attrs.items():
            if col not in [Protocol.id.property, Protocol.steps.property]:
                setattr(self, col.key, getattr(p, col.key))

        return self

    def to_dict(self):
        d = {k: getattr(self, k) for k in inspect(Study).attrs.keys() if k is not Study.patient.key}
        d["body_area"] = {'name': d["body_area"].name}
        d["area_kind"] = {'name': d["area_kind"].name}

        d["steps"] = [p.to_dict() for p in d["steps"]]

        return d

    @staticmethod
    def from_dict(d):
        steps = d.pop("steps")
        d = {k: v for k, v in d.items() if k in inspect(Study).attrs.keys()}
        s = Study(**d)
        s.body_area = BodyAreaEnum[s.body_area['name']]
        s.area_kind = AreaKindEnum[s.area_kind['name']]

        for step in steps:
            s.steps.append(StudyStep.from_dict(step))

        return s


class AbstractStep(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    auto_calculated = Column(Enum(AutoCalculatedEnum),
                             default=AutoCalculatedEnum.table_speed)
    patient_position = Column(
        Enum(PatientPositionEnum),
        default=PatientPositionEnum.head_to_gentry_on_back_arms_to_legs)
    scan_type = Column(Enum(ScanTypeEnum), default=ScanTypeEnum.scout_two_side)
    table_motion = Column(Enum(TableMotionEnum),
                          default=TableMotionEnum.moving)

    collimator_width = Column(Float, default=0.03)
    collimator_width_max = Column(Float, default=1.67)
    collimator_width_min = Column(Float, default=0.03)
    dose_max = Column(Float, default=3)
    end_voice_cmd_uid = Column(String, default='')
    fluoro_scan_number_of_views = Column(Integer, default=64)
    is_auto_start = Column(Boolean, default=False)
    is_cardio_start = Column(Boolean, default=False)
    kv = Column(Float, default=0)
    kv_max = Column(Float, default=140)
    kv_min = Column(Float, default=0)
    ma = Column(Float, default=0)
    ma_max = Column(Float, default=1485)
    ma_min = Column(Float, default=0)
    name = Column(String, default='')
    number = Column(Integer, default=-1)
    number_of_volume_images = Column(Integer, default=0)
    pitch = Column(Float, default=0.5)
    pitch_max = Column(Float, default=1.5)
    pitch_min = Column(Float, default=0.5)

    # sequence_name = Column(Float, default=0.03), # cyclic

    start_delay = Column(Float, default=0)
    start_position = Column(Float, default=0)
    start_position_max = Column(Float, default=300)
    start_position_min = Column(Float, default=-200)
    start_voice_cmd_uid = Column(String, default='')
    stop_position = Column(Float, default=100)
    stop_position_max = Column(Float, default=200)
    stop_position_min = Column(Float, default=-200)
    table_height = Column(Float, default=500)
    table_height_max = Column(Float, default=1000)
    table_height_min = Column(Float, default=500)
    table_speed = Column(Float, default=2.4)
    table_speed_max = Column(Float, default=100)
    table_speed_min = Column(Float, default=0)

    point_zero = Column(Float, default=0)

    # Reconstruction
    reconstruction_width = Column(Float, default=0)
    reconstruction_height = Column(Float, default=0)
    reconstruction_resolution = Column(Enum(ResolutionEnum), default=ResolutionEnum.res256)
    reconstruction_axial_distance = Column(Float, default=0)
    reconstruction_slice_size = Column(Float, default=0)
    reconstruction_kernel = Column(Enum(KernelEnum), default=KernelEnum.standard)
    reconstruction_noise_reduction = Column(Float, default=0)
    reconstruction_metal_correction = Column(Enum(MetalCorrectionEnum), default=MetalCorrectionEnum.disabled)
    fbp_method = Column(Enum(FBPMethodEnum), default=FBPMethodEnum.fbp_only)
    das_method = Column(Enum(DASMethodEnum), default=DASMethodEnum.das_1)

    # Trigger
    contrast_type = Column(Enum(ContrastTypeEnum), default=ContrastTypeEnum.no_contrast)
    contrast_density = Column(Float, default=0)
    contrast_density_min = Column(Float, default=0)
    contrast_density_max = Column(Float, default=0)
    contrast_volume = Column(Float, default=0)
    contrast_volume_min = Column(Float, default=0)
    contrast_volume_max = Column(Float, default=0)
    contrast_solvent_volume = Column(Float, default=0)
    contrast_solvent_volume_min = Column(Float, default=0)
    contrast_solvent_volume_max = Column(Float, default=0)
    contrast_start_delay = Column(Integer, default=0)
    contrast_step_delay = Column(Integer, default=0)
    contrast_only_manual = Column(Boolean, default=False)
    contrast_max_wait = Column(Integer, default=0)
    contrast_max_check_count = Column(Integer, default=0)
    contrast_concentration_threshold = Column(Float, default=0)
    contrast_diameter_roi_circle = Column(Float, default=0)
    contrast_diameter_roi_circle_min = Column(Float, default=0)
    contrast_diameter_roi_circle_max = Column(Float, default=0)

    area_start_x = Column(Float, default=0)
    area_stop_x = Column(Float, default=0)
    area_start_y = Column(Float, default=0)
    area_stop_y = Column(Float, default=0)
    area_start_z = Column(Float, default=0)
    area_stop_z = Column(Float, default=0)

    target_roi = Column(Float, default=0)
    max_trigger_wait = Column(Float, default=0)
    max_checks = Column(Float, default=0)
    zone_diameter = Column(Float, default=0)
    step_delay = Column(Float, default=0)

    # lost from protocol editor
    dose = Column(Float, default=0)
    ecg_trigger = Column(Float, default=0)
    manual_start = Column(Boolean, default=False)
    zone_diameter_min = Column(Float, default=0)
    zone_diameter_max = Column(Float, default=0)

    # sequence number
    order = Column(Integer, index=True, default=-1)


class ProtocolStep(AbstractStep):
    __tablename__ = 'protocol_step_table'

    protocol_id = Column(Integer, ForeignKey('protocol_table.id'), nullable=False)

    def to_dict(self):
        d = {k: getattr(self, k) for k in inspect(ProtocolStep).attrs.keys() if k is not ProtocolStep.protocol.key}
        for k, v in d.items():
            if isinstance(v, enum.Enum):
                d[k] = {'name': d[k].name}

        return d

    @staticmethod
    def from_examples_dict(d):

        res_map = {
            '256x256': ResolutionEnum.res256,
            '512x512': ResolutionEnum.res512,
            '1024x1024': ResolutionEnum.res1024,
            '2048x2048': ResolutionEnum.res2048,
        }

        # flatten the dict
        d = d.copy()
        for sub in ["reconstruction", "contrast", "area"]:
            for k, v in d[sub].items():
                d[f"{sub}_{k}"] = v
            del d[sub]

        d = {k: v for k, v in d.items() if k in inspect(ProtocolStep).attrs.keys()}
        p = ProtocolStep(**d)
        p.auto_calculated = AutoCalculatedEnum[p.auto_calculated['name']]
        p.table_motion = TableMotionEnum[p.table_motion['name']]
        p.scan_type = ScanTypeEnum[p.scan_type['name']]
        p.patient_position = PatientPositionEnum[p.patient_position['name']]
        p.contrast_type = ContrastTypeEnum[p.contrast_type['name']]
        p.reconstruction_kernel = KernelEnum[p.reconstruction_kernel]
        p.reconstruction_resolution = res_map[p.reconstruction_resolution]
        p.reconstruction_metal_correction = MetalCorrectionEnum.enabled if p.reconstruction_metal_correction \
            else MetalCorrectionEnum.disabled

        return p

    @staticmethod
    def from_dict(data: dict) -> 'ProtocolStep':
        step = ProtocolStep(**{k: v for k, v in data.items() if k in inspect(ProtocolStep).attrs.keys()})
        step.auto_calculated = AutoCalculatedEnum[step.auto_calculated['name']]
        step.table_motion = TableMotionEnum[step.table_motion['name']]
        step.scan_type = ScanTypeEnum[step.scan_type['name']]
        step.patient_position = PatientPositionEnum[step.patient_position['name']]
        step.contrast_type = ContrastTypeEnum[step.contrast_type['name']]
        step.reconstruction_kernel = KernelEnum[step.reconstruction_kernel['name']]
        step.reconstruction_resolution = ResolutionEnum[step.reconstruction_resolution['name']]
        step.reconstruction_metal_correction = MetalCorrectionEnum[step.reconstruction_metal_correction['name']]
        step.fbp_method = FBPMethodEnum[step.fbp_method['name']]
        step.das_method = DASMethodEnum[step.das_method['name']]

        return step

    def auto_lists(self):
        """
        Get list of values for combo boxes depend on other controls
        :return: dict {combo_control: values_list, ...}
        """
        result = {}

        if self.kv == 0:
            result[ProtocolStep.ma] = [0]
        elif self.kv == 70:
            result[ProtocolStep.ma] = [525, 641]
        elif self.kv == 80:
            result[ProtocolStep.ma] = [525, 641]
        elif self.kv == 90:
            result[ProtocolStep.ma] = [641, 765]
        elif self.kv == 100:
            result[ProtocolStep.ma] = [765, 896]
        elif self.kv == 110:
            result[ProtocolStep.ma] = [765, 896]
        elif self.kv == 120:
            result[ProtocolStep.ma] = [896, 1034]
        elif self.kv == 130:
            result[ProtocolStep.ma] = [1034, 1178]
        elif self.kv == 140:
            result[ProtocolStep.ma] = [1178, 1329, 1485]

        return result

    def auto_calculate(self):
        """
        Get values for controls depend on other controls
        :return: dict {control: value, ...}
        """
        result = {}

        if self.auto_calculated == AutoCalculatedEnum.collimator:
            T = 1  # TODO what it is
            V = self.table_speed
            p = self.pitch
            k = 1  # TODO what it is

            result[ProtocolStep.collimator_width] = T * V / (k * p)

        elif self.auto_calculated == AutoCalculatedEnum.table_speed:
            k = 1  # TODO what it is
            T = 1  # TODO what it is
            p = self.pitch
            W = self.collimator_width
            result[ProtocolStep.table_speed] = (k * p * W) / T

        return result

    def validate_types(self):
        """
        Check step values and convert each to model correct type
        in case conversion error return set of error controls
        :return: set {error_control, ...}
        :return:
        """

        errors = set()
        for k in inspect(ProtocolStep).class_manager.attributes:
            if k is ProtocolStep.protocol or k is ProtocolStep.id or k is ProtocolStep.protocol_id:
                continue

            val = getattr(self, k.name)
            try:
                correct_type_value = k.type.python_type(val)
                setattr(self, k.name, correct_type_value)
            except ValueError as e:
                errors.add(k)

        return errors

    def validate(self):
        """
        Check step values and return set of controls with error
        :return: set {error_control, ...}
        """
        tabs = self.get_tabs_enabled()
        errors = set()

        if not self.name:
            errors.add(ProtocolStep.name)

        if not self.ma_min <= self.ma_max:
            errors.add(ProtocolStep.ma_min)
            errors.add(ProtocolStep.ma_max)
        elif not self.ma_min <= self.ma <= self.ma_max:
            errors.add(ProtocolStep.ma)

        if tabs[ProtocolTabsEnum.recon]:  # check only active tabs
            if self.collimator_width > 1:
                errors.add(ProtocolStep.collimator_width)

        # FOR TEST
        # errors.add(ProtocolStep.table_height)
        return errors

    def get_tabs_enabled(self):
        """
        Get tabs which enabled in step editor
        :return: dict {tab: bool, ...}
        """

        result = {s: True for s in ProtocolTabsEnum}
        if self.scan_type == ScanTypeEnum.scout_two_side:
            result[ProtocolTabsEnum.recon] = False

        return result


class StudyStep(AbstractStep):
    __tablename__ = 'study_step_table'

    study_id = Column(Integer, ForeignKey('study_table.id'), nullable=False)
    state = Column(Enum(StepStateEnum), default=StepStateEnum.edit)
    dicom_path = Column(String())

    # report data
    scan_dose = Column(Float, default=None)
    scan_passed = Column(Boolean, default=False)
    scan_time = Column(Integer, default=None)
    scan_rotation_direction = Column(String, default=None)
    scan_focal_spots = Column(Float, default=None)
    scan_scan_interrupted = Column(Boolean, default=False)
    scan_timestamp = Column(Integer, default=None)
    scan_study_id = Column(Integer, default=-1)
    scan_series_number = Column(Integer, default=-1)

    # calculated fields
    dose_gray = Column(Float, default=None)
    dose_sievert = Column(Float, default=None)
    frame_count = Column(Integer, default=100)

    def auto_calculate(self):
        """
        Get values for controls depend on other controls
        :return: dict {control: value, ...}
        """
        result = ProtocolStep.auto_calculate(self)

        dose_gray = result[StudyStep.dose_gray] = self.kv * self.ma * self.frame_count / 1e6
        born = datetime.strptime(self.study.patient.date_of_birth, "%d.%m.%Y")
        age_in_months = (datetime.now().year - born.year) * 12 + datetime.now().month - born.month
        result[StudyStep.dose_sievert] = (dose_gray * sievert_factor[self.study.body_area] *
                                          sievert_age_factor(age_in_months)[self.study.body_area])

        return result

    def update_report(self, data):

        self.scan_dose = data["dose"]
        self.scan_passed = data["passed"]
        self.scan_time = data["scan_time"]
        self.scan_rotation_direction = data["rotation_direction"]
        self.scan_focal_spots = data["focal_spots"]
        self.scan_scan_interrupted = data["scan_interrupted"]
        self.scan_timestamp = data["timestamp"]
        self.scan_study_id = data["study_id"]
        self.scan_series_number = data["series_number"]

    def __repr__(self):
        return f"{self.id} {self.scan_type}"

    def get_control_buttons_enabled(self, parent: Study):

        buttons_state = {
            ControlButtonsEnum.prev: True,
            ControlButtonsEnum.next: True,
            ControlButtonsEnum.send_to_hw: True,
            ControlButtonsEnum.start_scan: False,
            ControlButtonsEnum.stop_scan: False,
            ControlButtonsEnum.show_report: False,
        }

        if self.state == StepStateEnum.data_sent:
            buttons_state[ControlButtonsEnum.send_to_hw] = False
            buttons_state[ControlButtonsEnum.start_scan] = True

        if self.state == StepStateEnum.run:
            buttons_state[ControlButtonsEnum.send_to_hw] = False
            buttons_state[ControlButtonsEnum.stop_scan] = True

        if self.state == StepStateEnum.done:
            buttons_state[ControlButtonsEnum.send_to_hw] = False
            buttons_state[ControlButtonsEnum.show_report] = True

        if parent.steps[0] is self:
            buttons_state[ControlButtonsEnum.prev] = False

        if parent.steps[-1] is self:
            buttons_state[ControlButtonsEnum.next] = False

        if self.scan_type == ScanTypeEnum.area_select:
            buttons_state[ControlButtonsEnum.send_to_hw] = False
            buttons_state[ControlButtonsEnum.start_scan] = False
            buttons_state[ControlButtonsEnum.start_scan] = False

        return buttons_state

    def from_protocol_step(self, ps: ProtocolStep):
        """
        copy all except id and protocol
        :param study: parent
        :param ps: protocol_step
        """
        for k, col in inspect(ProtocolStep).attrs.items():
            if col not in [ProtocolStep.id.property, ProtocolStep.protocol_id.property]:
                setattr(self, col.key, getattr(ps, col.key))

        return self

    def to_dict(self):
        d = {k: getattr(self, k) for k in inspect(StudyStep).attrs.keys() if k is not StudyStep.study.key}
        for k, v in d.items():
            if isinstance(v, enum.Enum):
                d[k] = {'name': d[k].name}

        return d

    @staticmethod
    def from_dict(d):
        d = {k: v for k, v in d.items() if k in inspect(StudyStep).attrs.keys()}
        p = StudyStep(**d)
        p.auto_calculated = AutoCalculatedEnum[p.auto_calculated['name']]
        p.table_motion = TableMotionEnum[p.table_motion['name']]
        p.scan_type = ScanTypeEnum[p.scan_type['name']]
        p.patient_position = PatientPositionEnum[p.patient_position['name']]
        p.contrast_type = ContrastTypeEnum[p.contrast_type['name']]
        p.reconstruction_kernel = KernelEnum[p.reconstruction_kernel['name']]
        p.reconstruction_resolution = ResolutionEnum[p.reconstruction_resolution['name']]
        p.das_method = DASMethodEnum[p.das_method['name']]
        p.fbp_method = FBPMethodEnum[p.fbp_method['name']]
        p.state = StepStateEnum[p.state['name']]
        # p.contrast_density = ContrastDensityEnum[p.contrast_density['name']]
        p.reconstruction_metal_correction = MetalCorrectionEnum[p.reconstruction_metal_correction['name']]
        return p


class Patient(Base):
    __tablename__ = 'patient_table'

    studies: List['Study'] = relationship("Study", backref="patient")

    id = Column(Integer(), primary_key=True)
    name = Column(String())
    family = Column(String())
    middle_name = Column(String())
    date_of_birth = Column(String())
    sex = Column(String())
    phantom = Column(String())
    height = Column(String())
    weight = Column(String())

    # body_area TODO

    def to_dict(self):
        d = {k: getattr(self, k) for k in inspect(Patient).attrs.keys() if k is not Patient.studies.key}
        for k, v in d.items():
            if isinstance(v, enum.Enum):
                d[k] = {'name': d[k].name}

        return d

    @staticmethod
    def from_dict(d):
        patient = Patient(**d)
        return patient

    @staticmethod
    def from_examples_dict(d):
        id = d.pop("id")
        body_area = d.pop("body_area")
        body_area = BodyAreaEnum[body_area["name"]]

        step = StudyStep(scan_type=ScanTypeEnum.area_select)
        study = Study(body_area=body_area, steps=[step])
        patient = Patient(studies=[study], **d)

        return patient


class Operator(Base):
    __tablename__ = 'operators_table'
    id = Column(Integer(), primary_key=True)
    name = Column(String())
    position = Column(String())
    password_hash = Column(Integer())
    access = Column(Enum(OperatorAccessEnum), default=OperatorAccessEnum.operator)

    def to_dict(self):
        d = {k: getattr(self, k) for k in inspect(Operator).attrs.keys()}
        for k, v in d.items():
            if isinstance(v, enum.Enum):
                d[k] = {'name': d[k].name}

        return d

    @staticmethod
    def from_dict(d):
        operator = Operator(**d)
        operator.access = OperatorAccessEnum[operator.access['name']]
        return operator

    @staticmethod
    def from_examples_dict(d):
        access_map = {
            "EXPERT": OperatorAccessEnum.superuser,
            "TECHNICAL_SERVICE": OperatorAccessEnum.admin,
            "USER": OperatorAccessEnum.operator
        }

        operator = Operator()
        operator.name = d["name"]
        operator.position = d["position"]
        operator.access = access_map[d["access"]]
        # TEMP
        operator.password_hash = int(d["password"] or 0)

        return operator

    @staticmethod
    def get_enabled_windows(access: OperatorAccessEnum):
        result = set()

        if access == OperatorAccessEnum.operator:
            result.add(MainWindowTabsEnum.scan)
            result.add(MainWindowTabsEnum.protocol_designer)

        if access == OperatorAccessEnum.superuser:
            result.add(MainWindowTabsEnum.scan)
            result.add(MainWindowTabsEnum.scan)
            result.add(MainWindowTabsEnum.protocol_designer)
            result.add(MainWindowTabsEnum.reports)
            result.add(MainWindowTabsEnum.dose_logs)

        if access == OperatorAccessEnum.admin:
            result.add(MainWindowTabsEnum.scan)
            result.add(MainWindowTabsEnum.protocol_designer)
            result.add(MainWindowTabsEnum.reports)
            result.add(MainWindowTabsEnum.dose_logs)
            result.add(MainWindowTabsEnum.patients)
            result.add(MainWindowTabsEnum.operators)
            result.add(MainWindowTabsEnum.maintenance)

        return result


class HWTest(Base):
    __tablename__ = 'hardware_test_table'

    id = Column(Integer(), primary_key=True)
    time_done = Column(DateTime(timezone=True), default=datetime.utcnow)
    report_json = Column(String, default="{}")

    def to_dict(self) -> dict:
        # TODO
        return {}

    @staticmethod
    def from_dict(d: dict) -> 'HWTest':
        # TODO
        return HWTest()


if __name__ == "__main__":
    p = ProtocolStep()
    p.step_delay = 10
    c = StudyStep()
    c.from_protocol_step(p)
    c
