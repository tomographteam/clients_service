"""
Модуль управления сканированием
обрабатывает актуальное состояние шагов сканирования
отдает статус для всех клиентов
получает команды от клиентов, валидирует и отправляет на томограф
"""

import enum
import json
from time import time
import datetime
import os

import aiohttp
import sqlalchemy
from sqlalchemy.orm import Session, joinedload, raiseload

from src.scsc_client import SCSCClient
from src.model_.data.enums import ControlButtonsEnum
from src.model import Protocol, ProtocolStep, StudyStep, Study, StepStateEnum, HWTest, Patient, Operator
from sqlalchemy import create_engine


class DBController:
    def __init__(self):

        self._db_path = os.path.join(os.path.dirname(__file__), '../../db/tom.db')
        self.engine = create_engine(f'sqlite:///{self._db_path}')
        self.session = Session(self.engine)

    async def get_protocols_handler(self) -> list:
        with self.session as session:
            protocols = session.query(Protocol).all()
            return [p.to_dict() for p in protocols]

    async def get_operators_handler(self) -> list:
        with self.session as session:
            operators = session.query(Operator).all()
            return [o.to_dict() for o in operators]

    async def get_all_study_steps_handler(self) -> list:
        with self.session as session:
            steps = session.query(StudyStep).all()
            return [s.to_dict() for s in steps]

    async def get_patients_handler(self) -> list:
        with self.session as session:
            patients = session.query(Patient).all()
            return [p.to_dict() for p in patients]

    async def get_hw_tests_handler(self) -> list:
        with self.session as session:
            hw_tests = session.query(HWTest).all()
            return [t.to_dict() for t in hw_tests]

    async def get_studies_with_patients_handler(self) -> list:
        with self.session as session:
            studies = session.query(Study, Patient).join(Patient).all()
            return [[s.to_dict(), p.to_dict()] for s, p in studies]

    async def get_study_protocols_handler(self) -> list:
        with self.session as session:
            studies = session.query(Study).all()
            return [s.to_dict() for s in studies]

    async def get_protocol_steps_handler(self, protocol_id: int) -> list:
        with self.session as session:
            steps = session.query(ProtocolStep).filter(
                ProtocolStep.protocol_id == protocol_id).all()
            return [s.to_dict() for s in steps]

    async def get_study_steps_handler(self, study_id: Study) -> list:
        with self.session as session:
            return [s.to_dict() for s in session.query(StudyStep).filter(
                    StudyStep.study_id == study_id).all()]

    async def save_protocol_handler(self, protocol: dict) -> bool:
        with self.session as session:
            upd_protocol = session.merge(Protocol.from_dict(protocol))
            session.commit()
            return upd_protocol.id

    async def save_protocol_step_handler(self, step: dict) -> bool:
        with self.session as session:
            upd_step = session.merge(ProtocolStep.from_dict(step))
            session.commit()
            return upd_step.id

    async def delete_protocol_handler(self, protocol_id: int) -> bool:
        with self.session as session:
            session.delete(session.query(Protocol).get(protocol_id))
            session.commit()
            return True

    async def delete_protocol_step_handler(self, step_id: int) -> bool:
        with self.session as session:
            session.delete(session.query(ProtocolStep).get(step_id))
            session.commit()
            return True

    async def get_all_study_steps(self) -> list:
        with self.session as session:
            return [s.to_dict() for s in session.query(StudyStep).all()]

    async def get_study_by_id_handler(self, study_id: int) -> dict:
        with self.session as session:
            return session.query(Study).get(study_id).to_dict()

    async def get_operator_by_id(self, operator_id: int) -> dict:
        with self.session as session:
            return session.query(Operator).get(operator_id).to_dict()

    async def save_operator_handler(self, operator: dict) -> int:
        with self.session as session:
            upd_operator = session.merge(Operator.from_dict(operator))
            session.commit()
            return upd_operator.id

    async def delete_operator_handler(self, operator_id: int) -> int:
        with self.session as session:
            session.delete(session.query(Operator).get(operator_id))
            session.commit()
            return True

    async def get_patient_by_id_handler(self, patient_id: int) -> dict:
        with self.session as session:
            return session.query(Patient).get(patient_id).to_dict()

    async def save_patient_handler(self, patient: dict) -> int:
        with self.session as session:
            upd_patient = session.merge(Patient.from_dict(patient))
            session.commit()
            return upd_patient.id

    async def get_calib_reports_handler(self) -> list:
        with self.session as session:
            return []  # [c.to_dict() for c in session.query(CalibReport).all()]

    async def fill_out_calib_table_handler(self, kv_ma_list: list) -> bool:
        with self.session as session:
            # for (kv, ma) in kv_ma_list:
            #     report = CalibReport(
            #         kv=kv,
            #         ma=ma,
            #         # Don't use in Prod.
            #         air_calib_operator_id=1,
            #         air_calib_time_done=datetime.datetime.now(),
            #         dark_calib_operator_id=2,
            #         dark_calib_time_done=datetime.datetime.now(),
            #         water_calib_operator_id=3,
            #         water_calib_time_done=datetime.datetime.now(),
            #         multi_pin_calib_operator_id=2,
            #         multi_pin_calib_time_done=datetime.now()
            #         )

            #     session.add(report)
            #     session.commit()
            return True

    async def clear_out_calib_table_handler(self) -> bool:
        with self.session as session:
            # session.query(CalibReport).delete()
            # session.commit()
            return True

    async def do_command(self, command: str, **params):
        """
        handle a commands from client
        :param command: command name, see details in each handler
        :param params: command params, see details in each handler
        :return:
        """
        handler = getattr(self, command + "_handler", None)
        if handler:
            result = await handler(**params)
            return result

        print("Invalid command", command)
        return None


class TomographController(SCSCClient):
    """
    Commands from clients processor/validator
    """
    def __init__(self):

        super().__init__()

        self.study: Study = None
        self._db_path = os.path.join(os.path.dirname(__file__), '../../db/tom.db')
        self.engine = create_engine(f'sqlite:///{self._db_path}')
        self.session = Session(self.engine)
        self.reset()

    def reset(self):
        """
        init state of model
        loads firs available study
        :return:
        """
        # read the DB

        self.study = self.session.query(Study).options(joinedload('*')).first()
        self.study.timestamp = time()
        self.session.commit()

    def dump(self) -> dict:
        """
        serialize the model
        :return:
        """
        data = self.study.to_dict()
        return data

    async def prepare_scan_finished_handler(self, data):
        """
        prepare_scan_finished_handler from SCSC
        :return:
        """
        step: StudyStep = self.study.steps[self.study.current_step]
        step.state = StepStateEnum.data_sent
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def scan_report_handler(self, data):
        """
        scan report from SCSC to model
        :param data:
        :return:
        """
        step: StudyStep = self.study.steps[self.study.current_step]
        step.update_report(data)
        auto_calculate = step.auto_calculate()
        for attr, val in auto_calculate.items():
            setattr(step, attr.key, val)
        step.scan_time = datetime.datetime.now()  # TODO zero from emulator

        self.study.timestamp = time()
        self.session.commit()
        return True

    async def recon_finished_handler(self, data):
        """
        recon data from SCSC to model
        :param data:
        :return:
        """
        step: StudyStep = self.study.steps[self.study.current_step]
        step.dicom_path = data["result_path"]
        step.state = StepStateEnum.done
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def test_equipment_finished_handler(self, data):
        """
        test data from SCSC to DB
        :param data:
        :return:
        """
        report = HWTest(report_json=json.dumps(data))
        self.session.add(report)
        self.study.timestamp = time()
        self.session.commit()
        return True

    def set_step_attrib(self, attrib, value) -> bool:
        """
        step parameter to model
        :param attrib: name of parameter, StudyStep attribute
        :param value: parameter value
        :return: success or no
        """

        step: StudyStep = self.study.steps[self.study.current_step]
        if step.state == StepStateEnum.data_sent:
            step.state = StepStateEnum.edit  # return to edit, needs data send again

        if step.state != StepStateEnum.edit:
            return False

        try:
            if isinstance(getattr(step, attrib), enum.Enum):
                setattr(step, attrib, type(getattr(step, attrib))[value])
                self.study.timestamp = time()
                self.session.commit()
                return True
            if isinstance(getattr(step, attrib), float):
                setattr(step, attrib, float(value))
                self.study.timestamp = time()
                self.session.commit()
                return True
            if isinstance(getattr(step, attrib), int):
                setattr(step, attrib, int(value))
                self.study.timestamp = time()
                self.session.commit()
                return True

            return False

        except AttributeError:
            return False

    async def send_scsc_command(self, device: str, command_name: str, payload: dict):
        """
        command from client to SCSC
        :param device: scanner subdivide
        :param command_name: see API specification
        :param payload: command parameters. see API specification
        :return:
        """

        url = f'http://{self.host}:{self.port}/tango/rest/v11/commands'
        _auth = aiohttp.BasicAuth('tango-cs', 'tango')

        url_payload = {'host': f'{self.host}:10000', 'device': device, 'name': command_name}
        if payload:
            url_payload['input'] = json.dumps(payload)
        url_payload = [url_payload, ]

        print(url_payload)
        async with aiohttp.ClientSession() as session:
            print("send")
            try:
                async with session.put(url,
                                   auth=_auth,
                                   json=url_payload,
                                   timeout=2
                                   ) as response:
                    if response.status == 200:
                            result_str = await response.read()
                            if result_str:
                                result = json.loads(result_str)
                                print("result", result)
                                return result
                            else:
                                print("result none")
                                return "none"
                    else:
                        response_data = await response.read()
                        return f'Tango error status={response.status} url={url} payload={payload} response={response_data}'

            except Exception as exception:
                print(f'scsc client error={exception} result_str={result_str}')


    async def do_command(self, command: str, **params):
        """
        handle a commands from client
        :param command: command name, see details in each handler
        :param params: command params, see details in each handler
        :return:
        """
        handler = getattr(self, command + "_command_handler", None)
        if handler:
            await handler(**params)
            return True

        print("Invalid command", command)
        return False

    async def move_table_up_command_handler(self):
        """
        Move table at one step
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="move_patient_table",
                                     payload={"direction": ControlButtonsEnum.move_table_up.name})

    async def move_table_down_command_handler(self):
        """
        Move table at one step
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="move_patient_table",
                                     payload={"direction": ControlButtonsEnum.move_table_down.name})

    async def move_table_forward_command_handler(self):
        """
        Move table at one step
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="move_patient_table",
                                     payload={"direction": ControlButtonsEnum.move_table_forward.name})

    async def move_table_backward_command_handler(self):
        """
        Move table at one step
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="move_patient_table",
                                     payload={"direction": ControlButtonsEnum.move_table_backward.name})

    async def move_table_to_x_command_handler(self, x_coordinate: int):
        """
        Move table to destination
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="MoveHorizontal",
                                     payload={"position": x_coordinate, "speed": 10})

    async def move_table_to_y_command_handler(self, y_coordinate):
        """
        Move table to destination
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="MoveVertical",
                                     payload={"position": y_coordinate})

    async def stop_table_command_handler(self):
        """
        Stop table if it's moving
        :return:
        """
        await self.send_scsc_command('test/ptable/1', command_name="stop_table",
                                     payload={})

    async def import_protocol_command_handler(self, protocol_id: int):
        """
        Assign targeted protocol to the model
        Replace all ols steps by steps from the protocol
        :param protocol_id: DB index
        :return:
        """
        protocol = self.session.query(Protocol).filter(Protocol.id == protocol_id).first()
        protocol_steps = [StudyStep().from_protocol_step(s) for s in protocol.steps]
        while len(self.study.steps) > 1:
            self.study.steps.remove(self.study.steps[1])
        for step in protocol_steps:
            self.session.add(step)
            self.study.steps.append(step)
        self.study.protocol_name = protocol.name
        self.session.flush()

        print(len(self.study.steps), self.study.steps)
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def next_command_handler(self):
        """
        Move view to next study step
        :return:
        """
        self.study.current_step = min(len(self.study.steps) - 1, self.study.current_step + 1)

        self.study.timestamp = time()
        self.session.commit()
        return True

    async def prev_command_handler(self):
        """
        Move view to previous study step
        :return:
        """
        self.study.current_step = max(0, self.study.current_step - 1)
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def add_step_command_handler(self, step_id):
        """
        Add custom step to the study
        :param step_id: Db key
        :return:
        """
        step = self.session.query(ProtocolStep).filter(ProtocolStep.id == step_id).first()
        new_step = StudyStep(study_id=self.study.id).from_protocol_step(step)

        self.study.steps.insert(self.study.current_step + 1, new_step)
        for order, step in enumerate(self.study.steps):
            step.order = order

        self.study.timestamp = time()
        self.session.commit()
        return True

    async def skip_step_command_handler(self):
        """
        delete next step (exact after current) from study
        :return:
        """
        self.study.steps.remove(self.study.steps[self.study.current_step + 1])

        self.study.timestamp = time()
        self.session.commit()
        return True

    async def select_study_command_handler(self, study_id):
        """
        Change study in model, load state from DB
        :param study_id: DB key
        :return:
        """
        self.study = self.session.query(Study).filter(Study.id == study_id).first()

        self.study.timestamp = time()
        self.session.commit()
        return True

    async def send_to_hw_command_handler(self):
        """
        Sends current step data to scanner, should be used before start scan
        :return:
        """
        step: StudyStep = self.study.steps[self.study.current_step]

        if step.state != StepStateEnum.edit:
            return False

        payload = {'scan_id': {'study_id': '2b25c82f579e404e88e7422ca829bc7d', 'series_number': None},
                   'scan': {
                       'SeriesType': 1,  # step.scan_type.name, #1,
                       'table_motion': step.table_motion.name,  # 'moving',
                       'number_of_volume_images': step.number_of_volume_images,  # 0,
                       'fluoro_scan_number_of_views': step.fluoro_scan_number_of_views,  # 0,
                       'TableStartPos': step.start_position,  # 0,
                       'TableStopPos': step.stop_position,  # 500,
                       'TableSpeed': step.table_speed,  # 24.0,
                       'table_height': step.table_height,  # 600,
                       'Kv': step.kv,  # 100,
                       'Ma': step.ma,  # 765,
                       'CollWidth': step.collimator_width,  # 1.2,
                       'Pitch': step.pitch,  # 0.5
                   },
                   'recon': {
                       'ScoutWinWidth': step.reconstruction_width,  # 513,
                       'ScoutWinLength': step.reconstruction_height,  # 514,
                       'ReconKernelIndex': step.reconstruction_kernel.name,  # 0,
                       'ReconNoiseReduced': step.reconstruction_noise_reduction,  # 1,
                       'ReconMetalCorr': step.reconstruction_metal_correction.name,  # 1,
                       'patient position': step.patient_position.name,  # 'FFDR',
                       'method': step.fbp_method.name,  # 'FBP + 5 SART',
                       'das_mode': step.das_method.name,  # 'DAS1+2'
                   },
                   'trigger': {
                       'TriggerW': step.area_stop_x,  # 0,
                       'TriggerX': step.area_stop_x,  # 0,
                       'TriggerY': step.area_stop_y,  # 0,
                       'TriggerScanPeriod': step.area_stop_x,  # 0,
                       'TriggerScanMaxCount': step.contrast_max_check_count,  # 20,
                       'TriggerDensityThreshold': step.contrast_density_max,  # 3
                   }
                   }

        await self.send_scsc_command('test/dsebct/1', 'PrepareScanWithParams', payload)
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def start_scan_command_handler(self):
        """
        Start scan, step parameters should be sent to scanner before start
        :return:
        """
        await self.send_scsc_command('test/dsebct/1', 'StartScan', None)
        self.study.timestamp = time()
        self.session.commit()
        return True

    async def run_hardware_test_command_handler(self):
        """
        Start hardware test
        :return:
        """
        await self.send_scsc_command('test/dsebct/1', 'start_test_equipment', None)
        self.study.timestamp = time()
        self.session.commit()
        return True
