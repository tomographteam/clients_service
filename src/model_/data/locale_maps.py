from src.model import (AreaKindEnum, AutoCalculatedEnum, BodyAreaEnum,
                       BoolEnum, ContrastDensityEnum, ContrastTypeEnum,
                       ControlButtonsEnum, DASMethodEnum, FBPMethodEnum,
                       KernelEnum, MetalCorrectionEnum, Operator,
                       OperatorAccessEnum, Patient, PatientPositionEnum,
                       Protocol, ProtocolStep, ProtocolTabsEnum,
                       ResolutionEnum, ScanTypeEnum, TableMotionEnum)
from src.model_.data.enums import (AcceptActionsEnum, ButtonTitlesEnum, DialogTitlesEnum, ErrorsEnum,
                                   InfoLabelTitlesEnum, LanguagesEnum,
                                   MainWindowTabsEnum, ParameterPropsEnum, TabTitlesEnum, TableHeadersEnum)

EN = LanguagesEnum.EN
RU = LanguagesEnum.RU

MAP = {
    AutoCalculatedEnum.table_speed: {
        EN: 'Table speed',
        RU: 'Скорость стола м/с'},
    AutoCalculatedEnum.collimator: {
        EN: 'Collimator',
        RU: 'Колиматор'},

    ScanTypeEnum.scout_two_side: {
        EN: 'Scout 2D',
        RU: 'Scout 2D'},
    ScanTypeEnum.helical: {
        EN: 'Helical',
        RU: 'Helical'},
    ScanTypeEnum.fluoro: {
        EN: 'Fluoro',
        RU: 'Fluoro'},
    ScanTypeEnum.trigger_start: {
        EN: 'Trigger',
        RU: 'Trigger'},
    ScanTypeEnum.trigger_start_first_slice: {
        EN: 'Trigger start first slice',
        RU: 'Trigger start first slice'},
    ScanTypeEnum.cardiac: {
        EN: 'Cardiac',
        RU: 'Cardiac'},
    ScanTypeEnum.radiology: {
        EN: 'Radiology',
        RU: 'Radiology'},
    ScanTypeEnum.hi_res: {
        EN: 'High res',
        RU: 'High res'},
    ScanTypeEnum.flow: {
        EN: 'Flow',
        RU: 'Flow'},
    ScanTypeEnum.cine: {
        EN: 'Cine',
        RU: 'Cine'},
    ScanTypeEnum.dark_calib: {
        EN: 'Dark calibration',
        RU: 'Калибровка "по тёмному"'},
    ScanTypeEnum.air_calib: {
        EN: 'Air calibration',
        RU: 'Воздушная калибровка'},
    ScanTypeEnum.multi_Pin_Calib: {
        EN: 'MultiPin calibration',
        RU: 'Мультиконтактная калибровка'},
    ScanTypeEnum.upper_half: {
        EN: 'Upper half',
        RU: 'Верхняя половина'},
    ScanTypeEnum.bad_tile_detection: {
        EN: 'Bad title detection',
        RU: 'Bad title detection'},
    ScanTypeEnum.water_calib: {
        EN: 'Water calibration',
        RU: 'Водная калибровка'},
    ScanTypeEnum.preprocess_only: {
        EN: 'Preprocess only',
        RU: 'Preprocess only'},
    ScanTypeEnum.xray_tuning: {
        EN: 'XRay tuning',
        RU: 'XRAY настройка'},
    ScanTypeEnum.null_processing: {
        EN: 'Null processing',
        RU: 'Null processing'},

    TableMotionEnum.stationary: {
        EN: 'Stationary',
        RU: 'Stationary'},
    TableMotionEnum.moving: {
        EN: 'Moving',
        RU: 'Moving'},

    ScanTypeEnum.area_select: {
        EN: 'Select',
        RU: 'Select'},

    ResolutionEnum.res256: {
        EN: '256x256',
        RU: '256x256'},
    ResolutionEnum.res512: {
        EN: '512x512',
        RU: '512x512'},
    ResolutionEnum.res1024: {
        EN: '1024x1024',
        RU: '1024x1024'},
    ResolutionEnum.res2048: {
        EN: '2048x2048',
        RU: '2048x2048'},

    KernelEnum.standard: {
        EN: 'Standart',
        RU: 'Standart'},
    KernelEnum.langs: {
        EN: 'Langs',
        RU: 'Langs'},
    KernelEnum.bones: {
        EN: 'Bones',
        RU: 'Bones'},

    MetalCorrectionEnum.disabled: {
        EN: 'Disabled',
        RU: 'Выключено'},
    MetalCorrectionEnum.enabled: {
        EN: 'Enabled',
        RU: 'Включено'},

    FBPMethodEnum.fbp_only: {
        EN: 'FBP Only',
        RU: 'FBP Only'},
    FBPMethodEnum.fbp_1_sart: {
        EN: 'FBP + 1 SART',
        RU: 'FBP + 1 SART'},
    FBPMethodEnum.fbp_2_sart: {
        EN: 'FBP + 2 SART',
        RU: 'FBP + 2 SART'},
    FBPMethodEnum.fbp_3_sart: {
        EN: 'FBP + 3 SART',
        RU: 'FBP + 3 SART'},
    FBPMethodEnum.fbp_4_sart: {
        EN: 'FBP + 4 SART',
        RU: 'FBP + 4 SART'},
    FBPMethodEnum.fbp_5_sart: {
        EN: 'FBP + 5 SART',
        RU: 'FBP + 5 SART'},

    DASMethodEnum.das_1: {
        EN: 'DAS1',
        RU: 'DAS1'},
    DASMethodEnum.das_2: {
        EN: 'DAS2',
        RU: 'DAS2'},
    DASMethodEnum.das_1_2: {
        EN: 'DAS1+2',
        RU: 'DAS1+2'},
    DASMethodEnum.file: {
        EN: 'File',
        RU: 'File'},

    ContrastTypeEnum.no_contrast: {
        EN: 'No contrast',
        RU: 'Без контраста'},
    ContrastTypeEnum.contrast_1: {
        EN: 'Contrast 1',
        RU: 'Contrast 1'},

    ContrastDensityEnum.d300: {
        EN: '300',
        RU: '300'},
    ContrastDensityEnum.d350: {
        EN: '350',
        RU: '350'},
    ContrastDensityEnum.d370: {
        EN: '370',
        RU: '370'},
    ContrastDensityEnum.d400: {
        EN: '400',
        RU: '400'},

    AreaKindEnum.adult: {
        EN: 'Adult',
        RU: 'Взрослый'},
    AreaKindEnum.trauma: {
        EN: 'Trauma',
        RU: 'Травма'},

    Protocol.name: {
        EN: 'Name',
        RU: 'Имя'},
    Protocol.example_protocol: {
        EN: 'Is example',
        RU: 'Пример'},
    Protocol.sievert_factor: {
        EN: 'Sievert factor',
        RU: 'коэффициент Зиверта'},
    Protocol.body_area: {
        EN: 'Body area',
        RU: 'Область тела'},
    Protocol.area_kind: {
        EN: 'Area kind',
        RU: 'Вид области'},
    Protocol.author: {
        EN: 'Author',
        RU: 'Автор'},

    ProtocolStep.collimator_width: {
        EN: 'Collimator width',
        RU: 'Ширина коллиматора'},

    ProtocolStep.auto_calculated: {
        EN: 'Auto calculated',
        RU: 'Авто вычисление'},
    ProtocolStep.patient_position: {
        EN: 'Patient position',
        RU: 'Позиция пациента'},
    ProtocolStep.scan_type: {
        EN: 'Scan type',
        RU: 'Тип сканирования'},
    ProtocolStep.table_motion: {
        EN: 'Table motion',
        RU: 'Движение стола'},

    ProtocolStep.kv: {
        EN: 'kV',
        RU: 'kV'},
    ProtocolStep.ma: {
        EN: 'Beam current, mA',
        RU: 'Ток луча, mA'},
    ProtocolStep.pitch: {
        EN: 'Pitch',
        RU: 'Питч'},
    ProtocolStep.start_position: {
        EN: 'Start position',
        RU: 'Позиция-старт'},
    ProtocolStep.stop_position: {
        EN: 'Stop position',
        RU: 'Позиция-стоп'},
    ProtocolStep.table_height: {
        EN: 'Table height',
        RU: 'Высота стола'},
    ProtocolStep.table_speed: {
        EN: 'Table speed',
        RU: 'Скорость стола'},
    ProtocolStep.reconstruction_width: {
        EN: 'Width',
        RU: 'Ширина'},
    ProtocolStep.reconstruction_height: {
        EN: 'Height',
        RU: 'Высота'},
    ProtocolStep.reconstruction_resolution: {
        EN: 'Resolution',
        RU: 'Разрешение'},
    ProtocolStep.reconstruction_axial_distance: {
        EN: 'Axial distance',
        RU: 'Осевое расстояние'},
    ProtocolStep.reconstruction_slice_size: {
        EN: 'Slice size',
        RU: 'Размер среза'},
    ProtocolStep.reconstruction_kernel: {
        EN: 'Kernel',
        RU: 'Ядро'},
    ProtocolStep.reconstruction_noise_reduction: {
        EN: 'Noise reduction',
        RU: 'Шумоподавление'},
    ProtocolStep.reconstruction_metal_correction: {
        EN: 'Metal correction',
        RU: 'Коррекция металла'},
    ProtocolStep.fbp_method: {
        EN: 'FBP Method',
        RU: 'FBP Method'},
    ProtocolStep.das_method: {
        EN: 'DAS Method',
        RU: 'DAS Method'},
    ProtocolStep.point_zero: {
        EN: 'Point zero',
        RU: 'Нулевая точка'},
    ProtocolStep.contrast_type: {
        EN: 'Contrast type',
        RU: 'Тип контраста'},
    ProtocolStep.contrast_density: {
        EN: 'Contrast density',
        RU: 'Плотность контраста'},
    ProtocolStep.contrast_volume: {
        EN: 'Contrast volume',
        RU: 'Объём контраста'},
    ProtocolStep.contrast_solvent_volume: {
        EN: 'Solvent volume',
        RU: 'Объём растворителя'},
    ProtocolStep.start_delay: {
        EN: 'Start delay, s',
        RU: 'Задержка-старт, s'},
    ProtocolStep.target_roi: {
        EN: 'Target ROI',
        RU: 'Целевой ROI'},
    ProtocolStep.max_trigger_wait: {
        EN: 'Max. trigger wait',
        RU: 'Макс. ожидание триггера'},
    ProtocolStep.max_checks: {
        EN: 'Max. checks',
        RU: 'Макс. проверок'},
    ProtocolStep.zone_diameter: {
        EN: 'Zone diameter, mm',
        RU: 'Диаметер зоны, мм'},
    ProtocolStep.step_delay: {
        EN: 'Step delay',
        RU: 'Задержка шага'},

    ProtocolStep.number_of_volume_images: {
        EN: 'Number of volume images',
        RU: 'Кол-во изображений томов'},
    ProtocolStep.dose: {
        EN: 'Dose, mAc',
        RU: 'Доза, mAc'},
    ProtocolStep.start_voice_cmd_uid: {
        EN: 'Start voice cmd',
        RU: 'ГС-старт'},                    # TODO
    ProtocolStep.end_voice_cmd_uid: {
        EN: 'End voice cmd',
        RU: 'ГС-стоп'},                     # TODO
    ProtocolStep.is_auto_start: {
        EN: 'Is auto start',
        RU: 'Автозапуск'},
    ProtocolStep.is_cardio_start: {
        EN: 'Is cardio start',
        RU: 'Начинать ли кардио'},
    ProtocolStep.ecg_trigger: {
        EN: 'ECG trigger',
        RU: 'ЭКГ триггер'},
    ProtocolStep.manual_start: {
        EN: 'Manual start',
        RU: 'Ручной запуск'},
    ProtocolStep.contrast_step_delay: {
        EN: 'Trigger start step delay',
        RU: 'Задержка шага запуска триггера'},
    ProtocolStep.contrast_start_delay: {
        EN: 'Start delay',
        RU: 'Задержка запуска'},
    ProtocolStep.name: {
        EN: 'Step name',
        RU: 'Имя шага'},

    BodyAreaEnum.head: {
        EN: 'Head',
        RU: 'Голова'},
    BodyAreaEnum.neck: {
        EN: 'Neck',
        RU: 'Шея'},
    BodyAreaEnum.chest: {
        EN: 'Chest',
        RU: 'Грудь'},
    BodyAreaEnum.limbs: {
        EN: 'Limbs',
        RU: 'Конечности'},
    BodyAreaEnum.abdominal: {
        EN: 'Abdominal',
        RU: 'Брюшная полость'},
    BodyAreaEnum.pelvis: {
        EN: 'Pelvis',
        RU: 'Таз'},

    # ==========================================
    # Protocol
    DialogTitlesEnum.trigger_options: {
        EN: 'Trigger options',
        RU: 'Параметры триггера'},
    DialogTitlesEnum.reconstruction_options: {
        EN: 'Reconstruction',
        RU: 'Реконструкция'},
    DialogTitlesEnum.planing_options: {
        EN: 'Planing',
        RU: 'Планирование'},
    DialogTitlesEnum.scan_options: {
        EN: 'Scan options',
        RU: 'Параметры сканирования'},
    # DoseEvalWidget
    DialogTitlesEnum.all_scans: {
        EN: 'All scans',
        RU: 'Все'},
    DialogTitlesEnum.this_scan: {
        EN: 'This scan',
        RU: 'Текущее'},
    DialogTitlesEnum.evaluation: {
        EN: 'Eval',
        RU: 'Расчёт'},
    DialogTitlesEnum.gy: {
        EN: 'Gy:',
        RU: 'Гр:'},
    DialogTitlesEnum.sv_on_cm: {
        EN: 'Sv*cm',
        RU: 'Зв*см'},
    DialogTitlesEnum.cis_disconnected: {
        EN: 'Connection to CIS lost',
        RU: 'Соединение с CIS потеряно'},

    DialogTitlesEnum.accept_dialog: {
        EN: 'Confirmation of action required',
        RU: 'Требуется подтверждение действия'},
    # ==========================================

    # Global tabs ==============================
    MainWindowTabsEnum.scan: {
        EN: 'Scan',
        RU: 'Сканирование'},
    MainWindowTabsEnum.protocol_designer: {
        EN: 'Protocol designer',
        RU: 'Дизайнер протоколов'},
    MainWindowTabsEnum.reports: {
        EN: 'Reports and results',
        RU: 'Отчеты'},
    MainWindowTabsEnum.dose_logs: {
        EN: 'Dose logs',
        RU: 'Отчёты о дозах'},
    MainWindowTabsEnum.operators: {
        EN: 'Operators',
        RU: 'Операторы'},
    MainWindowTabsEnum.patients: {
        EN: 'Patients',
        RU: 'Пациенты'},
    MainWindowTabsEnum.maintenance: {
        EN: 'Maintenance',
        RU: 'Тех. обслуживание'},

    # Protocol tabs
    ProtocolTabsEnum.protocol: {
        EN: 'Protocol step',
        RU: 'Шаг протокола'},
    ProtocolTabsEnum.cardio: {
        EN: 'Cardio options',
        RU: 'Параметры кардио'},
    ProtocolTabsEnum.contrast: {
        EN: 'Contrast injector',
        RU: 'Инжектор контранста'},
    ProtocolTabsEnum.recon: {
        EN: 'Reconstruction options',
        RU: 'Параметры реконструкции'},
    ProtocolTabsEnum.desc: {
        EN: 'Description',
        RU: 'Описание'},

    # Step
    ControlButtonsEnum.next: {
        EN: 'Next >',
        RU: 'Далее >'},
    ControlButtonsEnum.prev: {
        EN: '< Prev',
        RU: '< Назад'},
    ControlButtonsEnum.send_to_hw: {
        EN: 'Send to HW',
        RU: 'Отправить на оборудование'},
    ControlButtonsEnum.pause_scan: {
        EN: 'Pause scan',
        RU: 'Пауза'},
    ControlButtonsEnum.start_scan: {
        EN: 'Start scan',
        RU: 'Запустить скан.'},
    ControlButtonsEnum.stop_scan: {
        EN: 'Stop scan',
        RU: 'Остановить скан.'},
    ControlButtonsEnum.show_report: {
        EN: 'Show Report',
        RU: 'Показать отчёт'},
    ControlButtonsEnum.run_hardware_test: {
        EN: 'HW Test',
        RU: 'Тест оборудования'},
    ControlButtonsEnum.morning_startup: {
        EN: 'Morning startup',
        RU: 'Утренний запуск'},
    ControlButtonsEnum.evening_shutdown: {
        EN: 'Evening shutdown',
        RU: 'Вечернее отключение'},

    # Study
    ControlButtonsEnum.add_step: {
        EN: 'Add',
        RU: 'Добавить шаг'},
    ControlButtonsEnum.skip_step: {
        EN: 'Skip',
        RU: 'Пропустить шаг'},

    # Patient Table
    ControlButtonsEnum.move_table_forward: {
        EN: 'Forward',
        RU: 'Вперёд'},
    ControlButtonsEnum.move_table_backward: {
        EN: 'Backward',
        RU: 'Назад'},
    ControlButtonsEnum.move_table_up: {
        EN: 'Up',
        RU: 'Вверх'},
    ControlButtonsEnum.move_table_down: {
        EN: 'Down',
        RU: 'Вниз'},
    ControlButtonsEnum.stop_table: {
        EN: 'Stop table',
        RU: 'Остановить стол'},

    # TopRightWidget | MainMenu
    ButtonTitlesEnum.main_menu: {
        EN: 'Main menu',
        RU: 'Главное меню'},
    ButtonTitlesEnum.calib: {
        EN: 'Calibration',
        RU: 'Калибровка'},
    ButtonTitlesEnum.ru: {
        EN: 'Russian version',
        RU: 'Русская версия'},
    ButtonTitlesEnum.en: {
        EN: 'English version',
        RU: 'Английская версия'},
    ButtonTitlesEnum.patient_table: {
        EN: 'Patient table',
        RU: 'Стол для пациента'},
    ButtonTitlesEnum.update_style: {
        EN: 'Update Theme',
        RU: 'Обновить Тему'},
    ButtonTitlesEnum.sensors: {
        EN: 'Sensors',
        RU: 'Сенсоры'},

    # ProtocolDesigner
    ButtonTitlesEnum.create_protocol: {
        EN: 'Create protocol',
        RU: 'Создать протокол'},
    ButtonTitlesEnum.create_step: {
        EN: 'Create step',
        RU: 'Создать шаг'},

    # Global
    ButtonTitlesEnum.edit: {
        EN: 'Edit',
        RU: 'Редактировать'},
    ButtonTitlesEnum.save: {
        EN: 'Save',
        RU: 'Сохранить'},
    ButtonTitlesEnum.delete: {
        EN: 'Delete',
        RU: 'Удалить'},
    ButtonTitlesEnum.update: {
        EN: 'Update',
        RU: 'Обновить'},
    ButtonTitlesEnum.create: {
        EN: 'Create',
        RU: 'Создать'},
    ButtonTitlesEnum.close: {
        EN: 'Close',
        RU: 'Закрыть'},
    ButtonTitlesEnum.add: {
        EN: 'Add',
        RU: 'Добавить'},
    ButtonTitlesEnum.select: {
        EN: 'Select',
        RU: 'Выбрать'},
    ButtonTitlesEnum.cancel: {
        EN: 'Cancel',
        RU: 'Отмена'},
    ButtonTitlesEnum.ok: {
        EN: 'OK',
        RU: 'OK'},
    ButtonTitlesEnum.accept: {
        EN: 'Accept',
        RU: 'Подтвердить'},

    # Patient table
    ButtonTitlesEnum.move_table_to_x_pos: {
        EN: 'Move to X',
        RU: 'в X'},
    ButtonTitlesEnum.move_table_to_y_pos: {
        EN: 'Move to Y',
        RU: 'в Y'},

    # Calibration
    ButtonTitlesEnum.send_to_hw: {
        EN: 'Send to hardware',
        RU: 'Отправить на оборудование'},
    ButtonTitlesEnum.clear_out_db: {
        EN: 'Clear out DB Table',
        RU: 'Очистить таблицу в БД'},

    # StudyStepTab
    ButtonTitlesEnum.select_protocol: {
        EN: 'Select protocol',
        RU: 'Выбрать протокол'},
    ButtonTitlesEnum.select_study: {
        EN: 'Select study',
        RU: 'Выбрать исследование'},

    # ReportsTab
    ButtonTitlesEnum.view_report: {
        EN: 'View report',
        RU: 'Посмотреть отчёт'},

    # DBErrorDialog
    ButtonTitlesEnum.repeat: {
        EN: 'Repeat',
        RU: 'Повторить'},

    ButtonTitlesEnum.continue_work: {
        EN: 'Continue work',
        RU: 'Продолжить работу'},

    # BoolEnum
    BoolEnum.disable: {
        EN: 'Disable',
        RU: 'Выключить'},
    BoolEnum.enable: {
        EN: 'Enable',
        RU: 'Включить'},

    # Patient positions
    PatientPositionEnum.head_to_gentry_on_back_arms_to_legs: {
        EN: 'Head to gentry on back, arms to legs',
        RU: 'Голова к Гентри на спине, руки к ногам'},
    PatientPositionEnum.head_to_gentry_on_back_arms_to_gentry: {
        EN: 'Head to gentry on back arms, to gentry',
        RU: 'Голова к Гентри на спине, руки к Гентри'},
    PatientPositionEnum.head_to_gentry_on_stomach_arms_under_head: {
        EN: 'Head to gentry on stomach, arms under head',
        RU: 'Голова на животе, руки под головой'},
    PatientPositionEnum.head_to_gentry_on_stomach_arms_to_gentry: {
        EN: 'Head to gentry on stomach, arms to gentry',
        RU: 'Голова к Гентри на животе, руки к Гентри'},
    PatientPositionEnum.head_to_gentry_on_right_side_arms_under_head: {
        EN: 'Head to gentry on right side, arms under head',
        RU: 'Голова к Гентри с правой стороны, руки под головой'},
    PatientPositionEnum.head_to_gentry_on_left_side_arms_under_head: {
        EN: 'Head to gentry on left side, arms under head',
        RU: 'Голова к Гентри на левой стороне, руки под головой'},
    PatientPositionEnum.feet_to_gentry_on_back_arms_to_gentry: {
        EN: 'Feet to gentry on back, arms to gentry',
        RU: 'Ноги в Гентри на спине, руки в Гентри'},
    PatientPositionEnum.feet_to_gentry_on_stomach_arms_under_head: {
        EN: 'Feet to gentry on stomach, arms under head',
        RU: 'Ноги на животе, руки под головой'},
    PatientPositionEnum.feet_to_gentry_on_right_side_arms_under_head: {
        EN: 'Feet to gentry on right side, arms under head',
        RU: 'Ноги в сторону Гентри с правой стороны, руки под голову'},
    PatientPositionEnum.feet_to_gentry_on_left_side_arms_under_head: {
        EN: 'Feet to gentry on left side arms under head',
        RU: 'Ноги в сторону Гентри на левой стороне руки под головой'},
    PatientPositionEnum.feet_to_gentry_on_back_arms_on_head: {
        EN: 'Feet to gentry on back, arms on head',
        RU: 'Ноги в сторону Гентри на спине, руки на голове'},

    # OperatorAccessEnum
    OperatorAccessEnum.read_only: {
        EN: 'READ ONLY',
        RU: 'ТОЛЬКО ПРОСМОТР'},
    OperatorAccessEnum.operator: {
        EN: 'Operator',
        RU: 'Оператор'},
    OperatorAccessEnum.superuser: {
        EN: 'Super user',
        RU: 'Суперпользователь'},
    OperatorAccessEnum.admin: {
        EN: 'Admin',
        RU: 'Администратор'},

    # Patient
    Patient.name: {
        EN: 'Name',
        RU: 'Имя'},
    Patient.family: {
        EN: 'Family',
        RU: 'Фамилия'},
    Patient.middle_name: {
        EN: 'Middle name',
        RU: 'Отчество'},
    Patient.date_of_birth: {
        EN: 'Date of birth',
        RU: 'Дата рождения'},
    Patient.sex: {
        EN: 'Gender',
        RU: 'Пол'},
    Patient.phantom: {
        EN: 'Phantom',
        RU: 'Фантом'},
    Patient.height: {
        EN: 'Height',
        RU: 'Высота'},
    Patient.weight: {
        EN: 'Weight',
        RU: 'Вес'},

    # Operator
    Operator.name: {
        EN: 'Name',
        RU: 'Имя'},
    Operator.access: {
        EN: 'Access',
        RU: 'Доступ'},
    Operator.password_hash: {
        EN: 'Password',
        RU: 'Пароль'},
    Operator.position: {
        EN: 'Position',
        RU: 'Позиция'},

    # InfoLabelTitles ====================================
    # Calibration
    InfoLabelTitlesEnum.calib_type: {
        EN: 'Calibration type:',
        RU: 'Тип калибровки:'},
    InfoLabelTitlesEnum.kv: {
        EN: 'kV:',
        RU: 'kV:'},
    InfoLabelTitlesEnum.ma: {
        EN: 'Beam current, mA:',
        RU: 'Ток луча, mA:'},
    InfoLabelTitlesEnum.last_time_done: {
        EN: 'Last performed:',
        RU: 'Последний запуск:'},
    InfoLabelTitlesEnum.operator: {
        EN: 'Operator:',
        RU: 'Оператор:'},

    # DBErrorDialog
    InfoLabelTitlesEnum.data_error_solution: {
        EN: 'Check the connection to CIS and repeat the request',
        RU: 'Проверьте соединение с CIS и повторите запрос'},
    InfoLabelTitlesEnum.data_error_dialog: {
        EN: 'Data handling error',
        RU: 'Ошибка работы с данными'},
    InfoLabelTitlesEnum.error_desc: {
        EN: 'Description of error',
        RU: 'Описание ошибки'},
    InfoLabelTitlesEnum.data_error_may_be_solution: {
        EN: 'A possible solution',
        RU: 'Возможное решение'},

    # ParameterPropsEnum
    ParameterPropsEnum.device_name: {
        EN: 'Device name:',
        RU: 'Имя устройства:'},
    ParameterPropsEnum.new_value: {
        EN: 'New value:',
        RU: 'Новое значение:'},
    ParameterPropsEnum.old_value: {
        EN: 'Current value:',
        RU: 'Текущее значение:'},
    ParameterPropsEnum.param_name: {
        EN: 'Parameter name:',
        RU: 'Название параметра:'},

    # TODO: Спросить насчёт имён табов
    # TabTitlesEnum
    TabTitlesEnum.patient: {
        EN: 'Patient',
        RU: 'Пациент'},
    TabTitlesEnum.planning: {
        EN: 'Planning',
        RU: 'План'},
    TabTitlesEnum.recon: {
        EN: 'Recon.',
        RU: 'Рекон.'},
    TabTitlesEnum.scan: {
        EN: 'Scan',
        RU: 'Скан'},
    TabTitlesEnum.trigger: {
        EN: 'Trigger',
        RU: 'Триггер'},
    # ProtocolDesignerTab
    TabTitlesEnum.examples_of_protocols: {
        EN: 'Examples of protocols',
        RU: 'Примеры протоколов'},
    TabTitlesEnum.user_protocols: {
        EN: 'User protocols',
        RU: 'Пользовательские протоколы'},

    # TableHeadersEnum
    # ProtocolDesignerTab
    TableHeadersEnum.protocol: {
        EN: 'Protocol',
        RU: 'Протокол'},
    TableHeadersEnum.area_kind: {
        EN: 'Area kind',
        RU: 'Вид области'},
    TableHeadersEnum.body_area: {
        EN: 'Body area',
        RU: 'Область тела'},
    TableHeadersEnum.step_name: {
        EN: 'Step name',
        RU: 'Имя шага'},
    TableHeadersEnum.scan_type: {
        EN: 'Scan type',
        RU: 'Тип сканирования'},
    # ReportsTab
    TableHeadersEnum.date_time: {
        EN: 'Date and time',
        RU: 'Дата и время'},
    TableHeadersEnum.full_name: {
        EN: 'Fullname',
        RU: 'Ф.И.О.'},
    TableHeadersEnum.dicom_file: {
        EN: 'DICOM-file',
        RU: 'DICOM-файл'},
    # DoseLogsTab
    TableHeadersEnum.date: {
        EN: 'Date',
        RU: 'Дата'},
    TableHeadersEnum.date_of_birth: {
        EN: 'Date of birth',
        RU: 'Дата рождения'},
    TableHeadersEnum.gy: {
        EN: 'Gy',
        RU: 'Гр'},
    TableHeadersEnum.sv_on_cm: {
        EN: 'Sv*cm',
        RU: 'Зв*см'},
    # PatientsTab
    TableHeadersEnum.name: {
        EN: 'Name',
        RU: 'Имя'},
    TableHeadersEnum.family: {
        EN: 'Family',
        RU: 'Фамилия'},
    TableHeadersEnum.middle_name: {
        EN: 'Middle name',
        RU: 'Отчество'},
    TableHeadersEnum.gender: {
        EN: 'Gender',
        RU: 'Пол'},
    # OperatorsTab
    TableHeadersEnum.access: {
        EN: 'Access',
        RU: 'Доступ'},
    # MaintenanceTab
    TableHeadersEnum.parameter: {
        EN: 'Parameter',
        RU: 'Параметр'},
    TableHeadersEnum.value: {
        EN: 'Value',
        RU: 'Значение'},
    TableHeadersEnum.time_done: {
        EN: 'Time done',
        RU: 'Время выполнения'},
    TableHeadersEnum.report_data: {
        EN: 'Report data',
        RU: 'Данные отчёта'},

    ErrorsEnum.get_studies_with_patients: {
        EN: 'Failed to obtain study and patient data',
        RU: 'Не удалось получить данные об исследованиях и пациентах'},
    ErrorsEnum.get_study_protocols: {
        EN: 'Failed to obtain study protocols',
        RU: 'Не удалось получить протоколы исследования'},
    ErrorsEnum.get_study_steps: {
        EN: 'It was not possible to get data on the steps of the study',
        RU: 'Не удалось получить данные о шагах исследования'},
    ErrorsEnum.get_study_by_id: {
        EN: 'Unable to retrieve study data by ID',
        RU: 'Не удалось получить данные исследования по ID'},
    ErrorsEnum.get_all_study_steps: {
        EN: 'Failed to get step data from all studies',
        RU: 'Не удалось получить данные о шагах со всех исследований'},

    ErrorsEnum.get_protocols: {
        EN: 'Failed to retrieve protocol data',
        RU: 'Не удалось получить данные о протоколах'},
    ErrorsEnum.save_protocol: {
        EN: 'Failed to save protocol data',
        RU: 'Не удалось сохранить данные протокола'},
    ErrorsEnum.delete_protocol: {
        EN: 'Failed to delete protocol data',
        RU: 'Не удалось удалить данные протокола'},

    ErrorsEnum.save_protocol_step: {
        EN: 'Failed to save protocol step data',
        RU: 'Не удалось сохранить данные шага протокола'},
    ErrorsEnum.get_protocol_steps: {
        EN: 'Failed to retrieve protocol step data',
        RU: 'Не удалось получить данные шага протокола'},
    ErrorsEnum.delete_protocol_step: {
        EN: 'Failed to delete protocol step data',
        RU: 'Не удалось удалить данные шага протокола'},

    ErrorsEnum.get_operators: {
        EN: 'Failed to get a list of operator data',
        RU: 'Не удалось получить список данных об операторах'},
    ErrorsEnum.get_operator_by_id: {
        EN: 'Failed to retrieve operator data by ID',
        RU: 'Не удалось получить данные оператора по ID'},
    ErrorsEnum.delete_operator: {
        EN: 'Failed to delete operator data',
        RU: 'Не удалось удалить данные оператора'},
    ErrorsEnum.save_operator: {
        EN: 'Failed to save operator data',
        RU: 'Не удалось сохранить данные об операторе'},

    ErrorsEnum.save_patient: {
        EN: 'Failed to save patient data',
        RU: 'Не удалось сохранить данные о пациенте'},
    ErrorsEnum.get_patients: {
        EN: 'Failed to retrieve patient data',
        RU: 'Не удалось получить данные о пациенте'},
    ErrorsEnum.get_patient_by_id: {
        EN: 'Failed to retrieve patient ID data',
        RU: 'Не удалось получить данные о пациенте по его ID'},

    ErrorsEnum.get_hw_tests: {
        EN: 'Unable to get a list of equipment tests',
        RU: 'Не удалось получить список тестов оборудования'},
    ErrorsEnum.get_calib_reports: {
        EN: 'Failed to get a list of calibration reports',
        RU: 'Не удалось получить список отчётов о калибровках'},

    ErrorsEnum.fill_out_calib_table: {
        EN: 'Failed to fill test all calibration data',
        RU: 'Не удалось заполнить тестовые все данные о калибровках'},
    ErrorsEnum.clear_out_calib_table: {
        EN: 'Failed to delete all calibration data',
        RU: 'Не удалось удалить все данные о калибровках'},

    AcceptActionsEnum.delete_protocol: {
        EN: 'Do you really want to delete the protocol?',
        RU: 'Вы действительно хотите удалить протокол?'},
    AcceptActionsEnum.delete_protocol_step: {
        EN: 'Do you really want to delete the protocol step?',
        RU: 'Вы действительно хотите удалить шаг протокола?'},
    AcceptActionsEnum.delete_operator: {
        EN: 'Do you really want to remove the operator?',
        RU: 'Вы действительно хотите удалить оператора?'}
}
