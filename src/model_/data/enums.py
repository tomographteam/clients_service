import enum
from enum import Enum


class DialogTitlesEnum(Enum):
    # ProtocolOptsTab
    trigger_options = 0
    reconstruction_options = enum.auto()
    planing_options = enum.auto()
    scan_options = enum.auto()

    this_scan = enum.auto()
    all_scans = enum.auto()
    evaluation = enum.auto()
    gy = enum.auto()
    sv_on_cm = enum.auto()

    cis_disconnected = enum.auto()
    accept_dialog = enum.auto()


class InfoLabelTitlesEnum(Enum):
    # Calib
    kv = 0
    ma = enum.auto()
    operator = enum.auto()
    last_time_done = enum.auto()
    calib_type = enum.auto()

    # DBErrorDialog
    db_error_solution = enum.auto()
    data_error_dialog = enum.auto()
    data_error_solution = enum.auto()
    data_error_may_be_solution = enum.auto()
    error_desc = enum.auto()


class LanguagesEnum(Enum):
    RU = 0
    EN = 1


class AutoCalculatedEnum(enum.Enum):
    collimator = 0
    table_speed = 1


class KernelEnum(enum.Enum):
    standard = 0
    langs = enum.auto()
    bones = enum.auto()


class FBPMethodEnum(enum.Enum):
    fbp_only = 0
    fbp_1_sart = enum.auto()
    fbp_2_sart = enum.auto()
    fbp_3_sart = enum.auto()
    fbp_4_sart = enum.auto()
    fbp_5_sart = enum.auto()


class MetalCorrectionEnum(enum.Enum):
    disabled = 0
    enabled = enum.auto()


class DASMethodEnum(enum.Enum):
    das_1 = 0
    das_2 = enum.auto()
    das_1_2 = enum.auto()
    file = enum.auto()


class ResolutionEnum(enum.Enum):
    res256 = 0
    res512 = enum.auto()
    res1024 = enum.auto()
    res2048 = enum.auto()


class PatientPositionEnum(enum.Enum):
    # 1. Головой к гентри Лежа на спине руки к ногам
    head_to_gentry_on_back_arms_to_legs = 0
    # 2. Головой к гентри Лежа на спине руки к гентри
    head_to_gentry_on_back_arms_to_gentry = enum.auto()
    # 3. Головой к гентри Лежа на животе руки под голову (лоб упирается в предплечья)
    head_to_gentry_on_stomach_arms_under_head = enum.auto()
    # 4. Головой к гентри Лежа на животе руки к гентри
    head_to_gentry_on_stomach_arms_to_gentry = enum.auto()
    # 5. Головой к гентри Лежа на правом боку руки под голову
    head_to_gentry_on_right_side_arms_under_head = enum.auto()
    # 6. Головой к гентри Лежа на левом боку руки под голову
    head_to_gentry_on_left_side_arms_under_head = enum.auto()
    # 7. Ногами к гентри Лежа на спине руки к гентри
    feet_to_gentry_on_back_arms_to_gentry = enum.auto()
    # 8. Ногами к гентри Лежа на животе руки под голову (лоб упирается в предплечья)
    feet_to_gentry_on_stomach_arms_under_head = enum.auto()
    # 9. Ногами к гентри Лежа на правом боку руки под голову
    feet_to_gentry_on_right_side_arms_under_head = enum.auto()
    # 10. Ногами к гентри Лежа на левом боку руки под голову
    feet_to_gentry_on_left_side_arms_under_head = enum.auto()
    # 11. на спине/ногами в гентри/руки над головой
    feet_to_gentry_on_back_arms_on_head = enum.auto()

    @classmethod
    def get_by_img_name(cls, img_name: str) -> "PatientPositionEnum":
        dict_ = {
            "head_to_gentry_on_back_arms_to_legs": cls.head_to_gentry_on_back_arms_to_legs,
            "head_to_gentry_on_back_arms_to_gentry": cls.head_to_gentry_on_back_arms_to_gentry,
            "head_to_gentry_on_stomach_arms_under_head": cls.head_to_gentry_on_stomach_arms_under_head,
            "head_to_gentry_on_stomach_arms_to_gentry": cls.head_to_gentry_on_stomach_arms_to_gentry,
            "head_to_gentry_on_right_side_arms_under_head": cls.head_to_gentry_on_right_side_arms_under_head,
            "head_to_gentry_on_left_side_arms_under_head": cls.head_to_gentry_on_left_side_arms_under_head,
            "feet_to_gentry_on_back_arms_to_gentry": cls.feet_to_gentry_on_back_arms_to_gentry,
            "feet_to_gentry_on_stomach_arms_under_head": cls.feet_to_gentry_on_stomach_arms_under_head,
            "feet_to_gentry_on_right_side_arms_under_head": cls.feet_to_gentry_on_right_side_arms_under_head,
            "feet_to_gentry_on_left_side_arms_under_head": cls.feet_to_gentry_on_left_side_arms_under_head,
            "feet_to_gentry_on_back_arms_on_head": cls.feet_to_gentry_on_back_arms_on_head,
        }
        return dict_.get(img_name)


class ScanTypeEnum(enum.Enum):
    scout_two_side = 0
    helical = enum.auto()
    trigger_start = enum.auto()
    area_select = enum.auto()
    fluoro = enum.auto()
    trigger_start_first_slice = enum.auto()
    cardiac = enum.auto()
    radiology = enum.auto()
    hi_res = enum.auto()
    flow = enum.auto()
    cine = enum.auto()
    dark_calib = enum.auto()
    air_calib = enum.auto()
    multi_Pin_Calib = enum.auto()
    upper_half = enum.auto()
    bad_tile_detection = enum.auto()
    water_calib = enum.auto()
    preprocess_only = enum.auto()
    xray_tuning = enum.auto()
    null_processing = enum.auto()


class ContrastTypeEnum(enum.Enum):
    no_contrast = 0
    contrast_1 = enum.auto()


class ContrastDensityEnum(enum.Enum):
    d300 = 0
    d350 = enum.auto()
    d370 = enum.auto()
    d400 = enum.auto()


class TableMotionEnum(enum.Enum):
    moving = 0
    stationary = enum.auto()


class BodyAreaEnum(enum.Enum):
    head = 0
    neck = enum.auto()
    chest = enum.auto()
    abdominal = enum.auto()
    pelvis = enum.auto()
    limbs = enum.auto()


class AreaKindEnum(enum.Enum):
    adult = 0
    trauma = enum.auto()


class ControlButtonsEnum(enum.Enum):
    next = 0
    prev = enum.auto()
    send_to_hw = enum.auto()
    start_scan = enum.auto()
    stop_scan = enum.auto()
    pause_scan = enum.auto()

    # Maintenance
    run_hardware_test = enum.auto()
    morning_startup = enum.auto()
    evening_shutdown = enum.auto()

    skip_step = enum.auto()
    add_step = enum.auto()
    select_study = enum.auto()
    import_protocol = enum.auto()
    show_report = enum.auto()

    # Patient table
    move_table_forward = enum.auto()
    move_table_backward = enum.auto()
    move_table_up = enum.auto()
    move_table_down = enum.auto()
    stop_table = enum.auto()
    move_table_to_x = enum.auto()
    move_table_to_y = enum.auto()


class ButtonTitlesEnum(enum.Enum):
    # TopRightWidget | MainMenu
    en = 0
    ru = enum.auto()
    patient_table = enum.auto()
    sensors = enum.auto()
    update_style = enum.auto()
    main_menu = enum.auto()
    calib = enum.auto()

    # Patient table
    move_table_to_x_pos = enum.auto()
    move_table_to_y_pos = enum.auto()

    # Global
    edit = enum.auto()
    update = enum.auto()
    save = enum.auto()
    delete = enum.auto()
    create = enum.auto()
    close = enum.auto()
    add = enum.auto()
    cancel = enum.auto()
    select = enum.auto()
    accept = enum.auto()
    ok = enum.auto()

    # ProtocolDesigner
    create_protocol = enum.auto()
    create_step = enum.auto()

    # Calib
    fill_out_db = enum.auto()
    clear_out_db = enum.auto()
    send_to_hw = enum.auto()

    # StudyStepTab
    select_study = enum.auto()
    select_protocol = enum.auto()

    # ReportsTab
    view_report = enum.auto()

    # DBErrorDialog
    repeat = enum.auto()

    # CISDisconnectedDialog
    continue_work = enum.auto()


class StepStateEnum(enum.Enum):
    edit = 0  # initial state, step fields can be updated
    data_sent = enum.auto()  # allow run scan
    run = enum.auto()  # running until done
    done = enum.auto()  # all data frozen, edit and delete disabled


class BoolEnum(enum.Enum):
    disable = 0
    enable = enum.auto()


class ProtocolTabsEnum(enum.Enum):
    protocol = 0
    recon = enum.auto()
    contrast = enum.auto()
    cardio = enum.auto()
    desc = enum.auto()


class OperatorAccessEnum(enum.Enum):
    read_only = 0
    operator = enum.auto()
    superuser = enum.auto()
    admin = enum.auto()


class ParameterPropsEnum(enum.Enum):
    device_name = 0
    param_name = enum.auto()
    old_value = enum.auto()
    new_value = enum.auto()


class CalibTypesEnum(enum.Enum):
    air = 0
    dark = enum.auto()
    water = enum.auto()
    multi_pin = enum.auto()


class MainWindowTabsEnum(enum.Enum):
    scan = 0
    protocol_designer = enum.auto()
    reports = enum.auto()
    dose_logs = enum.auto()
    patients = enum.auto()
    operators = enum.auto()
    maintenance = enum.auto()


# Сделал отдельный enum,
# чтобы можно было спокойно менять
# локализацию, не боясь,
# что она слетит где-то в другом месте
class TableHeadersEnum(enum.Enum):
    # ProtocolDesignerTab
    protocol = 0
    area_kind = enum.auto()
    body_area = enum.auto()
    step_name = enum.auto()
    scan_type = enum.auto()

    # RepostsTab
    date_time = enum.auto()
    full_name = enum.auto()
    dicom_file = enum.auto()

    # DoseLogsTab
    date = enum.auto()
    date_of_birth = enum.auto()
    gy = enum.auto()
    sv_on_cm = enum.auto()

    # PatientsTab
    name = enum.auto()
    family = enum.auto()
    middle_name = enum.auto()
    gender = enum.auto()

    # OperatorsTab
    access = enum.auto()

    # MaintenanceTab
    parameter = enum.auto()
    value = enum.auto()
    time_done = enum.auto()
    report_data = enum.auto()


class TabTitlesEnum(enum.Enum):
    # ProtocolOptsTabWidget
    patient = 0
    scan = enum.auto()
    recon = enum.auto()
    trigger = enum.auto()
    planning = enum.auto()

    # ProtocolDesignerTab
    examples_of_protocols = enum.auto()
    user_protocols = enum.auto()


class ErrorsEnum(enum.Enum):
    get_studies_with_patients = 0
    get_study_protocols = enum.auto()
    get_study_steps = enum.auto()
    get_study_by_id = enum.auto()
    get_all_study_steps = enum.auto()

    get_protocols = enum.auto()
    save_protocol = enum.auto()
    delete_protocol = enum.auto()

    save_protocol_step = enum.auto()
    get_protocol_steps = enum.auto()
    delete_protocol_step = enum.auto()

    get_operators = enum.auto()
    get_operator_by_id = enum.auto()
    delete_operator = enum.auto()
    save_operator = enum.auto()

    save_patient = enum.auto()
    get_patients = enum.auto()
    get_patient_by_id = enum.auto()

    get_hw_tests = enum.auto()
    get_calib_reports = enum.auto()

    fill_out_calib_table = enum.auto()
    clear_out_calib_table = enum.auto()


class AcceptActionsEnum(enum.Enum):
    delete_protocol = 0
    delete_protocol_step = enum.auto()
    delete_operator = enum.auto()
