from typing import Dict


class StyleManager:
    @staticmethod
    def render(style: str, palette: Dict[str, str]):
        for key, value in palette.items():
            style = style.replace(key, value)
        return style
