import glob
import json
import os
from typing import Dict, List, Union

from src.utils.singleton import singleton
from src.model_.managers.config_manager import IConfigManager, ConfigManager

cfg_manager: IConfigManager = ConfigManager()

KVMAListItemType = Dict[str, Dict[str, Union[int, float]]]
IntList = List[int]
NumberList = List[Union[float, int]]
PaletteDataset = Dict[str, Union[str, Dict[str, Dict[str, str]]]]


@singleton
class ResourceManager:
    def __init__(self) -> None:
        self._resource_path = cfg_manager.get_resource_path()

        with open(cfg_manager.get_protocol_json_path(), 'r',
                  encoding='utf-8') as f:
            json_ = json.load(f, parse_int=int, parse_float=float)
            self._kv_list: IntList = json_['kv_list']
            self._ma_list: IntList = json_['ma_list']
            self._pitch_list: NumberList = json_['pitch_list']
            self._kv_ma_dataset: List[KVMAListItemType] = json_['kv_ma']

    def get_kv_list(self) -> IntList:
        return self._kv_list

    def get_ma_list(self) -> IntList:
        return self._ma_list

    def get_pitch_list(self) -> NumberList:
        return self._pitch_list

    def get_kv_ma_list(self) -> List[KVMAListItemType]:
        return self._kv_ma_dataset

    def get_scan_area_images_dataset(self) -> Dict[str, str]:
        # TODO Нужно отрефакторить
        img_dir = cfg_manager.get_scan_area_images_path()
        img_list = [img for img in glob.glob(img_dir + '/*.png')
                    if not img.endswith('overlay.png')]
        dataset = {os.path.basename(img).replace('.png', ''):
                   img for img in img_list}
        return dataset

    def get_patient_pos_dataset(self) -> Dict[str, str]:
        # TODO Нужно отрефакторить
        img_dir = cfg_manager.get_patient_pos_images_path()
        img_list = glob.glob(img_dir + '/*.png')
        dataset = {os.path.basename(img).replace('.png', ''):
                   img for img in img_list}
        return dataset

    # TODO: Добавить типы
    def get_palette_dataset(self) -> PaletteDataset:
        palette_dataset: PaletteDataset = {}
        for fp in glob.glob(cfg_manager.get_palettes_path() + '/*.json'):
            with open(fp, 'r', encoding='utf-8') as f:
                palette_dataset.update({
                    os.path.basename(fp).replace('.json', ''): {
                        'path': fp,
                        'palette': json.load(f)}
                })
        return palette_dataset

    def get_qss(self) -> str:
        with open(cfg_manager.get_qss_path(), 'r', encoding='utf-8') as f:
            return f.read()


class IResourceManager:
    def get_kv_list(self) -> IntList:
        pass

    def get_ma_list(self) -> IntList:
        pass

    def get_pitch_list(self) -> NumberList:
        pass

    def get_kv_ma_list(self) -> List[KVMAListItemType]:
        pass

    def get_scan_area_images_dataset(self) -> Dict[str, str]:
        pass

    def get_patient_pos_dataset(self) -> Dict[str, str]:
        pass

    def get_palette_dataset(self) -> PaletteDataset:
        pass

    def get_qss(self) -> str:
        pass
