import os
from configparser import ConfigParser
from src.utils.singleton import singleton

ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../../../")


@singleton
class ConfigManager:
    cfg_path = os.path.join(ROOT_DIR, "config/config.ini")

    def __init__(self):
        self._cfg = ConfigParser()
        self._cfg.read(self.cfg_path)

        self._resource_path = os.path.join(ROOT_DIR, "resources")
        self._images_path = self._cfg['PATHS'].get('images', '/images')
        self._protocol_json_path = self._cfg['PATHS'].get('protocol_json',
                                                          'protocol.json')
        self._style_path = self._cfg['PATHS'].get('style', '/style')
        self._palettes_path = self._cfg['PATHS'].get('palettes', '/palettes')
        self._qss_path = self._cfg['PATHS'].get('qss', '/qss')

    def get_scan_area_images_path(self) -> str:
        return f'{self._images_path}/scan_areas'

    def get_human_image_path(self) -> str:
        return f'{self._images_path}/scan_areas/full_body/human.png'

    def get_patient_pos_images_path(self) -> str:
        return f'{self._images_path}/patient_position'

    def get_protocol_json_path(self) -> str:
        return f'{self._resource_path}/{self._protocol_json_path}'

    def get_resource_path(self) -> str:
        return self._resource_path

    def get_style_path(self) -> str:
        return f'{self._resource_path}/{self._style_path}'

    def get_palettes_path(self) -> str:
        return f'{self.get_style_path()}/{self._palettes_path}'

    def get_qss_path(self) -> str:
        return f'{self.get_style_path()}/{self._qss_path}'


class IConfigManager:
    def get_resource_path(self) -> str:
        pass

    def get_scan_area_images_path(self) -> str:
        pass

    def get_protocol_json_path(self) -> str:
        pass

    def get_patient_pos_images_path(self) -> str:
        pass

    def get_style_path(self) -> str:
        pass

    def get_palettes_path(self) -> str:
        pass

    def get_qss_path(self) -> str:
        pass

    def get_human_image_path(self) -> str:
        pass
