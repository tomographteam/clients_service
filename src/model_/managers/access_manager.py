from typing import Optional
from src.model import Operator
from src.model_.data.enums import OperatorAccessEnum
from src.utils.singleton import singleton
from PyQt5.QtCore import QObject, pyqtSignal


@singleton
class AccessManager(QObject):
    access_changed = pyqtSignal(object)  # OperatorAccessEnum

    def __init__(self):
        super().__init__()
        self._access_type: Optional[OperatorAccessEnum] = None

    def get_access(self) -> OperatorAccessEnum:
        return self._access_type

    def set_access(self, access: OperatorAccessEnum):
        if access is None:
            access = OperatorAccessEnum.read_only
        self._access_type = access
        self.access_changed.emit(access)
