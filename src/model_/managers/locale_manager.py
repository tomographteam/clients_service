from enum import Enum

from PyQt5.QtCore import pyqtSignal, QObject
from typing import Any, Iterable, List, Union

from src.model_.data.enums import LanguagesEnum
from src.model_.data.locale_maps import MAP
from src.utils.singleton import singleton


@singleton
class LocaleManager(QObject):
    need_translate = pyqtSignal()

    def __init__(self) -> None:
        super().__init__()
        self._current_locale = LanguagesEnum.RU

    def lang_changed(self, lang: LanguagesEnum):
        self._current_locale = lang
        self.need_translate.emit()

    def locale(self, key_obj: Union[Enum, Any]) -> str:
        return (MAP.get(key_obj,
                {self._current_locale: repr(key_obj)})[self._current_locale])


def locale(key_obj: Union[Enum, Any]) -> str:
    return LocaleManager().locale(key_obj)


def multi_locale(key_objs: Iterable[Union[Enum, Any]]) -> List[str]:
    return [locale(k) for k in key_objs]
