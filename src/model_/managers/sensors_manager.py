import sys

import aiohttp
import asyncio
import socket
import numpy as np
import time
import os

from PyQt5.QtCore import QObject, pyqtSignal
import json

from asyncqt import QtCore

from src.utils.singleton import singleton
from src.view.main_window.global_ui.status_bar import StatusBar


class ZeroBytesException(Exception):
    pass


class ECGMonitor(QtCore.QThread):


    # Create the signal
    blood_updated = QtCore.pyqtSignal(np.ndarray)

    host = os.environ.get('ECG_IP', "localhost")
    port = int(os.environ.get('ECG_PORT', 17040))

    def __init__(self, parent=None):
        super(ECGMonitor, self).__init__(parent)

    def run(self):

        while True:

            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(1)
                print("connecting", (self.host, self.port))
                s.connect((self.host, self.port))

                xon_message = bytes([0x11])
                s.send(xon_message)

                self.blood_data = np.empty((3, 4500), dtype=float)
                self.blood_data[:] = np.nan
                offset = 0

                while True:
                    try:
                        msg_bytes = bytes()
                        while len(msg_bytes) < 128:
                            new_bytes = s.recv(256)
                            if not new_bytes:
                                raise ZeroBytesException("Zero bytes received")
                            msg_bytes += new_bytes
                        # assert len(msg_bytes) == 128
                        array = np.frombuffer(msg_bytes, np.uint16)[3:63]
                        xray = np.bitwise_and(array, 0x80) >> 7
                        trigger = np.bitwise_and(array, 0x20) >> 5
                        heart = (np.bitwise_and(array, 0x7f00) >> 8) +  (np.bitwise_and(array, 0x1f) << 7)

                        self.blood_data[0, offset:offset+60] = trigger
                        self.blood_data[1, offset:offset+60] = xray
                        self.blood_data[2, offset:offset+60] = heart

                        self.blood_data[:, offset+60:offset+360] = np.nan
                        offset = (offset + 60) % self.blood_data.shape[1]

                        self.blood_updated.emit(self.blood_data)
                        # status_dict[self._get_blood] = True
                        msg_bytes = msg_bytes[128:]

                    except KeyboardInterrupt:
                        s.close()
                        return
                    except Exception as e:
                        print("ECGMonitor", e)
                        exc_type, exc_obj, exc_tb = sys.exc_info()
                        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                        print(exc_type, fname, exc_tb.tb_lineno)
                        time.sleep(5)
                        break
            except Exception as e:
                print("ECGMonitor", e)
                time.sleep(5)
                continue


@singleton
class SensorsManager(QObject):

    contrast_updated = pyqtSignal(dict)
    blood_updated = pyqtSignal(np.ndarray)
    oxygen_updated = pyqtSignal(dict)
    breath_updated = pyqtSignal(list)

    url_template = "http://127.0.0.1:8002{}"
    contrast_state_url = url_template.format("/contrast_injector/state/")
    blood_state_url = url_template.format("/blood_pressure/state/")
    oxygen_state_url = url_template.format("/oxygen/state/")
    breath_state_url = url_template.format("/breath_monitor/stream/")

    SET_CONTRAST_FLOW_SPEED = "/contrast_injector/set_contrast_flow_speed/{flow_speed}"
    SET_SOLVENT_FLOW_SPEED = "/contrast_injector/set_solvent_flow_speed/{flow_speed}"
    CONTRAST_INJECTOR_STOP = "/contrast_injector/contrast_injector_stop/"
    CONTRAST_INJECTOR_POUR = "/contrast_injector/contrast_injector_pour/"

    def __init__(self):

        super().__init__()

        self.contrast_data = {}
        self.blood_data = {}
        self.oxygen_data = {}
        self.breath_data = {}

        loop = asyncio.get_event_loop()
        loop.create_task(self._get_contrast())
        #loop.create_task(self._get_blood())
        self.ecg_thread = ECGMonitor()
        self.ecg_thread.blood_updated.connect(self.blood_updated)
        self.ecg_thread.start()

        loop.create_task(self._get_oxygen())
        loop.create_task(self._get_breath())
        loop.create_task(self._integrate_status())

        self.status_dict = {
            self._get_contrast: False,
            #self._get_blood: False,
            self._get_oxygen: False,
            self._get_breath: False,
        }

    async def _integrate_status(self):
        while True:
            StatusBar().update_sensors_status(all(self.status_dict.values()))
            await asyncio.sleep(1)

    async def _get_contrast(self):

        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(self.contrast_state_url) as resp:
                        self.contrast_data = await resp.json()

                    self.contrast_updated.emit(self.contrast_data)
                    self.status_dict[self._get_contrast] = True
                    await asyncio.sleep(1)

            except Exception as e:
                self.status_dict[self._get_contrast] = False
                await asyncio.sleep(3)


    async def _get_oxygen(self):

        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(self.blood_state_url) as resp:
                        self.oxygen_data = await resp.json()

                    self.oxygen_updated.emit(self.oxygen_data)
                    self.status_dict[self._get_oxygen] = True
                    await asyncio.sleep(1)

            except Exception as e:
                self.status_dict[self._get_oxygen] = False
                await asyncio.sleep(3)

    async def _get_breath(self):

        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(self.breath_state_url) as resp:
                        async for line in resp.content:
                            self.breath_data = json.loads(line.decode())

                            self.breath_updated.emit(self.breath_data)
                            self.status_dict[self._get_breath] = True

            except Exception as e:
                self.status_dict[self._get_breath] = False
                await asyncio.sleep(3)
