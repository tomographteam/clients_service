from typing import TYPE_CHECKING, Union

if TYPE_CHECKING:
    from src.db_manager import DataBaseManager
    from src.model_.managers.resource_manager import ResourceManager


ManagerType = Union["DataBaseManager", "ResourceManager"]


class _SingletonWrapper:
    def __init__(self, cls):
        self.__wrapped__ = cls
        self._instance = None

    def __call__(self, *args, **kwargs) -> ManagerType:
        if self._instance is None:
            self._instance = self.__wrapped__(*args, **kwargs)
        return self._instance


def singleton(cls: ManagerType) -> ManagerType:
    return _SingletonWrapper(cls)


if __name__ == "__main__":

    @singleton
    class A:
        def __init__(self, ff="gg"):
            pass

        def gg(self):
            pass

    a: DataBaseManager = A(343)
    b = A()
    print(a is b)
