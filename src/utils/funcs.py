import re


def is_float(element: str) -> bool:
    if re.match(r"^-?\d+(?:\.\d+)$", element) is None:
        return False
    return True
