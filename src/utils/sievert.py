from src.model_.data.enums import BodyAreaEnum


def sievert_age_factor(age_in_months):

    if age_in_months >= 17 * 12:
        return {
            BodyAreaEnum.head: 1,
            BodyAreaEnum.neck: 1,
            BodyAreaEnum.chest: 1,
            BodyAreaEnum.pelvis: 1,
            BodyAreaEnum.abdominal: 1,
            BodyAreaEnum.limbs: 1,
        }

    if age_in_months >= 13 * 12:
        return {
            BodyAreaEnum.head: 1.1,
            BodyAreaEnum.neck: 1.1,
            BodyAreaEnum.chest: 1.1,
            BodyAreaEnum.pelvis: 1.1,
            BodyAreaEnum.abdominal: 1.1,
            BodyAreaEnum.limbs: 1.1,
        }

    if age_in_months >= 8 * 12:
        return {
            BodyAreaEnum.head: 1.3,
            BodyAreaEnum.neck: 1.3,
            BodyAreaEnum.chest: 1.4,
            BodyAreaEnum.pelvis: 1.5,
            BodyAreaEnum.abdominal: 1.5,
            BodyAreaEnum.limbs: 1.5,  # todo уточнить
        }
    if age_in_months >= 3 * 12:
        return {
            BodyAreaEnum.head: 1.7,
            BodyAreaEnum.neck: 1.7,
            BodyAreaEnum.chest: 1.6,
            BodyAreaEnum.pelvis: 1.6,
            BodyAreaEnum.abdominal: 1.6,
            BodyAreaEnum.limbs: 1.6,  # todo уточнить
        }
    if age_in_months >= 6:
        return {
            BodyAreaEnum.head: 2.2,
            BodyAreaEnum.neck: 2.2,
            BodyAreaEnum.chest: 1.9,
            BodyAreaEnum.pelvis: 2,
            BodyAreaEnum.abdominal: 2,
            BodyAreaEnum.limbs: 2,  # todo уточнить
        }

    return {
        BodyAreaEnum.head: 2.6,
        BodyAreaEnum.neck: 2.6,
        BodyAreaEnum.chest: 2.2,
        BodyAreaEnum.pelvis: 2.4,
        BodyAreaEnum.abdominal: 2.4,
        BodyAreaEnum.limbs: 2.4,  # todo уточнить
    }


sievert_factor = {
    BodyAreaEnum.head: 0.0023,
    BodyAreaEnum.neck: 0.0054,
    BodyAreaEnum.chest: 0.017,
    BodyAreaEnum.abdominal: 0.015,
    BodyAreaEnum.pelvis: 0.019,
    BodyAreaEnum.limbs: 0.0082,
}
