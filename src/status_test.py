import aiohttp
import asyncio


url_template = 'http://0.0.0.0:10001/tango/rest/v11/hosts/127.0.0.1/devices/{request}'


async def main():

    all_attributes = {}
    all_data = {}

    async with aiohttp.ClientSession() as session:
        async with session.get(url_template.format(request='devices')) as resp:
            devices = await resp.json()

        for device in devices:
            async with session.get(url_template.format(request=f"{device}/attributes")) as resp:
                all_attributes[device] = "&".join([f"={a['name']}" for a in await resp.json()])

        while True:
            for device, attributes_query in all_attributes.items():
                async with session.get(url_template.format(
                        request=f"{device}/attributes/value?{attributes_query}")) as resp:
                    all_data[device] = await resp.json()

            print(all_data)
            await asyncio.sleep(1)

    return all_data

asyncio.run(main())
