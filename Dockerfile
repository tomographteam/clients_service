# syntax=docker/dockerfile:1

FROM jozo/pyqt5

WORKDIR .

RUN apt-get update && apt-get install -y \
    python3-pip fonts-liberation ttf-mscorefonts-installer && \
     fc-cache -f

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY --chown=qtuser:qtuser . .

ENV CIS_IP=localhost
ENV CIS_PORT=8082
ENV SCSC_IP=localhost
ENV SCSC_PORT=8080
ENV ECG_IP=localhost
ENV ECG_PORT=17040

ENTRYPOINT ["python3"]