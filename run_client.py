import asyncio
import os
import sys

from PyQt5.QtWidgets import QApplication, QShortcut
from PyQt5.QtGui import QFont, QKeySequence
from asyncqt import QEventLoop
from src.model_.managers.resource_manager import IResourceManager, ResourceManager
from src.model_.managers.style_manager import StyleManager

from src.model_manager import ModelManager
from src.view.main_window.main_window import MainWindow
from asyncqt import QEventLoop

resource_manager: IResourceManager = ResourceManager()


def main():
    app = QApplication(sys.argv)

    # app.setStyleSheet(ResourceManager().get_qss())
    # QFontDatabase.addApplicationFont('/resources/fonts/liberation-mono.ttf')
    app.setStyleSheet(
        StyleManager.render(
            ResourceManager().get_qss(),
            ResourceManager().get_palette_dataset()['gray']['palette'])
        )
    app.setFont(QFont('Arial'))

    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)

    client = MainWindow()
    client.setMinimumSize(1800, 900)
    client.showMaximized()
    client.showFullScreen()

    client.shortcut_open = QShortcut(QKeySequence('Ctrl+Shift+Del'), client)
    client.shortcut_open.activated.connect(quit)

    with loop:
        loop.create_task(ModelManager().state_update())
        loop.run_forever()


if __name__ == '__main__':
    main()
