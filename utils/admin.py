from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, session, g
from flask_admin import Admin
from sqlalchemy import event
from slugify import slugify
import os

db_path = os.path.join(os.path.dirname(__file__), '../db/tom.db')
db_uri = 'sqlite:///{}'.format(db_path)


app = Flask(__name__)
app.config.update({
    "SQLALCHEMY_TRACK_MODIFICATIONS": False,
    "SQLALCHEMY_DATABASE_URI": db_uri
})

db = SQLAlchemy(app)
admin = Admin(app, url="/", template_mode="bootstrap4")



@app.shell_context_processor
def load_url_objects(endpoint, values):
    if not values:
        return
    if "portal_slug" in values:
        g.portal = Portal.query.filter_by(slug=values.pop("portal_slug", None)).first()
        if g.portal:
            # save portal_id to session for top navbar link routes
            session["portal_id"] = g.portal.id
            if "account_id" in values:
                g.account = g.portal.accounts.filter_by(id=values.pop("account_id", None)).first()


class PortalView(ModelView):
    pass  # /portals


from src.model import Patient

admin.add_view(PortalView(Patient, db.session, name="Patient", endpoint="portals"))
#admin.add_view(AccountView(Account, db.session, name="Accounts", endpoint="accounts"))


@app.before_first_request
def prep_db():
    db.create_all()


if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.run(debug=True)