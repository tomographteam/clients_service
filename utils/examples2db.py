from src.model import Protocol, ProtocolStep, Base, Study, Patient, Operator

import glob
import os
import json
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from tabulate import tabulate

# new db
db_path = os.path.join(os.path.dirname(__file__), '../db/tom.db')
engine = create_engine(f'sqlite:///{db_path}')
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

with Session(engine) as session:
    examples_path = os.path.join(os.path.dirname(__file__), '../resources/protocol_examples/*.json')
    for example in glob.glob(examples_path):

        with open(example, "r") as  f:
            data = json.load(f)
            s = Protocol.from_examples_dict(data)
            session.add(s)

    session.commit()

    result = [(p.name, p.scan_type, s.name) for p, s in session.query(ProtocolStep, Protocol).join(Protocol).order_by(ProtocolStep.protocol_id).all()]
    print(tabulate(result, headers=["Protocol Step", "Scan type", "Protocol"], tablefmt="orgtbl"))

    print()

    studies_path = os.path.join(os.path.dirname(__file__), '../resources/study_examples/*.json')
    for example in glob.glob(studies_path):
        with open(example, "r") as f:
            data = json.load(f)
            for d in data:
                p = Patient.from_examples_dict(d)
                session.add(p)

    session.commit()

    result = [(p.family, s.body_area) for s, p in
              session.query(Study, Patient).join(Patient).order_by(Patient.family).all()]
    print(tabulate(result, headers=["Family", "Body Area"], tablefmt="orgtbl"))

    print()

    operators_path = os.path.join(os.path.dirname(__file__), '../resources/operator_examples/*.json')
    for example in glob.glob(operators_path):
        with open(example, "r") as f:
            data = json.load(f)
            o = Operator.from_examples_dict(data)
            session.add(o)

    session.commit()

    result = [(o.name, o.access) for o in
              session.query(Operator).order_by(Operator.name).all()]
    print(tabulate(result, headers=["Name", "Access"], tablefmt="orgtbl"))
