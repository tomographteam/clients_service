#!/usr/bin/python3.7
#encoding: UTF-8

from threading import Timer
from time import time

import asyncio
import math
import random
import signal
from time import sleep

class Config:

    def __init__(self):
        self.lead = 1
        self.filter = 0
        self.gain = 1

    def _print(self):
        print("Lead = "+str(self.lead))
        print("Filt = "+str(self.filter))
        print("Gain = "+str(self.gain))


class Monitor:

    def __init__(self, c: Config):
        self.heart_rate = 77

        self.data_id = 0
        self.data = [0 for i in range(0, 128)]

        self.ecg = [i for i in range(0, 60)]
        self.xray = [int((i >= 10) & (i < 20)) for i in range(0, 60)]
        #self.xray = [1 for i in range(0, 60)]

        print(self.xray)
        self.trig_idx = 100

        self.cfg = c
        self.data[0] = 0xFF
        self.encode()
        self.data[126] = 0
        self.data[127] = 0xFE


    def generate_ecg_data(self, beg=0):
        self.data_id = self.data_id+1
        # self.ecg = [(int(beg)+i) & 0xFFF for i in range(0, 60)]
        self.ecg = [int(2000+300*math.sin((self.data_id*60 + i)/3.14/10)) & 0xFFF for i in range(0, 60)]
        # print("ecg=" + str(self.ecg))

    def encode(self):
        self.encode_config(self.cfg)
        self.encode_heart_rate(self.heart_rate + random.randint(-5,5))
        self.encode_waveform(self.xray, self.trig_idx, self.ecg)

    def encode_config(self, c: Config):
        self.data[1] = c.lead
        self.data[2] = c.filter & 1
        self.data[3] = c.gain & 3

    def encode_heart_rate(self, hr: int):
        self.data[4] = (hr>>7)&0x03
        self.data[5] = (hr   )&0x7F

    def encode_waveform(self, xray, trig_idx, ecg):
        for i in range(0,60):
            first_byte =   (0x80 if xray[i] else 0) \
                         | (0x20 if i % trig_idx == 0  else 0) \
                         | (0x1F & (ecg[i]>>7))

            second_byte =  (0x7F & ecg[i])

            self.data[6 + i*2 + 0] = first_byte
            self.data[6 + i*2 + 1] = second_byte

        #print("data=" + str(self.data[6:127]))

    def _print(self):
        self.cfg._print()
        tr = [(1 if self.trig_idx == i  else 0) for i in range(0, 60)]
        print("HR   = "+str(self.heart_rate))
        print("Trig = "+str(self.trig_idx))
        print("TRIG = "+str(tr))
        print("XRAY = "+str(self.xray))
        print("ECG  = "+str(self.ecg))

    def get_bytes(self):
        #print("data=" + str(self.data[6:127]))
        self.data[126] += 1
        self.data[126] = self.data[126] % 256
        print("data=" + str(self.data))
        return bytes(self.data)

    def print_bytes(self):
        print("BYTES = " + self.get_bytes().hex())




class MonitorProtocol(asyncio.Protocol):

    def connection_made(self, transport):
        print("connection_made")
        self.transport = transport
        self.monitor = Monitor(Config())
        self.data_enabled = False

        self.periodic_foo_period_s = 0.250
        self.timer_tbeg = time()
        self.timer_i = 1
        Timer(self.periodic_foo_period_s, self.timer_handler).start()

    def timer_handler(self):
        # Calculate interval for next timer and start it
        tnow = time()
        self.timer_i = self.timer_i + 1
        tnext = self.timer_tbeg + (self.periodic_foo_period_s * self.timer_i)
        dt = tnext - tnow
        Timer(dt, self.timer_handler).start()

        # Generate data, send if need
        self.monitor.generate_ecg_data(time())
        self.monitor.encode()
        if self.data_enabled:
            s = self.monitor.get_bytes()
            """
            Debug string
            s = " Time " + str(tnow)
            s = s + " Data " + str(self.monitor.data_id) + "\r\n"
            """
            
            self.transport.write(s[0:5])
            sleep(0.05)
            self.transport.write(s[5:100])
            sleep(0.05)
            self.transport.write(s[100:128])
        pass

    def data_received(self, data):
        print("RX " + str(data) + " d[0]=" + str(data[0]))
        if data[0] == 0x11:
            # XON
            print("data_enabled = True")
            self.data_enabled = True
        elif data[0] == 0x13:
            # XOFF
            print("data_enabled = False")
            self.data_enabled = False


async def main(host, port):
    loop = asyncio.get_running_loop()
    server = await loop.create_server(MonitorProtocol, host, port)
    await server.serve_forever()

if __name__ == "__main__":
    print("Monitor started.")
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    asyncio.run(main("localhost", 17040))
