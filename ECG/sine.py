from math import sin, pi

from PyQt5.QtGui import QPainter, QPainterPath
from PyQt5.QtWidgets import QWidget, QApplication, QSlider, QLabel


class Widget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)

        path = QPainterPath()
        for x in range(self.width()):
            y = sin(x / 10) * self.height() / 2
            path.lineTo(x, y + (self.height() / 2))
        painter.drawPath(path)

if __name__ == '__main__':
    app = QApplication([])
    widget = Widget()
    widget.show()
    widget.resize(600, 300)
    app.exec_()