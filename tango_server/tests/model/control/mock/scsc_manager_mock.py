
class SCSCManagerMock:
    def __init__(self, event_handler=None):
        self._event_handler = event_handler

    def start_scan(self, event):
        return True

    def handle_server_event(self, event):
        return self._event_handler.handle_event( event)
