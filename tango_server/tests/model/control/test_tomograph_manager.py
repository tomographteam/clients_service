# from src.model.control.tomograph_manager import TomographManager
# from src.model.control.event.event import EventType
# from src.model.control.event.event import Event
# from mock.scsc_manager_mock import SCSCManagerMock

# def test_tomograph_manager_handle_non_existing_event():
#     tomograph_manager = TomographManager()
#     event = Event(EventType.scsc_scan_report)
#     tomograph_manager.handle_event(event)
#     assert (tomograph_manager.handle_event(event) is False)
#
# def test_tomograph_manager_handle_start_empty_event():
#     tomograph_manager = TomographManager()
#     event = Event( EventType.empty)
#     assert (tomograph_manager.handle_event(event) is True)
#
# def test_tomograph_manager_handle_start_scan_event():
#     tomograph_manager = TomographManager()
#     event = Event( EventType.start_scan)
#     tomograph_manager.device_manager._subsystemManager = SCSCManagerMock()
#     assert (tomograph_manager.handle_event(event) is True)
#
# def test_tomograph_manager_scsc_scan_report_event():
#     tomograph_manager = TomographManager()
#     event = Event( EventType.scsc_scan_report)
#     tomograph_manager.device_manager._subsystemManager._SCSCManager = SCSCManagerMock( tomograph_manager.device_manager._subsystemManager)
#     assert (tomograph_manager.device_manager._subsystemManager._SCSCManager.handle_server_event(event) is True)

# def test_tomograph_manager_handle_get_patient_list_event():
#     tomograph_manager = TomographManager()
#     event = Event( EventType.get_patient_list)
#     assert (tomograph_manager.handle_event(event) is True)
