from tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.scsc_tango_server import SCSCTangoServer

try:
    from scsc_emulator_server import SCSCEmulatorServer
except ImportError:
    from tests.model.equipment.subsystem_device_manager.scsc_emulator.scsc_emulator_server import SCSCEmulatorServer

import asyncio


# if __name__ == "__main__":
#     tango_emulator = SCSCEmulatorServer()
#

async def main(loop):
    scsc_emulator = SCSCTangoServer()
    await scsc_emulator.start_server()


loop = asyncio.get_event_loop()
asyncio.ensure_future(main(loop))
loop.run_forever()
