url_template = 'http://{rest_host}:{rest_port}/tango/rest/rc4/hosts/{tango_host}/{tango_port}/devices/sys/{device}/1/attributes'

template = {
    'name': '',
    'value': '{url}/value',
    'info': '{url}/info',
    'properties': '{url}/properties',
    'history': '{url}/history',
    '_links': {
        '_self': '{url}'
    }
}
