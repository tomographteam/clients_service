import asyncio
import os
import random
from asyncio import sleep
from typing import List, Tuple

from PIL.Image import Image

from tango_server.model.control.event.event import Event
from tango_server.model.data.dicom_manager.scan_results.general import ScanResultGeneral
from tango_server.model.data.dicom_manager.scan_results.scan_results import ScanResult
from tango_server.model.data.dicom_manager.scan_results.scout import ScanResultScout
from tango_server.model.data.dicom_manager.scan_results.tomograph_state import TomographState
from tango_server.model.data.patient_position import PatientPositions
from tango_server.model.data.protocol.scan_types import ScanTypes
from tango_server.model.data.result_manager.reconstruction_client.client import ReconstructionClient
from tango_server.model.data.scan_task import ScanTaskId
from tango_server.model.equipment.scsc_manager.devices.dsebct import DsebctStateList
from tango_server.model.equipment.scsc_manager.devices.pes import PesStates
from tango_server.model.equipment.scsc_manager.events import SCSCEventTypes
from tango_server.utils.settings import get_tomograph_config
from tango_server.view.components.functions import scale
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


async def run_command(*args):
    """Run command in subprocess.

    Example from:
        http://asyncio.readthedocs.io/en/latest/subprocess.html
    """
    # Create subprocess
    process = await asyncio.create_subprocess_exec(
        *args, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )

    # Status
    print("Started: %s, pid=%s" % (args, process.pid), flush=True)

    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()

    # Progress
    if process.returncode == 0:
        print(
            "Done: %s, pid=%s, result: %s"
            % (args, process.pid, stdout.decode().strip()),
            flush=True,
        )
    else:
        print(
            "Failed: %s, pid=%s, result: %s"
            % (args, process.pid, stderr.decode().strip()),
            flush=True,
        )

    # Result
    result = stdout.decode().strip()

    # Return stdout
    return result


class DSEBCT(TangoDevice):
    device_name = TangoDevice.config['DSEBCT']
    _patient_table_width = get_tomograph_config()['patient_table_width']
    _patient_table_len = get_tomograph_config()['patient_table_len']
    _max_z = get_tomograph_config()['scan_area_max_z']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Mode = 0
        self.SeriesType = 0
        self.TableStartPos = 0
        self.TableStopPos = 0
        self.TableSpeed = 0
        self.Kv = 0
        self.Ma = 0
        self.CollWidth = 1
        self.Pitch = 0
        self.ScoutWinWidth = 0
        self.ScoutWinLength = 0
        self.TriggerW = 0
        self.TriggerX = 0
        self.TriggerY = 0
        self.TriggerScanPeriod = 0
        self.TriggerScanMaxCount = 0
        self.TriggerDensityThreshold = 0
        self.ReconKernelIndex = 0
        self.ReconNoiseReduced = 0
        self.ReconMetalCorr = 0
        self.DLP = 0
        self.ScanTime = 0
        self.mAs = 0
        self.study_id = ''
        self.series_number = 0
        self.temperature = 20
        self.hvpsstatus = 0
        self.pesstatus = 0
        self._state = 'ON'

        self._trigger_start_done = False
        self._ask_stop = False
        self._pause = False
        self._scan_started = False
        self._tmg = tmg
        self.prepare_scan_failure = False
        self.reconstruction_client = ReconstructionClient(tmg.loop)
        # tmg.loop.create_task(self.reconstruction_client.connect_as_scsc())
        # tmg.loop.create_task(self._dicom_stream())
        # tmg.loop.create_task(self._work())

    @property
    def state(self) -> str:
        # self._state = random.choice(DsebctStateList.get_list()).name
        return self._state

    @state.setter
    def state(self, value: str):
        self._state = value

    @property
    def status(self):
        return f'status text for {self._state}'

    @status.setter
    def status(self, value: str):
        pass

    def get_commands_list(self) -> List[Tuple[str, str]]:
        return [
            ('start_warm_up', 'DevVoid'),
            ('StopWork', 'DevVoid'),
            ('PrepareScanWithParams', 'DevString'),
        ]

    def get_attributes_list(self) -> List[str]:
        return ['Mode', 'SeriesType', 'TableStartPos', 'TableStopPos', 'TableSpeed', 'Kv', 'Ma', 'CollWidth', ]

    #
    # @property
    # def state(self):

    async def _work(self):
        while True:
            if self.temperature > 20:
                self.temperature -= 1
            await sleep(1)

    async def _dicom_stream(self):
        while True:
            if self._scan_started and not self._pause:
                scan_task_id = ScanTaskId(self.study_id, self.series_number)
                self._tmg.loop.create_task(self.reconstruction_client.add(scan_task_id, self.SeriesType))
            await asyncio.sleep(0.3)

    async def start_warm_up(self, params: dict):
        self.send_event(Event(SCSCEventTypes.work_start))
        for t in range(5):
            await sleep(1)
            print(f'Warm up: {t}/5')
            if self._ask_stop:
                self._ask_stop = False
                break
        self.send_event(Event(SCSCEventTypes.work_complete))
        self.send_event(Event(SCSCEventTypes.warm_up_finished))

    async def calibration(self, params: dict):
        self.send_event(Event(SCSCEventTypes.work_start))
        for t in range(5):
            await sleep(1)
            print(f'calibration: {t}/5')
            if self._ask_stop:
                self._ask_stop = False
                break
        else:
            self.send_event(Event(SCSCEventTypes.calibration_finished))
        self.send_event(Event(SCSCEventTypes.work_complete))

    async def StopWork(self, params: dict = None):
        self._ask_stop = True
        self._pause = False

    async def continue_scan(self, params: dict = None):
        self._pause = False
        self.send_event(Event(SCSCEventTypes.scan_continued))

    async def pause_scan(self, params: dict = None):
        self._pause = True
        self.send_event(Event(SCSCEventTypes.scan_paused))

    async def morning_startup(self, params: dict = None):
        print('morning_startup')

    async def evening_shutdown(self, params: dict = None):
        print('evening_shutdown')

    async def start_test_equipment(self, params: dict = None):
        self.send_event(Event(SCSCEventTypes.start_test_equipment_result, {'result': 'OK'}))
        await sleep(3)
        TEST_TANGO_RESULT = {
            'pes': 'success' if random.random() < 0.90 else 'failed',
            'hvps': 'success' if random.random() < 0.90 else 'failed',
            'bce': 'success' if random.random() < 0.90 else 'failed',
            'wwires': 'success' if random.random() < 0.90 else 'failed',
            'collimator': 'success' if random.random() < 0.90 else 'failed',
            'patient_table': 'success' if random.random() < 0.90 else 'failed',
            'chiller': 'success' if random.random() < 0.90 else 'failed',
            'turbo_pump': 'success' if random.random() < 0.90 else 'failed',
            'ion_pump': 'success' if random.random() < 0.90 else 'failed',
            'ptable': 'success' if random.random() < 0.90 else 'failed',
        }
        self.send_event(Event(SCSCEventTypes.test_equipment_finished, TEST_TANGO_RESULT))

    # async def PrepareScan(self):
    # self.send_event(Event(SCSCEventTypes.work_start))
    # await self._tmg.PTABLE.move_table_to(self.TableStartPos)
    # self.send_event(Event(SCSCEventTypes.work_complete))
    # self.send_event(Event(SCSCEventTypes.prepare_scan_finished))

    async def say_cmd(self, params: dict):
        name = params['name']
        fp = params['filepath']
        lang = params['lang']

        print(f'<dsebct.py> (SAY_CMD): [{name} | {lang} | {fp}]')
        if fp:
            await run_command('aplay', fp)

    async def PrepareScanWithParams(self, params: dict):
        self.SeriesType = params['scan']['SeriesType']
        self.TableStartPos = params['scan']['TableStartPos']
        self.TableStopPos = params['scan']['TableStopPos']
        self.TriggerScanPeriod = params['trigger']['TriggerScanPeriod']
        print(params)
        self.send_event(Event(SCSCEventTypes.work_start))
        await self._tmg.PTABLE.move_table_to(self.TableStartPos)
        # await asyncio.sleep(3)
        if self.prepare_scan_failure:
            self.send_event(Event(SCSCEventTypes.prepare_scan_failed, {'info': 'Всё сломалось'}))
        else:
            self.send_event(Event(SCSCEventTypes.prepare_scan_finished))
        self.send_event(Event(SCSCEventTypes.work_complete))

    async def StartScan(self, params: dict = None):
        self._tmg.notify_work_start()
        self._ask_stop = False
        self._tmg.PES.state = PesStates.running.name
        self._tmg.PES.PackVoltage -= 15
        # self._say_voice_cmd(scan_task.protocol.get_start_voice_command())

        self.SeriesType = int(self.SeriesType)
        scan_type = ScanTypes.load_by_series_type(self.SeriesType)

        if ScanTypes.is_general_scan(scan_type):
            await self._general_scan()
        elif self.SeriesType == ScanTypes.fluoro.index:
            await self._fluoro_scan()
        elif self.SeriesType == ScanTypes.scout_two_side.index:
            await self._scout_scan()

        elif self.SeriesType in [ScanTypes.trigger_start.index, ScanTypes.trigger_start_first_slice.index]:
            await self._trigger_start()

        self._tmg.PES.state = PesStates.open.name
        self._tmg.notify_work_complete()
        # elif scan_type == ScanTypes.sure_start:
        #     await self._sure_start()
        #     await self._tmg.reconstruction_client.from_controller_scan_finished(scan_task)
        # self._say_voice_cmd(scan_task.protocol.get_end_voice_command())

    def _sure_start_data(self):
        data_dir = 'resources/DICOM-examples/heart_surface/DATA'
        for file_name in sorted(os.listdir(data_dir)):
            yield os.path.join(data_dir, file_name)

    async def _trigger_start(self):
        data = self._sure_start_data()
        self._trigger_start_done = False
        img_num = 0
        scan_task_id = ScanTaskId(self.study_id, self.series_number)
        while True:
            # self._tmg.loop.create_task(self.reconstruction_client.add(scan_task_id, self.SeriesType))
            data = {'study_id': self.study_id, 'series_number': self.series_number,
                    'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm'}
            self.send_event(Event(SCSCEventTypes.recon_finished, data))

            img_num += 1
            if self.SeriesType == ScanTypes.trigger_start_first_slice.index:
                break
            await sleep(max(self.TriggerScanPeriod, 1))
            if self._ask_stop:
                break
            if self._trigger_start_done:
                break
        self._ask_stop = False
        self._trigger_start_done = False
        await self.reconstruction_client.from_controller_scan_finished(scan_task_id)
        result = ScanResult(scan_task_id, TomographState())
        result.dose = 1
        self.send_event(Event(SCSCEventTypes.scan_report, result.to_dict()))
        # data = {'study_id': self.study_id, 'series_number': self.series_number,
        #         'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm'}
        # self.send_event(Event(SCSCEventTypes.recon_finished, data))

    async def trigger_start_done(self, params: dict = None):
        self._trigger_start_done = True

    async def _general_scan(self):
        scan_task_id = ScanTaskId(self.study_id, self.series_number)
        tmg = self._tmg
        self.TableStartPos = int(self.TableStartPos)
        self.TableStopPos = int(self.TableStopPos)
        self.TableSpeed = float(self.TableSpeed)
        tmg.PTABLE.h_pos = int(tmg.PTABLE.h_pos)
        scan_interrupted = False
        speed = self.TableSpeed
        speed = 10
        tmg.PTABLE.h_pos = self.TableStartPos

        # print(f'start_position={start_position}, stop_position={stop_position}')
        sign = 1 if self.TableStartPos < self.TableStopPos else -1
        print(f'GENERAL SCAN start_position={self.TableStartPos} stop_position={self.TableStopPos}')
        self._scan_started = True
        while tmg.PTABLE.h_pos != self.TableStopPos:
            print(tmg.PTABLE.h_pos, self.TableStopPos, self._ask_stop)
            await asyncio.sleep(1)
            if not self._pause:
                diff = abs(tmg.PTABLE.h_pos - self.TableStopPos)
                move_by = int(min(speed, diff) * sign)
                tmg.PTABLE.h_pos += move_by
                self.temperature += 15
            if self._ask_stop:
                scan_interrupted = True
                break
        self._ask_stop = False

        result = ScanResultGeneral(scan_task_id, TomographState())
        result.dose = 1
        result.scan_interrupted = scan_interrupted
        self._scan_started = False
        await self.reconstruction_client.from_controller_scan_finished(scan_task_id)

        self.send_event(Event(SCSCEventTypes.scan_report, result.to_dict()))
        data = {'study_id': self.study_id, 'series_number': self.series_number,
                'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm'}
        self.send_event(Event(SCSCEventTypes.recon_finished, data))

    async def _fluoro_scan(self):
        print('FLUORO SCAN')
        scan_task_id = ScanTaskId(self.study_id, self.series_number)
        scan_interrupted = False
        self._scan_started = True
        idx = 0
        while idx <= 3:
            print(f'FLUORO SCAN {idx}')
            await asyncio.sleep(1)
            if not self._pause:
                idx += 1
            if self._ask_stop:
                scan_interrupted = True
                break
        self._ask_stop = False

        result = ScanResultGeneral(scan_task_id, TomographState())
        result.scan_interrupted = scan_interrupted
        result.dose = 1
        self._scan_started = False
        await self.reconstruction_client.from_controller_scan_finished(scan_task_id)

        self.send_event(Event(SCSCEventTypes.scan_report, result.to_dict()))
        data = {'study_id': self.study_id, 'series_number': self.series_number,
                'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm'}
        self.send_event(Event(SCSCEventTypes.recon_finished, data))

    async def _scout_scan(self):
        scan_task_id = ScanTaskId(self.study_id, self.series_number)
        result, img_front, img_side = self._make_scout_result(scan_task_id)
        result.dose = 1

        # await asyncio.gather(self.reconstruction_client.add(scan_task_id, self.SeriesType),
        #                     self.reconstruction_client.add(scan_task_id, self.SeriesType))
        # await self.reconstruction_client.from_controller_scan_finished(scan_task_id)
        self.send_event(Event(SCSCEventTypes.scan_report, result.to_dict()))

        # data = { 'study_id': self.study_id, 'series_number': self.series_number, 'result_path': 'resources/DICOM/recon_volume.dcm'}
        data = {'study_id': self.study_id, 'series_number': self.series_number,
                'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12563145517671026622959411839454595104.dcm'}
        # data = {'study_id': self.study_id, 'series_number': self.series_number,'result_path': 'resources/DICOM/1.2.826.0.1.3680043.8.498.12761213340114088162458482324012885000.dcm'}

        self.send_event(Event(SCSCEventTypes.recon_finished, data))

    def _make_scout_result(self, scan_task_id: ScanTaskId) -> Tuple[ScanResultScout, Image, Image]:
        # protocol = self._scan_task.protocol

        front, side = PatientPositions.head_to_gentry_on_back_arms_to_legs.get_position_imgs()
        start_x = 0
        stop_x = 500
        image_front = self._prepare_scout_img(front, start_x, stop_x, self._patient_table_width)

        side_img = self._prepare_scout_img(side, start_x, stop_x, self._max_z)

        result = ScanResultScout(scan_task_id, TomographState())
        result.scan_time = 0
        result.passed = 0
        return result, image_front, side_img

    def _prepare_scout_img(self, img: Image, start_x, stop_x, max_x) -> Image:
        return self._cut_img(img, start_x, stop_x, max_x)

    def _cut_img(self, img: Image, start_x, stop_x, max_x) -> Image:
        max_y = self._patient_table_len
        start_y = 0
        stop_y = max_y

        start_x_scaled = scale(0, max_x, start_x, 0, img.width)
        start_y_scaled = scale(0, max_y, start_y, 0, img.height)
        width = min(max_x - start_x_scaled, max_y - start_y, stop_y - start_y, stop_x - start_x)
        img = img.crop((start_x_scaled, start_y_scaled, start_x_scaled + width, start_y_scaled + width))
        return img
