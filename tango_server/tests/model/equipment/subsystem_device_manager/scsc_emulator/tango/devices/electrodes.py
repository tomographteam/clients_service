from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class ELECTRODES(TangoDevice):
    device_name = TangoDevice.config['ELECTRODES']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Valve: int = 0
        self.Pressure: int = 0
        self.OpenPressure: int = 0
        self.OpenPressure: int = 0
        self.ClosePressure: int = 0

        self.Mon0: int = 0
        self.Mon1: int = 0
        self.Mon2: int = 0
        self.Mon3: int = 0
        self.Mon4: int = 0
        self.Mon5: int = 0
