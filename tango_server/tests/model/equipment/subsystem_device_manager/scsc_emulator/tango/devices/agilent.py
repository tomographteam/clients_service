from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class AGILENT(TangoDevice):
    device_name = TangoDevice.config['AGILENT']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.pressure1: float = 1
        self.pressure2: float = 2
        self.pressure3: float = 3
