from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class NEGPOWER(TangoDevice):
    device_name = TangoDevice.config['NEGPOWER']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.SerialNumber: int = 0
        self.Alarms: int = 0
        self.ch_tc_temp: int = 35
