from typing import List

from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class CHILLER(TangoDevice):
    device_name = TangoDevice.config['CHILLER']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.RS: int = 1
        self.RT: int = 2
