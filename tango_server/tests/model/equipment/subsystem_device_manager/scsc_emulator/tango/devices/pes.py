from asyncio import sleep
from typing import List

from tango_server.model.equipment.scsc_manager.devices.pes import PesStates
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class PES(TangoDevice):
    device_name = TangoDevice.config['PES']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)

        self._PackVoltage = 480.4
        self.PackCurrent = 0
        self.BusVoltage = 0
        self._soc = 66
        self._state = PesStates.open

        self._ask_stop = False
        self._move_direction = ''
        #tmg.loop.create_task(self._work())

    @property
    def state(self) -> str:
        return self._state.name

    @property
    def PackVoltage(self) -> float:
        return self._PackVoltage

    @PackVoltage.setter
    def PackVoltage(self, value):
        self._PackVoltage = int(value)

    @property
    def soc(self) -> float:
        v_min = 360
        v_max = 504
        return int(100 * (int(self._PackVoltage) - v_min) / (v_max - v_min))

    @property
    def SOC(self) -> float:
        return self.soc

    @state.setter
    def state(self, value: str):
        self._state = PesStates.load_by_name(value)

    def get_attributes_list(self) -> List[str]:
        return ['PackVoltage', 'SOC', 'state', 'PackCurrent', 'BusVoltage' ]

    async def Charge(self, event_data):
        self._state = PesStates.standby
        print(self._state)

    async def Idle(self, event_data):
        self._state = PesStates.open

    async def _work(self):
        while True:
            if self._PackVoltage < 480 and self._state != PesStates.running:
                self._state = PesStates.standby
                self._PackVoltage += 1
            if self._PackVoltage >= 480 and self._state == PesStates.standby:
                self._state = PesStates.open

            await sleep(1)
