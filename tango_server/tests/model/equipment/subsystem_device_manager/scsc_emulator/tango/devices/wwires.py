from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class WWIRES(TangoDevice):
    device_name = TangoDevice.config['WWIRES']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Err: bool = False
        self.Busy: bool = False
