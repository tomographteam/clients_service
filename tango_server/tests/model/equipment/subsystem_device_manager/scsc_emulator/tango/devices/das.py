from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class _DAS(TangoDevice):

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.das_status: bool = False
        self.num_robs: int = 1


class DAS_1(_DAS):
    device_name = TangoDevice.config['DAS_1']


class DAS_2(_DAS):
    device_name = TangoDevice.config['DAS_2']
