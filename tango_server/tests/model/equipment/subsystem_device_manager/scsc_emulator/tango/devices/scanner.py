from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class SCANNER(TangoDevice):
    device_name = TangoDevice.config['SCANNER']
    _ro_attributes = ('event_string', 'result_path', 'version')

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.scan_mode: int = 0
        self.table_position_start: int = 0
        self.table_position_end: int = 0
        self.table_speed: int = 0
        self.kv: int = 0
        self.ma: int = 0
        self.collimator_width: float = 5.6
        self.number_of_sweeps: int = 0
        self.number_of_sweeps_heat: int = 0
        self.use_hvps: bool = True
        self.use_wwires: bool = False
        self.use_das1: bool = True
        self.use_das2: bool = False
        self.use_recon: bool = True
        self.check_robs: bool = False
        self.use_ptable: bool = True
        self.use_bce: bool = False
        self.event_string: int = 0
        self.study_id: str = 'study_id'
        self.series_number: int = 0
        self.result_path: str = 'result_path'
        self.patient_position: str = 'patient_position'
        self.version: str = 'version'

    async def Start(self):
        pass

    async def Prepare(self):
        pass

    async def Pause(self):
        pass

    async def Stop(self):
        pass

    async def Resume(self):
        pass
