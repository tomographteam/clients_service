from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class ECG(TangoDevice):
    device_name = TangoDevice.config['ECG']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Lead: int = 0
        self.Filter: bool = False
