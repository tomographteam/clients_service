from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class COLLIMATOR(TangoDevice):
    device_name = TangoDevice.config['COLLIMATOR']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.CollWidth: float = 11.123
        self.m_pos_epsilon: float = 0
        self.m_pos: float = 110.125
        self.moving: bool = False
        self.state = 'Default state'

    async def SetWidth(self, param):
        print(f'COLLIMATOR SetWidth param = {param}')
        self.CollWidth = param


class COLLIMATOR_M(TangoDevice):
    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.IsMoving: bool = False
        self.Position: int = 10


class COLLIMATOR_M_1(COLLIMATOR_M):
    device_name = TangoDevice.config['COLLIMATOR_M_1']


class COLLIMATOR_M_2(COLLIMATOR_M):
    device_name = TangoDevice.config['COLLIMATOR_M_2']


class COLLIMATOR_M_3(COLLIMATOR_M):
    device_name = TangoDevice.config['COLLIMATOR_M_3']
