from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class RECON(TangoDevice):
    device_name = TangoDevice.config['RECON']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.connected: bool = False
        self.number_of_sweeps_dark: int = 0
