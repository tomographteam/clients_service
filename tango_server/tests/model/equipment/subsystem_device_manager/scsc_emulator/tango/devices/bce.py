from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class BCE(TangoDevice):
    device_name = TangoDevice.config['BCE']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.version: str = ''
        self.PresetNum: int = 0
        self.fpga_state: int = 0
