from asyncio import sleep
from typing import List

from tango_server.model.data.move_patient_table_command import MovePatientTableCommand
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class PTABLE(TangoDevice):
    device_name = TangoDevice.config['PTABLE']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.h_pos = 0
        self.v_pos = 0
        self.hspeed = 0
        self.CradleState = 0
        self.ElevState = 0
        self.zero_plane_h_pos = 0

        self._ask_stop = False
        self._move_direction = ''

    def ask_stop(self):
        self._ask_stop = True

    def stop_table(self, event_data):
        self.ask_stop()

    async def move_table_to(self, target_position: int, table_speed=500):
        if self.h_pos == target_position:
            return True
        target_position = int(target_position)
        sign = 1 if self.h_pos < target_position else -1
        self._tmg.table_moved = True
        self._tmg.notify_work_start()
        # self._tmg.notify_table_move_start(target_position, table_speed)
        self._ask_stop = False
        while self.h_pos != target_position:
            if self._ask_stop:
                break
            await sleep(1)
            diff = abs(self.h_pos - target_position)
            # print(diff)
            move_by = min(table_speed, diff) * sign
            self.h_pos = int(self.h_pos + move_by)
        self._ask_stop = False
        self._tmg.notify_work_complete()
        self._tmg.table_moved = False

    async def move_patient_table(self, data: dict):
        print(f'move_patient_table {data}')
        self._move_direction = data.get('direction') or data.get('input')

    async def MoveHorizontal(self, data: dict):
        print(f'MoveHorizontal {data}')
        position = data['position']
        speed = data['speed']
        await self.move_table_to(position, speed or 10)
        # self._move_direction = data.get('direction') or data.get('input')

    async def MoveVertical(self, data: dict):
        print(f'MoveVertical {data}')
        self.v_pos = data['position']
        # self._move_direction = data.get('direction') or data.get('input')

    async def work(self):
        step = 30
        while True:
            direction = self._move_direction
            self._move_direction = ''
            if direction == MovePatientTableCommand.forward:
                self.h_pos += step
            if direction == MovePatientTableCommand.backward:
                self.h_pos -= step
            if direction == MovePatientTableCommand.up:
                self.v_pos += step
            if direction == MovePatientTableCommand.down:
                self.v_pos -= step

            if direction:
                print(f'move_patient_table: h_pos={self.h_pos} v_pos={self.v_pos}')
            await sleep(0.1)
