from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class NEGPOWERMINI_1(TangoDevice):
    device_name = TangoDevice.config['NEGPOWERMINI_1']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Alarms: int = 0
        self.Alarms_clear: bool = False
        self.temp_pump: int = 10


class NEGPOWERMINI_2(TangoDevice):
    device_name = TangoDevice.config['NEGPOWERMINI_2']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.Alarms: int = 0
        self.Alarms_clear: bool = False
        self.temp_pump: int = 11
