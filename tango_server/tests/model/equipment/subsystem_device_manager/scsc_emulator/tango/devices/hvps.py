from typing import List

from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice

try:
    from ..tomograph import Tomograph
except ImportError:
    pass


class HVPS_CT(TangoDevice):
    device_name = TangoDevice.config['HVPS_CT']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.mb_poll_period: int = 0
        self.mb_address: str = 'str'
        self.mb_port: int = 0

        self.DC_Bus: float = 0
        self.DC_Bus_kA: float = 0
        self.DischargeCounter: float = 0
        self.Fil_I1: float = 0
        self.Fil_I2: float = 0
        self.Fil_Lim1: float = 0
        self.Fil_Lim2: float = 0
        self.Fil_Ref1: float = 0
        self.Fil_Ref2: float = 0
        self.Fil_V1: float = 0
        self.Fil_V2: float = 0
        self.INV_RH_CMD: int = 0
        self.Inv_HL: float = 0
        self.Inv_HV: float = 0
        self.Inv_LL: float = 0
        self.Inv_T: float = 0
        self.Iout1: float = 0
        self.Iout2: float = 0
        self.MA_ERR_ERR: bool = False
        self.MA_ERR_IMA: bool = False
        self.MA_ERR_MN: bool = False
        self.MA_ERR_MN30: bool = False
        self.MA_ERR_PL1: bool = False
        self.MA_ERR_PL2: bool = False
        self.MA_ERR_UV: bool = False
        self.MA_Ref1p: float = 0
        self.MA_Ref2p: float = 0
        self.MA_Refn: float = 0
        self.MEAN_V: float = 0
        self.OUT_A: float = 0
        self.OUT_V: float = 0
        self.RANGE_V: float = 0
        self.RH_STATUS: int = 0
        self.Vma1p: float = 0
        self.Vma2p: float = 0
        self.VmaOut1: float = 0
        self.VmaOut2: float = 0
        self.Vman: float = 0
        self.connected: bool = False
        self.ctSTS: int = 0
        self.isCT: bool = False
        self.isFL_PWR: bool = False
        self.isFL_REF: bool = False
        self.isMA_EN: bool = False
        self.isMA_PWR: bool = False
        self.isMA_REF: bool = False
        self.isON: bool = False
        self.isPower: bool = False
        self.isStart: bool = False
        self.rLED1: int = 0
        self.rLED2: int = 0
        self.rSTATUS: int = 0
        self.rSTATUS_PLD: int = 0
        self.rTEMP: float = 0
        self.version: str = ''
        self.FI_ERR_OC1: bool = False
        self.FI_ERR_OC2: bool = False
        self.FI_ERR_LOC1: bool = False
        self.FI_ERR_LOC2: float = 0
        self.FI_ERR_VRECT: bool = False
        self.FI_ERR_OVT: float = 0


class HVPS_NWL(TangoDevice):
    device_name = TangoDevice.config['HVPS_NWL']

    def __init__(self, tmg: 'Tomograph'):
        super().__init__(tmg)
        self.input_V: int = 0
        self.rb_filament1_idc: int = 0
        self.rb_filament1_vdc: int = 0
        self.set_modanode1_vdc: int = 0
        self.rb_filament2_idc: int = 0
        self.rb_filament2_vdc: int = 0
        self.set_modanode2_vdc: int = 0
        self.set_beam_out_kv: int = 0
