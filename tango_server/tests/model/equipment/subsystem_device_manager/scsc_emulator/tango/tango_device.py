from typing import List, Callable, Tuple

from tango_server.model.control.event.event import Event
from tango_server.utils.settings import Settings

try:
    from .tomograph import Tomograph
except ImportError:
    pass


class TangoDevice:
    config = Settings.inst().get_device_name_config()
    device_name: str = ''
    _ro_attributes: Tuple[str] = tuple()

    def __init__(self, tmg: 'Tomograph'):
        self._tmg = tmg
        self.events: List[Event] = []
        self.zmq_events: List[Event] = []
        self.state = 'Default state'
        self.status = 'Default status'

    def get_commands_list(self) -> List[Tuple[str, str]]:
        return []

    async def work(self):
        pass

    def get_attributes_list(self) -> List[str]:
        result = []
        base_class_dir = dir(TangoDevice)
        for attr_name in dir(self):
            if attr_name.startswith('_'):
                continue
            if attr_name in base_class_dir:
                continue
            if isinstance(getattr(self, attr_name), Callable):
                continue
            if attr_name in ['events', 'zmq_events', 'send_event', 'work', 'get_attributes_list', 'config',
                             'send_event', ]:
                continue
            result.append(attr_name)
        return result

    def send_event(self, event: Event):
        print(f'send_event = {event}')
        self.events.append(event)
        self.zmq_events.append(event)

    def __repr__(self):
        return self.device_name

    def is_attribute_writable(self, attribute_name: str) -> bool:
        return attribute_name not in self._ro_attributes
