import os
import sys
from random import random

sys.path.insert(0, os.path.join(os.getcwd(), 'tango_server', 'vendor'))

import asyncio
import json
import logging
import os
from time import time
from typing import List, Optional

import aiohttp_jinja2
import jinja2
from aiohttp import web
from aiohttp.web_request import Request

from tango_server.model.control.event.event import Event

from tango_server.model.equipment.scsc_manager.client import SCSCClient
from tango_server.model.equipment.scsc_manager.events import SCSCEventTypes
from tango_server.utils.logs.log import LogsFileHandler, exception_formatter
from tango_server.utils.settings import get_server_config
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango import attribute_list_item
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.attribute_info import attribute_info_template

from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tango_device import TangoDevice
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.tomograph import Tomograph
from tango_server.tests.model.equipment.subsystem_device_manager.scsc_emulator.tango.devices import *


class SCSCTangoServer:
    _server_config = get_server_config()
    url_template = '/tango/rest/v11/hosts/{tango_server}/devices/{request}'

    temperature_green = 50
    temperature_yellow = 100
    temperature_red = 160

    def __init__(self, loop):
        self._loop = loop
        # self._host = self._server_config['tango_rest_server']
        # self._port = self._server_config['tango_rest_port']
        self._host = '0.0.0.0'
        self._port = self._server_config['tango_rest_port']
        self._tango_server = self._server_config['tango_server']
        self._tango_port = self._server_config['tango_port']

        self._devices: List[TangoDevice] = []
        self._event_stream_id = 0
        self._app = web.Application()

        self.DSEBCT: Optional[DSEBCT] = None
        self.PTABLE: Optional[PTABLE] = None
        self.PES: Optional[PES] = None
        aiohttp_jinja2.setup(self._app, loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))
        self._register_urls()

    def add_device(self, device: TangoDevice):
        self._devices.append(device)

    def _get_device(self, device_name: str) -> TangoDevice:
        for device in self._devices:
            if device.device_name.upper() == device_name.upper():
                return device

    def _register_urls(self):
        # print('http://' + self._host + ':' + self._port + self._url_set_attribute('{attribute_name}'))

        print('_url_get_attribute_values_list', self._url_commands_list())
        self._app.add_routes([

            web.put(self._url_set_attribute(), self._set_attribute),
            web.get(self._url_get_attribute(), self._get_attribute),
            web.get(self._url_get_state(), self._get_state),
            web.get(self._url_get_attribute_info(), self._get_attribute_info),
            web.get(self._url_get_attribute_values_list(), self._get_attribute_values_list),
            web.get(self._url_get_attribute_list(), self._get_attribute_list),
            web.get(self._url_get_device_list(), self._get_device_list),
            web.get(self._url_commands_list(), self._get_commands_list),

            web.put('/tango/rest/v11/commands', self._run_command),

            web.get(SCSCClient.event_stream_template, self._event_stream),

            web.post(SCSCClient.event_subscriptions_template, self._tango_subscriptions),
            web.get('/menu', self._menu),
            web.post('/send_alert', self._send_alert),
            web.post('/send_alert_ok', self._send_alert_ok),
            web.post('/warm_up_requested', self._warm_up_requested),
            web.post('/set_temperature_green', self._set_temperature_green),
            web.post('/setup_zero_plane', self._setup_zero_plane),
            web.post('/send_work_start', self._send_work_start),
            web.post('/set_temperature_red', self._set_temperature_red),
            web.post('/set_pes_voltage', self._set_pes_voltage),
            web.post('/set_pes_state', self._set_pes_state),
            web.post('/set_dsebct_state', self._set_dsebct_state),
            web.post('/set_prepare_scan_failure', self._set_prepare_scan_failure),

        ])

    async def _menu(self, request):
        context = {}
        response = aiohttp_jinja2.render_template('menu.html', request, context)
        # response.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:8002'
        return response

    async def _send_alert(self, request):
        data = await request.text()
        self.DSEBCT.send_event(Event(SCSCEventTypes.handle_alert, {'message': data}))
        return web.json_response({'result': 'OK'})

    async def _send_alert_ok(self, request):
        data = await request.text()
        self.DSEBCT.send_event(Event(SCSCEventTypes.handle_alert_ok, {'message': data}))
        return web.json_response({'result': 'OK'})

    async def _warm_up_requested(self, request):
        data = await request.text()
        self.DSEBCT.send_event(Event(SCSCEventTypes.warm_up_requested, {'message': data}))
        return web.json_response({'result': 'OK'})

    async def _set_temperature_green(self, request):
        data = await request.text()
        self.DSEBCT.temperature = self.temperature_green
        return web.json_response({'result': 'OK'})

    async def _setup_zero_plane(self, request):
        data = await request.text()
        # print('_setup_zero_plane')
        self.PTABLE.zero_plane_h_pos = self.PTABLE.h_pos
        self.DSEBCT.send_event(Event(SCSCEventTypes.zero_plane_setup, {'h_pos': self.PTABLE.h_pos}))
        return web.json_response({'result': 'OK'})

    async def _send_work_start(self, request):
        data = await request.text()
        # print('_setup_zero_plane')
        self.DSEBCT.send_event(Event(SCSCEventTypes.work_start))
        return web.json_response({'result': 'OK'})

    async def _set_temperature_red(self, request):
        data = await request.text()
        self.DSEBCT.temperature = self.temperature_red
        return web.json_response({'result': 'OK'})

    async def _set_pes_voltage(self, request):
        data = await request.json()
        self.PES.PackVoltage = data['voltage']
        return web.json_response({'result': 'OK'})

    async def _set_pes_state(self, request):
        data = await request.json()
        # print(data)
        self.PES.state = data['state']
        return web.json_response({'result': 'OK'})

    async def _set_dsebct_state(self, request):
        data = await request.json()
        self.DSEBCT.state = data['state']
        return web.json_response({'result': 'OK'})

    async def _set_prepare_scan_failure(self, request):
        data = await request.json()
        # print(data)
        self.DSEBCT.prepare_scan_failure = bool(data['prepare_scan_failure'])
        return web.json_response({'result': 'OK'})

    async def _run_command(self, request: Request):
        data = await request.content.read(1024 * 10)
        payload = json.loads(data.decode('utf-8'))
        payload = payload[0]
        assert payload['host']
        # assert payload['input']
        device = self._get_device(payload['device'])
        command_name = payload['name']
        command_payload = json.loads(payload.get('input', '{}'))

        print(f'_run_command command_name={command_name} device={device}')
        if not hasattr(device, command_name) or not callable(device.__getattribute__(command_name)):
            print('command not found!')
            print(f'hasattr(device, command_name)={hasattr(device, command_name)}')
            print(f'callable(device.__getattribute__(command_name)={callable(device.__getattribute__(command_name))}')
            return web.Response(status=404)

        # await device.__getattribute__(command_name)(command_payload)
        loop = asyncio.get_event_loop()
        loop.create_task(device.__getattribute__(command_name)(command_payload))
        return web.Response(status=200)

    def _get_gevice_from_request(self, request) -> TangoDevice:
        device_group = request.match_info['device_group']
        device_name = request.match_info['device_name']
        device_numb = request.match_info['device_numb']
        return self._get_device(f'{device_group}/{device_name}/{device_numb}')

    async def _get_attribute(self, request: Request):
        attribute_name = request.match_info['attribute_name']
        # print(f'find device {device}')
        device = self._get_gevice_from_request(request)
        if not hasattr(device, attribute_name):
            # print(f'{device} dont have {attribute_name} ')
            return web.Response(status=404)
        result = {
            'name': attribute_name,
            'value': device.__getattribute__(attribute_name),
            'quality': 'ATTR_VALID',
            'timestamp': int(time()),
        }
        return web.json_response(result)

    async def _get_state(self, request: Request):
        device = self._get_gevice_from_request(request)
        result = {
            'state': device.state,
            'status': device.status,
        }
        return web.json_response(result)

    async def _get_attribute_values_list(self, request: Request):
        names_list = list(request.query.values())
        # if ['state', 'PackVoltage', 'SOC'] != names_list:
        #     sleep_time = random() * 3
        #     print(f'{names_list} sleep {sleep_time}')
        #     await asyncio.sleep(sleep_time)

        device = self._get_gevice_from_request(request)
        # print(f'_get_attribute_values_list request={request.query.values()}')
        # print(f'_get_attribute_values_list device_name={device_name}, device={device}')
        result = []
        for attribute_name in names_list:
            # print(f'_get_attribute_values_list {device_name}.{attribute_name}')

            # print(f'find device {device}')

            if not hasattr(device, attribute_name):
                return web.Response(status=404)
            attr_dict = {
                'name': attribute_name,
                'value': device.__getattribute__(attribute_name),
                'quality': 'ATTR_VALID',
                'timestamp': int(time()),
            }
            # print(attr_dict)
            result.append(attr_dict)
        # print(result)
        return web.json_response(result)

    async def _get_device_list(self, request):
        return web.json_response([str(d) for d in self._devices])

    async def _get_attribute_list(self, request: Request):
        # print('_get_attribute_list', request)
        device = self._get_gevice_from_request(request)
        # print(f'device={device}')
        if not device:
            return web.json_response([])
        # template = {'name': '',
        #     'value': '{url}/value',
        #     'info': '{url}/info',
        #     'properties': '{url}/properties',
        #     'history': '{url}/history',
        #     '_links': {
        #         '_self': '{url}'}
        template = attribute_list_item.template

        url = attribute_list_item.url_template.format(
            rest_host=self._host,
            rest_port=self._port,
            tango_host=self._tango_server,
            tango_port=self._tango_port,
            device=device.device_name
        )
        result = []
        for attr_name in device.get_attributes_list():
            # print(f'attr_name={attr_name}')
            result.append({
                'name': attr_name,
                'value': template['value'].format(url=url),
                'info': template['info'].format(url=url),
                'properties': template['properties'].format(url=url),
                'history': template['history'].format(url=url),
                '_links': {'_self': url},
            })
        # print(result)
        return web.json_response(result)

    async def _get_commands_list(self, request: Request):
        print('_get_commands_list', request)
        device = self._get_gevice_from_request(request)
        print(f'device={device}')
        if not device:
            return web.json_response([])
        result = []
        for command_name, in_type in device.get_commands_list():
            result.append({
                "id": f"{self._host}:{self._port}/{device.device_name}/{command_name}",
                "name": f"{command_name}",
                "device": f"{device.device_name}",
                "host": f"{self._tango_server}:{self._tango_port}",
                "info": {
                    "cmd_name": f"{command_name}",
                    "level": "OPERATOR",
                    "cmd_tag": 0,
                    "in_type": f"{in_type}",
                    "out_type": "DevVoid",
                    "in_type_desc": "Uninitialised",
                    "out_type_desc": "Uninitialised"
                }
            })
        # print(result)
        return web.json_response(result)

    async def _get_attribute_info(self, request):
        print('_get_attribute_info')
        attribute_name = request.match_info['attribute_name']
        device = self._get_gevice_from_request(request)
        if not hasattr(device, attribute_name):
            return web.Response(status=404)
        value = device.__getattribute__(attribute_name)
        from tango_server.model.equipment.scsc_manager.attribute import DataType
        attr_type = DataType.DevDouble
        if isinstance(value, bool):
            attr_type = DataType.DevBoolean
        elif isinstance(value, int):
            attr_type = DataType.DevUShort
        elif isinstance(value, str):
            attr_type = DataType.DevString
        result = attribute_info_template.copy()
        result['name'] = attribute_name
        result['data_type'] = attr_type.name

        writable = 'READ' if not device.is_attribute_writable(attribute_name) else 'READ_WRITE'
        result['name'] = attribute_name
        result['writable'] = writable
        print(result)

        return web.json_response(result)

    async def _set_attribute(self, request: Request):
        attribute_name = request.match_info['attribute_name']
        device = self._get_gevice_from_request(request)
        value = request.rel_url.query['v']
        # print(f'_set_attribute attribute_name={attribute_name} device={device} value={value}')
        if not hasattr(device, attribute_name):
            return web.Response(status=404)
        device.__setattr__(attribute_name, value)
        result = {
            'name': attribute_name,
            'value': device.__getattribute__(attribute_name),
            'quality': 'ATTR_VALID',
            'timestamp': int(time())
        }
        return web.json_response(result)

    def _make_tango_url(self, request: str) -> str:
        return self.url_template.format(
            tango_server=self._tango_server,
            request=request
        )

    def _url_get_attribute(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/attributes/{attribute_name}/value')

    def _url_get_state(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/state')

    def _url_get_attribute_values_list(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/attributes/value')

    def _url_get_attribute_list(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/attributes')

    def _url_get_device_list(self) -> str:
        return self._make_tango_url('devices')


    def _url_get_attribute_info(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/attributes/{attribute_name}/info')

    def _url_set_attribute(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/attributes/{attribute_name}/value')

    def _url_command(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/commands/{command_name}')

    def _url_commands_list(self) -> str:
        return self._make_tango_url('{device_group}/{device_name}/{device_numb}/commands')

    async def _event_stream(self, request):
        interval = 0.2
        resp = web.StreamResponse(status=200,
                                  reason='OK',
                                  headers={'Content-Type': 'application/json'})
        if resp is None:
            return

        await resp.prepare(request)
        while True:
            for device in self._devices:
                while device.events:
                    event = device.events.pop(0)
                    try:
                        response_data = event.dump()
                        response_str = json.dumps(response_data) + '\n\n\n'
                        if resp is None:
                            return
                        await resp.write(response_str.encode('utf-8'))
                    except:
                        continue

            await asyncio.sleep(interval)

    async def _tango_subscriptions(self, request: Request):
        # print(f'_tango_subscriptions {request}')
        if not request.body_exists:
            return
        body = await request.read()
        payload = json.loads(body.decode('utf-8'))
        # print(payload)
        events = []
        for idx, event_data in enumerate(payload):
            # print(event_data['device'], event_data['attribute'], event_data['type'])
            events.append({
                'id': idx,
                'target': {
                    'host': event_data['host'],
                    'device': event_data['device'],
                    'attribute': event_data['attribute'],
                    'type': event_data['type'],
                }
            })
        self._event_stream_id += 1
        resp = {
            'failures': [],
            'events': events,
            'id': self._event_stream_id

        }
        return web.json_response(resp)

    def start_server(self):

        web.run_app(self._app, host=self._host, port=self._port)
        self

def run():
    loop = asyncio.get_event_loop()
    server = SCSCTangoServer(loop)
    tmg = Tomograph()
    for device in tmg.devices:
        server.add_device(device)
    server.DSEBCT = tmg.DSEBCT
    server.PTABLE = tmg.PTABLE
    server.PES = tmg.PES
    #tmg.start()

    async def start(_app):
        task1 = asyncio.create_task(tmg.PTABLE.work())
        task2 = asyncio.create_task(tmg._events())
        task3 = asyncio.create_task(tmg.DSEBCT.reconstruction_client.connect_as_scsc())
        task4 = asyncio.create_task(tmg.DSEBCT._dicom_stream())
        task5 = asyncio.create_task(tmg.DSEBCT._work())
        task6 = asyncio.create_task(tmg.PES._work())

        yield

        task1.cancel()
        task2.cancel()
        task3.cancel()
        task4.cancel()
        task5.cancel()
        task6.cancel()

    server._app.cleanup_ctx.append(start)
    server.start_server()


if __name__ == '__main__':
    exceptions_handler = LogsFileHandler('scsc-exceptions.log', maxBytes=10000000, backupCount=2)
    exceptions_handler.setLevel(logging.ERROR)
    exceptions_handler.setFormatter(exception_formatter)
    exceptions_logger = logging.getLogger('scsc-exceptions')
    exceptions_logger.addHandler(exceptions_handler)
    try:
        run()
    except Exception as e:
        logger = logging.getLogger('scsc-exceptions')
        logger.exception(e, exc_info=True)
        raise e

    # print(d.url_set_attribute('Ma', '2'))
