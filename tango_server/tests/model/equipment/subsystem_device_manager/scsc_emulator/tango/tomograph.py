import asyncio
import json

import zmq
from zmq.asyncio import Context

from tango_server.model.control.event.event import Event
from tango_server.model.equipment.scsc_manager.events import SCSCEventTypes
from .devices import *


class Tomograph:
    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self._work_now = False

        self.DSEBCT = DSEBCT(self)
        self.PTABLE = PTABLE(self)
        self.PES = PES(self)
        self.HVPS_CT = HVPS_CT(self)
        self.HVPS_NWL = HVPS_NWL(self)
        self.CHILLER = CHILLER(self)
        self.COLLIMATOR = COLLIMATOR(self)
        self.COLLIMATOR_M_1 = COLLIMATOR_M_1(self)
        self.COLLIMATOR_M_2 = COLLIMATOR_M_2(self)
        self.COLLIMATOR_M_3 = COLLIMATOR_M_3(self)
        self.BCE = BCE(self)
        self.WWIRES = WWIRES(self)
        self.NEGPOWER = NEGPOWER(self)
        self.NEGPOWERMINI_1 = NEGPOWERMINI_1(self)
        self.NEGPOWERMINI_2 = NEGPOWERMINI_2(self)
        self.DAS_1 = DAS_1(self)
        self.DAS_2 = DAS_2(self)
        self.RECON = RECON(self)
        self.ECG = ECG(self)
        self.ELECTRODES = ELECTRODES(self)
        self.AGILENT = AGILENT(self)
        self.SCANNER = SCANNER(self)
        self.devices = [self.DSEBCT, self.PTABLE, self.PES,
                        self.HVPS_CT, self.HVPS_NWL,
                        self.CHILLER, self.COLLIMATOR, self.BCE,
                        self.WWIRES, self.NEGPOWER,
                        self.NEGPOWERMINI_1, self.NEGPOWERMINI_2,
                        self.DAS_1, self.DAS_2, self.RECON,
                        self.ELECTRODES, self.ECG,
                        self.COLLIMATOR_M_1, self.COLLIMATOR_M_2, self.COLLIMATOR_M_3,
                        self.AGILENT, self.SCANNER]
        self._zmq_ctx = Context.instance()




    def notify_work_start(self):
        if self._work_now:
            return
        self._work_now = True
        self.DSEBCT.send_event(Event(SCSCEventTypes.work_start))

    def notify_work_complete(self):
        if not self._work_now:
            return
        self._work_now = False

        self.DSEBCT.send_event(Event(SCSCEventTypes.work_complete))

    async def _events(self):
        pub = self._zmq_ctx.socket(zmq.PUB)
        pub.bind('tcp://0.0.0.0:5563')
        while True:
            for device in self.devices:
                while device.zmq_events:
                    event = device.zmq_events.pop(0)
                    response_data = event.dump()
                    msg_bytes = json.dumps(response_data).encode('utf-8')
                    print(f'ZMQ Send event: ', msg_bytes)
                    await pub.send_multipart([b'DSEBCT', msg_bytes])
            await asyncio.sleep(0.01)
