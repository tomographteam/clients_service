attribute_info_template = {
    'name': '',
    'writable': 'READ_WRITE',
    'data_format': 'SCALAR',
    'data_type': 'DevDouble',
    'max_dim_x': 1,
    'max_dim_y': 0,
    'description': 'mAs produced by current scan.',
    'label': 'mAs',
    'unit': 'mAs',
    'standard_unit': 'No standard unit',
    'display_unit': 'No display unit',
    'format': '%6.2f',
    'min_value': 'Not specified',
    'max_value': 'Not specified',
    'min_alarm': 'Not specified',
    'max_alarm': 'Not specified',
    'writable_attr_name': 'None',
    'level': 'OPERATOR',
    'extensions': [],
    'alarms': {
        'min_alarm': 'Not specified',
        'max_alarm': 'Not specified',
        'min_warning': 'Not specified',
        'max_warning': 'Not specified',
        'delta_t': 'Not specified',
        'delta_val': 'Not specified',
        'extensions': [],
        'tangoObj': {
            'min_alarm': 'Not specified',
            'max_alarm': 'Not specified',
            'min_warning': 'Not specified',
            'max_warning': 'Not specified',
            'delta_t': 'Not specified',
            'delta_val': 'Not specified',
            'extensions': []
        }
    },
    'events': {
        'ch_event': {
            'rel_change': 'Not specified',
            'abs_change': 'Not specified',
            'extensions': [],
            'tangoObj': {'rel_change': 'Not specified', 'abs_change': 'Not specified', 'extensions': []}
        },
        'per_event': {'period': '1000', 'extensions': [], 'tangoObj': {'period': '1000', 'extensions': []}},
        'arch_event': {
            'rel_change': 'Not specified',
            'abs_change': 'Not specified',
            'period': 'Not specified',
            'extensions': [],
            'tangoObj': {
                'rel_change': 'Not specified',
                'abs_change': 'Not specified',
                'period': 'Not specified',
                'extensions': []
            }
        },
        'tangoObj': {
            'ch_event': {'rel_change': 'Not specified', 'abs_change': 'Not specified', 'extensions': []},
            'per_event': {'period': '1000', 'extensions': []},
            'arch_event': {
                'rel_change': 'Not specified',
                'abs_change': 'Not specified',
                'period': 'Not specified',
                'extensions': []
            }
        }
    },
    'sys_extensions': [],
    'isMemorized': False,
    'isSetAtInit': False,
    'memorized': 'NOT_MEMORIZED',
    'root_attr_name': 'Not specified',
    'enum_label': []
}
