import os
import sys

import PIL

sys.path.insert(0, os.path.join(os.getcwd(), 'tango_server', 'vendor'))

import asyncio
import io
import struct
from asyncio import sleep, StreamWriter, StreamReader
from typing import List, Optional, Dict

from PIL import Image
from dicompylercore.dicomparser import DicomParser

from tango_server.model.data.protocol.scan_types import ScanTypes
from tango_server.model.data.result_manager.reconstruction_client.message import Message, Verbs, DataSizes
from tango_server.model.data.scan_task import ScanTaskId
from tango_server.utils.settings import get_reconstruction_server_config, get_tomograph_config
from tango_server.view.components.functions import scale
from tango_server.utils.settings import get_tomograph_config, get_path_config

def load_sample_data():
    data_dir = 'resources/DICOM-examples/heart_surface/DATA'
    for file_name in sorted(os.listdir(data_dir)):
        yield os.path.join(data_dir, file_name)


class Task:
    _patient_table_width = get_tomograph_config()['patient_table_width']
    _patient_table_len = get_tomograph_config()['patient_table_len']
    _max_z = get_tomograph_config()['scan_area_max_z']
    _sample_data = load_sample_data()

    def __init__(self, scan_task_id: ScanTaskId, scan_type, img_num: int):
        self.scan_task_id = scan_task_id
        self.scan_type = scan_type
        self.img_num = img_num
        self.img_data = None
        self.is_processed = False

    async def process(self):
        print(f'img process {self.scan_task_id} {self.img_num} start')
        await sleep(0.3)
        if ScanTypes.scout_two_side == self.scan_type:
            img = self._make_scout_image()
        else:
            img = self._make_general_image()
        img_out = io.BytesIO()
        img.save(img_out, format="JPEG")
        self.img_data = img_out.getvalue()
        # img.show()
        assert isinstance(img, Image.Image)
        # draw = ImageDraw.Draw(img)
        # draw.text((10, 10), 'reconstruction')
        # img.show()
        # with io.BytesIO() as output:
        #     img.save(output, format="JPEG")
        #     self.img_data = output.getvalue()
        self.is_processed = True
        print(f'img process {self.scan_task_id} {self.img_num} end')

    def __repr__(self):
        return f'{self.scan_task_id.study_id}/{self.scan_task_id.series_number}/{self.img_num}: {self.is_processed}'

    def _get_next_sample(self):
        try:
            return self._sample_data.__next__()
        except StopIteration:
            self._sample_data = load_sample_data()
            return self._sample_data.__next__()

    def _make_general_image(self) -> Image.Image:
        try:
            dicom_parser = DicomParser(self._get_next_sample())
            return dicom_parser.GetImage()
        except:
            return Image.Image()

    @staticmethod
    def _file_path(name: str, side: str) -> str:
        _images_scan_areas = get_path_config()['images_scan_areas']
        return os.path.join(_images_scan_areas, f'{name}_{side}.png')

    def get_position_imgs(self) :
        front = PIL.Image.open(self._file_path('head_to_gentry_on_back_arms_to_legs', 'front'))
        side = PIL.Image.open(self._file_path('head_to_gentry_on_back_arms_to_legs', 'side'))
        return front, side


    def _make_scout_image(self) -> Image:
        front, side = self.get_position_imgs()
        start_x = 0
        stop_x = 500
        image_front = self._prepare_scout_img(front, start_x, stop_x, self._patient_table_width)

        side_img = self._prepare_scout_img(side, start_x, stop_x, self._max_z)
        if self.img_num == 0:
            return image_front
        else:
            return side_img

    def _prepare_scout_img(self, img: Image, start_x, stop_x, max_x) -> Image:
        return self._cut_img(img, start_x, stop_x, max_x)

    def _cut_img(self, img: Image, start_x, stop_x, max_x) -> Image:
        max_y = self._patient_table_len
        start_y = 0
        stop_y = max_y

        start_x_scaled = scale(0, max_x, start_x, 0, img.width)
        start_y_scaled = scale(0, max_y, start_y, 0, img.height)
        width = min(max_x - start_x_scaled, max_y - start_y, stop_y - start_y, stop_x - start_x)
        img = img.crop((start_x_scaled, start_y_scaled, start_x_scaled + width, start_y_scaled + width))
        return img


class ReconstructionServer:
    _server_config = get_reconstruction_server_config()

    def __init__(self, loop):
        self._loop = loop
        self._host = self._server_config['host']
        self._port = self._server_config['port']

        self._scan_tasks: Dict[ScanTaskId, int] = {}
        self._tasks: List[Task] = []
        self._tasks_by_id = {}
        self._gui_writer: Optional[StreamWriter] = None
        self._scan_complete_tasks: List[ScanTaskId] = []

    def start_server(self):
        self._loop.create_task(self.start_work())
        self._loop.run_until_complete(asyncio.start_server(self._handler, self._host, self._port, loop=self._loop))
        self._loop.run_forever()

    async def _handler(self, reader: StreamReader, writer: StreamWriter):
        data = await reader.read(Message.header_size)
        msg = Message.load(data)
        if msg.count:
            msg.payload = await reader.read(msg.count * msg.data_size.size)

        # if msg.verb == Verbs.FROM_CONTROLLER_START_RECON:
        #     self._loop.create_task(self._handle_FROM_CONTROLLER_START_RECON(msg))

        if msg.verb == Verbs.FROM_GUI_CONNECTION_REQUEST:
            self._loop.create_task(self._gui_worker(reader, writer))

        elif msg.verb == Verbs.FROM_CONTROLLER_CONNECTION_REQUEST:
            self._loop.create_task(self._controller_worker(reader, writer))
        else:
            writer.close()

    async def _send_msg(self, writer: StreamWriter, msg: Message):
        writer.write(msg.pack())
        await writer.drain()

    async def _gui_worker(self, reader: StreamReader, writer: StreamWriter):
        self._gui_reader = reader
        self._gui_writer = writer
        try:
            while True:
                data = await self._gui_reader.read(Message.header_size)
                if data:
                    msg = Message.load(data)
                    if msg.verb == Verbs.FROM_CLIENT_PING:
                        await self._send_msg(writer, Message(Verbs.FROM_SERVER_PING))
                    print('_gui_worker', msg)
                await asyncio.sleep(0.01)
        except RuntimeError:
            writer.close()

    async def _controller_worker(self, reader: StreamReader, writer: StreamWriter):
        self._controller_reader = reader
        self._controller_writer = writer
        try:
            while True:
                data = await self._controller_reader.read(Message.header_size)
                if data:
                    msg = Message.load(data)
                    if msg.count:
                        msg.payload = await self._controller_reader.read(msg.count)
                    if msg.verb == Verbs.FROM_CLIENT_PING:
                        await self._send_msg(self._controller_writer, Message(Verbs.FROM_SERVER_PING))
                    elif msg.verb == Verbs.FROM_CONTROLLER_ADD:
                        self._loop.create_task(self._add(msg))
                    elif msg.verb == Verbs.FROM_CONTROLLER_SCAN_FINISHED:
                        self._loop.create_task(self._handle_notify_recon_task_done(msg))

                        print('_controller_worker', msg)
                await asyncio.sleep(0.01)
        except RuntimeError:
            self._controller_writer.close()

    async def _handle_FROM_CONTROLLER_START_RECON(self, msg: Message):
        response = Message

    async def _add(self, msg: Message):
        study_id_len = struct.unpack('>i', msg.payload[:4])[0]
        offset = 4
        study_id = msg.payload[offset:offset + study_id_len].decode('utf-8')
        offset += study_id_len
        series_number = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4
        series_type = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4

        scan_type = ScanTypes.load_by_series_type(series_type)
        scan_task_id = ScanTaskId(study_id, series_number)
        img_num = self._scan_tasks.get(scan_task_id, -1) + 1
        self._scan_tasks[scan_task_id] = img_num
        task = Task(scan_task_id, scan_type, img_num)
        self._tasks.append(task)
        # self._tasks_by_id[(scan_task_id.study_id, scan_task_id.series_number, img_num)] = task
        # return web.json_response({'status': 'ok'})

    async def _notify_gui_task_done(self, task: Task):
        msg = Message(Verbs.FROM_SERVER_IMG_RECON_FINISHED)
        msg.data_size = DataSizes.byte1

        with io.BytesIO() as payload:
            payload.write(ScanTaskId.study_id_len.to_bytes(4, 'big'))
            payload.write(task.scan_task_id.study_id.encode('utf-8'))
            payload.write(task.scan_task_id.series_number.to_bytes(4, 'big'))
            payload.write(task.img_num.to_bytes(4, 'big'))
            payload.write(len(task.img_data).to_bytes(4, 'big'))
            payload.write(task.img_data)
            msg.payload = payload.getvalue()
        if self._gui_writer:
            print(f'RECON: send message len(msg.payload)={len(msg.payload)}')
            await self._send_msg(self._gui_writer, msg)

        if task.scan_task_id in self._scan_complete_tasks:
            await self._notify_gui_recon_done(task.scan_task_id)

    async def _handle_notify_recon_task_done(self, msg: Message):
        print(f'_handle_notify_recon_task_done msg={msg}')
        assert msg.verb == Verbs.FROM_CONTROLLER_SCAN_FINISHED
        study_id_len = struct.unpack('>i', msg.payload[:4])[0]
        offset = 4
        study_id = msg.payload[offset:offset + study_id_len].decode('utf-8')
        offset += study_id_len
        series_number = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4

        scan_task_id = ScanTaskId(study_id, series_number)
        self._scan_complete_tasks.append(scan_task_id)
        if len(self._not_completed_recon_for_scan_task(scan_task_id)) == 0:
            await self._notify_gui_recon_done(scan_task_id)

    def _not_completed_recon_for_scan_task(self, scan_task_id: ScanTaskId) -> List[Task]:
        return list(filter(lambda task: task.scan_task_id == scan_task_id and not task.is_processed, self._tasks))

    async def _notify_gui_recon_done(self, scan_task_id: ScanTaskId):
        print(f'_notify_gui_recon_done scan_task_id={scan_task_id}')
        msg_to_gui = Message(Verbs.FROM_SERVER_RECON_FINISHED)
        msg_to_gui.data_size = DataSizes.byte1
        with io.BytesIO() as payload:
            payload.write(ScanTaskId.study_id_len.to_bytes(4, 'big'))
            payload.write(scan_task_id.study_id.encode('utf-8'))
            payload.write(scan_task_id.series_number.to_bytes(4, 'big'))
            msg_to_gui.payload = payload.getvalue()
        if self._gui_writer:
            await self._send_msg(self._gui_writer, msg_to_gui)

    async def start_work(self):
        while True:
            for task in self._tasks:
                if not task.is_processed:
                    await task.process()
                    if task.is_processed:
                        await self._notify_gui_task_done(task)

            await sleep(0.01)


if __name__ == '__main__':
    server = ReconstructionServer(asyncio.get_event_loop())
    server.start_server()
