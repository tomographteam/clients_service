import asyncio
import threading
import time

import pytest
from tests.model.equipment.subsystem_device_manager.scsc_emulator.tango_emulator_server import SCSCEmulatorServer

from src.model_.control.event.event import Event, EventType
from src.model_.equipment.subsystem_device_manager import SCSCAttr, SCSCManager


async def main(loop):
    tango_emulator = SCSCEmulatorServer()
    await tango_emulator.start()


def start_server():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    asyncio.ensure_future(main(loop))
    loop.run_forever()


@pytest.fixture(scope="module", autouse=True)
def server_fixture():
    thread = threading.Thread(target=start_server)
    thread.daemon = True
    thread.start()
    time.sleep(2)


@pytest.fixture(scope="module")
def scsc_manager():
    return SCSCManager()


def test_start_scanning(scsc_manager):
    data= Event(EventType.start_scan)
    response = scsc_manager.start_scanning(data)
    print("response", response)
    assert 'OK' == response['result']


def test_stop_scanning(scsc_manager):
    response = scsc_manager.stop_scanning()
    print('response', response)
    assert 'OK' == response['result']


def test_to_start_position(scsc_manager):
    response = scsc_manager.to_start_position()
    print('response', response)
    assert 'OK' == response['result']


def test_get_parameters_bce(scsc_manager):
    attr_list = SCSCAttr.BCE_ATTR['attr']

    response = scsc_manager.get_parameters_bce()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_wwire(scsc_manager):
    attr_list = SCSCAttr.WWIRES_ATTR['attr']

    response = scsc_manager.get_parameters_wwire()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_turbo_pump(scsc_manager):
    attr_list = SCSCAttr.TURBO_PUMP_ATTR['attr']

    response = scsc_manager.get_parameters_turbo_pump()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_pes(scsc_manager):
    attr_list = SCSCAttr.PES_ATTR['attr']

    response = scsc_manager.get_parameters_pes()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_chiller(scsc_manager):
    attr_list = SCSCAttr.CHILLER_ATTR['attr']

    response = scsc_manager.get_parameters_chiller()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_collimator(scsc_manager):
    attr_list = SCSCAttr.COLLIMATOR_ATTR['attr']

    response = scsc_manager.get_parameters_collimator()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_dsebct(scsc_manager):
    attr_list = SCSCAttr.DSEBCT_ATTR['attr']

    response = scsc_manager.get_parameters_dsebct()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_hvpsct(scsc_manager):
    attr_list = SCSCAttr.HVPS_CT_ATTR['attr']

    response = scsc_manager.get_parameters_hvps_ct()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_hvpsnwl(scsc_manager):
    attr_list = SCSCAttr.HVPS_NWL_ATTR['attr']

    response = scsc_manager.get_parameters_hvps_nwl()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_io_pu(scsc_manager):
    attr_list = SCSCAttr.ION_PU_ATTR['attr']

    response = scsc_manager.get_parameters_ion_pump()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list


def test_get_parameters_patient_table(scsc_manager):
    attr_list = SCSCAttr.PATIENT_TABLE_ATTR['attr']

    response = scsc_manager.get_parameters_patient_table()
    print('response', response)
    attrs = response
    for attr in attrs:
        assert attr['name'] in attr_list

def test_get_patrameters_devices(scsc_manager):
    dev_list = ['dsebct', 'pes', 'hvps_ct', 'bce', 'wwires', 'collimator', 'hvps_nwl',
                'patient_table', 'chiller', 'turbo_pump', 'ion_pump']
    response = scsc_manager.get_parameters_devices()
    print('response common', response)
    for key, value in response.items():
        assert key in dev_list
