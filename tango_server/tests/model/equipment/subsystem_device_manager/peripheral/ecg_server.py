import asyncio
import csv
import gzip
import io
import math
from asyncio import StreamWriter, StreamReader
from datetime import datetime

from src.model_.equipment.peripheral.ecg_monitor.protocol_message import EcgMessage, Waveform, unix_time_millis
from src.utils.settings import get_ecg_server_config
from src.view.components.functions import scale


def csv_reader(file_obj):
    reader = csv.reader(io.TextIOWrapper(file_obj), delimiter='\t')
    reader.__next__()
    reader.__next__()
    reader.__next__()
    for row in reader:
        yield row


epoch = datetime.utcfromtimestamp(0)


def load_sample_data():
    csv_path = 'resources/DICOM-examples/ecg.csv.gz'
    with gzip.open(csv_path, 'r') as f:
        for date, lead_0, lead_1, lead_2, lead_3, _ in csv_reader(f):
            yield unix_time_millis(datetime.strptime(date, '%Y/%m/%d %H:%M:%S.%f')), \
                  float(lead_0), float(lead_1), float(lead_2), float(lead_3)


def test_waveform(sample_data: tuple) -> Waveform:
    return Waveform(int(scale(-0.2, 0.45, sample_data[1], 0, 1 << 12)))


class EcgServer:
    _server_config = get_ecg_server_config()
    _tick_per_second = 4
    _rows_per_chunk = 60

    def __init__(self, loop):
        self._loop = loop
        self._host = self._server_config['host']
        self._port = self._server_config['port']
        print(self._host, self._port)

    def start_server(self):
        self._loop.run_until_complete(asyncio.start_server(self._handler, self._host, self._port, loop=self._loop))
        self._loop.run_forever()

    async def _handler(self, reader: StreamReader, writer: StreamWriter):
        self._loop.create_task(self._console_worker(reader, writer))

    @staticmethod
    def _pulse() -> int:
        t = datetime.now().second % 360
        return int(math.sin(t / 360 * 6.28) * 5 + 92)

    async def _console_worker(self, reader: StreamReader, writer: StreamWriter):
        print('ECG Server: client connected')
        data = load_sample_data()
        try:
            while True:
                msg = EcgMessage()
                msg.pulse = self._pulse()
                for i in range(self._rows_per_chunk):
                    try:
                        data.__next__()
                        data.__next__()
                        data.__next__()
                        sample = data.__next__()
                        # print(sample[1], test_waveform(sample).voltage)
                        msg.waveform_data.append(test_waveform(sample))
                    except StopIteration:
                        data = load_sample_data()
                writer.write(msg.pack())
                await asyncio.gather(writer.drain(), asyncio.sleep(1.0 / self._tick_per_second))
        except RuntimeError as e:
            writer.close()
            print(f'ECG Server: {e}')
        except (BrokenPipeError, ConnectionResetError):
            pass
        writer.close()
        print('ECG Server: client disconnected')


if __name__ == '__main__':
    # print(int(scale(-0.2, 0.45, 0.416502406180192, 0, 1 << 12)))
    server = EcgServer(asyncio.get_event_loop())
    server.start_server()
