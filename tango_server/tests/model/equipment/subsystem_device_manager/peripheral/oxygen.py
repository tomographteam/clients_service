import random

from tango_server.model.equipment.peripheral.oxygen.state import OxygenMonitorState


class OxygenMonitor:
    def _get_random_state(self) -> OxygenMonitorState:
        state = OxygenMonitorState()
        state.oxygen = int(90 + random.randrange(-5, 10))
        return state

    def get_state(self) -> OxygenMonitorState:
        return self._get_random_state()
