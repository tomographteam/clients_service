import math
from asyncio import sleep
from datetime import datetime

from tango_server.model.equipment.peripheral.ecg_monitor.protocol_message import unix_time_millis

epoch = datetime.utcfromtimestamp(0)


class BreathMonitor:
    _tick_per_second = 5

    def __init__(self):
        self._ask_stop = False
        self._keep_seconds = 100
        self.breath_data = []
        self.breath_data_max_len = self._keep_seconds * self._tick_per_second

    def stop(self):
        self._ask_stop = True

    @staticmethod
    def _breath(now) -> int:
        t = (now.second + now.microsecond / 1000000) % 3.3
        s = math.sin((t / 3.3) * 6.28)
        return int(s * 30 + 50)

    @classmethod
    def _sample_data(cls):
        while True:
            now = datetime.now()
            yield unix_time_millis(now), cls._breath(now) - 50

    def _add_breath_data(self, time: int, breath: int):
        self.breath_data.append((time, breath))
        if len(self.breath_data) > self.breath_data_max_len:
            self.breath_data = self.breath_data[-self.breath_data_max_len:]

    async def work(self):
        for time, breath in self._sample_data():
            self._add_breath_data(time, breath)
            if self._ask_stop:
                break

            await sleep(1 / self._tick_per_second)
