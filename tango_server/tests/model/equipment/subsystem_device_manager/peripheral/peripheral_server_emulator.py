import asyncio
import json

from aiohttp import web
from aiohttp.web_request import Request

from tango_server.model.equipment.peripheral.blood_pressure.client import BloodPressureMonitorClient
from tango_server.model.equipment.peripheral.breath_monitor.client import BreathMonitorClient
from tango_server.model.equipment.peripheral.contrast_injector import ContrastInjectorClient
from tango_server.model.equipment.peripheral.oxygen.client import OxygenMonitorClient
from tango_server.utils.settings import get_peripheral_server_config
from tango_server.tests.model.equipment.subsystem_device_manager.peripheral.blood_pressure import BloodPressureMonitor
from tango_server.tests.model.equipment.subsystem_device_manager.peripheral.breath_monitor import BreathMonitor
from tango_server.tests.model.equipment.subsystem_device_manager.peripheral.contrast_injector import ContrastInjector
from tango_server.tests.model.equipment.subsystem_device_manager.peripheral.oxygen import OxygenMonitor


class PeripheralServer:
    _server_config = get_peripheral_server_config()

    def __init__(self):
        self._host = self._server_config['contrast_injector_host']
        self._port = self._server_config['contrast_injector_port']
        self._app = web.Application()
        self._app.add_routes(self._routes())
        self._enable_blood_pressure = True
        self._enable_breath_monitor = True
        self._enable_oxygen = True

        self._contrast_injector = ContrastInjector()
        self._blood_pressure = BloodPressureMonitor()
        self._oxygen = OxygenMonitor()
        self._breath_monitor = BreathMonitor()

    def _routes(self):
        return [
            web.get(ContrastInjectorClient.STATE_TEMPLATE, self._contrast_injector_state),
            web.post(ContrastInjectorClient.SET_CONTRAST_FLOW_SPEED, self._set_contrast_flow_speed),
            web.post(ContrastInjectorClient.SET_SOLVENT_FLOW_SPEED, self._set_liquid_flow_speed),
            web.post(ContrastInjectorClient.CONTRAST_INJECTOR_STOP, self._contrast_injector_stop),
            web.post(ContrastInjectorClient.CONTRAST_INJECTOR_POUR, self._contrast_injector_pour),

            web.get(BloodPressureMonitorClient.STATE_TEMPLATE, self._blood_pressure_state),

            web.get(OxygenMonitorClient.STATE_TEMPLATE, self._oxygen_state),

            web.get(BreathMonitorClient.STREAM_TEMPLATE, self._breath_stream),

            # web.get(EcgMonitorClient.STREAM_TEMPLATE, self._ecg_stream),
            # web.get(EcgMonitorClient.PULSE_TEMPLATE, self._pulse),

            web.post('/enable_monitors', self._enable_monitors),

        ]

    async def _enable_monitors(self, request: Request):
        data = await request.json()
        # self._enable_ecg = data['ecg']
        self._enable_blood_pressure = data['blood_pressure']
        self._enable_breath_monitor = data['breath_monitor']
        self._enable_oxygen = data['oxygen']
        return web.json_response({'status': 'ok'})

    async def _breath_stream(self, request):
        interval = 0.2
        resp = web.StreamResponse(status=200,
                                  reason='OK',
                                  headers={'Content-Type': 'application/json'})
        if resp is None:
            return

        await resp.prepare(request)
        while True:
            try:
                if not self._enable_breath_monitor:
                    await asyncio.sleep(0.1)
                    continue
                response_data = self._breath_monitor.breath_data
                response_str = json.dumps(response_data) + '\n'
                if resp is None:
                    return
                await resp.write(response_str.encode('utf-8'))
                await asyncio.sleep(interval)
            except Exception as e:
                break

    async def _set_liquid_flow_speed(self, request):
        flow_speed = float(request.match_info['flow_speed'])
        print(f'_set_liquid_flow_speed {flow_speed}')
        self._contrast_injector.set_solvent_flow_speed(flow_speed)
        return web.json_response({'status': 'ok'})

    async def _contrast_injector_stop(self, request):
        print(f'_contrast_injector_stop')
        self._contrast_injector.set_solvent_flow_speed(0)
        self._contrast_injector.set_contrast_flow_speed(0)
        return web.json_response({'status': 'ok'})

    async def _contrast_injector_pour(self, request):
        print(f'_contrast_injector_pour')
        self._contrast_injector.pour()
        return web.json_response({'status': 'ok'})

    async def _set_contrast_flow_speed(self, request):
        flow_speed = float(request.match_info['flow_speed'])
        print(f'_set_contrast_flow_speed {flow_speed}')
        self._contrast_injector.set_contrast_flow_speed(flow_speed)
        return web.json_response({'status': 'ok'})

    async def _contrast_injector_state(self, request):
        state = self._contrast_injector.get_state()
        return web.json_response(state.to_dict())

    async def _blood_pressure_state(self, request):
        state = self._blood_pressure.get_state()
        state.enabled = self._enable_blood_pressure
        return web.json_response(state.to_dict())

    async def _oxygen_state(self, request):
        state = self._oxygen.get_state()
        state.enabled = self._enable_oxygen
        return web.json_response(state.to_dict())

    def start_server(self):

        async def start(_app):
            task1 = asyncio.create_task(self._contrast_injector.work())
            task2 = asyncio.create_task(self._breath_monitor.work())

            yield

            task1.cancel()
            task2.cancel()


        self._app.cleanup_ctx.append(start)

        # loop.create_task(self._contrast_injector.work())
        # loop.create_task(self._breath_monitor.work())
        web.run_app(self._app, host=self._host, port=self._port)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    server = PeripheralServer()
    server.start_server()
