from asyncio import sleep

from tango_server.model.equipment.peripheral.contrast_injector import ContrastInjectorState


class ContrastInjector:
    _tick_per_second = 1

    def __init__(self):
        self._contrast_volume = 112
        self._solvent_volume = 500
        self._contrast_flow_speed = 0
        self._solvent_flow_speed = 0
        self._pressure = 0
        self._ask_stop = False

    def stop(self):
        self._ask_stop = True

    def set_contrast_flow_speed(self, flow_speed):
        self._contrast_flow_speed = flow_speed

    def set_solvent_flow_speed(self, flow_speed):
        self._solvent_flow_speed = flow_speed

    def get_state(self) -> ContrastInjectorState:
        state = ContrastInjectorState()
        state.contrast_volume = self._contrast_volume
        state.solvent_volume = self._solvent_volume
        state.contrast_flow_speed = self._contrast_flow_speed
        state.solvent_flow_speed = self._solvent_flow_speed
        state.pressure = self._pressure
        return state

    def pour(self):
        self._contrast_volume = 112
        self._solvent_volume = 500

    async def work(self):
        while not self._ask_stop:
            self._contrast_volume -= self._contrast_flow_speed / self._tick_per_second
            self._solvent_volume -= self._solvent_flow_speed / self._tick_per_second

            if self._contrast_volume <= 0:
                self._contrast_volume = 0
                self._contrast_flow_speed = 0

            if self._solvent_volume <= 0:
                self._solvent_volume = 0
                self._solvent_flow_speed = 0

            self._pressure = 1 if self._contrast_flow_speed > 0 or self._solvent_flow_speed > 0 else 0
            await sleep(1 / self._tick_per_second)
