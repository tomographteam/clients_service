import random

from tango_server.model.equipment.peripheral.blood_pressure.state import BloodPressureMonitorState


class BloodPressureMonitor:
    def _get_random_state(self) -> BloodPressureMonitorState:
        state = BloodPressureMonitorState()
        state.systolic = int(120 + random.randrange(-15, 15))
        state.dystolic = int(80 + random.randrange(-15, 15))
        return state

    def get_state(self) -> BloodPressureMonitorState:
        return self._get_random_state()
