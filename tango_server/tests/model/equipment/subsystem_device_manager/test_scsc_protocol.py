import json
import time

import pytest
from src.model_.equipment.subsystem_device_manager.scsc_manager import SCSCManager

from src.model_.control.event.event import Event, EventType

start_event_data = {
    "Groups": [{
        "scan": {
            "protocol": "test",
            "scan_type": "test",
            "stop_position": 123,
            "auto_calculated": "test",
            "pitch": 0.5,
            "collimator_width": 0.011,
            "table_speed": 10,
            "kv": 120,
            "dose": 3.2,
            "ma": 100
        },
        "recon": {
            "width": 123.456,
            "height": 123.456,
            "resolution": "test",
            "axial_distance": 123,
            "slice_size": 123.456
        }
    }, {
        "scan": {
            "protocol": "test",
            "scan_type": "test",
            "stop_position": 123,
            "auto_calculated": "test",
            "pitch": 0.5,
            "collimator_width": 0.011,
            "table_speed": 10,
            "kv": 120,
            "dose": 3.2,
            "ma": 100
        },
        "recon": {
            "width": 123.456,
            "height": 123.456,
            "resolution": "test",
            "axial_distance": 123,
            "slice_size": 123.456,
            "scan_size": 123.456,

        }
    }]
}

set_table_position = '100'

@pytest.fixture(scope="session", autouse=True)
def event_data():
    return {'start_event_data': json.dumps(start_event_data), 'set_table_position_event_data': set_table_position}


@pytest.fixture(scope="session")
def scsc_manager():
    return SCSCManager()


def test_start_scanning(requests_mock, scsc_manager, event_data):
    requests_mock.get('http://0.0.0.0:8000/tango/rest/rc4/subscriptions/1/event-stream', text=json.dumps({'result':'OK'}))
    adapter = requests_mock.put('http://0.0.0.0:8000/tango/rest/rc4/hosts/0.0.0.0/10000/devices/control/dsebct/1/commands/StartScan', text=json.dumps({'result':'OK'}))
    time.sleep(1)

    request_data = event_data['start_event_data']
    data = Event(EventType.start_scan, request_data)
    response = scsc_manager.start_scanning(data)
    assert 'OK' == str(response['result'])
    assert request_data == adapter.last_request.json()

def test_stop_scanning(requests_mock, scsc_manager):
    requests_mock.get('http://0.0.0.0:8000/tango/rest/rc4/subscriptions/1/event-stream', text=json.dumps({'result':'OK'}))
    adapter = requests_mock.put('http://0.0.0.0:8000/tango/rest/rc4/hosts/0.0.0.0/10000/devices/control/dsebct/1/commands/StopScan', text=json.dumps({'result':'OK'}))
    time.sleep(1)
    response = scsc_manager.stop_scanning()
    assert 'OK' == str(response['result'])

def test_state(requests_mock, scsc_manager):
    requests_mock.get('http://0.0.0.0:8000/tango/rest/rc4/subscriptions/1/event-stream', text=json.dumps({'result':'OK'}))
    adapter = requests_mock.put('http://0.0.0.0:8000/tango/rest/rc4/hosts/0.0.0.0/10000/devices/control/dsebct/1/commands/State', text=json.dumps({'result':'OK'}))
    time.sleep(1)
    response = scsc_manager._pes_status()
    assert 'OK' == str(response['result'])

def test_status(requests_mock, scsc_manager):
    requests_mock.get('http://0.0.0.0:8000/tango/rest/rc4/subscriptions/1/event-stream', text=json.dumps({'result':'OK'}))
    adapter = requests_mock.put('http://0.0.0.0:8000/tango/rest/rc4/hosts/0.0.0.0/10000/devices/control/dsebct/1/commands/Status', text=json.dumps({'result':'OK'}))
    time.sleep(1)
    response = scsc_manager.status()
    assert 'OK' == str(response['result'])

def test_set_table_position(requests_mock, scsc_manager, event_data):
    requests_mock.get('http://0.0.0.0:8000/tango/rest/rc4/subscriptions/1/event-stream', text=json.dumps({'result':'OK'}))
    adapter = requests_mock.put('http://0.0.0.0:8000/tango/rest/rc4/hosts/0.0.0.0/10000/devices/control/dsebct/1/commands/SetTablePosition', text=json.dumps({'result':'OK'}))
    time.sleep(1)

    request_data = event_data['set_table_position_event_data']
    data = Event(EventType.start_scan, request_data)
    response = scsc_manager.set_table_position(data)
    assert 'OK' == str(response['result'])
    assert request_data == adapter.last_request.text

