import logging
from logging import getLogger

import asyncio
import io
import struct
from asyncio import StreamReader, StreamWriter
from typing import Optional, Callable

import PIL
from PIL.Image import Image

from tango_server.model.data.result_manager.reconstruction_client.message import Message, Verbs, DataSizes
from tango_server.model.data.scan_task import ScanTaskId
from tango_server.utils.settings import get_reconstruction_server_config


class ReconstructionClient:
    _log = logging.getLogger('recon')
    _server_config = get_reconstruction_server_config()

    def __init__(self, loop):
        self._host = self._server_config['host']
        self._port = self._server_config['port']
        self._loop = loop

        self._reader: Optional[StreamReader] = None
        self._writer: Optional[StreamWriter] = None
        self.on_img_recon_finished: Optional[Callable] = None
        self.on_recon_finished: Optional[Callable] = None

    async def connect_as_gui(self):
        self._reader, self._writer = await asyncio.open_connection(self._host, self._port,
                                                                   loop=self._loop)
        await self._send_msg(Message(Verbs.FROM_GUI_CONNECTION_REQUEST))
        self._loop.create_task(self._wait_message())

    async def connect_as_scsc(self):
        self._reader, self._writer = await asyncio.open_connection(self._host, self._port,
                                                                   loop=self._loop)
        await self._send_msg(Message(Verbs.FROM_CONTROLLER_CONNECTION_REQUEST))
        self._loop.create_task(self._wait_message())

    async def start_recon(self):
        await self._send_msg(Message(Verbs.FROM_CLIENT_PING))

    async def _send_msg(self, msg: Message):
        if not msg:
            return
        self._writer.write(msg.pack())
        await self._writer.drain()

    async def _wait_message(self):
        while True:
            data = await self._reader.read(Message.header_size)
            if data:
                msg = Message.load(data)
                # print(msg)
                if msg.count:
                    need_bytes_count = msg.count * msg.data_size.size
                    self._log.debug(f'_recon_img_finished_handler wait msg len={need_bytes_count}')
                    received_bytes_count = 0
                    try:
                        while received_bytes_count < need_bytes_count:
                            payload = await self._reader.read(need_bytes_count)
                            received_bytes_count += len(payload)
                            # self._log.debug(f'payload len={len(payload)}, {msg.payload}')
                            msg.payload += payload
                            self._log.debug(
                                f'received_bytes_count={received_bytes_count} need_bytes_count={need_bytes_count}')
                    except Exception as e:
                        self._log.exception(e, exc_info=True)

                    self._log.debug(f'_recon_img_finished_handler msg payload len={len(msg.payload)}')
                    # msg.payload

                if msg.verb == Verbs.FROM_SERVER_PING:
                    await self._send_msg(Message(Verbs.FROM_CLIENT_PING))
                elif msg.verb == Verbs.FROM_SERVER_IMG_RECON_FINISHED:
                    self._loop.create_task(self._recon_img_finished_handler(msg))
                elif msg.verb == Verbs.FROM_SERVER_RECON_FINISHED:
                    self._loop.create_task(self._recon_finished_handler(msg))
            await asyncio.sleep(0.01)

    async def _recon_img_finished_handler(self, msg: Message):
        self._log.debug(f'_recon_img_finished_handler len(msg.payload)={len(msg.payload)}')
        study_id_len = struct.unpack('>i', msg.payload[:4])[0]
        offset = 4
        study_id = msg.payload[offset:offset + study_id_len].decode('utf-8')
        offset += study_id_len
        series_number = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4
        img_num = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4
        img_len = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4
        img_data = msg.payload[offset: offset + img_len]
        self._log.debug(f'_recon_img_finished_handler len(img_data)={len(img_data)} img_len={img_len}')
        img = PIL.Image.open(io.BytesIO(img_data))
        self._log.debug(f'_recon_img_finished_handler img={img}')
        f_name = f'/storage/emulated/0/____tmg/Tomograph/app_data/frame_{img_num}.jpeg'
        try:
            with open(f_name + '_dump', 'wb') as f:
                f.write(img_data)
            img.save(f_name, 'JPEG')
        except Exception as e:
            self._log.exception(f'_recon_img_finished_handler e={e}', exc_info=True)
        if self.on_img_recon_finished:
            await self.on_img_recon_finished(study_id, series_number, img_num, img)

    async def _recon_finished_handler(self, msg: Message):
        study_id_len = struct.unpack('>i', msg.payload[:4])[0]
        offset = 4
        study_id = msg.payload[offset:offset + study_id_len].decode('utf-8')
        offset += study_id_len
        series_number = struct.unpack('>i', msg.payload[offset:offset + 4])[0]
        offset += 4

        if self.on_recon_finished:
            await self.on_recon_finished(study_id, series_number)

    async def add(self, scan_task_id: ScanTaskId, series_type: int):
        msg = Message(Verbs.FROM_CONTROLLER_ADD)
        msg.data_size = DataSizes.byte1

        with io.BytesIO() as payload:
            payload.write(ScanTaskId.study_id_len.to_bytes(4, 'big'))
            payload.write(scan_task_id.study_id.encode('utf-8'))
            payload.write(scan_task_id.series_number.to_bytes(4, 'big'))
            payload.write(series_type.to_bytes(4, 'big'))

            msg.payload = payload.getvalue()
            # msg.payload.close()
        await self._send_msg(msg)

    async def from_controller_scan_finished(self, scan_task_id: ScanTaskId):
        msg = Message(Verbs.FROM_CONTROLLER_SCAN_FINISHED)
        msg.data_size = DataSizes.byte1

        with io.BytesIO() as payload:
            payload.write(ScanTaskId.study_id_len.to_bytes(4, 'big'))
            payload.write(scan_task_id.study_id.encode('utf-8'))
            payload.write(scan_task_id.series_number.to_bytes(4, 'big'))

            msg.payload = payload.getvalue()

        await self._send_msg(msg)

    # def close(self):
    #     loop = asyncio.get_event_loop()
    #     asyncio.wait_for(loop.create_task(self._session.close()), 1)

    # async def ls(self):
    #     print(await self._fetch('list'))

# async def main():


# client = ReconstructionClient(loop)
# await client.connect_as_scsc()
# scan_task = ScanTask.from_json(open('scan_task.json', 'r').read())
# img = PIL.Image.open(open('resources/images/rentgen.jpg', 'rb'))
# await client.add(scan_task, 0, img)
# await client.start_recon()
# await asyncio.gather(*[client.add('test', 1, i) for i in range(100)])
# await asyncio.gather(*[client.add('test', 2, i) for i in range(100)])
# print(await client.status('test', 1))
# print(await client.status('test', 2))
# print(await client.get('test', 1, 2))
# await client.ls()

#
# #
# #
# if __name__ == '__main__':
#     loop = asyncio.get_event_loop()
#     loop.create_task(main())
#     loop.run_forever()
