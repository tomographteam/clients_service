import asyncio
import uuid

from src.model_.data.protocol.scan_types import ScanTypes
from src.model_.data.result_manager.reconstruction_client.client import ReconstructionClient
from src.model_.data.scan_task import ScanTaskId


async def main():
    client = ReconstructionClient(loop)
    await client.connect_as_scsc()
    # scan_task = ScanTask.from_json(open('scan_task.json', 'r').read())
    # img = PIL.Image.open(open('resources/images/rentgen.jpg', 'rb'))
    task_id = ScanTaskId(uuid.uuid4().hex, 1)
    await client.add(task_id, ScanTypes.scout_two_side)
    await client.add(task_id, ScanTypes.scout_two_side)
    # await client.start_recon()


#
#
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()
