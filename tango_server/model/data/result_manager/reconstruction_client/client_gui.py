import asyncio

from src.model_.data.result_manager.reconstruction_client.client import ReconstructionClient


async def main():
    client = ReconstructionClient(loop)
    await client.connect_as_gui()


#
#
if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()
