import struct
from typing import List
from typing import Optional

from tango_server.model.data.data_enum import DataEnumList
from tango_server.model.data.data_enum import DataEnumMember


class _BinaryMember(DataEnumMember):
    def __init__(self, name: str, code: int):
        super().__init__(name)
        self.code = code


class _BinaryList(DataEnumList):
    @classmethod
    def load_by_code_eq(cls, code: int) -> Optional[_BinaryMember]:
        for obj in cls.get_list():
            if obj.code == code:
                return obj

    @classmethod
    def load_by_code_and(cls, code: int) -> Optional[_BinaryMember]:
        for obj in cls.get_list():
            if obj.code & code:
                return obj

    @classmethod
    def get_list(cls) -> List[_BinaryMember]:
        return cls._list


class Verb(_BinaryMember):
    pass


# Протокол основан на описаниях и коде imfcl и recon_client, но в значительной степени додуман для
# того что бы им можно было пользоватся.

# Код эмулятора сервера:
# tests/model/equipment/subsystem_device_manager/scsc_emulator/reconstruction_emulator/reconstruction_server.py
# Код клиента, общий для gui и scsc tango_server/model/data/result_manager/reconstruction_client/client.py
# клиент при подключении шлёт сообщение FROM_CONTROLLER_CONNECTION_REQUEST/FROM_GUI_CONNECTION_REQUEST
#           SCSC -> Recon server
# Код эмулятора SCSC tests/model/equipment/subsystem_device_manager/scsc_emulator/tango/dsebct.py
# SCSC отправляет данные на реконструкцию с сообщениями FROM_CONTROLLER_ADD, метод Client.add
# разбирается в ReconstructionServer._add
# по завершению сканирования SCSC уведомляет об этом сервер сообщением FROM_CONTROLLER_SCAN_FINISHED,
# метод Client.from_controller_scan_finished разбирается в ReconstructionServer._handle_notify_recon_task_done

#           Recon server -> GUI
# FROM_SERVER_IMG_RECON_FINISHED готовое изображение для GUI для отображения в фильме,
# метод ReconstructionServer._notify_gui_task_done разбирается в Client._recon_img_finished_handler
# FROM_SERVER_RECON_FINISHED уведомление для GUI о том, что изображений в этом сканировании больше не будет
# метод ReconstructionServer._notify_gui_recon_done, разбирается в Client._recon_finished_handler

class Verbs(_BinaryList):
    # клиент представляется контроллером и будет отправлять данные для реконструкции
    FROM_CONTROLLER_CONNECTION_REQUEST = Verb('FROM_CONTROLLER_CONNECTION_REQUEST', 0x0101)
    # клиент представляется GUI и будет получать готовые изображения
    FROM_GUI_CONNECTION_REQUEST = Verb('FROM_GUI_CONNECTION_REQUEST', 0x0103)
    # просто пинг для проверки
    FROM_CLIENT_PING = Verb('FROM_CLIENT_PING', 0x0104)
    FROM_SERVER_PING = Verb('FROM_SERVER_PING', 0x0105)
    # не используется
    FROM_SERVER_CONNECTION_SUCCESS = Verb('FROM_CONTROLLER_CONNECTION_REQUEST', 0x0201)
    # не используется
    FROM_SERVER_RECON_STARTED = Verb('FROM_SERVER_RECON_STARTED', 0x0202)

    # готовое изображение для GUI для отображения в фильме
    FROM_SERVER_IMG_RECON_FINISHED = Verb('FROM_SERVER_IMG_RECON_FINISHED', 0x0204)
    # уведомление для GUI о том, что изображений в этом сканировании больше не будет
    FROM_SERVER_RECON_FINISHED = Verb('FROM_SERVER_RECON_FINISHED', 0x0203)

    # от scsc данные для реконструкции
    FROM_CONTROLLER_ADD = Verb('FROM_CONTROLLER_ADD', 0x0106)
    # от scsc уведомление для сервера о том, что сканирование завершено
    FROM_CONTROLLER_SCAN_FINISHED = Verb('FROM_CONTROLLER_SCAN_FINISHED', 0x0107)

    # не используется
    FROM_CONTROLLER_START_RECON = Verb('FROM_CONTROLLER_START_RECON', 0x0102)

    _UNKNOWN_MESSAGE = Verb('_UNKNOWN_MESSAGE', 0)

    _list = [FROM_CONTROLLER_CONNECTION_REQUEST, FROM_GUI_CONNECTION_REQUEST, FROM_SERVER_CONNECTION_SUCCESS,
             FROM_SERVER_RECON_STARTED, FROM_SERVER_IMG_RECON_FINISHED,
             FROM_SERVER_RECON_FINISHED, FROM_CONTROLLER_START_RECON, FROM_CONTROLLER_ADD,
             FROM_CONTROLLER_SCAN_FINISHED,

             FROM_CLIENT_PING, FROM_SERVER_PING
             ]


class DataSize(_BinaryMember):
    def __init__(self, name: str, size: int, code: int):
        super().__init__(name, code)
        self.size = size


class DataSizes(_BinaryList):
    byte1 = DataSize('byte1', 1, 0x01 << 0)
    byte2 = DataSize('byte2', 2, 0x01 << 1)
    byte4 = DataSize('byte4', 4, 0x01 << 2)
    byte8 = DataSize('byte8', 8, 0x01 << 3)

    _list = [byte1, byte2, byte4, byte8]


class DataType(_BinaryMember):
    pass


class DataTypes(_BinaryList):
    binary = DataType('binary', 0x01 << 4)
    integer = DataType('integer', 0x01 << 5)
    floating = DataType('floating', 0x01 << 6)

    _list = [binary, integer, floating]


class Message:
    header_size = 8
    _header_format = '>BHIx'

    _has_verb_code = 0x01 << 7

    def __init__(self, verb: Verb):
        self.has_verb = True
        self.data_type = DataTypes.integer
        self.data_size = DataSizes.byte4
        self.verb = verb
        self.count = 0
        self.payload: [int] = []

    def pack(self):
        msg = bytearray()
        s = struct.Struct(self._header_format)
        type_and_data_byte_size = self.data_type.code | self.data_size.code
        if self.has_verb:
            type_and_data_byte_size |= self._has_verb_code

        header = s.pack(type_and_data_byte_size, self.verb.code, len(self.payload))
        msg.extend(header)
        # payload = bytearray()
        # for val in self.payload:
        #     payload.extend(val.to_bytes(4, 'big'))
        msg.extend(self.payload)
        return msg

    @classmethod
    def load(cls, data):
        header = data[:cls.header_size]
        type_and_data_byte_size, verb, count = struct.unpack(cls._header_format, header)
        msg = Message(Verbs.load_by_code_eq(verb))
        msg.has_verb = bool(type_and_data_byte_size | cls._has_verb_code)
        msg.data_type = DataTypes.load_by_code_and(type_and_data_byte_size)
        msg.data_size = DataSizes.load_by_code_and(type_and_data_byte_size)
        msg.count = count
        msg.payload = data[cls.header_size:]
        return msg

    def __repr__(self):
        data_type = self.data_type or ''
        data_size = self.data_size or ''
        count = self.count or ''
        return f'Message: {self.verb.name} {data_type} {data_size} {count}'.strip()
