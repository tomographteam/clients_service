import os
from typing import Callable, List, Optional

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.utils.settings import get_path_config
from tango_server.view.translator import Translator


class ScanType(DataEnumMember):
    _icons_path = get_path_config()['ui_icons']

    def __init__(self, name: str, index: int, icon: str, tr_func: Callable):
        super().__init__(name, tr_func)
        self.icon = icon
        self.index = index

    def get_icon_path(self) -> str:
        return os.path.join(self._icons_path, self.icon)


class ScanTypes(DataEnumList):
    _tr = Translator.inst()
    scout_two_side = ScanType(name='scout_two_side',
                              index=1,
                              icon='scout.png',
                              tr_func=_tr.tr_scan_scout_two_side)

    # scout_front = ScanType('scout_front', _tr.tr_scan_scout_front)
    # scout_side = ScanType('scout_side', _tr.tr_scan_scout_side)
    helical = ScanType(name='helical',
                          index=10,
                          icon='spiral.png',
                          tr_func=_tr.tr_general_scan)

    fluoro = ScanType(name='fluoro',
                      index=2,
                      icon='fluoro.png',
                      tr_func=_tr.tr_fluoro_scan)

    trigger_start = ScanType(name='trigger_start',
                             index=102,
                             icon='syringe.png',
                             tr_func=_tr.tr_scan_trigger_start)

    trigger_start_first_slice = ScanType(name='trigger_start_first_slice',
                                         index=4,
                                         icon='',
                                         tr_func=_tr.tr_scan_trigger_start)

    cardiac = ScanType(name='cardiac',
                       icon='spiral.png',
                       tr_func=_tr.tr_cardiac_scan,
                       index=0)

    radiology = ScanType(name='radiology',
                         icon='spiral.png',
                         tr_func=_tr.tr_radiology_scan,
                         index=3)

    hi_res = ScanType(name='hiRes',
                      icon='spiral.png',
                      tr_func=_tr.tr_hi_res_scan,
                      index=4)

    flow = ScanType(name='flow',
                    icon='spiral.png',
                    tr_func=_tr.tr_flow_scan,
                    index=5)

    cine = ScanType(name='cine',
                    icon='spiral.png',
                    tr_func=_tr.tr_cine_scan,
                    index=6)

    dark_calib = ScanType(name='darkCalib',
                          icon='spiral.png',
                          tr_func=_tr.tr_dark_calib_scan,
                          index=7)

    air_calib = ScanType(name='airCalib',
                         icon='spiral.png',
                         tr_func=_tr.tr_air_calib_scan,
                         index=8)

    multi_Pin_Calib = ScanType(name='multiPinCalib',
                               icon='spiral.png',
                               tr_func=_tr.tr_multi_pin_calib_scan,
                               index=9)

    upper_half = ScanType(name='upperHalf',
                          icon='spiral.png',
                          tr_func=_tr.tr_upper_half_scan,
                          index=11)

    bad_tile_detection = ScanType(name='badDexelDetection',
                                  icon='spiral.png',
                                  tr_func=_tr.tr_bad_tile_detection_scan,
                                  index=12)

    water_calib = ScanType(name='waterCalib',
                           icon='spiral.png',
                           tr_func=_tr.tr_water_calib_scan,
                           index=13)

    preprocess_only = ScanType(name='preprocessOnly',
                           icon='spiral.png',
                           tr_func=_tr.tr_preprocess_only_scan,
                           index=14)

    xray_tuning = ScanType(name='xrayTuning',
                               icon='spiral.png',
                               tr_func=_tr.tr_xray_tuning_scan,
                               index=15)

    null_processing = ScanType(name='nullProcessing',
                           icon='spiral.png',
                           tr_func=_tr.tr_null_processing_scan,
                           index=16)

    # cardio_start = ScanType(name='cardio_start',
    #                         index=3,
    #                         icon='heart.png',
    #                         tr_func=_tr.tr_cardio_start)

    calibration_scan_types = (air_calib, multi_Pin_Calib, water_calib, dark_calib)
    scout_scan_types = (scout_two_side,)
    general_scan_types = (helical, cardiac, radiology, hi_res, flow, cine, upper_half, 
                          bad_tile_detection, preprocess_only, xray_tuning, null_processing,
                          air_calib, multi_Pin_Calib, water_calib, dark_calib)

    _list = [scout_two_side, helical, fluoro, trigger_start,
             cardiac, radiology, hi_res, flow, cine, dark_calib, air_calib, multi_Pin_Calib, upper_half,
             bad_tile_detection, water_calib, preprocess_only, xray_tuning, null_processing
             ]

    @classmethod
    def get_list(cls) -> List[ScanType]:
        return cls._list

    @classmethod
    def is_scout_scan(cls, scan_type: ScanType):
        return scan_type in cls.scout_scan_types

    @classmethod
    def is_general_scan(cls, scan_type: ScanType):
        return scan_type in cls.general_scan_types

    @classmethod
    def load_by_series_type(cls, series_type: int) -> Optional[ScanType]:
        for scan_type in cls.get_list():
            if scan_type.index == series_type:
                return scan_type


class AutoCalculated(DataEnumMember):
    pass


class AutoCalculates(DataEnumList):
    _tr = Translator.inst()
    Collimator = AutoCalculated('collimator', _tr.tr_collimator)
    TableSpeed = AutoCalculated('table_speed', _tr.tr_table_speed)
    _list = (Collimator, TableSpeed)

    @classmethod
    def get_list(cls):
        return cls._list


class TableMotion(DataEnumMember):
    pass


class TableMotions(DataEnumList):
    _tr = Translator.inst()
    Moving = TableMotion('moving', _tr.tr_table_motions_moving)
    Stationary = TableMotion('stationary', _tr.tr_table_motions_stationary)
    _list = (Stationary, Moving)

    @classmethod
    def get_list(cls):
        return cls._list


class ContrastType(DataEnumMember):
    pass


class ContrastTypes(DataEnumList):
    _tr = Translator.inst()
    no_contrast = ContrastType('no_contrast', _tr.tr_no_contrast)
    contrast_1 = ContrastType('contrast_1', _tr.tr_contrast_1)
    _list = (no_contrast, contrast_1)

    @classmethod
    def get_list(cls):
        return cls._list
