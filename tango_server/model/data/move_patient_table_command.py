

class MovePatientTableCommand:
    forward = 'move_table_forward'
    backward = 'move_table_backward'
    up = 'move_table_up'
    down = 'move_table_down'


class MoveTableToPosition:
    def __init__(self, position: int, speed: int):
        self.position = position
        self.speed = speed

    def to_dict(self):
        return {
            'position': self.position,
            'speed': self.speed
        }
