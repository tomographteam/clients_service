import os
from functools import lru_cache
from typing import Tuple, List, Optional, Dict

import PIL
from PIL.Image import Image
from PyQt5.QtGui import QPixmap

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.utils.settings import get_tomograph_config, get_path_config
from tango_server.view.translator import Translator


class PatientPosition(DataEnumMember):
    _max_x = get_tomograph_config()['patient_table_width']
    _max_y = get_tomograph_config()['patient_table_len']
    _max_z = get_tomograph_config()['scan_area_max_z']
    _img_path = get_path_config()['patient_position']

    _images_scan_areas = get_path_config()['images_scan_areas']

    _cache: Dict[str, QPixmap] = {}

    def __init__(self, name: str, tr_func=None, dicom_name: str = None, ):
        super().__init__(name, tr_func)
        self.dicom_name = dicom_name

    def __hash__(self) -> int:
        return self.name.__hash__()

    @classmethod
    def _file_path(cls, name: str, side: str) -> str:
        return os.path.join(cls._images_scan_areas, f'{name}_{side}.png')

    def get_position_imgs(self) -> Tuple[Image, Image]:
        front = self._try_open_image(self._file_path('head_to_gentry_on_back_arms_to_legs', 'front'))
        side = self._try_open_image(self._file_path('head_to_gentry_on_back_arms_to_legs', 'side'))
        # front = self._try_open_image(self._file_path(self.name, 'front'))
        # side = self._try_open_image(self._file_path(self.name, 'side'))
        return front, side

    def _try_open_image(self, file_path: str):
        if os.path.isfile(file_path):
            return PIL.Image.open(file_path)
        else:
            return PIL.Image.Image()

    def get_pixmap(self) -> QPixmap:
        return self._get_pixmap_by_name(self.name)

    @lru_cache(maxsize=None)
    def _get_pixmap_by_name(self, name: str):
        path = os.path.join(self._img_path, name) + '.png'
        return QPixmap(path)


class PatientPositions(DataEnumList):
    _tr = Translator.inst()

    # 1. Головой к гентри Лежа на спине руки к ногам
    head_to_gentry_on_back_arms_to_legs = PatientPosition('head_to_gentry_on_back_arms_to_legs',
                                                          _tr.tr_head_to_gentry_on_back_arms_to_legs, 'HFS')
    # 2. Головой к гентри Лежа на спине руки к гентри
    head_to_gentry_on_back_arms_to_gentry = PatientPosition('head_to_gentry_on_back_arms_to_gentry',
                                                            _tr.tr_head_to_gentry_on_back_arms_to_gentry)

    # 3. Головой к гентри Лежа на животе руки под голову (лоб упирается в предплечья)
    head_to_gentry_on_stomach_arms_under_head = PatientPosition('head_to_gentry_on_stomach_arms_under_head',
                                                                _tr.tr_head_to_gentry_on_stomach_arms_under_head, 'HFP')
    # 4. Головой к гентри Лежа на животе руки к гентри
    head_to_gentry_on_stomach_arms_to_gentry = PatientPosition('head_to_gentry_on_stomach_arms_to_gentry',
                                                               _tr.tr_head_to_gentry_on_stomach_arms_to_gentry)
    # 5. Головой к гентри Лежа на правом боку руки под голову
    head_to_gentry_on_right_side_arms_under_head = PatientPosition('head_to_gentry_on_right_side_arms_under_head',
                                                                   _tr.tr_head_to_gentry_on_right_side_arms_under_head,
                                                                   'HFDR')
    # 6. Головой к гентри Лежа на левом боку руки под голову
    head_to_gentry_on_left_side_arms_under_head = PatientPosition('head_to_gentry_on_left_side_arms_under_head',
                                                                  _tr.tr_head_to_gentry_on_left_side_arms_under_head,
                                                                  'HFDL')
    # 7. Ногами к гентри Лежа на спине руки к гентри
    feet_to_gentry_on_back_arms_to_gentry = PatientPosition('feet_to_gentry_on_back_arms_to_gentry',
                                                            _tr.tr_feet_to_gentry_on_back_arms_to_gentry, 'FFS')

    # 8. Ногами к гентри Лежа на животе руки под голову (лоб упирается в предплечья)
    feet_to_gentry_on_stomach_arms_under_head = PatientPosition('feet_to_gentry_on_stomach_arms_under_head',
                                                                _tr.tr_feet_to_gentry_on_stomach_arms_under_head, 'FFP')

    # 9. Ногами к гентри Лежа на правом боку руки под голову
    feet_to_gentry_on_right_side_arms_under_head = PatientPosition('feet_to_gentry_on_right_side_arms_under_head',
                                                                   _tr.tr_feet_to_gentry_on_right_side_arms_under_head,
                                                                   'FFDR')
    # 10. Ногами к гентри Лежа на левом боку руки под голову
    feet_to_gentry_on_left_side_arms_under_head = PatientPosition('feet_to_gentry_on_left_side_arms_under_head',
                                                                  _tr.tr_feet_to_gentry_on_left_side_arms_under_head,
                                                                  'FFDL')
    # 11. на спине/ногами в гентри/руки над головой
    feet_to_gentry_on_back_arms_on_head = PatientPosition('feet_to_gentry_on_back_arms_on_head',
                                                                  _tr.tr_feet_to_gentry_on_left_side_arms_under_head,
                                                                  'FFSAH')

    _list = [head_to_gentry_on_back_arms_to_legs, head_to_gentry_on_back_arms_to_gentry,
             head_to_gentry_on_stomach_arms_under_head, head_to_gentry_on_stomach_arms_to_gentry,
             head_to_gentry_on_right_side_arms_under_head, head_to_gentry_on_left_side_arms_under_head,
             feet_to_gentry_on_back_arms_to_gentry, feet_to_gentry_on_stomach_arms_under_head,
             feet_to_gentry_on_right_side_arms_under_head, feet_to_gentry_on_left_side_arms_under_head,
             feet_to_gentry_on_back_arms_on_head
             ]

    @classmethod
    def get_list(cls) -> List[PatientPosition]:
        return cls._list

    @classmethod
    def load(cls, name_dict: dict) -> Optional[PatientPosition]:
        return super().load(name_dict)
