from typing import Optional, List, Any


class DataEnumMember:
    def __init__(self, name: str, tr_func=None):
        self.name: str = name
        self._tr_func = tr_func

    def tr(self):
        if self._tr_func:
            return self._tr_func()
        else:
            return self.name

    def to_dict(self):
        return {'name': self.name}

    def __repr__(self):
        return self.tr()

    def __eq__(self, o: object) -> bool:
        if isinstance(o, DataEnumMember):
            return o.name == self.name
        return super().__eq__(o)

    def __hash__(self) -> int:
        return self.name.__hash__()


class DataEnumList:
    @classmethod
    def load(cls, name_dict: dict) -> Optional[DataEnumMember]:
        if name_dict is None:
            return None
        for obj in cls.get_list():
            if obj.name == name_dict['name']:
                return obj
        return None

    @classmethod
    def load_by_name(cls, name: str) -> Optional[DataEnumMember]:
        for obj in cls.get_list():
            if obj.name.lower() == (name or '').lower():
                return obj
        return None

    @classmethod
    def get_list(cls) -> List[Any]:
        raise NotImplemented
