import json


class ScanTaskId:
    _date_format = '%Y.%m.%d %H:%M:%S'
    study_id_len = 32

    def __init__(self, study_id: str, series_number: int):
        super().__init__()

        self.study_id = study_id
        self.series_number = int(series_number)

    def __eq__(self, other: object) -> bool:
        if not (isinstance(other, ScanTaskId)):
            return False
        else:
            return self.study_id == other.study_id and self.series_number == other.series_number

    def __hash__(self) -> int:
        return f'{self.study_id} {self.series_number}'.__hash__()

    @classmethod
    def from_json(cls, json_str: str) -> 'ScanTaskId':
        data = json.loads(json_str)
        return cls.from_dict(data)

    @classmethod
    def from_dict(cls, data: dict) -> 'ScanTaskId':
        task = ScanTaskId(data['study_id'], int(data['series_number']))
        return task

    def to_json(self):
        return json.dumps(self.to_dict(), indent=4)

    def to_dict(self):
        return {
            'study_id': self.study_id,
            'series_number': self.series_number,
        }

    def __repr__(self):
        return f'{self.study_id} {self.series_number}'

