from typing import List, Any

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.view.translator import Translator


class AccessType(DataEnumMember):

    def __lt__(self, other: 'AccessType') -> bool:
        if self == AccessTypes.USER and other in [AccessTypes.EXPERT, AccessTypes.TECHNICAL_SERVICE]:
            return True
        if self == AccessTypes.EXPERT and other == AccessTypes.TECHNICAL_SERVICE:
            return True
        return False


class AccessTypes(DataEnumList):
    _tr = Translator.inst()
    USER = AccessType('USER', _tr.tr_user)
    EXPERT = AccessType('EXPERT', _tr.tr_expert)
    TECHNICAL_SERVICE = AccessType('TECHNICAL_SERVICE', _tr.tr_technical_service)

    _list = [USER, EXPERT, TECHNICAL_SERVICE]

    @classmethod
    def get_list(cls) -> List[Any]:
        return cls._list
