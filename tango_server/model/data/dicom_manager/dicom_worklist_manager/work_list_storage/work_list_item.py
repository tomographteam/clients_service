from pydicom.dataset import Dataset
from pydicom.sequence import Sequence
from src.model_.data.dicom_manager.dicom_utils import DicomTags
from src.model_.data.dicom_manager.dicom_utils.dicom_converter import DicomConverter
from pydicom.dataelem import RawDataElement, DataElement_from_raw


class WorkListItem:

    def __init__(self, data_set=None):
        self.item_id = None
        self.data_set = data_set
        self.mpps_sop_instance_uid = None

    @staticmethod
    def generate_item_id(data_set):
        if data_set and isinstance(data_set, Dataset):
            return data_set.RequestedProcedureID +\
                   '_' + \
                   data_set.ScheduledProcedureStepSequence[0].ScheduledProcedureStepID
        return None

    @property
    def data_set(self):
        return self._data_set

    @data_set.setter
    def data_set(self, value):
        if isinstance(value, Dataset):
            self._data_set = value
            self.item_id = self.generate_item_id(self._data_set)
        else:
            raise TypeError("type must be a pydicom.dataset.Dataset")

    def get_json(self):
        item = {}
        if self.data_set:
            item = self.process_data_set(self.data_set)
        item['item_id'] = self.item_id
        return item

    def process_data_set(self, data_set):
        item = {}
        for value in data_set.elements():
            if isinstance(value.value, Sequence) and value.keyword == DicomTags.SCHEDULED_PROCEDURE_STEP_SEQUENCE:
                convert_key = DicomConverter.convert_tag_to_json(value.keyword)
                item[convert_key] = {}
                for item_value in value.value:
                    item[convert_key] = self.process_data_set(item_value)
            else:
                if isinstance(value, RawDataElement):
                    value = DataElement_from_raw(value)

                convert_key = DicomConverter.convert_tag_to_json(value.keyword)
                convert_value = DicomConverter.convert_data_to_json(value.keyword, value.value)
                item[convert_key] = convert_value

        return item
