from .work_list_item import WorkListItem
from src.model_.data.dicom_manager.dicom_utils import DicomTags


class WorkListStorage:
    def __init__(self):
        self._work_list_items = {}

    def set_work_list_items(self, work_list_data):
        for data_set in work_list_data:
            for value in data_set.elements():
                if value.keyword == DicomTags.SCHEDULED_PROCEDURE_STEP_SEQUENCE:
                    for item_value in value.value:
                        data_set.ScheduledProcedureStepSequence = [item_value]
                        work_list_item = WorkListItem(data_set)
                        if work_list_item.item_id:
                            self._work_list_items[work_list_item.item_id] = work_list_item

    def get_work_list_items(self):
        return self._work_list_items

    def get_work_list_items_json(self):
        items = []
        for key, work_list_item in self._work_list_items.items():
            items.append(work_list_item.get_json())
        return items

    def get_work_list_item(self, item_id):
        return self._work_list_items.get(item_id, None)

    def get_work_list_item_json(self, item_id):
        item = self.get_work_list_item(item_id)
        if item:
            return item.get_json()
        return item
