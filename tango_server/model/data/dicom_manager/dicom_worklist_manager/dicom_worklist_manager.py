from src.model_.data.dicom_manager.dicom_worklist_manager.work_list_storage import WorkListStorage
from src.model_.data.dicom_manager.dicom_scu import DicomScuWorklist
from src.model_.data.dicom_manager.dicom_scu import DicomScuMPPS


class DicomWorkListManager:

    def __init__(self):
        self.work_list_storage = WorkListStorage()
        self.dicom_scu_mpps = DicomScuMPPS()
        self.dicom_scu_work_list = DicomScuWorklist()

    def get_work_list(self):
        work_list_data = self.dicom_scu_work_list.get_work_list()
        self.work_list_storage.set_work_list_items(work_list_data)
        return self.work_list_storage

    def in_progress(self, work_list_item_id, data):
        work_list_item = WorkListStorage.get_work_list_item(work_list_item_id)
        self.dicom_scu_mpps.in_progress(work_list_item, data)

    def complete(self, work_list_item_id, data):
        work_list_item = WorkListStorage.get_work_list_item(work_list_item_id)
        self.dicom_scu_mpps.complete(work_list_item, data)

    def discontinued(self, work_list_item_id, data):
        work_list_item = WorkListStorage.get_work_list_item(work_list_item_id)
        self.dicom_scu_mpps.discontinued(work_list_item, data)
