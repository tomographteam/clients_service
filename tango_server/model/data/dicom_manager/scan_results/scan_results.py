import base64
import io
import json
from time import time
from typing import Union, Optional, Dict

import PIL
from PIL.Image import Image

from tango_server.model.data.dicom_manager.scan_results.tomograph_state import TomographState
from tango_server.model.data.rotation_direction import RotationDirection, RotationDirections
from tango_server.model.data.scan_task import  ScanTaskId


class ScanResult:
    def __init__(self, scan_task_id: ScanTaskId, tomograph_state: Optional[TomographState]):
        self.scan_task  = None
        self.scan_task_id: ScanTaskId = scan_task_id
        self.tomograph_state: Optional[TomographState] = tomograph_state
        self.scan_time = 0
        self.passed = 0
        self.dose = 0
        self.table_height = 0  # The distance in mm of the top of the patient table to the center of rotation; below the center is positive.
        # Direction of rotation of the source when relevant, about nearest principal axis of equipment.
        self.rotation_direction: RotationDirection = RotationDirections.CW
        self.focal_spots = 1.2
        self.scan_interrupted = False

    def __repr__(self):
        interrupted = 'Interrupted' if self.scan_interrupted else ''
        return f'Study: {self.scan_task_id} {interrupted}'.strip()

    @classmethod
    def from_json(cls, json_str: str) -> 'ScanResult':
        data = json.loads(json_str)
        return cls.from_dict(data)

    @classmethod
    def from_dict(cls, data: Dict) -> 'ScanResult':
        scan_task_id = ScanTaskId.from_dict(data)
        tomograph_state_data = data.get('tomograph_state')
        if tomograph_state_data:
            tomograph_state = TomographState.from_dict(tomograph_state_data)
        else:
            tomograph_state = TomographState()
        obj = cls(scan_task_id, tomograph_state)
        obj.dose = int(data['dose'])
        obj.passed = int(data['passed'])

        # obj.focal_spots = data['focal_spots']
        # obj.scan_interrupted = bool(data['scan_interrupted'])
        # obj.rotation_direction = RotationDirections.load_by_name(data['rotation_direction'])
        return obj

    def to_json(self):
        return json.dumps(self.to_dict(), indent=4)

    def to_dict(self) -> Dict:
        scan_task_id = self.scan_task_id.to_dict()
        data = {
            'tomograph_state': self.tomograph_state.to_dict(),
            'dose': self.dose,
            'passed': self.passed,
            'scan_time': self.scan_time,
            'rotation_direction': self.rotation_direction.name,
            'focal_spots': self.focal_spots,
            'scan_interrupted': self.scan_interrupted,
            'timestamp': int(time()),
        }
        data.update(scan_task_id)
        return data

    @staticmethod
    def _encode_img_file(img: Union[Image, None]) -> str:
        if img is None:
            return ''
        output = io.BytesIO()
        img.save(output, format='JPEG')
        return base64.b64encode(output.getvalue()).decode('utf-8')

    @classmethod
    def _encode_binary_file(cls, file_name) -> str:
        frame_data = open(file_name, 'rb').read()
        return base64.b64encode(frame_data).decode('utf-8')

    @staticmethod
    def _decode_img_data(data: str) -> Image:
        if data == '' or data is None:
            return PIL.Image.new('RGB', (256, 256))
        return PIL.Image.open(io.BytesIO(base64.b64decode(data)))


def scan_result_fabric(event_data) -> ScanResult:
    from tango_server.model.data.dicom_manager.scan_results.scout import ScanResultScout
    from tango_server.model.data.dicom_manager.scan_results.general import ScanResultGeneral
    try:
        return ScanResultScout.from_dict(event_data)
    except:
        return ScanResultGeneral.from_dict(event_data)


def frame_result_fabric(event_data):
    from tango_server.model.data.dicom_manager.scan_results.frame_result import FrameResult
    return FrameResult.from_dict(event_data)
