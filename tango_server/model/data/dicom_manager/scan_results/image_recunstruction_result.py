import os
from typing import Tuple

import PIL
from PIL import ImageDraw, ImageFont
from PIL.Image import Image
from PIL.Image import Image

#from dicompylercore.dicomparser import DicomParser
#from tango_server.model.data2.patient import Patient
#from tango_server.model.data.scan_task import ScanTask
from tango_server.model.data.scout_image import ScoutImage
from tango_server.utils.settings import get_path_config


class ImageReconstructionResult:
    _font_path = os.path.join(get_path_config()['fonts'], 'liberation-mono.ttf')

    def __init__(self, scan_task, img_num: int, img: ScoutImage):
        self.scan_task = scan_task
        self.img_num = img_num
        self.image = img
        self._text_color = '#ffa02f'
        try:
            self._text_font = ImageFont.truetype(self._font_path, 20)
        except ImportError:
            self._text_font = None

    def __repr__(self) -> str:
        return f'{self.scan_task}: {self.img_num}'

    def get_img_with_params(self) -> Image:
        image = self.image
        rgb_img = PIL.Image.new("RGBA", image.size)
        rgb_img.paste(image)
        draw = ImageDraw.Draw(rgb_img)

        scan_task = self.scan_task

        self._text(draw, (5, 10), scan_task.patient.get_full_name())
        self._text(draw, (5, 22), scan_task.patient.get_id())
        dob = scan_task.patient.get_date_of_birth().strftime('%d.%m.%Y')
        self._text(draw, (5, 34), dob)
        self._text(draw, (5, 46), str(scan_task.patient.get_body_area()))

        h = image.size[0] - 15
        self._text(draw, (5, h - 12), f'mA {scan_task.protocol.get_ma()}')
        self._text(draw, (5, h), f'kV {scan_task.protocol.get_kv()}')

        return rgb_img

    def _text(self, draw: ImageDraw, pos: Tuple[int, int], text: str):
        draw.text(pos, text=text, fill=self._text_color, font=self._text_font)


def main():
    dicom_parser = DicomParser('resources/DICOM-examples/heart_surface/DATA/001155')
    dicom_image = dicom_parser.GetImage()
    scan_task = ScanTask.from_json(open('scan_task.json', 'r').read())
    rr = ImageReconstructionResult(scan_task, 0, dicom_image)
    rr.get_img_with_params().show()


if __name__ == '__main__':
    main()
