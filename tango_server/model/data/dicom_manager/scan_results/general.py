from typing import List

from PIL.Image import Image

from tango_server.model.data.dicom_manager.scan_results.image_recunstruction_result import ImageReconstructionResult
from tango_server.model.data.dicom_manager.scan_results.scan_results import ScanResult
from tango_server.model.data.dicom_manager.scan_results.tomograph_state import TomographState
from tango_server.model.data.scan_task import ScanTaskId


class ScanResultGeneral(ScanResult):

    def __init__(self, scan_task_id: ScanTaskId, tomograph_state: TomographState):
        super().__init__(scan_task_id, tomograph_state)
        self.result_images: List[Image] = []

    def add_reconstruction_result(self, result: ImageReconstructionResult):
        self.result_images.append(result.image)
