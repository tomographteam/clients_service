from typing import Optional

from tango_server.model.data.dicom_manager.scan_results.image_recunstruction_result import ImageReconstructionResult
from tango_server.model.data.dicom_manager.scan_results.scan_results import ScanResult
from tango_server.model.data.dicom_manager.scan_results.tomograph_state import TomographState
from tango_server.model.data.scan_task import ScanTaskId
from tango_server.model.data.scout_image import ScoutImage


class ScanResultScout(ScanResult):
    def __init__(self, scan_task: ScanTaskId, tomograph_state: Optional[TomographState]):
        super().__init__(scan_task, tomograph_state)
        self.front_img: Optional[ScoutImage] = None
        self.side_img: Optional[ScoutImage] = None
        self.front_recon: Optional[ImageReconstructionResult] = None
        self.side_recon: Optional[ImageReconstructionResult] = None

    def add_reconstruction_result(self, result: ImageReconstructionResult):
        if result.img_num == 0:
            self.front_img = result.image
            self.front_recon = result
        elif result.img_num == 1:
            self.side_img = result.image
            self.side_recon = result

    # @classmethod
    # def from_dict(cls, data: dict) -> 'ScanResultScout':
    #     scan_task = super().from_dict(data)
    #     assert isinstance(scan_task, ScanResultScout)
    #     scan_task.front_img = cls._decode_img_data(data['front_img'])
    #     scan_task.side_img = cls._decode_img_data(data['side_img'])
    #     return scan_task
    #
    # def to_dict(self) -> dict:
    #     data = super().to_dict()
    #     data['front_img'] = self._encode_img_file(self.front_img)
    #     data['side_img'] = self._encode_img_file(self.side_img)
    #     return data
