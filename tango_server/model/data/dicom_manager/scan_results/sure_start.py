from typing import Optional

from src.model_.data.dicom_manager.scan_results.scan_results import ScanResult
from src.model_.data.dicom_manager.scan_results.tomograph_state import TomographState
from src.model_.data.scan_task import ScanTask


class RoiState:
    def __init__(self):
        self.roi = 0
        self.wait = 0
        self.check_count = 0


class TriggerStartResult(ScanResult):
    def __init__(self, scan_task: Optional[ScanTask], roi_state: RoiState, tomograph_state: Optional[TomographState]):
        super().__init__(scan_task, tomograph_state)
        self.roi_state = roi_state
        self.manual_start = False
