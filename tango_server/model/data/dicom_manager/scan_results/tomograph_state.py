import json


class TomographState:
    def __init__(self):
        self.dsebct = True
        self.pes = True
        self.hvps = True
        self.bce = True
        self.wwires = True
        self.collimator = True
        self.patient_table = True
        self.chiller = True
        self.turbo_pump = True
        self.ion_pump = True

    @classmethod
    def from_json(cls, json_str: str) -> 'TomographState':
        data = json.loads(json_str)
        return cls.from_dict(data)

    @classmethod
    def from_dict(cls, data: dict) -> 'TomographState':
        state = TomographState()
        state.dsebct = data['dsebct']
        state.pes = data['pes']
        state.hvps = data['hvps']
        state.bce = data['bce']
        state.wwires = data['wwires']
        state.collimator = data['collimator']
        state.patient_table = data['patient_table']
        state.chiller = data['chiller']
        state.turbo_pump = data['turbo_pump']
        state.ion_pump = data['ion_pump']

        return state

    def to_json(self):
        return json.dumps(self.to_dict(), indent=4)

    def to_dict(self):
        return {
            'dsebct': self.dsebct,
            'pes': self.pes,
            'hvps': self.hvps,
            'bce': self.bce,
            'collimator': self.collimator,
            'patient_table': self.patient_table,
            'wwires': self.wwires,
            'chiller': self.chiller,
            'turbo_pump': self.turbo_pump,
            'ion_pump': self.ion_pump,
        }
