from typing import Union

from PIL.Image import Image

from src.model_.data.dicom_manager.scan_results.scan_results import ScanResult
from src.model_.data.dicom_manager.scan_results.tomograph_state import TomographState
from src.model_.data.scan_task import ScanTask


class FrameResult(ScanResult):
    def __init__(self, scan_task: ScanTask, tomograph_state: TomographState):
        super().__init__(scan_task, tomograph_state)
        self.frame: Union[Image, None] = None

    @classmethod
    def from_dict(cls, data: dict) -> 'FrameResult':
        scan_task = super().from_dict(data)
        assert isinstance(scan_task, FrameResult)
        scan_task.frame = cls._decode_img_data(data['frame'])
        return scan_task

    def to_dict(self) -> dict:
        data = super().to_dict()
        data['frame'] = self._encode_img_file(self.frame)
        return data
