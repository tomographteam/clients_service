from pydicom.dataset import Dataset
from .dicom_scu import DicomScu
import datetime
from pydicom.uid import generate_uid
from pynetdicom.sop_class import ModalityPerformedProcedureStepSOPClass
from pynetdicom import build_context
from ..dicom_utils.dicom_converter import DicomConverter


class PerformedProcedureStepStatus:
    IN_PROGRESS = 'IN PROGRESS'
    COMPLETED = 'COMPLETED'
    DISCONTINUED = 'DISCONTINUED'


class DicomScuMPPS(DicomScu):

    def __init__(self):
        contexts = [build_context(ModalityPerformedProcedureStepSOPClass)]
        super().__init__(contexts)

    def in_progress(self, work_item_data_set, new_data_set=None, number_sps=0):
        today = self.get_current_datetime()
        step_seq_work_item = work_item_data_set.ScheduledStepAttributesSequence
        ds = Dataset()

        ds.ScheduledStepAttributesSequence = [Dataset()]
        step_seq = ds.ScheduledStepAttributesSequence
        step_seq[number_sps].StudyInstanceUID = step_seq_work_item[number_sps].StudyInstanceUID
        step_seq[number_sps].ReferencedStudySequence = []
        step_seq[number_sps].AccessionNumber = step_seq_work_item.AccessionNumber
        step_seq[number_sps].RequestedProcedureID = step_seq_work_item.RequestedProcedureID
        step_seq[number_sps].RequestedProcedureDescription = step_seq_work_item.RequestedProcedureDescription
        step_seq[number_sps].ScheduledProcedureStepID = step_seq_work_item[number_sps].ScheduledProcedureStepID
        step_seq[number_sps].ScheduledProcedureStepDescription = step_seq_work_item[number_sps].ScheduledProcedureStepDescription
        step_seq[number_sps].ScheduledProcedureProtocolCodeSequence = []

        ds.PatientName = work_item_data_set.PatientName
        ds.PatientID = work_item_data_set.PatientID
        ds.PatientBirthDate = work_item_data_set.PatientBirthDate
        ds.PatientSex = work_item_data_set.PatientSex
        ds.ReferencedPatientSequence = []

        ds.PerformedProcedureStepStartDate = today.strftime('%Y%m%d')
        ds.PerformedProcedureStepStartTime = today.strftime('%H%M')
        ds.PerformedProcedureStepStatus = PerformedProcedureStepStatus.IN_PROGRESS
        ds.PerformedProcedureStepID = today.strftime('%Y%m%d%H%M%S%T')
        ds.PerformedStationAETitle = 'Modality_title'
        ds.PerformedStationName = 'Modality'
        ds.PerformedLocation = ''
        ds.PerformedProcedureStepDescription = ''
        ds.PerformedProcedureTypeDescription = ''
        ds.PerformedProcedureCodeSequence = []
        ds.PerformedProcedureStepEndDate = ''
        ds.PerformedProcedureStepEndTime = ''

        ds.Modality = work_item_data_set.Modality
        ds.StudyID = ''
        ds.PerformedProtocolCodeSequence = []
        ds.PerformedSeriesSequence = []

        work_item_data_set.mpps_sop_instance_uid = self.n_create(ds, ModalityPerformedProcedureStepSOPClass)
        return work_item_data_set.mpps_sop_instance_uid

    def set(self, work_item_data_set, new_data_set=None, number_sps=0):

        if new_data_set and work_item_data_set.mpps_sop_instance_uid:
            data_set = Dataset()
            today = self.get_current_datetime()
            data_set.PerformedProcedureStepEndDate = today.strftime('%Y%m%d')
            data_set.PerformedProcedureStepEndTime = today.strftime('%H%M')
            data_set.PerformedProcedureStepStatus = new_data_set.PerformedProcedureStepStatus
            data_set.PerformedProcedureStepDescription = ''
            data_set.PerformedProcedureTypeDescription = ''
            data_set.PerformedProcedureCodeSequence = []
            data_set.PerformedProtocolCodeSequence = []
            data_set.PerformedSeriesSequence = [Dataset()]
            series_seq = data_set.PerformedSeriesSequence
            series_seq[number_sps].PerformingPhysicianName = work_item_data_set[number_sps].ScheduledPerformingPhysicianName
            series_seq[number_sps].ProtocolName = work_item_data_set.ProtocolName
            series_seq[number_sps].OperatorName = work_item_data_set.OperatorsName
            series_seq[number_sps].SeriesInstanceUID = work_item_data_set.SeriesInstanceUID
            series_seq[number_sps].SeriesDescription = work_item_data_set.SeriesDescription
            series_seq[number_sps].RetrieveAETitle = ''
            series_seq[number_sps].ReferencedImageSequence = work_item_data_set.ReferencedImageSequence

            return self.n_set(data_set,
                              ModalityPerformedProcedureStepSOPClass,
                              work_item_data_set.mpps_sop_instance_uid)

        return None

    def complete(self, work_item_data_set=None, new_data_set=None, number_sps=0):
        if not new_data_set:
            new_data_set = Dataset()
        new_data_set.PerformedProcedureStepStatus = PerformedProcedureStepStatus.COMPLETED
        return self.set(work_item_data_set, new_data_set, number_sps)

    def discontinued(self, work_item_data_set=None, new_data_set=None, number_sps=0):
        if not new_data_set:
            new_data_set = Dataset()
        new_data_set.PerformedProcedureStepStatus = PerformedProcedureStepStatus.DISCONTINUED
        return self.set(work_item_data_set, new_data_set, number_sps)

    @staticmethod
    def get_current_datetime():
        return datetime.datetime.today()




