from pydicom.dataset import Dataset
from pydicom.filereader import dcmread
from pynetdicom import build_context
from pynetdicom.sop_class import CTImageStorage

from .dicom_scu import DicomScu


class DicomScuStore(DicomScu):

    def __init__(self, pacs_address: str, pacs_port: int):
        contexts = [build_context(CTImageStorage)]
        super().__init__(pacs_address, pacs_port, contexts)

    def store_dicom_file(self, dicom_filename: str):
        dataset = dcmread(dicom_filename)
        self.store_dataset(dataset)

    def store_dataset(self, dataset: Dataset):
        self.c_store(dataset)
