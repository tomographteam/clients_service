import logging

from pydicom.dataset import Dataset
from pynetdicom import AE


class DicomScu:
    _logger = logging.getLogger('pacs')

    def __init__(self, pacs_address: str, pacs_port: int, contexts):
        self.ae = AE()
        self.ae.requested_contexts = contexts
        self._pacs_address = pacs_address
        self._pacs_port = int(pacs_port)

    def c_find(self, ds, sop_model):
        assoc = self.ae.associate(self._pacs_address, self._pacs_port)
        result = []
        if assoc.is_established:
            responses = assoc.send_c_find(ds, sop_model)
            result = self.handle_responses(responses)
            assoc.release()
        else:
            self._logger.error('Association rejected, aborted or never connected')
            return None
        return result

    def c_store(self, ds: Dataset):
        # ds = dcmread(dicom_file)
        self._logger.info('Store dataset')
        assoc = self.ae.associate(self._pacs_address, self._pacs_port)

        if assoc.is_established:
            status = assoc.send_c_store(ds)

            if status:
                self._logger.debug('C-STORE request status: 0x{0:04x}'.format(status.Status))
            else:
                self._logger.error('Connection timed out, was aborted or received invalid response')
            assoc.release()
        else:
            self._logger.error('Association rejected, aborted or never connected')

    def n_create(self, data_set, sop_model, mpps_instance_uid=None):
        assoc = self.ae.associate(self.pacs_config['ip_address'], int(self.pacs_config['port']))
        result = []
        if assoc.is_established:
            status, responses = assoc.send_n_create(
                data_set,
                sop_model,
                mpps_instance_uid
            )
            result = self.handle_responses(responses)
            assoc.release()
        else:
            print('Association rejected, aborted or never connected')
        return result

    def n_set(self, data_set, sop_model, mpps_instance_uid=None):
        assoc = self.ae.associate(self.pacs_config['ip_address'], int(self.pacs_config['port']))
        result = []
        if assoc.is_established:
            status, responses = assoc.send_n_create(
                data_set,
                sop_model,
                mpps_instance_uid
            )
            result = self.handle_responses(responses)
            assoc.release()
        else:
            print('Association rejected, aborted or never connected')
        return result

    @staticmethod
    def handle_responses(responses):
        items = []
        for (status, identifier) in responses:
            if status:
                if status.Status in (0xFF00, 0xFF01):
                    items.append(identifier)
            else:
                print('Connection timed out, was aborted or received invalid response')
        return items

    @staticmethod
    def fill_data_set(data_set, data_dict):
        for key, value in data_dict.items():
            try:
                data_set[key].value = value
            except KeyError:
                print('Error: {} is not a valid DICOM keyword'.format(key))
        return data_set
