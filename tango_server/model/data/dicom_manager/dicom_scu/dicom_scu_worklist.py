import logging
from typing import List

from pydicom.dataset import Dataset
from pynetdicom import build_context
from pynetdicom.sop_class import ModalityWorklistInformationFind

from .dicom_scu import DicomScu
from ...patient import Patient

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass

DUMMY_WORKLIST = [
    {
        "id": 20504561,
        "family": "Stupin",
        "name": "Alexsandr",
        "middle_name": "Sergeevich",
        "date_of_birth": "12.06.1979",
        "sex": "male",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'chest'}
    },
    {
        "id": 20504563,
        "family": "Erofeev",
        "name": "Andrey",
        "middle_name": "Petrovich",
        "date_of_birth": "12.06.1979",
        "sex": "male",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'neck'}
    },
    {
        "id": 20504562,
        "family": "Ivanova",
        "name": "Anna",
        "middle_name": "Andreevna",
        "date_of_birth": "11.07.1987",
        "sex": "female",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'head'}
    }, {
        "id": 20504564,
        "family": "Plotnikov",
        "name": "Evgeniy",
        "middle_name": "Alexandrovich",
        "date_of_birth": "12.06.1983",
        "sex": "male",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'abdominal'}
    }, {
        "id": 20504577,
        "family": "Salrukov",
        "name": "Konstantin",
        "middle_name": "Olegovic",
        "date_of_birth": "14.02.1990",
        "sex": "male",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'limbs'}
    }, {
        "id": 20504567,
        "family": "Lushnikov",
        "name": "Nikolay",
        "middle_name": "Sergeevich",
        "date_of_birth": "20.06.1976",
        "sex": "male",
        "height": 0,
        "weight": 0,
        "body_area": {"name": 'pelvis'}
    },
]


class DicomScuWorklist(DicomScu):
    _logger = logging.getLogger('pacs')

    def __init__(self, tomograph_manager: 'TomographManager'):
        self._tomograph_manager = tomograph_manager
        contexts = [build_context(ModalityWorklistInformationFind)]
        settings = tomograph_manager.settings_manager.get_settings()

        super().__init__(settings.pacs_address, settings.pacs_port, contexts)

    def get_work_list(self, name_filter: str) -> List[Patient]:
        self._logger.info('Request worklist')
        ds = self.create_work_list_dataset()
        ds.PatientName = f'*{name_filter}*'
        try:
            worklist = self.c_find(ds, ModalityWorklistInformationFind)
        except:
            worklist = None
        if worklist is None:
            self._logger.warning('Worklist is None, using DUMMY_WORKLIST')
            worklist = DUMMY_WORKLIST
        else:
            self._logger.info(f'Received {len(worklist)} records in worklist')
        patients = []
        for patient_data in worklist:
            patients.append(Patient.from_dicom_dataset(patient_data))
        return patients

    @staticmethod
    def create_work_list_dataset():
        ds = Dataset()
        ds.QueryRetrieveLevel = 'PATIENT'
        ds.PatientID = ''
        ds.PatientName = ''
        ds.PatientSex = ''
        ds.PatientBirthDate = ''
        ds.PatientWeight = ''
        ds.PatientSize = ''
        ds.RequestedProcedureID = ''
        ds.RequestedProcedureDescription = ''
        ds.StudyInstanceUID = ''
        ds.AccessionNumber = ''
        ds.ScheduledProcedureStepSequence = [Dataset()]
        item = ds.ScheduledProcedureStepSequence[0]
        item.Modality = ''
        item.ScheduledProcedureStepLocation = ''
        item.ScheduledStationName = ''
        item.ScheduledProcedureStepStartDate = ''
        item.ScheduledProcedureStepStartTime = ''
        item.ScheduledProcedureStepEndDate = ''
        item.ScheduledProcedureStepEndTime = ''
        item.ScheduledPerformingPhysicianName = ''
        item.ScheduledStationAETitle = ''
        item.ScheduledProcedureStepDescription = ''
        item.ScheduledProcedureStepStatus = ''
        item.ScheduledProcedureStepID = ''
        return ds
