from .dicom_tags import DicomTags
import re


class DicomConverter:

    PATIENT_FIRST_NAME = 'first_name'
    PATIENT_MIDDLE_NAME = 'middle_name'
    PATIENT_SECOND_NAME = 'second_name'

    CONVERT_VALUE_TO_JSON = {
        DicomTags.PATIENT_BIRTH_DAY: "DicomConverter.convert_patient_birth_day_to_json(value)",
        DicomTags.PATIENT_NAME: "DicomConverter.convert_patient_name_to_json(value)"
    }

    @staticmethod
    def convert_data_to_json(key, value):
        return eval(DicomConverter.CONVERT_VALUE_TO_JSON.get(key, 'value'))

    @staticmethod
    def convert_data_to_dicom(key, value):
        pass

    @staticmethod
    def convert_tag_to_json(key):
        return '_'.join(re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', key)).lower()

    @staticmethod
    def convert_tag_to_dicom(key):
        pass

    @staticmethod
    def convert_patient_name_to_json(value):
        return {DicomConverter.PATIENT_FIRST_NAME: value.family_name,
                DicomConverter.PATIENT_MIDDLE_NAME: value.middle_name,
                DicomConverter.PATIENT_SECOND_NAME: value.name}

    @staticmethod
    def convert_patient_name_to_dicom(value):
        pass

    @staticmethod
    def convert_patient_birth_day_to_json(value):
        return value[6:] + '.' + value[4:6] + '.' + value[:4]

    @staticmethod
    def convert_patient_birth_day_to_dicom(value):
        pass
