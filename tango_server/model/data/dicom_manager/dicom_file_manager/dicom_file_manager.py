import datetime
import logging
import os
import time
from typing import Iterable

import numpy as np
import pydicom
from PIL.Image import Image
from pydicom.dataset import Dataset
from pydicom.uid import generate_uid
from pynetdicom.sop_class import CTImageStorage

from src.model_.data.dicom_manager.dicom_file_manager.common_params import CommonDicomParams
from src.model_.data.dicom_manager.dicom_scu.dicom_scu_store import DicomScuStore
from src.model_.data.dicom_manager.scan_results.image_recunstruction_result import ImageReconstructionResult
from src.model_.data.dicom_manager.scan_results.scan_results import ScanResult
from src.utils.settings import get_path_config


class DicomFileManager:
    TEMPLATE_FILE = get_path_config()['dicom_template']
    _result_path = get_path_config()['scan_results']
    _logger = logging.getLogger('pacs')

    def __init__(self, pacs_address: str, pacs_port: int):
        self.dicom_store = DicomScuStore(pacs_address, pacs_port)

    def store_file(self, file_path):
        self.dicom_store.store_dicom_file(file_path)

    def _read_file(self, filename=None):
        if not filename:
            filename = self.TEMPLATE_FILE
        data_set = pydicom.dcmread(filename)
        return data_set

    def write_file(self, dataset: Dataset, filename=None) -> str:
        if not filename:
            filename = self._generate_filename()
        if dataset:
            dt = datetime.datetime.now()
            dataset.ContentDate = dt.strftime('%Y%m%d')
            time_str = dt.strftime('%H%M%S.%f')
            dataset.ContentTime = time_str

            dataset.save_as(filename)
        return filename

    def _generate_filename(self, filename=None):
        if filename is None:
            filename = pydicom.uid.generate_uid() + '.dcm'
        return os.path.join(self._result_path, filename)

    @staticmethod
    def _images_list_to_pixel_array(images: Iterable[Image]) -> np.array:
        return np.array(list(map(lambda img: np.asarray(img), images)))

    def create_dataset(self,
                       scan_result: ScanResult,
                       common_params: CommonDicomParams,
                       file_path) -> Dataset:

        scan_task = scan_result.scan_task
        data_set = self._read_file(file_path)
        data_set.file_meta.MediaStorageSOPClassUID = CTImageStorage
        data_set.file_meta.MediaStorageSOPInstanceUID = generate_uid()
        # data_set.file_meta.ImplementationClassUID = generate_uid()

        data_set.SOPInstanceUID = data_set.file_meta.MediaStorageSOPInstanceUID
        data_set.SOPClassUID = data_set.file_meta.MediaStorageSOPClassUID
        data_set.StudyID = scan_result.scan_task.study_id
        data_set.SeriesNumber = scan_result.scan_task.series_number
        data_set.FrameOfReferenceUID = scan_result.scan_task.frame_of_reference_UID
        # del data_set.InstanceNumber
        # del data_set.AcquisitionNumber

        d = str(datetime.date.today()).replace('-', '')
        data_set.ContentDate = d
        data_set.StudyDate = d
        data_set.SeriesDate = d
        data_set.ContentTime = str(time.time())
        data_set.AcquisitionDate = str(datetime.date.today()).replace('-', '')
        data_set.SeriesTime = str(time.time())
        data_set.AcquisitionTime = str(time.time())
        data_set.StudyTime = str(time.time())
        del data_set.ContentTime

        data_set.InstitutionName = common_params.InstitutionName
        data_set.InstitutionAddress = common_params.InstitutionAddress
        data_set.DateOfLastCalibration = common_params.DateOfLastCalibration
        data_set.TimeOfLastCalibration = common_params.TimeOfLastCalibration
        data_set.OperatorsName = common_params.OperatorsName

        data_set.StudyDescription = '___Study Description___'
        data_set.SeriesDescription = '___Series Description___'
        data_set.PerformingPhysicianName = common_params.PerformingPhysicianName

        data_set.Manufacturer = common_params.Manufacturer
        data_set.ManufacturerModelName = common_params.ManufacturerModelName
        data_set.DeviceSerialNumber = common_params.DeviceSerialNumber
        data_set.SoftwareVersions = common_params.SoftwareVersions
        data_set.StationName = common_params.StationName

        data_set.ImageComments = '___ImageComments___'
        # del data_set.ReferencedImageSequence
        # del data_set.SourceImageSequence
        # data_set.ReferencedImageSequence = ['']

        if scan_result.scan_task.patient:
            data_set.PatientID = scan_result.scan_task.patient.get_id()
            data_set.PatientName = scan_result.scan_task.patient.get_save_name()
            data_set.PatientBirthDate = scan_result.scan_task.patient.get_date_of_birth()
            data_set.PatientAge = scan_result.scan_task.patient.get_age_string()
            data_set.PatientSex = scan_result.scan_task.patient.get_sex_str()
            data_set.PatientWeight = scan_result.scan_task.patient.get_weight()
            data_set.PatientSize = scan_result.scan_task.patient.get_weight() / 100
            data_set.PatientPosition = scan_result.scan_task.protocol.get_patient_position().dicom_name

        data_set.BodyPartExamined = scan_task.protocol.get_sqn().get_body_area().name
        data_set.ProtocolName = scan_task.protocol.get_sqn().get_name()
        data_set.ContrastBolusStartTime = None
        data_set.ContrastBolusStopTime = None
        data_set.GeneratorPower = scan_task.protocol.get_kv()
        data_set.SingleCollimationWidth = scan_task.protocol.get_collimator_width()
        data_set.TableSpeed = scan_task.protocol.get_table_speed()
        data_set.SpiralPitchFactor = scan_task.protocol.get_pitch()
        data_set.ContrastBolusVolume = scan_task.protocol.contrast.volume
        data_set.ContrastBolusTotalDose = scan_task.protocol.contrast.volume

        data_set.PositionReferenceIndicator = ''

        del data_set.ContrastBolusStartTime
        del data_set.ContrastBolusStopTime
        # del data_set.ContrastFlowRate
        # del data_set.ContrastFlowDuration
        # del data_set.ContrastBolusIngredientConcentration
        data_set.CTDIvol = scan_result.dose
        data_set.ScanOptions = '___ScanOptions___'
        data_set.TableHeight = scan_result.table_height
        # del data_set.GantryDetectorTilt

        reconstruction = scan_task.protocol.get_reconstruction()
        data_set.SliceThickness = reconstruction.slice_size
        data_set.ConvolutionKernel = reconstruction.kernel.name

        data_set.WindowCenterWidthExplanation = ''

        data_set.StudyInstanceUID = generate_uid()
        data_set.SeriesInstanceUID = generate_uid()

        # pixel_array = self._images_list_to_pixel_array(map(lambda rcn: rcn.image, reconstructions))
        # data_set.Columns = pixel_array.shape[-2]
        # data_set.Rows = pixel_array.shape[-1]
        # data_set.NumberOfFrames = pixel_array.shape[0]
        # print(f'data_set.NumberOfFrames = {data_set.NumberOfFrames}')
        # if pixel_array.dtype != np.uint16:
        #    pixel_array = pixel_array.astype(np.uint16)
        # data_set.PixelData = pixel_array.tobytes()
        # data_set.PixelData = pixel_array.tobytes()
        # data_set.SmallestImagePixelValue = pixel_array.min()
        # data_set.LargestImagePixelValue = pixel_array.max()
        # data_set.remove_private_tags()
        # if dicom_data:
        #     self._fill_data_set(data_set, dicom_data)

        # create_filename = self._write_file(filename, data_set)
        # print(data_set)
        return data_set

    def _edit_file(self, filename, dicom_data):
        data_set = self._read_file(filename)
        self._fill_data_set(data_set, dicom_data)
        edit_filename = self.write_file(filename, data_set)
        return edit_filename

    @staticmethod
    def _fill_data_set(data_set, data_dict):

        for key, value in data_dict.items():
            print(key)
            print(value)
            try:
                data_set[key].value = value
            except KeyError:
                print('Error: {} is not a valid DICOM keyword'.format(key))

    @staticmethod
    def _fill_uid_attribute(attr_key, data_dict):
        attr_value = data_dict.get(attr_key, None)
        if attr_value:
            return attr_value

        return generate_uid()

# if __name__ == "__main__":
#     dwm = DicomScuWorklist()
#     r = dwm.get_work_list()
#     scan_task = ScanTask()
#     scan_task.patient = Patient.from_dicom_dataset(r[0])
#     sqn = ProtocolsSequence()
#     scan_task.protocol = Protocol(sqn)
#     scan_task.protocols_sequence = sqn
#     sqn.save_protocol_step(scan_task.protocol)
#     scan_result = ScanResult(scan_task, None)
#     file_manager = DicomFileManager()
#     img = PIL.Image.open(open('/home/dndred/dev/Tomograph/resources/1/19.jpg', 'rb'))
#     # img = PIL.Image.new('RGB', (256, 256))
#     dataset = file_manager.create_dataset(scan_result, CommonDicomParams(), [img, ])
#     file_path = file_manager.write_file(dataset)
#     ds = dcmread(file_path)
#     print(ds)

# file_manager.store_file(file_path)
