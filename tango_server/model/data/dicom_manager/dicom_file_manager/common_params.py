from datetime import datetime

from src.model_.data.settings_manager import Settings


class CommonDicomParams:
    def __init__(self, settings: Settings):
        self.InstitutionName = settings.institution_name
        self.InstitutionAddress = settings.institution_address
        self.OperatorsName = settings.operator.get_name() if settings.operator else ''

        self.Manufacturer = settings.manufacturer
        self.ManufacturerModelName = settings.manufacturer_model_name
        self.DeviceSerialNumber = settings.device_serial_number
        self.SoftwareVersions = settings.software_versions

        self.PerformingPhysicianName = ''

        self.StationName = settings.station_name
        last_calibration = settings.last_calibration
        assert isinstance(last_calibration, datetime)
        self.DateOfLastCalibration = last_calibration.strftime('%Y%m%d')
        self.TimeOfLastCalibration = last_calibration.strftime('%H%M%S.000000')
