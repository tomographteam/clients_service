from typing import Optional, List

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.view.translator import Translator


class RotationDirection(DataEnumMember):
    pass


class RotationDirections(DataEnumList):
    _tr = Translator.inst()

    CW = RotationDirection('CW', _tr.tr_rotation_direction_clockwise)
    CC = RotationDirection('CC', _tr.tr_rotation_direction_counter_clockwise)
    _list = [CW, CC]

    @classmethod
    def get_list(cls) -> List[RotationDirection]:
        return cls._list

    @classmethod
    def load(cls, name_dict: dict) -> Optional[RotationDirection]:
        return super().load(name_dict)
