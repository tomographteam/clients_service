import logging
import threading
from enum import Enum
from queue import Queue
from typing import Optional, Callable, List, Type, Tuple

from src.model_.data.data_manager import DataManager
from src.model_.data.settings_manager import SettingsManager
from src.model_.equipment.device_manager import DeviceManager
from src.model_.equipment.peripheral.peripheral_manager import PeripheralManager
from src.presenter.presenter import Presenter


class TomographManager:
    log = logging.getLogger('event')

    def __init__(self):
        self._event_queue = Queue()
        self.settings_manager = SettingsManager(self)
        self.device_manager = DeviceManager(self)
        self.data_manager = DataManager(self)
        self.peripheral_manager = PeripheralManager(self)

        self.presenter: Optional[Presenter] = None
        self._async_events: List[Tuple[Type[Enum], Callable]] = []
        from src.model_.control.events_map import EventsMap
        self._eventsMap = EventsMap.events

    def register_async_event_map(self, event_type: Type[Enum], event_handler: Callable):
        self._async_events.append((event_type, event_handler))

    def start(self):
        self._read_event_queue_thread = threading.Thread(target=self._read_event_queue)
        self._read_event_queue_thread.daemon = True
        self._read_event_queue_thread.name = 'Model event loop'
        self._read_event_queue_thread.start()

    def handle_event(self, event):
        return self._event_queue.put(event)

    def set_presenter(self, presenter):
        self.presenter = presenter
        self.settings_manager.set_presenter(presenter)
        self.device_manager.set_presenter(presenter)
        self.data_manager.set_presenter(presenter)
        self.peripheral_manager.set_presenter(presenter)

    def _read_event_queue(self):
        self.log.debug('TomographManager _read_event_queue start')
        while True:
            try:
                event = self._event_queue.get()
                # print(event.event_type)
                self.log.debug(event.event_type)
                if event.event_type in self._eventsMap:
                    script = self._eventsMap[event.event_type]
                    run = getattr(script, 'run')
                    run(self, event)
                else:
                    handled = False
                    for event_type, event_handler in self._async_events:
                        if isinstance(event.event_type, event_type):
                            event_handler(event)
                            handled = True
                    if not handled:
                        self.log.debug(f'event {event.event_type} not handled')
                self.log.debug('event process end')
            except Exception as e:
                self.log.error(f'TomographManager _read_event_queue start error={e}', exc_info=True)

    # def scsc_scan_report(self, event: Event):
    #     print(event)
    #
    # def handle_start_scan(self, event: Event):
    #     print(event)
