from .event.event import EventType
from .script import *


class EventsMap:
    events = {

        # EventType.cardio_start_start: 'scsc.cardio_start_start',
        # EventType.cardio_start_cancel: 'scsc.cardio_start_cancel',
        # EventType.cardio_start_done: 'scsc.cardio_start_done',
        EventType.start_scan: scsc.start_scan,
        EventType.prepare_scan: scsc.prepare_scan,
        EventType.trigger_start_done: scsc.trigger_start_done,
        EventType.set_table_position: scsc.set_table_position,
        # EventType.move_patient_table: scsc.move_patient_table,
        EventType.stop_work: scsc.stop_work,
        # EventType.start_warm_up: scsc.start_warm_up,
        # EventType.calibration: scsc.calibration,
        # EventType.scsc_scan_report: scsc.scsc_scan_report,
        # EventType.start_test_equipment: scsc.start_test_equipment,
        # EventType.test_equipment_finished: scsc.handle_test_equipment_finished,
        # EventType.get_devices_params: scsc.get_devices_params,
        # EventType.work_start: scsc.notify_view,
        # EventType.work_complete: scsc.notify_view,
        # EventType.prepare_scan_finished: scsc.notify_view,
        # EventType.prepare_scan_failed: scsc.notify_view,
        EventType.table_move_start: scsc.notify_view,
        EventType.table_move_stop: scsc.notify_view,

        # EventType.handle_alert: scsc.notify_view,
        # EventType.handle_alert_ok: scsc.notify_view,
        # EventType.calibration_finished: scsc.notify_view,
        EventType.warm_up_requested: scsc.notify_view,
        EventType.warm_up_finished: scsc.notify_view,
        # EventType.tomograph_state: scsc.tomograph_state,

        # EventType.get_scan_reports: get_scan_reports,
        EventType.get_settings: get_settings,
        EventType.set_settings: set_settings,
        EventType.set_operator: set_operator,

        EventType.ecg_data: notify_view,
        EventType.pulse_data: notify_view,
        EventType.breath_monitor_data: notify_view,
        EventType.bp_monitor_data: notify_view,
        # EventType.new_dicom_frame: notify_view,
        # EventType.new_sure_start_dicom_frame: notify_view,
        # EventType.new_cardio_start_dicom_frame: notify_view,

        EventType.get_patient_list: get_patient_list,
        # EventType.scan_reports_upd: notify_view,

        EventType.settings_update: notify_view,
        EventType.operator_update: notify_view,

        # EventType.protocols_upd: notify_view,
        # EventType.protocols_get: data.protocols.protocols_get,
        # EventType.protocol_save: data.protocols.protocol_save,
        # EventType.protocol_delete: data.protocols.protocol_delete,
        # EventType.protocol_rename: data.protocols.protocol_rename,

        EventType.contrast_injector_set_contrast_flow_speed:
            peripheral.contrast_injector.contrast_injector_set_contrast_flow_speed,
        EventType.contrast_injector_set_solvent_flow_speed:
            peripheral.contrast_injector.contrast_injector_set_solvent_flow_speed,
        EventType.contrast_injector_stop:
            peripheral.contrast_injector.contrast_injector_stop,
        EventType.contrast_injector_pour:
            peripheral.contrast_injector.contrast_injector_pour,

    }
