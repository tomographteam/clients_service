import enum
import json
from typing import Union

from tango_server.model.control.event.event_type import BaseEventType


class EventType(BaseEventType):
    #           SCSC
    # Команды
    prepare_scan = 'prepare_scan'  # Подготовка томографа к сканированию, постановка стола в позицию, раскрутка трубки
    start_scan = 'start_scan'  # Запуск сканирования
    trigger_start_done = 'trigger_start_done'  # Остановка процедуры trigger start
    stop_work = 'stop_work'  # Команда прекратить любые операции (сканирование, движение стола, разогрев и т.п.)
    set_table_position = 'set_table_position'  # Установка стола в указанную позицию
    # move_patient_table = 'move_patient_table'  # Ручное управление столом пациента (MovePatientTableCommand).
    # start_warm_up = 'start_warm_up'  # Старт прогрева
    # calibration = 'calibration'  # Калибровка
    # Запуск процедуры тестирования оборудования. Ответ в start_test_equipment_result, далее test_equipment_finished
    start_test_equipment = 'start_test_equipment'
    # get_devices_params = 'get_devices_params'  # Запрос параметров. Ответ в get_devices_params_result
    get_patient_list = 'get_patient_list'  # Запрос списка пациентов. Ответ в get_patient_list_result

    # cardio_start_start = 'cardio_start_start'
    # cardio_start_cancel = 'cardio_start_cancel'
    # cardio_start_done = 'cardio_start_done'

    # new_dicom_frame = 'new_dicom_frame'  # Снимок
    # new_sure_start_dicom_frame = 'new_sure_start_dicom_frame'  # Снимок для sure start
    # new_cardio_start_dicom_frame = 'new_cardio_start_dicom_frame'  # Снимок для cardio start

    table_move_start = 'table_move_start'  # Перемещение стола пациента вне сканирования
    table_move_stop = 'table_move_stop'

    # События
    # prepare_scan_finished = 'prepare_scan_finished'  # Сообщение о успешной подготовке сканирования, готовность к старту
    # prepare_scan_failed = 'prepare_scan_failed'  # Сообщение о сбое, отмене готовности к старту

    # handle_alert = 'handle_alert'  # Аварийная ситуация
    # handle_alert_ok = 'handle_alert_ok'  # Прекращение аварийной ситуации
    # calibration_finished = 'calibration_finished'  # Завершение процедуры калибровки

    # start_test_equipment_result = 'start_test_equipment_result'  # Статус запуска тестирования - успешен или нет.
    # test_equipment_finished = 'test_equipment_finished'  # Результат тестирования оборудования

    # get_devices_params_result = 'get_devices_params_result'  # Параметры оборудования
    # work_start = 'work_start'  # Сообщение о начале прерываемой операции
    # work_complete = 'work_complete'  # О завершении или досрочном прекращении операции

    # tomograph_state = 'tomograph_state'  # Регулярное событие о состоянии томографа
    warm_up_requested = 'warm_up_requested'  # Запрос о прогреве
    warm_up_finished = 'warm_up_finished'  # Сообщение о завершении прогрева

    #           DataManager
    # scan_reports_upd = 'scan_reports_upd'  # Обновлён список отчётов о сканировании
    settings_update = 'settings_update'  # Объект с настройками приложения
    operator_update = 'operator_update'  # Обновление оператора

    get_scan_reports = 'get_scan_reports'  # Запрос на обновление отчётов о сканировании от ResultManager`а
    # SettingsManager
    get_settings = 'get_settings'  # Запрос настроек
    set_settings = 'set_settings'  # Обновление настроек
    set_operator = 'set_operator'  # Установка оператора

    #   ProtocolManager
    # protocols_get = 'protocols_get'  # Запросить протоколы
    # protocols_upd = 'protocols_upd'  # Обновление списка протоколов
    # protocol_save = 'protocol_save'  # Сохранить протокол. Протокол с тем же именем будет перезаписан
    # protocol_delete = 'protocol_delete'  # Удалить протокол
    # protocol_rename = 'protocol_rename'  # Переименовать протокол

    #   ResultManager
    # image_reconstruction_complete = 'image_reconstruction_complete'  # Событие окончания реконструкции изображения

    #   DicomScuWorklist
    get_patient_list_result = 'patient_list'  # Список пациентов

    #           PeripheralManager
    #   EcgMonitorManager
    ecg_data = 'ecg_data'  # Порция данных ЭКГ
    pulse_data = 'pulse_data'  # Данные пульса

    #   BreathMonitorManager
    breath_monitor_data = 'breath_monitor_data'  # Данные датчика контроля дыхания

    #   BloodPressureMonitorManager
    bp_monitor_data = 'bp_monitor_data'  # Данные датчика кровяного давления

    #   OxygenMonitorManager
    oxygen_monitor_data = 'oxygen_monitor_data'  # Данные датчика кислорода

    #   ContrastInjectorManager
    contrast_injector_state_upd = 'contrast_injector_state_upd'
    contrast_injector_set_contrast_flow_speed = 'contrast_injector_set_contrast_flow_speed'
    contrast_injector_set_solvent_flow_speed = 'contrast_injector_set_solvent_flow_speed'
    contrast_injector_stop = 'contrast_injector_stop'
    contrast_injector_pour = 'contrast_injector_pour'


class Event:
    def __init__(self, event_type: Union[enum.Enum, str], data=None):
        if isinstance(event_type, str):
            self.event_type = EventType(event_type)
        else:
            self.event_type = event_type

        self.data = data

    def dump(self):
        return {
            'event_type': self.event_type.name,
            'data': self.data_handler(self.data),
        }

    def data_handler(self, data):
        try:
            json_data = json.loads(data)
            return json_data
        except Exception as e:
            return data
