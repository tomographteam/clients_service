from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    if tomograph_manager.presenter:
        return tomograph_manager.presenter.handle_model_event(event)
