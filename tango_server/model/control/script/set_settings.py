from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager
from src.model_.data.settings_manager import Settings


def run(tomograph_manager: TomographManager, event: Event):
    settings = event.data
    assert isinstance(settings, Settings)
    tomograph_manager.settings_manager.set_settings(settings)
