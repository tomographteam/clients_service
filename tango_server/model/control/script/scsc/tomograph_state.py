import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.debug(f'tomograph_state {event.data}')
    if tomograph_manager.presenter:
        return tomograph_manager.presenter.handle_model_event(event)
