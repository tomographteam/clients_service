import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.info(f'set_table_position {event.data}')
    return tomograph_manager.device_manager.set_table_position(event)
