import logging

from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager
from src.model_.data.scan_task import ScanTask


def run(tomograph_manager: TomographManager, event: Event):
    scan_task = event.data
    assert isinstance(scan_task, ScanTask)
    logger = logging.getLogger('scsc')
    logger.info(f'prepare_scan: {scan_task}')

    # event = Event(EventType.prepare_scan_finished)
    # tomograph_manager.presenter.handle_view_event(event)

    tomograph_manager.device_manager.prepare_scan(event)
