import logging

from src.model_.control.event.event import Event
from src.model_.control.event.event import EventType
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc')
    logger.info(f'start_test_equipment')

    run_self_test_result = tomograph_manager.device_manager._subsystemManager._SCSCManager.run_self_test()
    resultEvent = Event(EventType.start_test_equipment_result, run_self_test_result)

    if (resultEvent.data is None):
        resultEvent.data = {'result': 'failed'}

    tomograph_manager.presenter.handle_model_event(resultEvent)
