import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.info(f'move_patient_table {event.data}')
    return tomograph_manager.device_manager.move_patient_table(event)
