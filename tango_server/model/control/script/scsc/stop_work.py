import logging

from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    logger = logging.getLogger('scsc')
    logger.info('STOP_WORK')
    tomograph_manager.peripheral_manager.contrast_injector_manager.contrast_injector_stop()

    return tomograph_manager.device_manager.stop_work(event)
