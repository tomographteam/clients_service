import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.info(f'calibration')
    return tomograph_manager.device_manager.calibration(event)
