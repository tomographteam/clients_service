import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.info(f'start_warm_up')
    return tomograph_manager.device_manager.start_warm_up(event)
