import logging

from src.model_.control.event.event import Event
from src.model_.control.event.event import EventType
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc')
    logger.info(f'get_devices_params')

    resultEvent = Event(EventType.get_devices_params_result,
                        tomograph_manager.device_manager._subsystemManager._SCSCManager.get_parameters_devices())
    tomograph_manager.presenter.handle_model_event(resultEvent)
