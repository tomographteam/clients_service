import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc')
    logger.info(f'handle_test_equipment_finished {event.data}')
    return tomograph_manager.presenter.handle_model_event(event)
