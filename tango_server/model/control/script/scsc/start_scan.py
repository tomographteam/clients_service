# import logging
#
# from src.model.control.event.event import Event
# from src.model.control.tomograph_manager import TomographManager
# from src.model.data.scan_task import ScanTask
#
#
# def run(tomograph_manager: TomographManager, event: Event):
#     scan_task = event.data
#     assert isinstance(scan_task, ScanTask)
#     logger = logging.getLogger('scsc')
#     logger.info(f'start_scan: {scan_task}')
#     tomograph_manager.data_manager.result_manager.scan_task_register(scan_task)
#     return tomograph_manager.device_manager.start_scan(event)
