import logging

from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event):
    logger = logging.getLogger('scsc')
    logger.info(f'trigger_start_done')
    return tomograph_manager.device_manager.trigger_start_done(event)
