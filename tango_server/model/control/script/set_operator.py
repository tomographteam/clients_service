import logging

from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager
from src.model_.data.operator.operator import Operator


def run(tomograph_manager: TomographManager, event: Event):
    operator = event.data
    assert isinstance(operator, Operator)
    logging.getLogger('user').debug(f'set_operator script {operator}')
    tomograph_manager.settings_manager.set_operator(operator)
