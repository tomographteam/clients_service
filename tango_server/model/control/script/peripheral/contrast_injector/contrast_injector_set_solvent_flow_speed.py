from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    if tomograph_manager.presenter:
        flow_speed = event.data
        assert isinstance(flow_speed, float)
        manager = tomograph_manager.peripheral_manager.contrast_injector_manager
        manager.set_solvent_flow_speed(flow_speed)
