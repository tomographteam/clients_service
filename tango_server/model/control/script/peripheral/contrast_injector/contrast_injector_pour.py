from src.model_.control.event.event import Event
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    if tomograph_manager.presenter:
        manager = tomograph_manager.peripheral_manager.contrast_injector_manager
        manager.contrast_injector_pour()
