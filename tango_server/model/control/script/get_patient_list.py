from src.model_.control.event.event import Event
from src.model_.control.event.event import EventType
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    if tomograph_manager.presenter:
        dwm = tomograph_manager.data_manager.worklist_manager
        name_filter = event.data['name_filter']
        result_event = Event(EventType.get_patient_list_result, dwm.get_work_list(name_filter))
        tomograph_manager.presenter.handle_model_event(result_event)
