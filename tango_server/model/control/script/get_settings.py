from src.model_.control.event.event import Event
from src.model_.control.event.event import EventType
from src.model_.control.tomograph_manager import TomographManager


def run(tomograph_manager: TomographManager, event: Event):
    if tomograph_manager.presenter:
        settings = tomograph_manager.settings_manager.get_settings()
        result_event = Event(EventType.settings_update, settings)
        tomograph_manager.presenter.handle_model_event(result_event)
