import asyncio
import logging
from asyncio import Queue
from enum import Enum
from typing import Optional, Type, Any, Awaitable

from tango_server.model.control.event.event import Event


try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass

log = logging.getLogger('events')


class AbstractEquipmentSubsystemManager:
    _logger = logging.getLogger('peripheral')
    timeout = 3
    _io_error_sleep = 3

    def __init__(self, tomograph_manager: 'TomographManager'):
        self._tomograph_manager = tomograph_manager
        self._presenter = None
        self._event_loop = None
        self._event_queue = Queue()
        self._events_map = {}
        event_type = self._event_type_fabric()
        if event_type:
            tomograph_manager.register_async_event_map(event_type, self._handle_event)

    def set_presenter(self, presenter):
        self._presenter = presenter
        self.start()

    def _handle_event(self, event: Event):
        if self._event_loop:
            self._event_loop.create_task(self._event_queue.put(event))

    def _event_type_fabric(self) -> Optional[Type[Enum]]:
        return None

    def start(self):
        self._event_loop = asyncio.get_event_loop()
        self._event_loop.create_task(self._read_event_queue())
        self._event_loop.run_until_complete(self._work())

    async def _read_event_queue(self):
        event_type = self._event_type_fabric()
        if event_type is None:
            return
        while True:
            if self._event_loop:
                event = await self._event_queue.get()
                log.debug(f'_read_event_queue {event.event_type}')
                assert isinstance(event, Event)
                assert isinstance(event.event_type, event_type)

                if event.event_type in self._events_map:
                    event_handler = self._events_map[event.event_type]
                    if asyncio.iscoroutinefunction(event_handler):
                        await event_handler(self._tomograph_manager, event)
                    else:
                        event_handler(self._tomograph_manager, event)
            else:
                await asyncio.sleep(0.01)

    async def _work(self):
        raise NotImplemented

    async def _timeout(self, future: Awaitable, timeout=None) -> Any:
        timeout = timeout or self.timeout
        return await asyncio.wait_for(future, timeout=timeout)

