import asyncio
import logging
from typing import Dict, List, Optional, Awaitable, Any

from src.model_.equipment.abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager
from src.model_.equipment.scsc_manager.attribute import DeviceAttribute, AttributeInfo, DeviceAttributesMap, \
    DeviceAttributeState, DeviceAttributeValue
from src.model_.equipment.scsc_manager.client import SCSCClient
from src.model_.equipment.scsc_manager.command import SCSCDeviceCommand
from src.model_.equipment.scsc_manager.devices.device import SCSCDevice

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class TangoManager(AbstractEquipmentSubsystemManager):
    _logger = logging.getLogger('tango')
    timeout = 3

    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)

        logging.getLogger('bootstrap').info('before init SCSCClient')
        # from src.model.equipment.scsc_manager.client import SCSCClient
        self._client: SCSCClient = SCSCClient()
        # self._client = None
        logging.getLogger('bootstrap').info('after init SCSCClient')
        self._attribute_info_cache: Dict[DeviceAttribute, AttributeInfo] = {}
        self._devices_commands_cache: Dict[SCSCDevice, List[SCSCDeviceCommand]] = {}

    async def set_attribute_info(self, attribute: DeviceAttribute):
        if attribute not in self._attribute_info_cache:
            info = await self._client.get_attribute_info(attribute)
            if info:
                self._attribute_info_cache[attribute] = info
        attribute.info = self._attribute_info_cache.get(attribute, None)

    async def set_commands_list(self, device: SCSCDevice):
        commands_list = self._devices_commands_cache.get(device, None)
        if commands_list is None:
            try:
                commands_list = await self._client.get_commands_list(device)
            except:
                pass
            self._devices_commands_cache[device] = commands_list
        device.user_runnable_commands = commands_list

    # maintenance_widget.py
    async def get_devices_attribute_values(self, devices: List[SCSCDevice]) -> DeviceAttributesMap:
        result = DeviceAttributesMap()
        for device_attribute_state in await asyncio.gather(*map(self.get_device_attributes_values, devices)):
            assert isinstance(device_attribute_state, DeviceAttributeState)
            result.state[device_attribute_state.device] = device_attribute_state
        return result

    async def get_device_attributes_values(self, device: SCSCDevice,
                                           attributes: Optional[List[DeviceAttribute]] = None) -> DeviceAttributeState:
        if not attributes:
            attributes = await self._client.get_attributes_list(device)
        else:
            pass
        info_tasks = []
        for attr in attributes:
            info_tasks.append(self.set_attribute_info(attr))
        try:
            await self._timeout(asyncio.gather(*info_tasks), 5)
        except asyncio.TimeoutError:
            self._logger.warning(f'get_device_attributes_values {device} timeoit error')

        await self.set_commands_list(device)
        values = await self._client.get_scalar_attributes_values(device, attributes)
        return values

    async def get_attribute_value(self, attribute: DeviceAttribute) -> DeviceAttributeValue:
        value = await self._client.get_attribute_value(attribute)
        await self.set_attribute_info(value.attribute)
        return value

    async def set_attribute_value(self, attr_value: DeviceAttributeValue) -> DeviceAttributeValue:
        return await self._client.set_attribute_value(attr_value)
