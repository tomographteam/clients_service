import logging
from typing import Dict

from src.model_.equipment.scsc_manager.command import SCSCDeviceCommand
from src.model_.equipment.scsc_manager.devices.device import SCSCDevice

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    logger.info(f'SCSC scsc_run_command')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    device: SCSCDevice = event.data['device']
    command: SCSCDeviceCommand = event.data['command']
    arguments: Dict = event.data.get('arguments')
    await scsc_manager.run_command(device, command, arguments)
