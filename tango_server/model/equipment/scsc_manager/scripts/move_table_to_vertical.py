import logging

from src.model_.data.move_patient_table_command import MoveTableToPosition

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    params: MoveTableToPosition = event.data
    logger = logging.getLogger(f'move_table_to_vertical {params}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return

    await scsc_manager.move_table_to_vertical(params)
