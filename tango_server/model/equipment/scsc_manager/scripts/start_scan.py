import logging

from src.model_.data.scan_task import ScanTask

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    scan_task = event.data
    assert isinstance(scan_task, ScanTask)
    logger.info(f'SCSC start_scan: {scan_task}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    # tomograph_manager.data_manager.voice_manager.play(scan_task.protocol.get_start_voice_command(),
    #                                                   scan_task.protocol.voice_command_lang)

    tomograph_manager.data_manager.result_manager.scan_task_register(scan_task)
    await scsc_manager.start_scan(scan_task)
