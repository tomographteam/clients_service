import logging

try:
    from src.model_.control.tomograph_manager import TomographManager
    from src.model_.equipment.scsc_manager.manager import SCSCManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    logger.info(f'SCSC stop_collimator_watch')

    scsc_manager: 'SCSCManager' = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    await scsc_manager.stop_collimator_watch()
