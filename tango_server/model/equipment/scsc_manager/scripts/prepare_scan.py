import logging

from src.model_.data.scan_task import ScanTask

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    scan_task = event.data
    assert isinstance(scan_task, ScanTask)
    logger.info(f'scsc_prepare_scan: {scan_task}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    await scsc_manager.prepare_scan(scan_task)
