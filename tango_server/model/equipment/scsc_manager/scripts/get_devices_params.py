import asyncio
import logging
from typing import Any, Awaitable
from src.model_.equipment.scsc_manager.events import SCSCEventTypes
from src.model_.control.event.event import Event

from src.model_.equipment.scsc_manager.devices.device import SCSCDevice
from src.model_.equipment.scsc_manager.devices.devices_list import SCSCDevices

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class _DeviceAttributesLoader:

    def __init__(self, device: SCSCDevice, tomograph_manager: 'TomographManager'):
        self._device = device
        self._tomograph_manager = tomograph_manager

    async def run(self):
        scsc_manager = self._tomograph_manager.device_manager.scsc_manager
        try:
            result = await self._timeout(
                scsc_manager.get_device_attributes_values(self._device, self._device.get_attributes_list()))

            result_event = Event(SCSCEventTypes.get_devices_params_result, result)
        except asyncio.TimeoutError:
            result_event = Event(SCSCEventTypes.get_devices_params_result_error, self._device)

        self._tomograph_manager.presenter.handle_model_event(result_event)

    async def _timeout(self, future: Awaitable) -> Any:
        return await asyncio.wait_for(future, timeout=5)


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    logger.info(f'get_devices_params')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    tasks = []
    for device in SCSCDevices.get_list():
        loader = _DeviceAttributesLoader(device, tomograph_manager)
        tasks.append(loader.run())

    await asyncio.gather(*tasks)
