import logging

from src.model_.control.event.event import Event
from src.model_.equipment.scsc_manager.devices.device import SCSCDevice

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    devices_list = event.data
    assert isinstance(devices_list, list)
    for device in devices_list:
        assert isinstance(device, SCSCDevice)

    logger.debug(f'scsc_get_devices_attribute_values Devices:{list(map(str, devices_list))}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    result = await scsc_manager.get_devices_attribute_values(devices_list)
    from src.model_.equipment.scsc_manager.events import SCSCEventTypes
    result_event = Event(SCSCEventTypes.get_devices_attribute_values_result, result)
    tomograph_manager.presenter.handle_model_event(result_event)
