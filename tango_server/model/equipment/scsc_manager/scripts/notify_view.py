try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    # logger = logging.getLogger('scsc_tangobox')
    # logger.info(f'scsc_get_devices_attribute_values')

    tomograph_manager.presenter.handle_view_event(event)
