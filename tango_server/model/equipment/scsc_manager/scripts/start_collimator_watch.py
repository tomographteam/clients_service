import logging

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    logger.info(f'SCSC start_collimator_watch')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    await scsc_manager.start_collimator_watch()
