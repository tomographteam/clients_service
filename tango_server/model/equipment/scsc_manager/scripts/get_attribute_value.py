import logging

from src.model_.control.event.event import Event
from src.model_.equipment.scsc_manager.attribute import DeviceAttribute

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    attribute = event.data
    assert isinstance(attribute, DeviceAttribute)
    logger.info(f'scsc_get_attribute_value: {attribute}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    result = await scsc_manager.get_attribute_value(attribute)
    from src.model_.equipment.scsc_manager.events import SCSCEventTypes
    result_event = Event(SCSCEventTypes.attribute_value_update, result)
    tomograph_manager.presenter.handle_model_event(result_event)
