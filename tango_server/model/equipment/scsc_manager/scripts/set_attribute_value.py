import logging

from src.model_.control.event.event import Event
from src.model_.equipment.scsc_manager.attribute import DeviceAttributeValue

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


async def run(tomograph_manager: 'TomographManager', event):
    if not tomograph_manager.presenter:
        return
    logger = logging.getLogger('scsc_tangobox')

    value = event.data
    assert isinstance(value, DeviceAttributeValue)
    logger.info(f'scsc_set_attribute_value: {value.attribute}={value.get_value_repr()}')

    scsc_manager = tomograph_manager.device_manager.scsc_manager
    if scsc_manager is None:
        logger.info(f'scsc_manager not initialized')
        return
    result_value = await scsc_manager.set_attribute_value(value)
    from src.model_.equipment.scsc_manager.events import SCSCEventTypes
    result_event = Event(SCSCEventTypes.attribute_value_update, result_value)
    tomograph_manager.presenter.handle_model_event(result_event)
