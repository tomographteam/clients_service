from ...control.event.event_type import BaseEventType


class SCSCEventTypes(BaseEventType):
    get_devices_attribute_values = 'scsc_get_devices_attribute_values'
    get_devices_attribute_values_result = 'scsc_get_devices_attribute_values_result'

    get_attribute_value = 'scsc_get_attribute_value'
    attribute_value_update = 'scsc_attribute_value_update'

    scsc_set_attribute_value = 'scsc_set_attribute_value'

    prepare_scan = 'prepare_scan'
    start_scan = 'start_scan'

    prepare_scan_finished = 'prepare_scan_finished'
    prepare_scan_failed = 'prepare_scan_failed'
    work_complete = 'work_complete'
    stop_work = 'stop_work'

    trigger_start_done = 'trigger_start_done'
    work_start = 'work_start'
    scan_report = 'scan_report'

    handle_alert = 'handle_alert'
    handle_alert_ok = 'handle_alert_ok'
    warm_up_requested = 'warm_up_requested'
    warm_up_finished = 'warm_up_finished'
    start_warm_up = 'start_warm_up'
    calibration = 'calibration'
    calibration_finished = 'calibration_finished'

    tomograph_connection_lost = 'tomograph_connection_lost'
    tomograph_state = 'tomograph_state'  # TomographState
    get_devices_params = 'get_devices_params'
    get_devices_params_result = 'get_devices_params_result'
    get_devices_params_result_error = 'get_devices_params_result_error'

    start_test_equipment = 'start_test_equipment'
    start_test_equipment_result = 'start_test_equipment_result'
    test_equipment_finished = 'test_equipment_finished'

    move_patient_table = 'move_patient_table'
    move_table_to_horizontal = 'move_table_to_horizontal'
    move_table_to_vertical = 'move_table_to_vertical'
    zero_plane_setup = 'zero_plane_setup'

    set_collimator_width = 'set_collimator_width'
    start_collimator_watch = 'start_collimator_watch'
    stop_collimator_watch = 'stop_attribute_watch'
    collimator_status_update = 'collimator_status_update'

    pause_scan = 'pause_scan'
    continue_scan = 'continue_scan'

    scan_paused = 'scan_paused'
    scan_continued = 'scan_continued'

    pes_status_update = 'pes_status_update'
    set_pes_battery_idle = 'set_pes_battery_idle'
    set_pes_battery_charging = 'set_pes_battery_charging'

    recon_finished = 'recon_finished'

    morning_startup = 'morning_startup'
    evening_shutdown = 'evening_shutdown'

    scsc_run_command = 'scsc_run_command'
