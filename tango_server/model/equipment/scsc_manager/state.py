from typing import Dict, Optional


class DeviceState:
    def __init__(self, state: str, status: str):
        self.state = state
        self.status = status

    @classmethod
    def from_dict(cls, data: Dict) -> Optional['DeviceState']:
        if data:
            return DeviceState(data['state'], data['status'])

    def is_none(self):
        return self.state is None or self.status is None


class TomographState:
    def __init__(self):
        self.temperature = 0
        self.pes_voltage = 0
        self.h_pos = 0
        self.v_pos = 0
        self.kv = 0
        self.work_in_progress = False
        self.error = False
        self.table_position = 0
        self.zero_plane_h_pos = 0
        self.dsebct_state: Optional[DeviceState] = None
