from typing import List, Optional

from tango_server.model.equipment.scsc_manager.attribute import DataType


class SCSCDeviceCommand:
    def __init__(self, name: str):
        self.name = name
        self.in_type: Optional[DataType] = None
        self.out_type: Optional[DataType] = None
        self.in_type_desc: str = ''
        self.out_type_desc: str = ''

    @classmethod
    def list_from_data(cls, data: List) -> List['SCSCDeviceCommand']:
        result: List[SCSCDeviceCommand] = []
        for cmd_data in data:
            cmd = SCSCDeviceCommand(cmd_data['name'])
            cmd.in_type = DataType.from_str(cmd_data['info'].get('in_type'))
            cmd.out_type = DataType.from_str(cmd_data['info'].get('out_type'))

            cmd.in_type_desc = cmd_data['info'].get('in_type_desc')
            cmd.out_type_desc = cmd_data['info'].get('out_type_desc')
            result.append(cmd)
        return result
