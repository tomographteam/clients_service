import asyncio
import atexit
import json
import logging
from json import JSONDecodeError
from typing import List, Optional, Dict, Callable, Union, Awaitable, Any

import aiohttp
import zmq
from aiohttp import ClientError, ClientConnectorError
from zmq.asyncio import Context

from tango_server.model.control.event.event import Event
from tango_server.model.equipment.scsc_manager.attribute import AttributeInfo, DeviceAttribute, DeviceAttributeValue, \
    AttributeDataFormat, DeviceAttributeState
from tango_server.model.equipment.scsc_manager.command import SCSCDeviceCommand
from tango_server.model.equipment.scsc_manager.devices.device import SCSCDevice
from tango_server.model.equipment.scsc_manager.devices.devices_list import SCSCDevices
from tango_server.model.equipment.scsc_manager.events import SCSCEventTypes
from tango_server.model.equipment.scsc_manager.state import DeviceState
from tango_server.utils.settings import get_server_config


class SCSCClient:
    timeout = 1
    url_template = 'http://{tango_rest_server}:{tango_rest_port}/tango/rest/v11/hosts/{tango_server}/devices/{request}'
    # url_v11_template = 'http://{tango_rest_server}:{tango_rest_port}/tango/rest/v11/{request}'
    get_attributes_template = '{device}/attributes'
    get_attribute_info_template = '{device}/attributes/{attribute_name}/info'
    get_commands_list_template = '{device}/commands'
    get_attributes_values_template = '{device}/attributes/value'
    get_attribute_value_template = '{device}/attributes/{attribute_name}/value'
    get_state_template = '{device}/state'
    set_attribute_value_template = '{device}/attributes/{attribute_name}/value'

    run_command_template = 'http://{tango_rest_server}:{tango_rest_port}/tango/rest/v11/commands'
    event_stream_template = '/tango/subscriptions/{id}/event-stream'
    event_subscriptions_template = '/tango/subscriptions/'

    # _set_attribute_value_template = 'http://{tango_rest_server}/tango/rest/rc4/attributes?async=true'
    _logger = logging.getLogger('scsc')
    _tango_log = logging.getLogger('tango')

    _server_config = get_server_config()

    def __init__(self):
        logging.getLogger('bootstrap').info('start init SCSCClient')

        self._tango_rest_server = self._server_config['tango_rest_server']
        self._tango_rest_port = self._server_config['tango_rest_port']
        self._zmq_port = 5563
        self._tango_server = self._server_config['tango_server']
        self._tango_port = self._server_config['tango_port']
        self._request_timeout = self._server_config['request_timeout']
        self._user = self._server_config['user']
        self._password = self._server_config['password']

        self._session = aiohttp.ClientSession()
        self._auth = aiohttp.BasicAuth(self._user, self._password)
        # logging.getLogger('bootstrap').info('init SCSCClient 1')

        # logging.getLogger('bootstrap').info(Context)
        # logging.getLogger('bootstrap').info(dir(Context))
        # logging.getLogger('bootstrap').info(Context.__bases__)
        self._zmq_ctx = Context.instance()
        # logging.getLogger('bootstrap').info(self._zmq_ctx)
        atexit.register(self.close)
        # logging.getLogger('bootstrap').info('init SCSCClient 2')

    def _make_tango_url(self, request: str) -> str:
        return self.url_template.format(
            tango_rest_server=self._tango_rest_server,
            tango_rest_port=self._tango_rest_port,
            tango_server=self._tango_server,
            request=request
        )

    def _debug_make_tangobox_url(self, request: str) -> str:
        return self.url_template.format(
            # tango_rest_server='tangobox',
            # tango_rest_port=10001,
            tango_rest_server=self._tango_rest_server,
            tango_rest_port=self._tango_rest_port,
            tango_server=self._tango_server,
            request=request
        )

    async def _get(self, request: str) -> Union[List, Dict]:
        result = {}
        url = self._make_tango_url(request)
        curl_cmd = f'curl -X GET -u "{self._user}:{self._password}" {url}'
        self._tango_log.info(curl_cmd)

        try:
            async with self._session.get(url, auth=self._auth) as response:
                if response.status == 200:
                    try:
                        result_str = ''
                        try:
                            result_str = await self._timeout(response.read())
                        except asyncio.TimeoutError:
                            self._logger.warning(f'url={url} timeout')
                        result = json.loads(result_str)
                    except Exception as e:
                        self._logger.error(f'scsc client error={e} result_str={result_str}')
                else:
                    self._logger.warning(f'url={url} response.status = {response.status}')
                    try:
                        result = await response.json()
                    except:
                        self._logger.warning(f'url={url} json response load error {response}')
                        pass
        except ConnectionRefusedError as e:
            pass
        except ClientConnectorError as e:
            pass
        except ClientError as e:
            self._logger.error(f'{e} url={url}', exc_info=True)
        return result

    async def _put(self, request: str, payload: dict = None) -> Dict:
        result = {}
        url = self._make_tango_url(request)
        self._tango_log.info(url)
        payload_str = f'-d \'{json.dumps(payload)}\'' if payload else ''
        tangobox_url = self._debug_make_tangobox_url(request)
        curl_cmd = '' \
                   f'curl -X PUT -H "Content-Type: application/json" -u "{self._user}:{self._password}" ' \
                   f' {payload_str} {tangobox_url}'
        self._tango_log.info(curl_cmd)
        try:
            async with self._session.put(url,
                                         auth=self._auth,
                                         json=payload,
                                         ) as response:
                if response.status == 200:
                    body = await response.read()
                    self._logger.warning(f'body={body}')
                    try:
                        result = json.loads(body)
                    except:
                        result = None
                else:
                    response_data = await response.read()
                    self._tango_log.error(
                        f'Tango error status={response.status} url={url} payload={payload} response={response_data}')
        except ClientError as e:
            self._logger.exception(f'{e} url={url}', exc_info=True)
        return result

    async def get_attributes_list(self, device: SCSCDevice) -> List[DeviceAttribute]:
        request = self.get_attributes_template.format(device=device.name)
        data = await self._get(request)
        self._logger.debug(f'get_attributes_list device={device} data={data}')
        # print(f'get_attributes_list device={device} data={data}')
        if isinstance(data, list):
            return list(map(lambda attr: DeviceAttribute(device, attr['name']), data))
        else:
            return []

    async def get_attribute_info(self, attribute: DeviceAttribute) -> Optional[AttributeInfo]:
        request = self.get_attribute_info_template.format(device=attribute.device.name,
                                                          attribute_name=attribute.name)
        data = await self._get(request)
        # self._logger.debug(f'get_attribute_info attribute={attribute} data={data}')
        return AttributeInfo.from_dict(attribute.device, data)

    async def get_commands_list(self, device: SCSCDevice) -> List[SCSCDeviceCommand]:
        request = self.get_commands_list_template.format(device=device.name)
        data = await self._get(request)
        return SCSCDeviceCommand.list_from_data(data)

    async def get_scalar_attributes_values(self, device: SCSCDevice, attributes: List[DeviceAttribute]) -> \
            DeviceAttributeState:
        scalar_names = []
        spectrum_attrs = []
        for attr in attributes:
            assert attr.device == device
            if attr.info and attr.info.data_format == AttributeDataFormat.SCALAR:
                scalar_names.append(attr.name)
            else:
                spectrum_attrs.append(attr)
        # ?attr={names_list}
        names_list = '&attr='.join(scalar_names)
        request = self.get_attributes_values_template.format(device=device.name) + f'?attr={names_list}'
        data = await self._get(request)
        self._logger.debug(f'get_scalar_attributes_values device={device} data={data}')
        state = DeviceAttributeState.from_dict(device, attributes, data)
        for attr in spectrum_attrs:
            state.attribute_values.append(DeviceAttributeValue(attr))
        return state

    async def get_attributes_values(self, device: SCSCDevice, attributes: List[DeviceAttribute]) -> \
            DeviceAttributeState:
        names = []
        for attr in attributes:
            assert attr.device == device
            names.append(attr.name)

        attr_str = '&attr='.join(names).lstrip('&')
        request = self.get_attributes_values_template.format(device=device.name) + '?attr=' + attr_str
        data = await self._get(request)
        # self._logger.debug(f'get_attributes_values device={device} data={data}')
        state = DeviceAttributeState.from_dict(device, attributes, data)
        for attr in attributes:
            state.attribute_values.append(DeviceAttributeValue(attr))
        return state

    async def get_attribute_value(self, attribute: DeviceAttribute) -> DeviceAttributeValue:
        request = self.get_attribute_value_template.format(device=attribute.device.name,
                                                           attribute_name=attribute.name)
        data = await self._get(request)
        # self._logger.debug(f'get_attribute_value attribute={attribute} data={data}')
        attr_value = DeviceAttributeValue.from_dict(data, attribute.device, attribute)
        return attr_value

    async def get_device_state(self, device: SCSCDevice) -> DeviceState:
        state_attribute = DeviceAttribute(device, 'state')
        status_attribute = DeviceAttribute(device, 'status')
        r = await self.get_attributes_values(device, [state_attribute, status_attribute])
        state = r.get_value(state_attribute)
        status = r.get_value(status_attribute)
        # print(state, status)
        device_state = DeviceState(state.value, status.value)
        return device_state

    async def set_attribute_value(self, attr_value: DeviceAttributeValue) -> DeviceAttributeValue:
        attribute = attr_value.attribute
        request = self.set_attribute_value_template.format(device=attribute.device.name,
                                                           attribute_name=attribute.name) + f'?v={attr_value.value}'

        data = await self._put(request)
        self._logger.debug(f'set_attribute_value attr_value={attr_value} data={data}')
        attr_value = DeviceAttributeValue.from_dict(data, attribute.device, attribute)
        return attr_value
        # attr_value = DeviceAttributeValue.from_dict(data, attribute.device, attribute)

    async def do_command(self, device: SCSCDevice, command: SCSCDeviceCommand, payload: Union[dict, any] = None):
        command_name = command.name
        url = self.run_command_template.format(
            tango_rest_server=self._tango_rest_server,
            tango_rest_port=self._tango_rest_port)
        # if isinstance(payload, dict) or payload is None:
        #     payload = payload or {}
        #     input = json.dumps(payload)
        # else:
        #     input = str(payload)
        url_payload = {
            'host': f'{self._tango_server}:{self._tango_port}',
            'device': device.name,
            'name': command.name,
        }
        if payload:
            url_payload['input'] = json.dumps(payload)
        url_payload = [url_payload, ]

        result = {}
        try:
            payload_str = f'-d \'{json.dumps(url_payload)}\'' if url_payload else ''
            curl_cmd = '' \
                       f'curl -X PUT -H "Content-Type: application/json" -u "{self._user}:{self._password}" ' \
                       f' {payload_str} {url}'

            self._tango_log.info(curl_cmd)

            async with self._session.put(url,
                                         auth=self._auth,
                                         json=url_payload,
                                         ) as response:
                if response.status == 200:
                    try:
                        result_str = await response.read()
                        if result_str:
                            result = json.loads(result_str)
                        else:
                            result = None
                    except Exception as e:
                        self._logger.error(f'scsc client error={e} result_str={result_str}')
                else:
                    response_data = await response.read()
                    self._tango_log.error(
                        f'Tango error status={response.status} url={url} payload={payload} response={response_data}')
        except ClientError as e:
            self._logger.exception(f'{e} url={url}', exc_info=True)

        self._logger.debug(f'do_command command_name={command_name} device={device}')
        return result

    async def subscribe(self, device: SCSCDevice, events: List[SCSCEventTypes]) -> int:
        url = f'http://{self._tango_rest_server}:{self._tango_rest_port}{self.event_subscriptions_template}'
        events_data = []
        # for event in events:
        events_data.append({
            'host': f'{self._tango_server}:{self._tango_port}',
            'device': device.name,
            'attribute': 'events_json',
            'type': 'change',
        })
        async with self._session.post(url, json=events_data, auth=self._auth) as resp:
            body = await resp.read()
            try:
                data = json.loads(body.decode('utf-8'))
                return int(data['id'])
            except:
                return 1

    async def read_event_stream(self, stream_id: int = 0, event_callback: Optional[Callable] = None):
        url = f'http://{self._tango_rest_server}:{self._tango_rest_port}' + self.event_stream_template.format(
            id=stream_id)
        loop = asyncio.get_event_loop()
        while True:
            try:
                async with self._session.get(url, auth=self._auth) as resp:
                    while True:
                        response_str = ''
                        # while '\n\n\n' not in response_str:
                        response_bytes = await resp.content.read(1024 * 10)
                        response_str += response_bytes.decode('utf-8')
                        for event_str in response_str.split('\n\n\n'):
                            event_str = event_str.strip()
                            if event_str:
                                self._tango_log.info(event_str)
                                event_dict = json.loads(event_str)
                                event_type = SCSCEventTypes[event_dict['event_type']]
                                event = Event(event_type, event_dict['data'])
                                if event_callback:
                                    loop.create_task(event_callback(event))
                        await asyncio.sleep(0.01)
            except:
                pass

    async def read_zmq_event_stream(self, event_callback: Optional[Callable] = None):

        self._logger.debug(f'read_zmq_event_stream start')

        url = f'tcp://{self._tango_rest_server}:{self._zmq_port}'
        self._logger.debug(f'zmq url={url}')
        try:
            sub = self._zmq_ctx.socket(zmq.SUB)
            self._logger.debug(f'zmq sub = {sub}')
            sub.connect(url)
        except Exception as e:
            self._logger.error(f'ZMQError: {e} url={url}')
            return
        self._logger.debug(f'ZMQ connect ok:  url={url}')
        sub.setsockopt(zmq.SUBSCRIBE, b'DSEBCT')
        loop = asyncio.get_event_loop()
        while True:
            try:
                topic, msg_bytes = await sub.recv_multipart()
                event_str = msg_bytes.decode('utf-8')
                if event_str:
                    self._tango_log.info(event_str)
                    try:
                        event_dict = json.loads(event_str)
                    except JSONDecodeError as e:
                        self._tango_log.error(f'JSONDecodeError: {e} event_str={event_str}')
                        continue
                    try:
                        event_type = event_dict['event_type']
                        event_data = event_dict['data']
                    except KeyError:
                        self._tango_log.error(f'KeyError: Incorrect event format {event_dict}')
                        continue
                    try:
                        event_type = SCSCEventTypes[event_type]
                    except KeyError:
                        self._tango_log.warning(f'KeyError: Unknown event_type {event_type}')
                        continue

                    event = Event(event_type, event_data)
                    if event_callback:
                        loop.create_task(event_callback(event))
                        await asyncio.sleep(0.01)
            except:
                pass

    def close(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._session.close())

    async def _timeout(self, future: Awaitable, timeout=None) -> Any:
        timeout = timeout or self.timeout
        return await asyncio.wait_for(future, timeout=timeout)


async def main():
    client = SCSCClient()

    print(await client.subscribe(SCSCDevices.DSEBCT, [SCSCEventTypes.work_complete, SCSCEventTypes.work_start]))
    # attr = DeviceAttribute(dev, 'long_scalar_w')
    # print(await client.get_attribute_value(attr))
    # attr_value = DeviceAttributeValue(attr)
    # attr_value.value = 123
    # value = await client.set_attribute_value(attr_value)
    # print(value)

    # r = await asyncio.gather(*([client.get_attribute_info(SCSCDevices.DSEBCT, attr_name) for attr_name in attributes]))
    # for attr in attributes:
    #     print(await client.get_attribute_info(SCSCDevices.DSEBCT, attr))
    #
    # r = await asyncio.gather(*([client.get_attribute_info(SCSCDevices.DSEBCT, attr_name) for attr_name in attributes]))
    # print(r)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([main()]))
