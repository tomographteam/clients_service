import asyncio
from asyncio import sleep
from typing import Dict, Callable, Optional, Awaitable, Any

from src.model_.control.event.event import Event
from src.model_.data.scan_task import ScanTask
from src.model_.data.voice_cmd_manager.voice_cmd import VoiceCmdData
from src.model_.data.voice_cmd_manager.voice_cmd_enums import VoiceCmdType, VoiceCmdTypes
from src.model_.equipment.scsc_manager.attribute import DeviceAttributesMap, DeviceAttribute, \
    DeviceAttributeValue, DeviceAttributeState
from src.model_.equipment.scsc_manager.devices.devices_list import SCSCDevices
from src.model_.equipment.scsc_manager.events import SCSCEventTypes
from .command import SCSCDeviceCommand
from .devices.collimator import COLLIMATOR, CollimatorStatus
from .devices.device import SCSCDevice
from .events_map import SCSCEventsMap
from .devices.dsebct import DSEBCT
from .devices.pes import PES, PesStatus, PesStates, PesState
from .devices.ptable import PTABLE
from .state import TomographState, DeviceState
from .tango_manager import TangoManager
from ...data.move_patient_table_command import MoveTableToPosition
from ...data.protocol.protocol import Protocol
from ...data.result_manager.result_manager import ResultManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class SCSCManager(TangoManager):

    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._events_map = SCSCEventsMap.events
        self._events_subscribe: Dict[SCSCEventTypes, Callable] = {}
        self._prepare_scan_finished_flag = False
        self._DSEBCT = DSEBCT()
        self._PTABLE = PTABLE()
        self._COLLIMATOR = COLLIMATOR()
        self._PES = PES()
        self._pes_status: Optional[PesStatus] = None
        self._state = TomographState()
        self._model_events_map = {
            SCSCEventTypes.scan_report: self._handle_scan_report,
            SCSCEventTypes.work_start: self._handle_work_start,
            SCSCEventTypes.work_complete: self._handle_work_complete,
            SCSCEventTypes.zero_plane_setup: self._handle_zero_plane_setup,
            SCSCEventTypes.recon_finished: self._handle_recon_finished,
        }
        self._collimator_watch_ask_stop = False
        self._collimator_watch_run = False

    def _event_type_fabric(self):
        return SCSCEventTypes
    
    def _tango_voice_cmd_fabric(self, protocol: Protocol, 
                                attr_prefix: str) -> Dict[str, str]:
        voice_cmd: VoiceCmdData = getattr(protocol, f'get_{attr_prefix}_voice_cmd')()
        lang = protocol.get_voice_cmd_lang().name
        data = {'name': '', 'filepath': '', 'lang': ''}
        if voice_cmd:
            data['name'] = voice_cmd.get_name_by_lang(lang)
            data['filepath'] = voice_cmd.get_filepath_by_lang(lang)
            data['lang'] = lang
        return data

    async def run_command(self, device: SCSCDevice, command: SCSCDeviceCommand, payload: dict = None):
        self._logger.debug(f'run_command {device} {command}')
        await self._client.do_command(device, command, payload)

    async def prepare_scan(self, task: ScanTask):
        self._logger.debug(f'PrepareScanWithParams {task}')
        await self._client.do_command(self._DSEBCT, self._DSEBCT.PrepareScanWithParams,
                                      task.to_tango_dict())

    async def stop_work(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.stop_work)

    async def pause_scan(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.pause_scan)

    async def continue_scan(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.continue_scan)

    async def trigger_start_done(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.trigger_start_done)

    async def _sync_prepare_scan(self, task: ScanTask):
        self._prepare_scan_finished_flag = False

        def prepare_scan_finished(event: Event):
            self._prepare_scan_finished_flag = True

        self._events_subscribe[SCSCEventTypes.prepare_scan_finished] = prepare_scan_finished
        await self.prepare_scan(task)

        while not self._prepare_scan_finished_flag:
            await asyncio.sleep(0.01)
        self._events_subscribe.pop(SCSCEventTypes.prepare_scan_finished)

    async def start_scan(self, task: ScanTask):
        # if task.protocol.get_scan_type() in (ScanTypes.trigger_start, ScanTypes.trigger_start_first_slice):
        #     await self._sync_prepare_scan(task)
        await asyncio.gather(self.set_attribute_value(DeviceAttributeValue(self._DSEBCT.study_id, 
                                                                           task.study_id)),
                             self.set_attribute_value(DeviceAttributeValue(self._DSEBCT.series_number, 
                                                                           task.series_number)))

        # Подготовка задачи под голосовую команду
        tango_voice_cmd_params = self._tango_voice_cmd_fabric(
            task.protocol, VoiceCmdTypes.start)
        
        # Запуск задач
        await self._client.do_command(self._DSEBCT, self._DSEBCT.StartScan)
        await self._client.do_command(self._DSEBCT, self._DSEBCT.say_cmd, tango_voice_cmd_params)

    async def start_test_equipment(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.start_test_equipment)

    async def start_warm_up(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.start_warm_up)

    async def calibration(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.calibration)

    async def _event_callback(self, event: Event):
        if event.event_type in self._events_subscribe:
            self._events_subscribe[event.event_type](event)

        if event.event_type in self._model_events_map:
            self._model_events_map[event.event_type](event)
        else:
            self._tomograph_manager.presenter.handle_model_event(event)

    def _handle_work_start(self, event: Event):
        self._state.work_in_progress = True
        self._tomograph_manager.presenter.handle_model_event(event)

    def _handle_work_complete(self, event: Event):
        self._state.work_in_progress = False
        self._tomograph_manager.presenter.handle_model_event(event)

    def _handle_zero_plane_setup(self, event: Event):
        try:
            self._state.zero_plane_h_pos = int(event.data['h_pos'])
        except (ValueError, KeyError) as e:
            self._state.zero_plane_h_pos = 0
            self._logger.error(f'Error _handle_zero_plane_setup parse message: {event.data}', e)

    def _handle_scan_report(self, event: Event):
        from src.model_.data.dicom_manager.scan_results.scan_results import scan_result_fabric
        result_manager: ResultManager = self._tomograph_manager.data_manager.result_manager
        scan_result = scan_result_fabric(event.data)
        scan_result.scan_task = result_manager.get_scan_task(scan_result.scan_task_id)
        event.data = scan_result
        self._tomograph_manager.presenter.handle_model_event(event)
        self._tomograph_manager.data_manager.result_manager.scan_result_register(event.data)

    def _handle_recon_finished(self, event: Event):
        result_manager = self._tomograph_manager.data_manager.result_manager
        result_manager.on_recon_finished(event)

    def _send_connection_lost_event(self):
        self._tomograph_manager.presenter.handle_model_event(Event(SCSCEventTypes.tomograph_connection_lost))

    async def _update_state(self):
        try:
            dsebct_state: Optional[DeviceState]
            ptable_attributes_future = self.get_device_attributes_values(self._PTABLE,
                                                                         [self._PTABLE.h_pos, self._PTABLE.v_pos,
                                                                          self._PTABLE.zero_plane_h_pos])
            ptable_attributes, temperature_attr, dsebct_state = await self._timeout(asyncio.gather(
                ptable_attributes_future,
                self.get_attribute_value(self._DSEBCT.temperature),
                self._client.get_device_state(self._DSEBCT)
            ))

            if dsebct_state is None or dsebct_state.is_none():
                return self._send_connection_lost_event()
            assert isinstance(ptable_attributes, DeviceAttributeState)
            h_pos_attr = ptable_attributes.get_value(self._PTABLE.h_pos)
            v_pos_attr = ptable_attributes.get_value(self._PTABLE.v_pos)
            zero_plane_h_pos = ptable_attributes.get_value(self._PTABLE.zero_plane_h_pos)

            self._state.temperature = temperature_attr.value or 0
            self._state.h_pos = h_pos_attr.value or 0
            self._state.v_pos = v_pos_attr.value or 0
            self._state.zero_plane_h_pos = zero_plane_h_pos.value or 0
            self._state.dsebct_state = dsebct_state
            self._tomograph_manager.presenter.handle_model_event(Event(SCSCEventTypes.tomograph_state, self._state))

        except asyncio.TimeoutError:
            self._send_connection_lost_event()
        except Exception as e:
            self._logger.exception(f'_update_state error {e}', exc_info=True)

    # CT Status widget
    async def get_devices_params(self) -> DeviceAttributesMap:
        result = DeviceAttributesMap()
        tasks = []
        for device in SCSCDevices.get_list():
            attributes = list(map(lambda name: DeviceAttribute(device, name), device.get_attribute_names_list()))
            tasks.append(self.get_device_attributes_values(device, attributes))

        for device_attribute_state in await asyncio.gather(*tasks):
            assert isinstance(device_attribute_state, DeviceAttributeState)
            result.state[device_attribute_state.device] = device_attribute_state

        return result

    async def move_patient_table(self, direction: str):
        await self._client.do_command(self._PTABLE, self._PTABLE.move_patient_table,
                                      {'direction': direction, 'input': direction})

    async def move_table_to_horizontal(self, params: MoveTableToPosition):
        await self._client.do_command(self._PTABLE, self._PTABLE.MoveHorizontal,
                                      params.to_dict())

    async def move_table_to_vertical(self, params: MoveTableToPosition):
        await self._client.do_command(self._PTABLE, self._PTABLE.MoveVertical,
                                      params.to_dict())

    async def set_collimator_width(self, width: int):
        await self._client.do_command(self._COLLIMATOR, self._COLLIMATOR.set_collimator_width, width)

    async def start_collimator_watch(self):
        self._event_loop.create_task(self._collimator_watch())

    async def stop_collimator_watch(self):
        self._collimator_watch_ask_stop = True

    async def _collimator_watch(self):
        if self._collimator_watch_run:
            return
        self._collimator_watch_run = True
        try:
            while not self._collimator_watch_ask_stop:
                await self._timeout(asyncio.gather(self._update_collimator_status(), sleep(0.3)), 2)
        except asyncio.TimeoutError:
            pass
        except Exception as e:
            self._logger.error(f'scsc collimator_watch error={e}')
        finally:
            self._collimator_watch_run = False
            self._collimator_watch_ask_stop = False

    async def _update_collimator_status(self):
        attributes = await self._client.get_attributes_values(self._COLLIMATOR,
                                                              [self._COLLIMATOR.state,
                                                               self._COLLIMATOR.status,
                                                               self._COLLIMATOR.CollWidth])
        status = CollimatorStatus()
        status.state = attributes.get_value(self._COLLIMATOR.state).value
        status.status = attributes.get_value(self._COLLIMATOR.status).value
        status.width = attributes.get_value(self._COLLIMATOR.CollWidth).value
        self._tomograph_manager.presenter.handle_model_event(Event(SCSCEventTypes.collimator_status_update, status))

    async def set_pes_battery_idle(self):
        await self._client.do_command(self._PES, self._PES.Idle)
        await self._update_pes_status()

    async def set_pes_battery_charging(self):
        await self._client.do_command(self._PES, self._PES.Charge)
        await self._update_pes_status()

    async def morning_startup(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.morning_startup)

    async def evening_shutdown(self):
        await self._client.do_command(self._DSEBCT, self._DSEBCT.evening_shutdown)

    async def _update_pes_status_task(self):
        while True:
            try:
                await self._timeout(asyncio.gather(self._update_pes_status(), sleep(3)), 5)
            except asyncio.TimeoutError:
                pass

    async def _update_pes_status(self):
        attributes = await self._client.get_attributes_values(self._PES,
                                                              [self._PES.state,
                                                               self._PES.PackVoltage,
                                                               self._PES.soc])
        state_value = attributes.get_value(self._PES.state).value
        if not state_value:
            return
        state = PesStates.load_by_name(attributes.get_value(self._PES.state).value)
        if not state:
            return
        assert isinstance(state, PesState)
        status = PesStatus(state)
        status.PackVoltage = round(attributes.get_value(self._PES.PackVoltage).value)
        status.soc = int(attributes.get_value(self._PES.soc).value)
        self._state.pes_voltage = status.PackVoltage
        self._pes_status = status
        self._tomograph_manager.presenter.handle_model_event(Event(SCSCEventTypes.pes_status_update, status))

    async def _work(self):
        # stream_id = await self._client.subscribe(SCSCDevices.DSEBCT,
        #                                          [SCSCEventTypes.work_complete, SCSCEventTypes.work_start])
        # self._event_loop.create_task(self._client.read_event_stream(
        #     stream_id=stream_id,
        #     event_callback=self._event_callback))
        pass
        self._event_loop.create_task(self._client.read_zmq_event_stream(event_callback=self._event_callback))
        self._event_loop.create_task(self._update_pes_status_task())

        while True:
            await asyncio.gather(self._update_state(), sleep(0.5))
