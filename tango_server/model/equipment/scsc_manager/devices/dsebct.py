import os
from functools import lru_cache
from typing import List, Callable

from PyQt5.QtGui import QPixmap

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.model.equipment.scsc_manager.command import SCSCDeviceCommand
from tango_server.utils.settings import get_path_config
from tango_server.view.translator import Translator
from .device import SCSCDevice
from ..attribute import DeviceAttribute
from ..state import DeviceState


class DSEBCT(SCSCDevice):
    StartScan = SCSCDeviceCommand('StartScan')
    start_test_equipment = SCSCDeviceCommand('start_test_equipment')
    start_warm_up = SCSCDeviceCommand('start_warm_up')
    calibration = SCSCDeviceCommand('calibration')
    morning_startup = SCSCDeviceCommand('morning_startup')
    evening_shutdown = SCSCDeviceCommand('evening_shutdown')
    pause_scan = SCSCDeviceCommand('pause_scan')
    continue_scan = SCSCDeviceCommand('continue_scan')
    trigger_start_done = SCSCDeviceCommand('trigger_start_done')
    PrepareScanWithParams = SCSCDeviceCommand('PrepareScanWithParams')
    stop_work = SCSCDeviceCommand('StopWork')
    say_cmd = SCSCDeviceCommand('say_cmd')

    def __init__(self):
        super().__init__('DSEBCT')
        self.SeriesType = DeviceAttribute(self, 'SeriesType')
        self.study_id = DeviceAttribute(self, 'study_id')
        self.series_number = DeviceAttribute(self, 'series_number')
        self.TableStartPos = DeviceAttribute(self, 'TableStartPos')
        self.TableStopPos = DeviceAttribute(self, 'TableStopPos')
        self.TableSpeed = DeviceAttribute(self, 'TableSpeed')
        self.Kv = DeviceAttribute(self, 'Kv')
        self.Ma = DeviceAttribute(self, 'Ma')
        self.Pitch = DeviceAttribute(self, 'Pitch')
        self.CollWidth = DeviceAttribute(self, 'CollWidth')

        self.ScoutWinWidth = DeviceAttribute(self, 'ScoutWinWidth')
        self.ScoutWinLength = DeviceAttribute(self, 'ScoutWinLength')

        self.ReconKernelIndex = DeviceAttribute(self, 'ReconKernelIndex')
        self.ReconNoiseReduced = DeviceAttribute(self, 'ReconNoiseReduced')
        self.ReconMetalCorr = DeviceAttribute(self, 'ReconMetalCorr')
        self.temperature = DeviceAttribute(self, 'temperature')

    @classmethod
    def get_attribute_names_list(cls) -> List[str]:
        return ['Kv', 'Ma', 'Pitch']


@lru_cache(maxsize=None)
def _get_pixmap(file_path: str) -> QPixmap:
    pixmap = QPixmap(file_path)
    return pixmap.scaledToHeight(32)


class DsebctState(DataEnumMember):
    _icons_path = os.path.join(get_path_config()['images'], 'dsebct_state')

    def __init__(self, name: str, tr_func: Callable, color: str = ''):
        super().__init__(name, tr_func)
        self.color = color

    def get_pixmap(self) -> QPixmap:
        return _get_pixmap(os.path.join(self._icons_path, f'{self.name}.png'))


class DsebctStateList(DataEnumList):
    _tr = Translator.inst()

    UNKNOWN = DsebctState('unknown', _tr.tr_dsebct_unknown, '#fd0000')
    NO_CONNECTION = DsebctState('no_connection', _tr.tr_dsebct_no_connection, '#585959')
    FAULT = DsebctState('fault', _tr.tr_dsebct_fault, '#fd0000')
    INIT = DsebctState('init', _tr.tr_dsebct_init, '#cacb79')
    OFF = DsebctState('off', _tr.tr_dsebct_off, '#fefbf9')
    ON = DsebctState('on', _tr.tr_dsebct_on, '#04f604')
    RUNNING = DsebctState('running', _tr.tr_dsebct_running, '#027d07')
    MOVING = DsebctState('moving', _tr.tr_dsebct_moving, '#879dfa')
    ALARM = DsebctState('alarm', _tr.tr_dsebct_alarm, '#f09003')
    STANDBY = DsebctState('standby', _tr.tr_dsebct_standby, '#f6ff02')

    @classmethod
    def get_list(cls) -> List[DsebctState]:
        return [cls.NO_CONNECTION, cls.FAULT, cls.INIT, cls.OFF, cls.ON, cls.RUNNING, cls.MOVING, cls.ALARM,
                cls.STANDBY]

    @classmethod
    def from_device_state(cls, device_state: DeviceState) -> DsebctState:
        state = cls.load_by_name(device_state.state)
        return state or cls.UNKNOWN
