from typing import List

from .device import SCSCDevice
from ..attribute import DeviceAttribute
from ..command import SCSCDeviceCommand


class PTABLE(SCSCDevice):
    move_patient_table = SCSCDeviceCommand('move_patient_table')
    MoveVertical = SCSCDeviceCommand('MoveVertical')
    MoveHorizontal = SCSCDeviceCommand('MoveHorizontal')

    def __init__(self):
        super().__init__(self._config['PTABLE'])
        self.h_pos = DeviceAttribute(self, 'h_pos')
        self.v_pos = DeviceAttribute(self, 'v_pos')
        self.zero_plane_h_pos = DeviceAttribute(self, 'zero_plane_h_pos')

    @classmethod
    def get_attribute_names_list(cls) -> List[str]:
        return ['h_pos', 'v_pos']
