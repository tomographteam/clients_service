from typing import List

from .device import SCSCDevice
from ..attribute import DeviceAttribute
from ..command import SCSCDeviceCommand


class CollimatorStatus:
    def __init__(self, ):
        self.state = ''
        self.status = ''
        self.width = 0

    def __repr__(self):
        return f'state={self.state} status={self.status} width={self.width}'


class COLLIMATOR(SCSCDevice):
    set_collimator_width = SCSCDeviceCommand('SetWidth')

    def __init__(self):
        super().__init__(self._config['COLLIMATOR'])
        self.state = DeviceAttribute(self, 'state')
        self.status = DeviceAttribute(self, 'status')
        self.CollWidth = DeviceAttribute(self, 'CollWidth')
        self.m_pos = DeviceAttribute(self, 'm_pos')
        self.moving = DeviceAttribute(self, 'moving')

    @classmethod
    def get_attribute_names_list(cls) -> List[str]:
        return ['state', 'status', 'CollWidth', 'm_pos', 'moving']
