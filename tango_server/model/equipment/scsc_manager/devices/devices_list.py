from typing import List

from tango_server.model.data.data_enum import DataEnumList
from tango_server.utils.settings import Settings
from .device import SCSCDevice
from .. import devices


class SCSCDevices(DataEnumList):
    _config = Settings.inst().get_device_name_config()

    DSEBCT = devices.DSEBCT()
    PTABLE = devices.PTABLE()
    PES = SCSCDevice('PES')
    HVPS_NWL = SCSCDevice('HVPS_NWL')
    HVPS_CT = SCSCDevice('HVPS_CT')
    CHILLER = SCSCDevice('CHILLER')
    COLLIMATOR = SCSCDevice('COLLIMATOR')
    COLLIMATOR_M_1 = SCSCDevice('COLLIMATOR_M_1')
    COLLIMATOR_M_2 = SCSCDevice('COLLIMATOR_M_2')
    COLLIMATOR_M_3 = SCSCDevice('COLLIMATOR_M_3')
    BCE = SCSCDevice('BCE')
    WWIRES = SCSCDevice('WWIRES')
    NEGPOWER = SCSCDevice('NEGPOWER')
    NEGPOWERMINI_1 = SCSCDevice('NEGPOWERMINI_1')
    NEGPOWERMINI_2 = SCSCDevice('NEGPOWERMINI_2')
    DAS_1 = SCSCDevice('DAS_1')
    DAS_2 = SCSCDevice('DAS_2')
    RECON = SCSCDevice('RECON')
    ECG = SCSCDevice('ECG')
    ELECTRODES = SCSCDevice('ELECTRODES')
    AGILENT = SCSCDevice('AGILENT')
    SCANNER = SCSCDevice('SCANNER')

    _list = [DSEBCT, HVPS_NWL, HVPS_CT, WWIRES, PES, PTABLE, CHILLER, COLLIMATOR,
             COLLIMATOR_M_1, COLLIMATOR_M_2, COLLIMATOR_M_3,
             BCE, NEGPOWER, NEGPOWERMINI_1, NEGPOWERMINI_2, DAS_1, DAS_2, RECON, ELECTRODES, AGILENT,
             ECG, SCANNER]

    @classmethod
    def get_list(cls) -> List[SCSCDevice]:
        return cls._list
