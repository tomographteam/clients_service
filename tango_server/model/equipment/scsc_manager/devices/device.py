from typing import List, Iterable

from tango_server.model.data.data_enum import DataEnumMember
from tango_server.model.equipment.scsc_manager.attribute import DeviceAttribute
from tango_server.model.equipment.scsc_manager.command import SCSCDeviceCommand
from tango_server.utils.settings import Settings


class SCSCDevice(DataEnumMember):
    _config = Settings.inst().get_device_name_config()

    def __init__(self, internal_name: str, tr_func=None):
        name = self._config.get(internal_name, internal_name)
        super().__init__(name, tr_func)
        self.internal_name = internal_name
        self.name: str = name
        self._tr_func = tr_func
        self.user_runnable_commands: List[SCSCDeviceCommand] = []

    @classmethod
    def get_attribute_names_list(cls) -> List[str]:
        return []

    def get_attributes_list(self) -> List[DeviceAttribute]:
        return [DeviceAttribute(self, name) for name in self.get_attribute_names_list()]

    def get_commands(self) -> Iterable[SCSCDeviceCommand]:
        for attr in type(self).__dict__.values():
            if isinstance(attr, SCSCDeviceCommand):
                yield attr
