from typing import List

from tango_server.model.data.data_enum import DataEnumMember, DataEnumList
from tango_server.view.translator import Translator
from .device import SCSCDevice
from ..attribute import DeviceAttribute
from ..command import SCSCDeviceCommand


class PesState(DataEnumMember):
    pass


class PesStates(DataEnumList):
    _tr = Translator.inst()

    unknown = PesState('unknown', _tr.tr_pes_unknown)
    init = PesState('init', _tr.tr_pes_init)
    standby = PesState('standby', _tr.tr_pes_standby)
    running = PesState('running', _tr.tr_pes_running)
    open = PesState('open', _tr.tr_pes_open)
    fault = PesState('fault', _tr.tr_pes_open)
    moving = PesState('moving', _tr.tr_pes_moving)
    on = PesState('on', _tr.tr_pes_on)
    alarm = PesState('alarm', _tr.tr_pes_alarm)
    close = PesState('close', _tr.tr_pes_alarm)

    @classmethod
    def get_list(cls) -> List[PesState]:
        return [cls.unknown, cls.init, cls.standby, cls.running, cls.open, cls.fault, cls.moving, cls.on, cls.alarm,
                cls.on, cls.alarm, cls.close]


class PesStatus:
    def __init__(self, state: PesState):
        self.state = state
        self.PackVoltage = 0
        self.soc = 0

    def __repr__(self):
        return f'state={self.state} PackVoltage={self.PackVoltage} soc={self.soc}'


class PES(SCSCDevice):
    MIN_WARNING = 420
    Idle = SCSCDeviceCommand('Idle')
    Charge = SCSCDeviceCommand('Charge')

    def __init__(self):
        super().__init__(self._config['PES'])
        self.PackVoltage = DeviceAttribute(self, 'PackVoltage')
        self.PackCurrent = DeviceAttribute(self, 'PackCurrent')
        self.BusVoltage = DeviceAttribute(self, 'BusVoltage')
        self.soc = DeviceAttribute(self, 'SOC')
        self.state = DeviceAttribute(self, 'state')

    @classmethod
    def get_attribute_names_list(cls) -> List[str]:
        return ['PackVoltage', 'state', 'SOC', 'PackCurrent', 'BusVoltage']
