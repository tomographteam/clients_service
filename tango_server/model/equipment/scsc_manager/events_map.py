from src.model_.equipment.scsc_manager.events import SCSCEventTypes
from src.model_.equipment.scsc_manager.scripts import *


class SCSCEventsMap:
    events = {
        SCSCEventTypes.get_devices_attribute_values: get_devices_attribute_values.run,
        SCSCEventTypes.get_devices_attribute_values_result: notify_view.run,

        SCSCEventTypes.get_attribute_value: get_attribute_value.run,
        SCSCEventTypes.attribute_value_update: notify_view.run,

        SCSCEventTypes.scsc_set_attribute_value: set_attribute_value.run,
        SCSCEventTypes.get_devices_params: get_devices_params.run,
        SCSCEventTypes.get_devices_params_result: notify_view.run,
        SCSCEventTypes.get_devices_params_result_error: notify_view.run,

        SCSCEventTypes.start_test_equipment: start_test_equipment.run,
        SCSCEventTypes.start_test_equipment_result: notify_view.run,
        SCSCEventTypes.test_equipment_finished: notify_view.run,

        SCSCEventTypes.move_patient_table: move_patient_table.run,
        SCSCEventTypes.move_table_to_horizontal: move_table_to_horizontal.run,
        SCSCEventTypes.move_table_to_vertical: move_table_to_vertical.run,

        SCSCEventTypes.prepare_scan: prepare_scan.run,
        SCSCEventTypes.prepare_scan_finished: notify_view.run,
        SCSCEventTypes.prepare_scan_failed: notify_view.run,
        SCSCEventTypes.stop_work: stop_work.run,
        SCSCEventTypes.trigger_start_done: trigger_start_done.run,
        SCSCEventTypes.start_scan: start_scan.run,
        SCSCEventTypes.start_warm_up: start_warm_up.run,
        SCSCEventTypes.calibration: calibration.run,
        SCSCEventTypes.scan_report: notify_view.run,
        SCSCEventTypes.handle_alert: notify_view.run,
        SCSCEventTypes.handle_alert_ok: notify_view.run,
        SCSCEventTypes.warm_up_requested: notify_view.run,
        SCSCEventTypes.warm_up_finished: notify_view.run,
        SCSCEventTypes.calibration_finished: notify_view.run,
        SCSCEventTypes.tomograph_state: notify_view.run,
        SCSCEventTypes.tomograph_connection_lost: notify_view.run,

        SCSCEventTypes.pause_scan: pause_scan.run,
        SCSCEventTypes.continue_scan: continue_scan.run,
        SCSCEventTypes.scan_paused: notify_view.run,
        SCSCEventTypes.scan_continued: notify_view.run,

        SCSCEventTypes.pes_status_update: notify_view.run,
        SCSCEventTypes.set_pes_battery_idle: set_pes_battery_idle.run,
        SCSCEventTypes.set_pes_battery_charging: set_pes_battery_charging.run,

        SCSCEventTypes.morning_startup: morning_startup.run,
        SCSCEventTypes.evening_shutdown: evening_shutdown.run,

        SCSCEventTypes.scsc_run_command: scsc_run_command.run,
        SCSCEventTypes.set_collimator_width: set_collimator_width.run,
        SCSCEventTypes.start_collimator_watch: start_collimator_watch.run,
        SCSCEventTypes.stop_collimator_watch: stop_collimator_watch.run,
        SCSCEventTypes.collimator_status_update: notify_view.run,

    }
