import enum
from datetime import datetime
from enum import Enum
from typing import Optional, List, Dict, Union, Any

from tango_server.model.data.access import AccessTypes

try:
    from tango_server.model.equipment.scsc_manager.devices.device import SCSCDevice
except ImportError:
    pass


class AttributeDataFormat(enum.Enum):
    SCALAR = 'SCALAR'
    SPECTRUM = 'SPECTRUM'
    IMAGE = 'IMAGE'

    @classmethod
    def from_str(cls, s) -> 'AttributeDataFormat':
        if hasattr(AttributeDataFormat, s):
            return AttributeDataFormat(s)
        else:
            print(s)
            raise NotImplemented


class DataType(Enum):
    DevVoid = 'DevVoid'
    DevULong = 'DevULong'
    DevLong = 'DevLong'
    DevShort = 'DevShort'
    DevUShort = 'DevUShort'
    DevDouble = 'DevDouble'
    DevFloat = 'DevFloat'
    DevULong64 = 'DevULong64'
    DevLong64 = 'DevLong64'
    DevVarLongArray = 'DevVarLongArray'
    DevVarCharArray = 'DevVarCharArray'
    DevVarUShortArray = 'DevVarUShortArray'
    DevVarULongArray = 'DevVarULongArray'

    DevUChar = 'DevUChar'

    DevEnum = 'DevEnum'
    DevBoolean = 'DevBoolean'
    DevString = 'DevString'
    State = 'State'
    IMAGE = 'IMAGE'

    @classmethod
    def from_str(cls, s: str) -> Optional['DataType']:
        if not s:
            return None
        if hasattr(DataType, s):
            return DataType(s)
        else:
            return None

    @classmethod
    def is_float(cls, t: 'DataType') -> bool:
        return t in (DataType.DevDouble, DataType.DevFloat)

    @classmethod
    def is_int(cls, t: 'DataType') -> bool:
        return t in (DataType.DevUShort, DataType.DevULong, DataType.DevLong, DataType.DevLong64, DataType.DevULong64)

    @classmethod
    def is_uint(cls, t: 'DataType') -> bool:
        return t in (DataType.DevUShort, DataType.DevULong, DataType.DevULong64)

    @classmethod
    def is_bool(cls, t: 'DataType') -> bool:
        return t in (DataType.DevBoolean,)

    @classmethod
    def is_str(cls, t: 'DataType') -> bool:
        return t in (DataType.DevString,)


class AttributeInfo:
    def __init__(self, device: 'SCSCDevice', name: str):
        self.device = device
        self.name = name
        self.writable = False
        self.data_format: AttributeDataFormat = AttributeDataFormat.SCALAR
        self.data_type = DataType.DevULong
        self.description = ''
        self.label = ''
        self.format = ''
        self.max_dim_x = 1
        self.max_dim_y = 0
        self.level: AccessTypes = AccessTypes.USER
        self.enum_labels: List[str] = []

    def get_max_size(self) -> int:
        return (self.max_dim_x or 1) * (self.max_dim_y or 1)

    def __repr__(self):
        return f'{self.device}/{self.name} ({self.label})'

    @classmethod
    def from_dict(cls, device: 'SCSCDevice', data: dict) -> Optional['AttributeInfo']:
        if 'name' not in data:
            return None
        obj = AttributeInfo(device, data['name'])
        obj.writable = data['writable'] in ['READ_WRITE', 'WRITE']
        obj.data_format = AttributeDataFormat.from_str(data['data_format'])
        obj.data_type = DataType.from_str(data['data_type'])
        obj.description = data['description']
        obj.label = data['label']
        obj.format = data['format']
        obj.max_dim_x = int(data['max_dim_x'])
        obj.max_dim_y = int(data['max_dim_y'])
        obj.level = cls._level_parse(data['level'])
        if 'enum_label' in data:
            obj.enum_labels = data['enum_label']
        return obj

    @classmethod
    def _level_parse(cls, level: str) -> AccessTypes:
        if level == 'OPERATOR':
            return AccessTypes.USER
        elif level == 'EXPERT':
            return AccessTypes.EXPERT
        elif level == 'TECHNICAL_SERVICE':
            return AccessTypes.TECHNICAL_SERVICE
        raise NotImplemented


class TangoValueError:
    def __init__(self):
        self.reason = ''
        self.severity = ''
        self.desc = ''
        self.origin = ''

    def __repr__(self):
        return f'{self.severity} - {self.reason}'

    @classmethod
    def from_dict(cls, data: dict) -> 'TangoValueError':
        obj = TangoValueError()
        obj.reason = data.get('reason', '')
        obj.severity = data.get('severity', '')
        obj.desc = data.get('desc', '')
        obj.origin = data.get('origin', '')
        return obj


class DeviceAttribute:
    def __init__(self, device: 'SCSCDevice', name: str):
        self.device = device
        self.name = name
        self.info: Optional[AttributeInfo] = None

    def __repr__(self):
        return f'{self.device}/{self.name}'

    def __hash__(self):
        return f'{self.device}{self.name.lower()}'.__hash__()

    def __eq__(self, other):
        if not isinstance(other, DeviceAttribute):
            return False
        else:
            return other.device == self.device and other.name.lower() == self.name.lower()


class DeviceAttributeValue:
    def __init__(self, attribute: DeviceAttribute, value: Union[float, int, str, None] = None):
        self.attribute = attribute
        self.quality = ''
        self.value: Union[float, int, str, None] = value
        self.timestamp = 0
        self.errors: List[TangoValueError] = []

    def get_value_repr(self) -> str:
        info = self.attribute.info
        if info is None:
            return str(self.value)
        data_format = self.attribute.info.data_format
        if data_format == AttributeDataFormat.SPECTRUM:
            if self.value is None:
                return f'[{data_format.name}]?'
            else:
                return f'[{data_format.name}]'
        return str(self.value)

    def set_value(self, s: Any):
        info = self.attribute.info
        if not info:
            self.value = s
            return
        if DataType.is_int(info.data_type):
            self.value = int(s)
        elif DataType.is_float(info.data_type):
            self.value = float(s)
        elif DataType.is_bool(info.data_type):
            self.value = bool(s)
        elif DataType.is_str(info.data_type):
            self.value = s
        else:
            self.value = s

    def timestamp_str(self) -> str:
        if self.timestamp == 0:
            return ''
        try:
            ms = self.timestamp % 1000
            return datetime.utcfromtimestamp(int(self.timestamp / 1000)).strftime(f'%H:%M:%S:{ms} %d.%m.%Y')
        except ValueError:
            return str(self.timestamp)

    def __repr__(self):
        if len(self.errors) > 0:
            errors = ', '.join(map(str, self.errors))
            return f'{self.attribute}: {errors}'
        else:
            return f'{self.attribute}: {self.value}'

    @classmethod
    def from_dict(cls, data: dict, device: 'SCSCDevice',
                  attribute: Optional[DeviceAttribute] = None) -> 'DeviceAttributeValue':
        if not attribute:
            attribute = DeviceAttribute(device, data['name'])
        obj = DeviceAttributeValue(attribute)
        obj.quality = data.get('quality', '')
        obj.severity = data.get('severity', '')
        obj.value = data.get('value', None)
        obj.timestamp = data.get('timestamp', 0)
        if 'errors' in data:
            for error_data in data['errors']:
                obj.errors.append(TangoValueError.from_dict(error_data))
        return obj


class DeviceAttributeState:
    def __init__(self, device: 'SCSCDevice'):
        self.device = device
        self.attribute_values: List[DeviceAttributeValue] = []
        self.errors: List[TangoValueError] = []

    def get_value(self, attribute: DeviceAttribute) -> Optional[DeviceAttributeValue]:
        for attribute_value in self.attribute_values:
            if attribute_value.attribute == attribute:
                return attribute_value

    @classmethod
    def from_dict(cls, device: 'SCSCDevice', attributes: List[DeviceAttribute], data: dict) -> 'DeviceAttributeState':
        state = DeviceAttributeState(device)
        if isinstance(data, dict) and data:
            for error_data in data['errors']:
                state.errors.append(TangoValueError.from_dict(error_data))
        else:
            for attr_data in data:
                attr_name = attr_data['name']
                for attr in attributes:
                    if attr.name == attr_name and attr.device == device:
                        attr_value = DeviceAttributeValue.from_dict(attr_data, device, attr)
                        break
                else:
                    attr_value = DeviceAttributeValue.from_dict(attr_data, device)
                state.attribute_values.append(attr_value)
        return state


class DeviceAttributesMap:
    def __init__(self):
        self.state: Dict['SCSCDevice', DeviceAttributeState] = {}
