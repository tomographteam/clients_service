import asyncio
import logging
import threading
from typing import Optional, Type

from src.model_.equipment.abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager
from src.presenter.presenter import Presenter

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass
log = logging.getLogger('events')


class AbstractDeviceManager:
    def __init__(self, tomograph_manager: 'TomographManager'):
        self.tomograph_manager = tomograph_manager
        self._presenter: Optional[Presenter] = None

    def _thread_manager_fabric(self, cls: Type[AbstractEquipmentSubsystemManager], set_to_attr: str,
                               presenter: Presenter):
        def _run(cls: Type[AbstractEquipmentSubsystemManager], set_to_object: 'AbstractDeviceManager', set_to_attr: str,
                 presenter: Presenter):
            try:
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                from src.model_.data.result_manager.result_manager import ResultManager

                manager = cls(self.tomograph_manager)
                assert isinstance(manager, AbstractEquipmentSubsystemManager)
                set_to_object.__setattr__(set_to_attr, manager)
                if presenter:
                    try:
                        manager.set_presenter(presenter)
                    except Exception as e:
                        log.exception(f'_thread_manager_fabric {cls} set presenter error', exc_info=True)
                        raise e
            except Exception as e:
                log.exception(e, exc_info=True)
                raise e

        thread = threading.Thread(target=_run, args=[cls, self, set_to_attr, presenter])
        thread.daemon = True
        thread.name = set_to_attr
        thread.start()
