from typing import Optional

from .abstract_device_manager import AbstractDeviceManager
from .scsc_manager.manager import SCSCManager
from ...presenter.presenter import Presenter


class DeviceManager(AbstractDeviceManager):

    def __init__(self, event_handler):
        super().__init__(event_handler)
        self.scsc_manager: Optional[SCSCManager] = None

    def set_presenter(self, presenter: Presenter):
        self._presenter = presenter
        self._thread_manager_fabric(SCSCManager, 'scsc_manager', presenter)
