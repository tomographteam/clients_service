from typing import Optional

from src.model_.equipment.abstract_device_manager import AbstractDeviceManager
from src.model_.equipment.peripheral.blood_pressure.manager import BloodPressureMonitorManager
from src.model_.equipment.peripheral.breath_monitor.manager import BreathMonitorManager
from src.model_.equipment.peripheral.contrast_injector import ContrastInjectorManager
from src.model_.equipment.peripheral.ecg_monitor.manager import EcgMonitorManager
from src.model_.equipment.peripheral.oxygen import OxygenMonitorManager
from src.presenter.presenter import Presenter

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class PeripheralManager(AbstractDeviceManager):
    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self.contrast_injector_manager: Optional[ContrastInjectorManager] = None
        self.blood_pressure_monitor_manager: Optional[BloodPressureMonitorManager] = None
        self.breath_monitor_manager: Optional[BreathMonitorManager] = None
        self.ecg_monitor_manager: Optional[EcgMonitorManager] = None
        self.oxygen_monitor_manager: Optional[OxygenMonitorManager] = None
        self._presenter: Optional[Presenter] = None

    def set_presenter(self, presenter: Presenter):
        self._presenter = presenter
        self._thread_manager_fabric(ContrastInjectorManager, 'contrast_injector_manager', presenter)
        self._thread_manager_fabric(BloodPressureMonitorManager, 'blood_pressure_monitor_manager', presenter)
        self._thread_manager_fabric(BreathMonitorManager, 'breath_monitor_manager', presenter)
        self._thread_manager_fabric(EcgMonitorManager, 'ecg_monitor_manager', presenter)
        self._thread_manager_fabric(OxygenMonitorManager, 'oxygen_monitor_manager', presenter)
