import asyncio
from asyncio import sleep

from aiohttp import ClientConnectorError

from tango_server.model.control.event.event import Event, EventType
from .client import BloodPressureMonitorClient
from ...abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class BloodPressureMonitorManager(AbstractEquipmentSubsystemManager):
    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._client: BloodPressureMonitorClient = BloodPressureMonitorClient()

        # def contrast_injector_stop(self):

    #     self._logger.info(f'contrast_injector_stop')
    #     self._event_loop.create_task(self._client.contrast_injector_stop())

    async def _check_state(self):
        if self._presenter is None:
            return

        assert isinstance(self._client, BloodPressureMonitorClient)
        state = await self._client.state()
        # self._logger.debug(f'Blood pressure state update: {state}')
        event = Event(EventType.bp_monitor_data)
        event.data = state
        self._presenter.handle_model_event(event)

    async def _work(self):
        while True:
            try:
                await self._check_state()
                await sleep(1)
                continue
            except asyncio.TimeoutError:
                pass
            except ClientConnectorError as e:
                pass
            await asyncio.sleep(self._io_error_sleep)
            # self._logger.info(e, exc_info=True)
