class BloodPressureMonitorState:
    def __init__(self):
        self.enabled = True
        self.systolic = 0
        self.dystolic = 0

    def __repr__(self):
        return f'systolic={self.systolic:.1f} dystolic={self.dystolic:.1f}'

    @classmethod
    def from_dict(cls, data: dict) -> 'BloodPressureMonitorState':
        obj = BloodPressureMonitorState()
        obj.systolic = data['systolic']
        obj.dystolic = data['dystolic']
        obj.enabled = data['enabled']
        return obj

    def to_dict(self) -> dict:
        data = {
            'systolic': self.systolic,
            'dystolic': self.dystolic,
            'enabled': self.enabled,
        }
        return data
