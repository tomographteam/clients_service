import asyncio
import atexit
from typing import Awaitable, Any

import aiohttp

from tango_server.model.equipment.peripheral.blood_pressure.state import BloodPressureMonitorState
from tango_server.utils.settings import get_peripheral_server_config


class BloodPressureMonitorClient:
    URL_TEMPLATE = 'http://{host}:{port}{url}'
    STATE_TEMPLATE = '/blood_pressure/state/'
    timeout = 2
    _server_config = get_peripheral_server_config()

    def __init__(self):
        self._host = self._server_config['blood_pressure_host']
        self._port = self._server_config['blood_pressure_port']
        self._session = aiohttp.ClientSession()
        atexit.register(self.close)

    async def _fetch(self, url):
        url = self.URL_TEMPLATE.format(host=self._host, port=self._port, url=url)
        async with self._session.get(url) as response:
            return await response.json()

    async def _post(self, url, data=None):
        url = self.URL_TEMPLATE.format(host=self._host, port=self._port, url=url)
        async with self._session.post(url, data=data) as response:
            return await response.json()

    async def state(self) -> BloodPressureMonitorState:
        url = self.STATE_TEMPLATE
        data = await self._timeout(self._fetch(url))
        assert isinstance(data, dict)
        return BloodPressureMonitorState.from_dict(data)

    async def _timeout(self, future: Awaitable) -> Any:
        return await asyncio.wait_for(future, timeout=self.timeout)

    def close(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._session.close())
