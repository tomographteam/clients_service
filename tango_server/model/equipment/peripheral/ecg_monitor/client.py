import asyncio
import atexit
import logging
from collections import Callable
from typing import Awaitable, Any

import aiohttp

from src.model_.equipment.peripheral.ecg_monitor.protocol_message import EcgMessage
from src.utils.settings import get_ecg_server_config


class EcgMonitorClient:
    URL_TEMPLATE = 'http://{host}:{port}{url}'
    STREAM_TEMPLATE = '/ecg_monitor/stream/'
    PULSE_TEMPLATE = '/ecg_monitor/pulse/'
    timeout = 2
    _logger = logging.getLogger('peripheral')

    _server_config = get_ecg_server_config()

    def __init__(self):
        self._host = self._server_config['host']
        self._port = self._server_config['port']

    async def _timeout(self, future: Awaitable) -> Any:
        return await asyncio.wait_for(future, timeout=self.timeout)

    async def read_ecg_stream(self, call_back: Callable):
        reader, writer = await self._timeout(
            asyncio.open_connection(self._host, self._port, loop=asyncio.get_event_loop()))
        await self._timeout(self._send_xon(writer))
        while True:
            data = bytes()
            while len(data) < EcgMessage.package_size:
                packet = await self._timeout(reader.read(EcgMessage.package_size))
                if not packet:
                    return
                data += packet
            if len(data) == EcgMessage.package_size:
                msg = EcgMessage.load(data)
                call_back(msg)

    async def _send_xon(self, writer):
        writer.write(bytes([0x11]))
        await writer.drain()


async def main():
    client = EcgMonitorClient()
    await client.read_ecg_stream(None)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    loop.run_forever()
