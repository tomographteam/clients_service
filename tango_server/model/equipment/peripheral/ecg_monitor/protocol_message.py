import struct
from datetime import datetime
from typing import List, Tuple

epoch = datetime.utcfromtimestamp(0)


def unix_time_millis(dt):
    return (dt - epoch).total_seconds() * 1000.0


def _bitfield(n):
    b = [int(digit) for digit in bin(n)[2:]]  # [2:] to chop off the "0b" part
    s = ''.join(map(str, b))
    return '0' * (16 - len(s)) + s


class Waveform:
    _format = '!H'
    _strict = struct.Struct(_format)
    _data_low_mask = 0x7f
    _data_hi_mask = 0x1f
    _voltage_mask = (1 << 12) - 1

    def __init__(self, voltage=0):
        self.x_ray_status = True  # b15 X-ray Status (0=off, 1=on) if used
        self._b14 = 0  # b14 (Future use)
        self.at_trigger_point = True  # b13  Set when sample is at the trigger point
        self._data_hi = 0  # b12..b8  Most significant 5 bits of waveform data value
        self._b7 = 0  # b7  Always 0
        self._data_low = 0  # b6..b0  Least significant 7 bits of waveform data value

        self.voltage = voltage

    @property
    def voltage(self) -> int:
        return (self._data_hi << 7) + self._data_low

    @voltage.setter
    def voltage(self, value: int):
        self._data_hi = (value >> 7) & self._data_hi_mask
        self._data_low = value & self._data_low_mask

    def pack(self) -> bytes:
        b = self.x_ray_status << 15 | \
            self.at_trigger_point << 13 | \
            (self._data_hi & self._data_hi_mask) << 8 | \
            (self._data_low & self._data_low_mask) << 0

        return self._strict.pack(b)

    @classmethod
    def load(cls, data: int) -> 'Waveform':
        obj = Waveform()
        obj.x_ray_status = bool(data & True << 15)
        obj.at_trigger_point = bool(data & True << 13)
        obj._data_hi = (data >> 8) & cls._data_hi_mask
        obj._data_low = (data >> 0) & cls._data_low_mask
        return obj


class EcgMessage:
    package_size = 128
    _format = '!BBBBBB120sxB'
    _struct = struct.Struct(_format)

    def __init__(self):
        self._byte_0 = 0xff  # Hex FF
        self.trigger_lead = 1  # Trigger Lead (1, 2 or 3 represents I. II. III)
        self.display_filter_status = 0  # Display Filter Status (0=off, 1=on)
        self.display_gain = 0  # Display Gain (0=5mm/mV, 1=10mm/mV, 2=20 mm/mV, 3=40 mm/mV)
        self._heart_rate_1 = 2  # Heart Rate (2 most significant bits) binary: 000000dd
        self._heart_rate_2 = 33  # Heart Rate (7 least significant bits) binary: 0ddddddd
        self.waveform_data: List[Waveform] = []  # Waveform Data – 60 samples, 2 bytes each (see below)
        self._byte_126 = 0  # unused
        self._byte_127 = 0xfe  # Hex FE

    @property
    def pulse(self) -> int:
        return (self._heart_rate_1 << 7) + self._heart_rate_2

    @pulse.setter
    def pulse(self, value: int):
        self._heart_rate_1 = value >> 7
        self._heart_rate_2 = value & ((1 << 7) - 1)

    def ecg_time_list(self) -> List[Tuple[float, float]]:
        t = unix_time_millis(datetime.now()) - 250.0
        result = []
        for waveform in self.waveform_data:
            result.append((t, waveform.voltage))
            t += 250 / 60
        return result

    def pack(self) -> bytes:
        waveform_data = b''.join(map(Waveform.pack, self.waveform_data))
        return self._struct.pack(self._byte_0, self.trigger_lead, self.display_filter_status, self.display_gain,
                                 self._heart_rate_1, self._heart_rate_2, waveform_data, self._byte_127)

    @classmethod
    def load(cls, data: bytes) -> 'EcgMessage':
        obj = EcgMessage()
        _, obj.trigger_lead, obj.display_filter_status, obj.display_gain, \
        obj._heart_rate_1, obj._heart_rate_2, waveform_data, _ = cls._struct.unpack(data)
        waveform_data_list = struct.Struct('!60H').unpack(waveform_data)
        obj.waveform_data = list(map(Waveform.load, waveform_data_list))
        return obj


if __name__ == '__main__':
    w = Waveform(3884)
    print(w.voltage)
    # m = EcgMessage()
    # m.pulse = 180
    # m = EcgMessage.load(m.pack())
    # print(m.pulse)
    # pri
    # print(1 << 9, _bitfield((1 << 9) - 1))
    # print(EcgMessage.load(EcgMessage().pack()))

    # w_data = Waveform().pack()
    # Waveform.load(w_data)
    # print(int(w_data))
    # print(w_data << 2)
