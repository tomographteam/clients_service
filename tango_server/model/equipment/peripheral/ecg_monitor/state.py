class PulseState:
    def __init__(self):
        self.enabled = True
        self.pulse = 0

    def __repr__(self):
        return f'pulse={self.pulse}'

    @classmethod
    def from_dict(cls, data: dict) -> 'PulseState':
        obj = PulseState()
        obj.pulse = data['pulse']
        obj.enabled = data['enabled']
        return obj

    def to_dict(self) -> dict:
        data = {
            'pulse': self.pulse,
            'enabled': self.enabled,
        }
        return data
