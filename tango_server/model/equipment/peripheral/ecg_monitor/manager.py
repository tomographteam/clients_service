import asyncio

from src.model_.control.event.event import EventType, Event
from .client import EcgMonitorClient
from .protocol_message import EcgMessage
from ...abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class EcgMonitorManager(AbstractEquipmentSubsystemManager):
    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._client: EcgMonitorClient = EcgMonitorClient()

    def _ecg_data_received(self, ecg_msg: EcgMessage):
        # self._logger.debug(f'ecg_data_received: {ecg_data}')
        if self._presenter:
            event = Event(EventType.ecg_data)
            event.data = ecg_msg
            # print(event)
            self._presenter.handle_model_event(event)

            # event = Event(EventType.pulse_data)
            # event.data = ecg_msg.pulse
            # self._presenter.handle_model_event(event)

    async def _read_ecg(self):
        while True:
            try:
                await self._client.read_ecg_stream(self._ecg_data_received)
                await asyncio.sleep(0.5)
                continue
            except asyncio.TimeoutError:
                pass
            except OSError:
                pass
            except Exception as e:
                self._logger.debug(e, exc_info=True)
            await asyncio.sleep(self._io_error_sleep)

    # async def _read_pulse(self):
    #     while True:
    #         try:
    #             pulse = await self._client.pulse()
    #             if self._presenter:
    #                 self._logger.debug(f'Pulse updated: {pulse}')
    #                 event = Event(EventType.pulse_data)
    #                 event.data = pulse
    #                 self._presenter.handle_model_event(event)
    #             await sleep(2)
    #         except ClientConnectorError as e:
    #             self._logger.debug(e, exc_info=True)
    #             pass

    async def _work(self):
        await self._read_ecg()
    #     await asyncio.gather(self._read_pulse(), self._read_ecg())
