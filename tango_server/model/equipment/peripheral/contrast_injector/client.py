import asyncio
import atexit
from asyncio import sleep
from typing import Awaitable, Any

import aiohttp

from tango_server.model.equipment.peripheral.contrast_injector.state import ContrastInjectorState
from tango_server.utils.settings import get_peripheral_server_config


class ContrastInjectorClient:
    URL_TEMPLATE = 'http://{host}:{port}{url}'
    STATE_TEMPLATE = '/contrast_injector/state/'
    SET_CONTRAST_FLOW_SPEED = '/contrast_injector/set_contrast_flow_speed/{flow_speed}'
    SET_SOLVENT_FLOW_SPEED = '/contrast_injector/set_solvent_flow_speed/{flow_speed}'
    CONTRAST_INJECTOR_STOP = '/contrast_injector/contrast_injector_stop/'
    CONTRAST_INJECTOR_POUR = '/contrast_injector/contrast_injector_pour/'

    _server_config = get_peripheral_server_config()
    timeout = 2

    def __init__(self):
        self._host = self._server_config['contrast_injector_host']
        self._port = self._server_config['contrast_injector_port']
        self._session = aiohttp.ClientSession()
        atexit.register(self.close)

    async def _fetch(self, url):
        url = self.URL_TEMPLATE.format(host=self._host, port=self._port, url=url)
        async with self._session.get(url) as response:
            return await response.json()

    async def _post(self, url, data=None):
        url = self.URL_TEMPLATE.format(host=self._host, port=self._port, url=url)
        response = await self._timeout(self._session.post(url, data=data))
        return await self._timeout(response.json())

    async def set_contrast_flow_speed(self, flow_speed: float):
        url = self.SET_CONTRAST_FLOW_SPEED.format(flow_speed=flow_speed)
        return await self._post(url)

    async def set_solvent_flow_speed(self, flow_speed: float):
        url = self.SET_SOLVENT_FLOW_SPEED.format(flow_speed=flow_speed)
        return await self._post(url)

    async def contrast_injector_stop(self):
        url = self.CONTRAST_INJECTOR_STOP
        return await self._post(url)

    async def contrast_injector_pour(self):
        url = self.CONTRAST_INJECTOR_POUR
        return await self._post(url)

    async def _timeout(self, future: Awaitable) -> Any:
        return await asyncio.wait_for(future, timeout=self.timeout)

    async def state(self) -> ContrastInjectorState:
        url = self.STATE_TEMPLATE
        data = await self._timeout(self._fetch(url))
        assert isinstance(data, dict)
        return ContrastInjectorState.from_dict(data)

    def close(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._session.close())


async def main():
    client = ContrastInjectorClient()
    print(await client.state())
    await asyncio.gather(client.set_solvent_flow_speed(1.2), client.set_contrast_flow_speed(0.7), sleep(3))
    print(await client.state())
    await sleep(3)
    print(await client.state())
    # await asyncio.gather(client.set_solvent_flow_speed(0), client.set_contrast_flow_speed(0))
    print(await client.state())


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.wait([main()]))
