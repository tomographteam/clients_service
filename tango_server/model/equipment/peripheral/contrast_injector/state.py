class ContrastInjectorState:
    def __init__(self):
        self.contrast_volume = 0
        self.solvent_volume = 0
        self.contrast_flow_speed = 0
        self.solvent_flow_speed = 0
        self.pressure = 0

    def __repr__(self):
        return f'contrast_volume={self.contrast_volume:.1f} solvent_volume={self.solvent_volume:.1f} ' + \
               f'contrast_flow_speed={self.contrast_flow_speed:.1f} solvent_flow_speed={self.solvent_flow_speed:.1f} ' + \
               f'pressure={self.pressure:.1f}'

    @classmethod
    def from_dict(cls, data: dict) -> 'ContrastInjectorState':
        obj = ContrastInjectorState()
        obj.contrast_volume = data['contrast_volume']
        obj.solvent_volume = data['solvent_volume']
        obj.contrast_flow_speed = data['contrast_flow_speed']
        obj.solvent_flow_speed = data['solvent_flow_speed']
        obj.pressure = data['pressure']
        return obj

    def to_dict(self) -> dict:
        data = {
            'contrast_volume': self.contrast_volume,
            'solvent_volume': self.solvent_volume,
            'contrast_flow_speed': self.contrast_flow_speed,
            'solvent_flow_speed': self.solvent_flow_speed,
            'pressure': self.pressure,
        }
        return data
