import asyncio
import logging
from asyncio import sleep

from aiohttp import ClientConnectorError

from tango_server.model.control.event.event import Event, EventType
from .client import ContrastInjectorClient
from ...abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class ContrastInjectorManager(AbstractEquipmentSubsystemManager):
    _logger = logging.getLogger('peripheral')

    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._client: ContrastInjectorClient = ContrastInjectorClient()

    def set_contrast_flow_speed(self, flow_speed: float):
        self._logger.info(f'set_contrast_flow_speed: {flow_speed}')
        self._event_loop.create_task(self._client.set_contrast_flow_speed(flow_speed))

    def set_solvent_flow_speed(self, flow_speed: float):
        self._logger.info(f'set_contrast_flow_speed: {flow_speed}')
        self._event_loop.create_task(self._client.set_solvent_flow_speed(flow_speed))

    def contrast_injector_stop(self):
        self._logger.info(f'contrast_injector_stop')
        self._event_loop.create_task(self._client.contrast_injector_stop())

    def contrast_injector_pour(self):
        self._logger.info(f'contrast_injector_pour')
        self._event_loop.create_task(self._client.contrast_injector_pour())

    async def _check_state(self):
        if self._presenter is None:
            return

        assert isinstance(self._client, ContrastInjectorClient)
        state = await self._client.state()
        # self._logger.debug(f'Contrast injector state update: {state}')
        event = Event(EventType.contrast_injector_state_upd)
        event.data = state
        self._presenter.handle_model_event(event)

    async def _work(self):
        while True:
            try:
                await self._check_state()
                await sleep(0.2)
                continue
            except asyncio.TimeoutError:
                pass
            except RuntimeError as e:
                self._logger.debug(e, exc_info=True)
            except ClientConnectorError as e:
                pass
            await asyncio.sleep(self._io_error_sleep)
                # self._logger.debug(e, exc_info=True)
