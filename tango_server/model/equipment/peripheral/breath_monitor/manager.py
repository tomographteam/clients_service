import asyncio
from typing import List

from aiohttp import ClientConnectorError

from src.model_.control.event.event import EventType, Event
from .client import BreathMonitorClient
from ...abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class BreathMonitorManager(AbstractEquipmentSubsystemManager):
    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._client: BreathMonitorClient = BreathMonitorClient()

    def _breath_data_received(self, breath_data: List):
        if self._presenter:
            event = Event(EventType.breath_monitor_data)
            event.data = breath_data
            self._presenter.handle_model_event(event)

    async def _work(self):
        while True:
            try:
                await self._client.read_breath_stream(self._breath_data_received)
                await asyncio.sleep(0.5)
                continue
            except asyncio.TimeoutError:
                pass
            except ClientConnectorError:
                pass
            except Exception as e:
                self._logger.debug(e, exc_info=True)
            await asyncio.sleep(self._io_error_sleep)

