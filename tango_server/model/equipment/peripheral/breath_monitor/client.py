import asyncio
import atexit
import json
from collections import Callable
from json import JSONDecodeError
from typing import Awaitable, Any

import aiohttp

from tango_server.utils.settings import get_peripheral_server_config


class BreathMonitorClient:
    URL_TEMPLATE = 'http://{host}:{port}{url}'
    STREAM_TEMPLATE = '/breath_monitor/stream/'
    timeout = 2

    _server_config = get_peripheral_server_config()

    def __init__(self):
        self._host = self._server_config['breath_monitor_host']
        self._port = self._server_config['breath_monitor_port']
        self._session = aiohttp.ClientSession()

    async def _timeout(self, future: Awaitable) -> Any:
        return await asyncio.wait_for(future, timeout=self.timeout)

    async def read_breath_stream(self, call_back: Callable):
        url = self.URL_TEMPLATE.format(host=self._host, port=self._port, url=self.STREAM_TEMPLATE)
        resp = await self._timeout(self._session.get(url))

        while True:
            response_str = ''
            while '\n\n\n' not in response_str:
                response_bytes = await self._timeout(resp.content.read(1024 * 10))
                response_str += response_bytes.decode('utf-8')
            try:
                data = json.loads(response_str.strip())
            except JSONDecodeError:
                data = None
            if data:
                call_back(data)

    def close(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._session.close())
