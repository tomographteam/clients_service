class OxygenMonitorState:
    def __init__(self):
        self.enabled = True
        self.oxygen: int = 0

    def __repr__(self):
        return f'oxygen={self.oxygen}'

    @classmethod
    def from_dict(cls, data: dict) -> 'OxygenMonitorState':
        obj = OxygenMonitorState()
        obj.oxygen = data['oxygen']
        obj.enabled = data['enabled']
        return obj

    def to_dict(self) -> dict:
        data = {
            'oxygen': self.oxygen,
            'enabled': self.enabled,
        }
        return data
