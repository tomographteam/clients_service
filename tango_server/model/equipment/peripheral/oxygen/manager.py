import asyncio
from asyncio import sleep

from aiohttp import ClientConnectorError

from tango_server.model.control.event.event import Event, EventType
from .client import OxygenMonitorClient
from ...abstract_equipment_subsystem_manager import AbstractEquipmentSubsystemManager

try:
    from src.model_.control.tomograph_manager import TomographManager
except ImportError:
    pass


class OxygenMonitorManager(AbstractEquipmentSubsystemManager):
    def __init__(self, tomograph_manager: 'TomographManager'):
        super().__init__(tomograph_manager)
        self._client: OxygenMonitorClient = OxygenMonitorClient()

    async def _check_state(self):
        if self._presenter is None:
            return

        assert isinstance(self._client, OxygenMonitorClient)
        state = await self._client.state()
        # self._logger.debug(f'Oxygen state update: {state}')
        event = Event(EventType.oxygen_monitor_data, state)
        self._presenter.handle_model_event(event)

    async def _work(self):
        while True:
            try:
                await self._check_state()
                await sleep(1)
                continue
            except asyncio.TimeoutError:
                pass
            except ClientConnectorError as e:
                # self._logger.info(e, exc_info=True)
                pass
            await asyncio.sleep(self._io_error_sleep)
