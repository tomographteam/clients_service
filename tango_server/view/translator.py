import logging
from enum import Enum
from typing import Optional, Callable, Any, List

from PyQt5.QtCore import QObject, pyqtSignal

from tango_server.model.data.data_enum import DataEnumList, DataEnumMember

try:
    from tango_server.model.data.settings_manager import Settings
except ImportError:
    pass


class Language(DataEnumMember):
    pass


class LanguagesTypes(DataEnumList):
    rus = Language('ru')
    en = Language('en')

    @classmethod
    def get_list(cls) -> List[Language]:
        return [cls.en, cls.rus]


class _Tr(property):

    def __init__(self, f_get: Optional[Callable[[Any], Any]] = ..., *args, **kwargs) -> None:
        self._f_get = f_get
        super().__init__(self._getter, *args, **kwargs)

    def _getter(self, translator: 'Translator') -> Callable[[], str]:
        return lambda: self._f_get(translator).get(translator.lang.name)


class Translator(QObject):
    need_update_language = pyqtSignal()
    _logger = logging.getLogger('user')
    __instance = None

    @staticmethod
    def inst():
        if Translator.__instance is None:
            Translator.__instance = Translator()
        return Translator.__instance

    def __init__(self):
        super().__init__()
        self.lang: Language = LanguagesTypes.en

        self._date_strftime = {'en': '%m/%d/%Y', 'ru': '%d.%m.%Y'}
        self._bool_success = {'en': 'Success', 'ru': 'Успешно'}
        self._bool_failed = {'en': 'Failed', 'ru': 'Ошибка'}
        self._male = {'en': 'male', 'ru': 'мужской'}
        self._female = {'en': 'female', 'ru': 'женский'}

    def set_language(self, language: Language):
        self._logger.info(f'set language {language}')
        self.lang = language
        self.need_update_language.emit()

    def get_language(self):
        return self.lang

    def tr_date(self, d):
        return d.strftime(self._date_strftime[self.lang.name])

    def tr_bool_success_failed(self, b):
        if b:
            return self._bool_success[self.lang.name]
        else:
            return self._bool_failed[self.lang.name]

    def tr_sex(self, sex):
        if sex in [True, 'male', 'Male']:
            return self._male[self.lang.name]
        else:
            return self._female[self.lang.name]

    def tr_float(self, f):
        if self.lang == 'en':
            return str(f)
        else:
            return str(f).replace('.', ',')

    @_Tr
    def tr_3d_prepare(self):
        return {'en': 'Setup.', 'ru': 'Подготовка.'}

    @_Tr
    def tr_3d_scan_step(self):
        return {'en': 'Scanning.', 'ru': 'Сканирование.'}

    @_Tr
    def tr_abdominal(self):
        return {'en': 'Abdominal', 'ru': 'Брюшная полость'}

    @_Tr
    def tr_access(self):
        return {'en': 'Access', 'ru': 'Режим'}

    @_Tr
    def tr_actions(self):
        return {'en': 'Actions', 'ru': 'Действия'}

    @_Tr
    def tr_actual(self):
        return {'en': 'Actual', 'ru': 'Факт'}

    @_Tr
    def tr_add(self):
        return {'en': 'Add', 'ru': 'Добавить'}

    @_Tr
    def tr_add_field(self):
        return {'en': 'Add field', 'ru': 'Добавить поле'}

    @_Tr
    def tr_add_operator(self):
        return {'en': 'Add operator', 'ru': 'Добавить оператора'}

    @_Tr
    def tr_add_patient_tab(self):
        return {'en': 'Add patient', 'ru': 'Новый пациент'}

    @_Tr
    def tr_address(self):
        return {'en': 'Address', 'ru': 'Адрес'}

    @_Tr
    def tr_adult(self):
        return {'en': 'Adult', 'ru': 'Взрослый'}

    @_Tr
    def tr_alarm(self):
        return {'en': 'Alarm', 'ru': 'Тревога'}

    @_Tr
    def tr_are_you_sure(self):
        return {'en': 'Are you sure?', 'ru': 'Вы уверены?'}

    @_Tr
    def tr_area_kind(self):
        return {'en': 'Protocol kind', 'ru': 'Тип протокола'}

    @_Tr
    def tr_author(self):
        return {'en': 'Author', 'ru': 'Автор'}

    @_Tr
    def tr_auto_calculation(self):
        return {'en': 'Auto calculation', 'ru': 'Автовычисление'}

    @_Tr
    def tr_auto_start(self):
        return {'en': 'Auto start', 'ru': 'Авто старт'}

    @_Tr
    def tr_auto_start_2_rows(self):
        return {'en': 'Auto\nstart', 'ru': 'Авто\nстарт'}

    @_Tr
    def tr_axial_distance(self):
        return {'en': 'Axial distance, mm', 'ru': 'Осевое р-ние, мм'}

    @_Tr
    def tr_backward(self):
        return {'en': 'Backward', 'ru': 'Назад'}

    @_Tr
    def tr_battery_charge_need(self):
        return {'en': 'Battery charge: {current} V, need {need}.',
                'ru': 'Заряд батареи: {current} В, требуется {need}.'}

    @_Tr
    def tr_bce(self):
        return {'en': 'BCE', 'ru': 'BCE'}

    @_Tr
    def tr_beam_adjustment(self):
        return {'en': 'Beam adjustment', 'ru': 'Регулировка луча'}

    @_Tr
    def tr_body_area_step_instruction_1(self):
        return {'en': 'Step {step_number}. Validation of input data. Correct the input if necessary.',
                'ru': 'Шаг {step_number}. Проверка входных данных. При необходимости, исправьте входные данные.'}

    @_Tr
    def tr_bool_failed(self):
        return {'en': 'Failed', 'ru': 'Ошибка'}

    @_Tr
    def tr_bool_success(self):
        return {'en': 'Success', 'ru': 'Успешно'}

    @_Tr
    def tr_bp_monitor(self):
        return {'en': 'Blood pressure monitor', 'ru': 'Монитор кровяного давления'}

    @_Tr
    def tr_bp_title(self):
        return {'en': 'Blood pressure:', 'ru': 'Кровяное давление:'}

    @_Tr
    def tr_brain(self):
        return {'en': 'Brain', 'ru': 'Мозг'}

    @_Tr
    def tr_breathe(self):
        return {'en': 'Breathe', 'ru': 'Дышите'}

    @_Tr
    def tr_breathe_monitor(self):
        return {'en': 'Breathe monitor', 'ru': 'Монитор дыхания'}

    @_Tr
    def tr_breathe_title(self):
        return {'en': 'Breathe:', 'ru': 'Дыхание:'}

    @_Tr
    def tr_calibration(self):
        return {'en': 'Calibration', 'ru': 'Калибровка'}

    @_Tr
    def tr_calibration_by_air(self):
        return {'en': 'Calibration\nby air', 'ru': 'Калибровка\nпо воздуху'}

    @_Tr
    def tr_calibration_finished(self):
        return {'en': 'Calibration finished', 'ru': 'Калибровка завершена'}

    @_Tr
    def tr_calibration_in_progress(self):
        return {'en': 'Calibration in progress', 'ru': 'Калибровка в процессе'}

    @_Tr
    def tr_camera(self):
        return {'en': 'Camera', 'ru': 'Камера'}

    @_Tr
    def tr_cancel(self):
        return {'en': 'Cancel', 'ru': 'Отмена'}

    @_Tr
    def tr_cardio_options(self):
        return {'en': 'Cardio options', 'ru': 'Параметры кардио'}

    @_Tr
    def tr_cardio_start(self):
        return {'en': 'ECG Triggered', 'ru': 'Старт с ЭКГ'}

    @_Tr
    def tr_cardio_start_frames(self):
        return {'en': 'Frames', 'ru': 'Кадров'}

    @_Tr
    def tr_cardio_start_frames_short(self):
        return {'en': 'Frames', 'ru': 'Кадр.'}

    @_Tr
    def tr_cardio_start_header(self):
        return {'en': 'ECG', 'ru': 'ЭКГ'}

    @_Tr
    def tr_cardio_start_instruction_1(self):
        return {'en': 'Step {step_number}. ECG Triggered. ', 'ru': 'Шаг {step_number}. Запуск с ЭКГ. '}

    @_Tr
    def tr_cardio_start_offset(self):
        return {'en': 'Offset', 'ru': 'Смещение'}

    @_Tr
    def tr_cardio_start_offset_short(self):
        return {'en': 'Offset', 'ru': 'Смещ.'}

    @_Tr
    def tr_cardio_start_scans(self):
        return {'en': 'Scans', 'ru': 'Сканирований'}

    @_Tr
    def tr_cardio_start_scans_short(self):
        return {'en': 'Scans', 'ru': 'Скан-ний'}

    @_Tr
    def tr_checking_report(self):
        return {'en': 'Checking report', 'ru': 'Отчёт о проверке'}

    @_Tr
    def tr_chest(self):
        return {'en': 'Chest', 'ru': 'Грудной отдел'}

    @_Tr
    def tr_child(self):
        return {'en': 'Child', 'ru': 'Ребёнок'}

    @_Tr
    def tr_chiller(self):
        return {'en': 'Chiller (cooling)', 'ru': 'Chiller (cooling)'}

    @_Tr
    def tr_close(self):
        return {'en': 'Close', 'ru': 'Закрыть'}

    @_Tr
    def tr_close_report(self):
        return {'en': 'Close report', 'ru': 'Закрыть'}

    @_Tr
    def tr_collimator(self):
        return {'en': 'Collimator', 'ru': 'Коллиматор'}

    @_Tr
    def tr_collimator_motor_1(self):
        return {'en': 'Collimator motor 1', 'ru': 'Мотор коллиматора 1'}

    @_Tr
    def tr_collimator_motor_2(self):
        return {'en': 'Collimator motor 2', 'ru': 'Мотор коллиматора 2'}

    @_Tr
    def tr_collimator_motor_3(self):
        return {'en': 'Collimator motor 3', 'ru': 'Мотор коллиматора 3'}

    @_Tr
    def tr_na(self):
        return {'en': 'N/A', 'ru': 'Н/П'}

    @_Tr
    def tr_collimator_width(self):
        return {'en': 'Collimator width, mm', 'ru': 'Ш. коллиматора, мм'}

    @_Tr
    def tr_collimator_width_short(self):
        return {'en': 'Collimator width', 'ru': 'Ш. коллиматора'}

    @_Tr
    def tr_collimator_width_short_min(self):
        return {'en': 'Collimator width min', 'ru': 'Ш. коллиматора мин'}

    @_Tr
    def tr_collimator_width_short_max(self):
        return {'en': 'Collimator width max', 'ru': 'Ш. коллиматора макс'}

    @_Tr
    def tr_collimator_width_attribute(self):
        return {'en': 'Collimator\nWidth', 'ru': 'Ширина\nКоллиматора'}

    @_Tr
    def tr_set_collimator_width(self):
        return {'en': 'Set width', 'ru': 'Установить ширину'}

    @_Tr
    def tr_collimator_width_current(self):
        return {'en': 'Current\nwidth', 'ru': 'Текущая\nширина'}

    @_Tr
    def tr_collimator_control(self):
        return {'en': 'Collimator Control', 'ru': 'Управление Коллиматором'}

    @_Tr
    def tr_collimator_position(self):
        return {'en': 'Collimator Position', 'ru': 'Положение Коллиматора'}

    @_Tr
    def tr_collimator_moving(self):
        return {'en': 'Collimator Moving', 'ru': 'Движение Коллиматора'}

    @_Tr
    def tr_confirm(self):
        return {'en': 'Confirm', 'ru': 'Подтвердить'}

    @_Tr
    def tr_connection_lost(self):
        return {'en': 'Lost connection with tomograph', 'ru': 'Потеряно соединение с томографом'}

    @_Tr
    def tr_connection_status_no(self):
        return {'en': 'Connection: NO', 'ru': 'Связь: НЕТ'}

    @_Tr
    def tr_connection_status_yes(self):
        return {'en': 'Connection: Yes', 'ru': 'Связь: Есть'}

    @_Tr
    def tr_continue(self):
        return {'en': 'Continue', 'ru': 'Продолжить'}

    @_Tr
    def tr_continue_work(self):
        return {'en': 'Continue work', 'ru': 'Продолжить работу'}

    @_Tr
    def tr_contrast_density(self):
        return {'en': 'Contrast density', 'ru': 'Плотность контраста'}

    @_Tr
    def tr_contrast_flow_speed(self):
        return {'en': 'Contrast flow', 'ru': 'Поток контраста'}

    @_Tr
    def tr_contrast_injector_monitor(self):
        return {'en': 'Contrast injector monitor', 'ru': 'Монитор инджектора контраста'}

    @_Tr
    def tr_contrast_injector(self):
        return {'en': 'Contrast injector', 'ru': 'Инжектор контраста'}

    @_Tr
    def tr_contrast_pressure(self):
        return {'en': 'Pressure', 'ru': 'Давление'}

    @_Tr
    def tr_contrast_type(self):
        return {'en': 'Contrast type', 'ru': 'Тип контраста'}

    @_Tr
    def tr_no_contrast(self):
        return {'en': 'No contrast', 'ru': 'Без контраста'}

    @_Tr
    def tr_contrast_1(self):
        return {'en': 'Contrast 1', 'ru': 'Контраст 1'}

    @_Tr
    def tr_contrast_volume(self):
        return {'en': 'Contrast volume, ml', 'ru': 'Объем контраста, мл'}

    @_Tr
    def tr_contrast_volume_short(self):
        return {'en': 'ml', 'ru': 'мл'}

    @_Tr
    def tr_control_panel(self):
        return {'en': 'Control panel', 'ru': 'Панель управления'}

    @_Tr
    def tr_create_protocol(self):
        return {'en': 'Create protocol', 'ru': 'Создать протокол'}

    @_Tr
    def tr_create_protocol_step(self):
        return {'en': 'Create step', 'ru': 'Создать шаг'}

    @_Tr
    def tr_current_position(self):
        return {'en': 'Current position', 'ru': 'Текущая позиция'}

    @_Tr
    def tr_current_scan_dose(self):
        return {'en': 'Dose for the current scan:', 'ru': 'Доза облучения текущего сканирования:'}

    @_Tr
    def tr_das(self):
        return {'en': 'DAS', 'ru': 'DAS'}

    @_Tr
    def tr_das_1(self):
        return {'en': 'DAS 1', 'ru': 'DAS 2'}

    @_Tr
    def tr_das_2(self):
        return {'en': 'DAS 2', 'ru': 'DAS 2'}

    @_Tr
    def tr_recon(self):
        return {'en': 'Recon', 'ru': 'Recon'}

    @_Tr
    def tr_electrodes(self):
        return {'en': 'Electrodes', 'ru': 'Electrodes'}

    @_Tr
    def tr_data_verification(self):
        return {'en': 'Data Verification', 'ru': 'Проверка данных'}

    @_Tr
    def tr_date_of_birth(self):
        return {'en': 'Date of birth', 'ru': 'Дата рождения'}

    @_Tr
    def tr_year(self):
        return {'en': 'Year', 'ru': 'Год'}

    @_Tr
    def tr_month(self):
        return {'en': 'Month', 'ru': 'Месяц'}

    @_Tr
    def tr_date_placeholder(self):
        return {'en': 'dd.mm.yyyy', 'ru': 'дд.мм.гггг'}

    @_Tr
    def tr_date_strftime(self):
        return {'en': '%m/%d/%Y', 'ru': '%d.%m.%Y'}

    @_Tr
    def tr_delete(self):
        return {'en': 'Delete', 'ru': 'Удалить'}

    @_Tr
    def tr_delete_exactly_q(self):
        return {'en': 'Delete exactly?', 'ru': 'Точно удалить?'}

    @_Tr
    def tr_department(self):
        return {'en': 'Department', 'ru': 'Отделение'}

    @_Tr
    def tr_description(self):
        return {'en': 'Description', 'ru': 'Характеристики'}

    @_Tr
    def tr_dicom_file_name(self):
        return {'en': 'DICOM file', 'ru': 'DICOM файл'}

    @_Tr
    def tr_direction_out(self):
        return {'en': 'Direction out', 'ru': 'Направление: наружу'}

    @_Tr
    def tr_disable(self):
        return {'en': 'Disable', 'ru': 'Выключен'}

    @_Tr
    def tr_do_you_want_to_reboot(self):
        return {'en': 'Do you want to reboot the computer?', 'ru': 'Вы хотите перезагрузить компьютер?'}

    @_Tr
    def tr_do_you_want_to_shutdown(self):
        return {'en': 'Do you want to shutdown the computer?', 'ru': 'Вы хотите выключить компьютер?'}

    @_Tr
    def tr_done(self):
        return {'en': 'Done', 'ru': 'Вып.'}

    @_Tr
    def tr_dont_breathe(self):
        return {'en': "Don't breathe", 'ru': 'Не дышите'}

    @_Tr
    def tr_dose(self):
        return {'en': 'Dose, mAs', 'ru': 'Доза, мАс'}

    @_Tr
    def tr_dose_excess(self):
        return {'en': 'Dose excess', 'ru': 'Превышение дозы'}

    @_Tr
    def tr_dose_gray(self):
        return {'en': 'Gy', 'ru': 'Гр'}

    @_Tr
    def tr_dose_evaluation(self):
        return {'en': 'Evaluation', 'ru': 'Оценка'}

    @_Tr
    def tr_dose_this_scan(self):
        return {'en': 'This scan', 'ru': 'Это сканирование'}

    @_Tr
    def tr_dose_all_scans(self):
        return {'en': 'All scans', 'ru': 'Все сканирования'}

    @_Tr
    def tr_scans_count(self):
        return {'en': 'Scans count', 'ru': 'Кол-во\nсканирований'}

    @_Tr
    def tr_dose_gray_sum(self):
        return {'en': 'Gy Sum', 'ru': 'Гр Сум'}

    @_Tr
    def tr_dose_gray_avg(self):
        return {'en': 'Gy Avg', 'ru': 'Гр Сред'}

    @_Tr
    def tr_dose_header(self):
        return {'en': 'Dose', 'ru': 'Доза'}

    @_Tr
    def tr_dose_journal_state(self):
        return {'en': 'Dose logs', 'ru': 'Журнал дозы'}

    @_Tr
    def tr_dose_journal_by_patient(self):
        return {'en': 'Total by patient', 'ru': 'Всего по пациентам'}

    @_Tr
    def tr_dose_report(self):
        return {'en': 'Report', 'ru': 'Отчёт'}

    @_Tr
    def tr_dose_max(self):
        return {'en': 'Dose max, mAs', 'ru': 'Доза макс, мАс'}

    @_Tr
    def tr_dose_sievert(self):
        return {'en': 'Sv*cm', 'ru': 'Зв*см'}

    @_Tr
    def tr_dose_sievert_sum(self):
        return {'en': 'Sv*cm Sum', 'ru': 'Зв*см Сум'}

    @_Tr
    def tr_dose_sievert_avg(self):
        return {'en': 'Sv*cm Avg', 'ru': 'Зв*см Сред'}

    @_Tr
    def tr_down(self):
        return {'en': 'Down', 'ru': 'Вниз'}

    @_Tr
    def tr_dsebct(self):
        return {'en': 'DSEBCT', 'ru': 'DSEBCT'}

    @_Tr
    def tr_scanner(self):
        return {'en': 'SCANNER', 'ru': 'SCANNER'}

    @_Tr
    def tr_ecg(self):
        return {'en': 'ECG', 'ru': 'ЭКГ'}

    @_Tr
    def tr_ecg_chugger(self):
        return {'en': 'ECG chugger', 'ru': 'ECG chugger'}

    @_Tr
    def tr_ecg_settings(self):
        return {'en': 'ECG settings', 'ru': 'Настройки ЭКГ'}

    @_Tr
    def tr_ecg_title(self):
        return {'en': 'ECG:', 'ru': 'ЭКГ:'}

    @_Tr
    def tr_emergency_patient(self):
        return {'en': 'Emergency', 'ru': 'Экстренный'}

    @_Tr
    def tr_emergency_scanning(self):
        return {'en': 'Emergency\nscan', 'ru': 'Экстренное\nсканирование'}

    @_Tr
    def tr_enable(self):
        return {'en': 'Enable', 'ru': 'Включен'}

    @_Tr
    def tr_end_voice_command(self):
        return {'en': 'End voice cmd', 'ru': 'Конец голос зап.'}

    @_Tr
    def tr_english(self):
        return {'en': 'English', 'ru': 'English'}

    @_Tr
    def tr_enter_param_name(self):
        return {'en': 'Enter parameter name', 'ru': 'Введите имя параметра'}

    @_Tr
    def tr_error(self):
        return {'en': 'Error', 'ru': 'Ошибка'}

    @_Tr
    def tr_esophagus(self):
        return {'en': 'Esophagus', 'ru': 'Пищевод'}

    @_Tr
    def tr_exit(self):
        return {'en': 'Exit', 'ru': 'Выход'}

    @_Tr
    def tr_expert(self):
        return {'en': 'Expert', 'ru': 'Экспертный'}

    @_Tr
    def tr_expert_password_message(self):
        return {'en': 'Enter the password to get the expert access',
                'ru': 'Введите пароль для входа в экспертный режим'}

    @_Tr
    def tr_family(self):
        return {'en': 'Second Name', 'ru': 'Фамилия'}

    @_Tr
    def tr_feet_to_gentry_on_back_arms_to_gentry(self):
        return {'en': 'Feet to the gentry Lying on the back hands to the gentry',
                'ru': 'Ногами к гентри Лежа на спине руки к гентри'}

    @_Tr
    def tr_feet_to_gentry_on_left_side_arms_under_head(self):
        return {'en': 'Feet to the gentry Lying on the left side of the hand under the head',
                'ru': 'Ногами к гентри Лежа на левом боку руки под голову'}

    @_Tr
    def tr_feet_to_gentry_on_right_side_arms_under_head(self):
        return {'en': 'Feet to the gentry Lying on the right side of the hand under the head',
                'ru': 'Ногами к гентри Лежа на правом боку руки под голову'}

    @_Tr
    def tr_feet_to_gentry_on_stomach_arms_under_head(self):
        return {'en': 'Feet to the gentry Lying on his stomach hands under his head',
                'ru': 'Ногами к гентри Лежа на животе руки под голову'}

    @_Tr
    def tr_female(self):
        return {'en': 'female', 'ru': 'женский'}

    @_Tr
    def tr_field_empty(self):
        return {'en': 'Field empty:', 'ru': 'Поле не заполнено:'}

    @_Tr
    def tr_finish(self):
        return {'en': 'Finish', 'ru': 'Готово'}

    @_Tr
    def tr_fluid_pressure(self):
        return {'en': 'Fluid pressure, kPa', 'ru': 'Давление жидкости, кПа'}

    @_Tr
    def tr_forward(self):
        return {'en': 'Forward', 'ru': 'Вперёд'}

    @_Tr
    def tr_front_camera(self):
        return {'en': 'Front camera', 'ru': 'Фронтальная камера'}

    @_Tr
    def tr_full_name(self):
        return {'en': 'Full name', 'ru': 'Ф.И.О.'}

    @_Tr
    def tr_gender(self):
        return {'en': 'Gender', 'ru': 'Пол'}

    @_Tr
    def tr_general_prepare_step_instruction_1(self):
        return {
            'en': 'Step {step_number}. 3D scan. Training. Check the scan area, scan settings and reconstruction parameters.\nClick the "Send to Hardware" button, then the "Next>" button.',
            'ru': 'Шаг {step_number}. 3D сканирование. Подготовка. Проверьте область и параметры\nсканирования и реконструкции. Нажмите кнопку ""Отправить оборудованию"", затем кнопку "Далее >".'}

    @_Tr
    def tr_general_scan(self):
        return {'en': '3D', 'ru': '3D'}

    @_Tr
    def tr_cardiac_scan(self):
        return {'en': 'Cardiac', 'ru': 'Cardiac'}

    @_Tr
    def tr_radiology_scan(self):
        return {'en': 'Radiology', 'ru': 'Radiology'}

    @_Tr
    def tr_hi_res_scan(self):
        return {'en': 'hiRes', 'ru': 'hiRes'}

    @_Tr
    def tr_flow_scan(self):
        return {'en': 'Flow', 'ru': 'Flow'}

    @_Tr
    def tr_cine_scan(self):
        return {'en': 'Cine', 'ru': 'Cine'}

    @_Tr
    def tr_dark_calib_scan(self):
        return {'en': 'darkCalib', 'ru': 'darkCalib'}

    @_Tr
    def tr_air_calib_scan(self):
        return {'en': 'airCalib', 'ru': 'airCalib'}

    @_Tr
    def tr_multi_pin_calib_scan(self):
        return {'en': 'multiPinCalib', 'ru': 'multiPinCalib'}

    @_Tr
    def tr_upper_half_scan(self):
        return {'en': 'upperHalf', 'ru': 'upperHalf'}

    @_Tr
    def tr_bad_tile_detection_scan(self):
        return {'en': 'badDexelDetection', 'ru': 'badDexelDetection'}

    @_Tr
    def tr_water_calib_scan(self):
        return {'en': 'waterCalib', 'ru': 'waterCalib'}

    @_Tr
    def tr_preprocess_only_scan(self):
        return {'en': 'preprocessOnly', 'ru': 'preprocessOnly'}

    @_Tr
    def tr_xray_tuning_scan(self):
        return {'en': 'xrayTuning', 'ru': 'xrayTuning'}

    @_Tr
    def tr_null_processing_scan(self):
        return {'en': 'nullProcessing', 'ru': 'nullProcessing'}

    @_Tr
    def tr_fluoro_scan(self):
        return {'en': 'Fluoro', 'ru': 'Fluoro'}

    @_Tr
    def tr_general_scan_step_instruction_1(self):
        return {'en': 'Step {step_number}. 3D scan. To start scanning, press "Start scan"',
                'ru': 'Шаг {step_number}. 3D сканирование. Для запуска нажмите "Запуск сканирования" и \nподтвердите нажатием красной кнопки на столе оператора.'}

    @_Tr
    def tr_go_back(self):
        return {'en': '< Back', 'ru': '< Назад'}

    @_Tr
    def tr_go_forward(self):
        return {'en': 'Next >', 'ru': 'Вперёд >'}

    @_Tr
    def tr_hardware_test(self):
        return {'en': 'Hardware test', 'ru': 'Проверка оборудования'}

    @_Tr
    def tr_hardware_test_in_progress(self):
        return {'en': 'Hardware test in progress..', 'ru': 'Проверка оборудования запущена..'}

    @_Tr
    def tr_has_intersections(self):
        return {'en': 'Intersections!', 'ru': 'Пересечения!'}

    @_Tr
    def tr_head(self):
        return {'en': 'Head', 'ru': 'Голова'}

    @_Tr
    def tr_head_to_gentry_on_back_arms_to_gentry(self):
        return {'en': 'Head to gentry Lying on your back hands to the gentry',
                'ru': 'Головой к гентри Лежа на спине руки к гентри'}

    @_Tr
    def tr_head_to_gentry_on_back_arms_to_legs(self):
        return {'en': 'Feet to the gentry Lying on the back hands to the gentry',
                'ru': 'Головой к гентри Лежа на спине руки к ногам'}

    @_Tr
    def tr_head_to_gentry_on_left_side_arms_under_head(self):
        return {'en': 'Head to gentry Lying on the left side of the arm under the head',
                'ru': 'Головой к гентри Лежа на левом боку руки под голову'}

    @_Tr
    def tr_head_to_gentry_on_right_side_arms_under_head(self):
        return {'en': 'Head to gentry Lying on the right side of the arm under the head',
                'ru': 'Головой к гентри Лежа на правом боку руки под голову'}

    @_Tr
    def tr_head_to_gentry_on_stomach_arms_to_gentry(self):
        return {'en': 'Head to the gentry Lying on his stomach hands to the gentry',
                'ru': 'Головой к гентри Лежа на животе руки к гентри'}

    @_Tr
    def tr_head_to_gentry_on_stomach_arms_under_head(self):
        return {'en': 'Head to gentry Lying on his stomach hands under his head ',
                'ru': 'Головой к гентри Лежа на животе руки под голову'}

    @_Tr
    def tr_heart(self):
        return {'en': 'Heart', 'ru': 'Сердце'}

    @_Tr
    def tr_heart_monitor(self):
        return {'en': 'Heart monitor', 'ru': 'Монитор сердца'}

    @_Tr
    def tr_height_mm(self):
        return {'en': 'Height, mm', 'ru': 'Высота, мм'}

    @_Tr
    def tr_height_sm(self):
        return {'en': 'Height, sm', 'ru': 'Рост, см'}

    @_Tr
    def tr_help(self):
        return {'en': 'Help', 'ru': 'Справка'}

    @_Tr
    def tr_help_about(self):
        return {'en': 'About', 'ru': 'О программе'}

    @_Tr
    def tr_about_version(self):
        return {'en': 'Version: {}', 'ru': 'Версия: {}'}

    @_Tr
    def tr_hide_top_panel(self):
        return {'en': 'Hide top panel', 'ru': 'Скрыть верхнюю панель'}

    @_Tr
    def tr_hvps(self):
        return {'en': 'HVPS', 'ru': 'HVPS'}

    @_Tr
    def tr_HVPS_Status(self):
        return {'en': 'HVPS Status', 'ru': 'HVPS Status'}

    @_Tr
    def tr_PES_Status(self):
        return {'en': 'PES Status', 'ru': 'PES Status'}

    @_Tr
    def tr_DC_Bus_Voltage(self):
        return {'en': 'DC Bus Voltage', 'ru': 'DC Bus Voltage'}

    @_Tr
    def tr_Filament_1_Current(self):
        return {'en': 'Filament 1 Current', 'ru': 'Filament 1 Current'}

    @_Tr
    def tr_Filament_1_Voltage(self):
        return {'en': 'Filament 1 Voltage', 'ru': 'Filament 1 Voltage'}

    @_Tr
    def tr_MA_1_Set(self):
        return {'en': 'MA 1 Set', 'ru': 'MA 1 Set'}

    @_Tr
    def tr_Filament_2_Current(self):
        return {'en': 'Filament 2 Current', 'ru': 'Filament 2 Current'}

    @_Tr
    def tr_Filament_2_Voltage(self):
        return {'en': 'Filament 2 Voltage', 'ru': 'Filament 2 Voltage'}

    @_Tr
    def tr_MA_2_Set(self):
        return {'en': 'MA 2 Set', 'ru': 'MA 2 Set'}

    @_Tr
    def tr_kVp(self):
        return {'en': 'kVp', 'ru': 'kVp'}

    @_Tr
    def tr_Chamber_Pressure(self):
        return {'en': 'Chamber Pressure', 'ru': 'Chamber Pressure'}

    @_Tr
    def tr_Source_1_Pressure(self):
        return {'en': 'Source 1 Pressure', 'ru': 'Source 1 Pressure'}

    @_Tr
    def tr_Source_2_Pressure(self):
        return {'en': 'Source 2 Pressure', 'ru': 'Source 2 Pressure'}

    @_Tr
    def tr_NEG_1_Temperature(self):
        return {'en': 'NEG 1 Temperature', 'ru': 'NEG 1 Temperature'}

    @_Tr
    def tr_NEG_2_Temperature(self):
        return {'en': 'NEG 2 Temperature', 'ru': 'NEG 2 Temperature'}

    @_Tr
    def tr_NEG_3_Temperature(self):
        return {'en': 'NEG 3 Temperature', 'ru': 'NEG 3 Temperature'}

    @_Tr
    def tr_Set_Temperature(self):
        return {'en': 'Set Temperature', 'ru': 'Set Temperature'}

    @_Tr
    def tr_DAS_1_Status(self):
        return {'en': 'DAS 1 Status', 'ru': 'DAS 1 Status'}

    @_Tr
    def tr_DAS_1_Number_of_boards(self):
        return {'en': 'DAS 1 Number of boards', 'ru': 'DAS 1 Number of boards'}

    @_Tr
    def tr_DAS_2_Status(self):
        return {'en': 'DAS 2 Status', 'ru': 'DAS 2 Status'}

    @_Tr
    def tr_DAS_2_Number_of_boards(self):
        return {'en': 'DAS 2 Number of boards', 'ru': 'DAS 2 Number of boards'}

    @_Tr
    def tr_Scan_Control(self):
        return {'en': 'Scan Control', 'ru': 'Scan Control'}

    @_Tr
    def tr_BCE_State(self):
        return {'en': 'BCE State', 'ru': 'BCE State'}

    @_Tr
    def tr_Source1_ICE1_Voltage(self):
        return {'en': 'Source1 ICE1 Voltage', 'ru': 'Source1 ICE1 Voltage'}

    @_Tr
    def tr_Source1_ICE2_Voltage(self):
        return {'en': 'Source1 ICE2 Voltage', 'ru': 'Source1 ICE2 Voltage'}

    @_Tr
    def tr_Source1_IRE_Voltage(self):
        return {'en': 'Source1 IRE Voltage', 'ru': 'Source1 IRE Voltage'}

    @_Tr
    def tr_Source2_ICE1_Voltage(self):
        return {'en': 'Source2 ICE1 Voltage', 'ru': 'Source2 ICE1 Voltage'}

    @_Tr
    def tr_Source2_ICE2_Voltage(self):
        return {'en': 'Source2 ICE2 Voltage', 'ru': 'Source2 ICE2 Voltage'}

    @_Tr
    def tr_Source2_IRE_Voltage(self):
        return {'en': 'Source2 IRE Voltage', 'ru': 'Source2 IRE Voltage'}

    @_Tr
    def tr_Horizontal_Speed(self):
        return {'en': 'Horizontal Speed', 'ru': 'Horizontal Speed'}

    @_Tr
    def tr_Horizontal_Position(self):
        return {'en': 'Horizontal Position', 'ru': 'Горизонтальная позиция'}

    @_Tr
    def tr_Vertical_Position(self):
        return {'en': 'Vertical Position', 'ru': 'Вертикальная позиция'}

    @_Tr
    def tr_Cradle_state(self):
        return {'en': 'Cradle state', 'ru': 'Cradle state'}

    @_Tr
    def tr_Elevation_State(self):
        return {'en': 'Elevation State', 'ru': 'Elevation State'}

    @_Tr
    def tr_Feed_Pressure(self):
        return {'en': 'Feed Pressure', 'ru': 'Feed Pressure'}

    @_Tr
    def tr_Valve_status(self):
        return {'en': 'Valve status', 'ru': 'Valve status'}

    @_Tr
    def tr_Pack_Voltage(self):
        return {'en': 'Pack Voltage', 'ru': 'Pack Voltage'}

    @_Tr
    def tr_use_hvps(self):
        return {'en': 'Use HVPS', 'ru': 'Используется HVPS'}

    @_Tr
    def tr_use_wwires(self):
        return {'en': 'Use WWIRES', 'ru': 'Используется WWIRES'}

    @_Tr
    def tr_Pack_Current(self):
        return {'en': 'Pack Current', 'ru': 'Pack Current'}

    @_Tr
    def tr_SOC(self):
        return {'en': 'SOC', 'ru': 'SOC'}

    @_Tr
    def tr_Bus_Voltage(self):
        return {'en': 'Bus Voltage', 'ru': 'Bus Voltage'}

    @_Tr
    def tr_Open_Pressure(self):
        return {'en': 'Open Pressure', 'ru': 'Open Pressure'}

    @_Tr
    def tr_Close_Pressure(self):
        return {'en': 'Close Pressure', 'ru': 'Close Pressure'}

    @_Tr
    def tr_hvps_ct(self):
        return {'en': 'HVPS CT', 'ru': 'HVPS CT'}

    @_Tr
    def tr_hvps_nwl(self):
        return {'en': 'HVPS NWL', 'ru': 'HVPS NWL'}

    @_Tr
    def tr_neg_power(self):
        return {'en': 'NEG power', 'ru': 'NEG power'}

    @_Tr
    def tr_neg_power_mini_1(self):
        return {'en': 'NEG power mini 1', 'ru': 'NEG power mini 1'}

    @_Tr
    def tr_neg_power_mini_2(self):
        return {'en': 'NEG power mini 2', 'ru': 'NEG power mini 2'}

    @_Tr
    def tr_icon(self):
        return {'en': 'Icon', 'ru': 'Иконка'}

    @_Tr
    def tr_illustration(self):
        return {'en': 'Illustration', 'ru': 'Иллюстрация'}

    @_Tr
    def tr_image_viewer(self):
        return {'en': 'Image viewer', 'ru': 'Просмотр снимков'}

    @_Tr
    def tr_information(self):
        return {'en': 'Information', 'ru': 'Информация'}

    @_Tr
    def tr_instruction(self):
        return {'en': 'Instruction', 'ru': 'Инструкции пользователя'}

    @_Tr
    def tr_interlock(self):
        return {'en': 'Interlock', 'ru': 'Блокировка'}

    @_Tr
    def tr_intestines(self):
        return {'en': 'Intestines', 'ru': 'Кишечник'}

    @_Tr
    def tr_ion_pump(self):
        return {'en': 'Ion pump (vacuum gauges)', 'ru': 'Ion pump (vacuum gauges)'}

    @_Tr
    def tr_is_auto_start(self):
        return {'en': 'Auto start, s', 'ru': 'Авто запуск, сек'}

    @_Tr
    def tr_kernel(self):
        return {'en': 'Kernel', 'ru': 'Ядро'}

    @_Tr
    def tr_kernel_bones(self):
        return {'en': 'Bones', 'ru': 'Кости'}

    @_Tr
    def tr_kernel_lungs(self):
        return {'en': 'Lungs', 'ru': 'Лёгкие'}

    @_Tr
    def tr_kernel_standard(self):
        return {'en': 'Standard', 'ru': 'Стандартное'}

    @_Tr
    def tr_kidneys(self):
        return {'en': 'Kidneys', 'ru': 'Почки'}

    @_Tr
    def tr_kv(self):
        return {'en': 'kV', 'ru': 'kV'}

    @_Tr
    def tr_language(self):
        return {'en': 'Language', 'ru': 'Язык'}

    @_Tr
    def tr_limbs(self):
        return {'en': 'Limbs', 'ru': 'Конечности'}

    @_Tr
    def tr_liver(self):
        return {'en': 'Liver', 'ru': 'Печень'}

    @_Tr
    def tr_low_battery(self):
        return {'en': 'Low battery!', 'ru': 'Низкий заряд батареи!'}

    @_Tr
    def tr_lungs(self):
        return {'en': 'Lungs', 'ru': 'Легкие'}

    @_Tr
    def tr_phantom_single_pin(self):
        return {'en': 'Single pin', 'ru': 'С одиночным стержнем'}

    @_Tr
    def tr_phantom_multi_pin(self):
        return {'en': 'Multi pin', 'ru': 'Многостержневой'}

    @_Tr
    def tr_phantom_water(self):
        return {'en': 'Water', 'ru': 'Водяной'}

    @_Tr
    def tr_phantom_spectral(self):
        return {'en': 'Spectral', 'ru': 'Спектральный'}

    @_Tr
    def tr_phantom_uniformity_1(self):
        return {'en': 'Uniformity 1', 'ru': 'Однородности 1'}

    @_Tr
    def tr_phantom_uniformity_2(self):
        return {'en': 'Uniformity 2', 'ru': 'Однородности 2'}

    @_Tr
    def tr_phantom_cone(self):
        return {'en': 'Cone', 'ru': 'Конусный'}

    @_Tr
    def tr_phantom_patient_scatter(self):
        return {'en': 'Patient Scatter', 'ru': 'Рассеивание пациентом'}

    @_Tr
    def tr_phantom_machine_scatter(self):
        return {'en': 'Patient Machine', 'ru': 'Рассеивание корпусом'}

    @_Tr
    def tr_phantom_patient_resolution(self):
        return {'en': 'Resolution', 'ru': 'Разрешения'}

    @_Tr
    def tr_phantom_torso(self):
        return {'en': 'Torso', 'ru': 'Тела'}

    @_Tr
    def tr_phantom_head(self):
        return {'en': 'Head', 'ru': 'Головы'}

    @_Tr
    def tr_ma(self):
        return {'en': 'Beam current, mA', 'ru': 'Ток пучка, мА'}

    @_Tr
    def tr_fluoro_number_of_views(self):
        return {'en': 'Number of views', 'ru': 'Число снимков'}

    @_Tr
    def tr_ma_short(self):
        return {'en': 'mA', 'ru': 'мА'}

    @_Tr
    def tr_maintenance(self):
        return {'en': 'Maintenance', 'ru': 'Техобслуживание'}

    @_Tr
    def tr_male(self):
        return {'en': 'male', 'ru': 'мужской'}

    @_Tr
    def tr_manual(self):
        return {'en': 'Manual', 'ru': 'В ручную'}

    @_Tr
    def tr_max(self):
        return {'en': 'Max', 'ru': 'Максимум'}

    @_Tr
    def tr_not_selected(self):
        return {'en': '{} not selected', 'ru': '{} не выбран'}

    @_Tr
    def tr_medical_institution(self):
        return {'en': 'Medical institution', 'ru': 'Медучреждение'}

    @_Tr
    def tr_medical_institution_header(self):
        return {'en': 'Medical institution information', 'ru': 'Информация об медучреждении'}

    @_Tr
    def tr_metal_correction(self):
        return {'en': 'Metal correction', 'ru': 'Коррекция металла'}

    @_Tr
    def tr_reconstruction_method(self):
        return {'en': 'Method', 'ru': 'Метод'}

    @_Tr
    def tr_recon_das_mode(self):
        return {'en': 'DAS mode', 'ru': 'DAS mode'}

    @_Tr
    def tr_middle_name(self):
        return {'en': 'Middle name', 'ru': 'Отчество'}

    @_Tr
    def tr_min(self):
        return {'en': 'Min', 'ru': 'Минимум'}

    @_Tr
    def tr_mode(self):
        return {'en': 'Mode', 'ru': 'Режим'}

    @_Tr
    def tr_ms(self):
        return {'en': 'ms', 'ru': 'мс'}

    @_Tr
    def tr_name(self):
        return {'en': 'First name', 'ru': 'Имя'}

    @_Tr
    def tr_name_scan(self):
        return {'en': 'Name scan', 'ru': 'Назовите сканирование'}

    @_Tr
    def tr_neck(self):
        return {'en': 'Neck', 'ru': 'Шея'}

    @_Tr
    def tr_new_patient(self):
        return {'en': 'New patient', 'ru': 'Новый пациент'}

    @_Tr
    def tr_new_scaning(self):
        return {'en': 'New scan', 'ru': 'Новое сканирование'}

    @_Tr
    def tr_new_session(self):
        return {'en': 'New session', 'ru': 'Новый сеанс'}

    @_Tr
    def tr_new_tab(self):
        return {'en': 'New task', 'ru': 'Новая задача'}

    @_Tr
    def tr_next(self):
        return {'en': 'Next', 'ru': 'Далее'}

    @_Tr
    def tr_no(self):
        return {'en': 'No', 'ru': 'Нет'}

    @_Tr
    def tr_no_free_windows(self):
        return {'en': 'Error', 'ru': 'No free windows!'}

    @_Tr
    def tr_noise_reduction(self):
        return {'en': 'Noise reduction', 'ru': 'Подавление шума'}

    @_Tr
    def tr_observation(self):
        return {'en': 'Observation', 'ru': 'Наблюдение'}

    @_Tr
    def tr_ok(self):
        return {'en': 'OK', 'ru': 'OK'}

    @_Tr
    def tr_open(self):
        return {'en': 'Open', 'ru': 'Открыть'}

    @_Tr
    def tr_operator(self):
        return {'en': 'Operator', 'ru': 'Оператор'}

    @_Tr
    def tr_operator_already_exists(self):
        return {'en': 'The operator with the same name already exists.', 'ru': 'Оператор с таким именем уже существует'}

    @_Tr
    def tr_operator_list(self):
        return {'en': 'Operator list', 'ru': 'Список операторов'}

    @_Tr
    def tr_operators(self):
        return {'en': 'Operators', 'ru': 'Операторы'}

    @_Tr
    def tr_out_of_scan_area_error(self):
        return {'en': 'Out of scan area', 'ru': 'Выход за пределы области сканирования'}

    @_Tr
    def tr_overheating_expected(self):
        return {'en': 'Overheating expected!', 'ru': 'Ожидается перегрев!'}

    @_Tr
    def tr_overheating_up_to(self):
        return {'en': 'Current temperature: {current_temperature}, heating to {future_temperature}.',
                'ru': 'Текущая темпиратура: {current_temperature}, нагрев до {future_temperature}.'}

    @_Tr
    def tr_oxygen_monitor(self):
        return {'en': 'Oxygen monitor', 'ru': 'Монитор кислорода'}

    @_Tr
    def tr_pacs(self):
        return {'en': 'PACS', 'ru': 'PACS'}

    @_Tr
    def tr_parameter(self):
        return {'en': 'Parameter', 'ru': 'Параметр'}

    @_Tr
    def tr_passed(self):
        return {'en': 'Passed, mm', 'ru': 'Пройдено, мм,)'}

    @_Tr
    def tr_password(self):
        return {'en': 'Password', 'ru': 'Пароль'}

    @_Tr
    def tr_patient(self):
        return {'en': 'Patient', 'ru': 'Пациент'}

    @_Tr
    def tr_patient_appointment(self):
        return {'en': 'Patient appointment:', 'ru': 'Назначение пациента:'}

    @_Tr
    def tr_patient_data(self):
        return {'en': 'Patient data', 'ru': 'Данные пациента'}

    @_Tr
    def tr_patient_id(self):
        return {'en': 'Patient ID', 'ru': 'Код пациента'}

    @_Tr
    def tr_patient_list(self):
        return {'en': 'Patient list', 'ru': 'Список пациентов'}

    @_Tr
    def tr_patient_position(self):
        return {'en': 'Patient position', 'ru': 'Положение пациента'}

    @_Tr
    def tr_patient_required(self):
        return {'en': 'Patient required', 'ru': 'Требуется пациент'}

    @_Tr
    def tr_patient_select(self):
        return {'en': 'Patient select', 'ru': 'Выбор пациента'}

    @_Tr
    def tr_patient_step_instruction_1(self):
        return {
            'en': 'Step {step_number}. Select a scan session.\nSelect the desired patient from the table, with the desired scan area.',
            'ru': 'Шаг {step_number}. Выбор сеанса сканирования.\nВыберите из таблицы нужного пациента, с необходимой областью сканирования.'}

    @_Tr
    def tr_patient_table(self):
        return {'en': 'Patient table', 'ru': 'Стол пациента'}

    @_Tr
    def tr_patient_table_in_move(self):
        return {'en': 'Patient table moves', 'ru': 'Стол пациента движется'}

    @_Tr
    def tr_pause(self):
        return {'en': 'Pause', 'ru': 'Пауза'}

    @_Tr
    def tr_pelvis(self):
        return {'en': 'Pelvis', 'ru': 'Таз'}

    @_Tr
    def tr_peripherals(self):
        return {'en': 'Peripherals', 'ru': 'Периферия'}

    @_Tr
    def tr_pes(self):
        return {'en': 'PES', 'ru': 'PES'}

    @_Tr
    def tr_pes_alarm(self):
        return {'en': 'Alarm', 'ru': 'Внимание'}

    @_Tr
    def tr_pes_alarm(self):
        return {'en': 'Close', 'ru': 'Закрыт'}

    @_Tr
    def tr_pes_fault(self):
        return {'en': 'Fault', 'ru': 'Неисправность'}

    @_Tr
    def tr_pes_init(self):
        return {'en': 'Initialization', 'ru': 'Инициализация'}

    @_Tr
    def tr_pes_moving(self):
        return {'en': 'Moving', 'ru': 'Переключается'}

    @_Tr
    def tr_pes_on(self):
        return {'en': 'Idle', 'ru': 'Бездействует'}

    @_Tr
    def tr_pes_unknown(self):
        return {'en': 'No connection', 'ru': 'Нет связи с батареей'}

    @_Tr
    def tr_phantom(self):
        return {'en': 'Phantom', 'ru': 'Фантом'}

    @_Tr
    def tr_phantom_list(self):
        return {'en': 'Phantom List', 'ru': 'Фантомы'}

    @_Tr
    def tr_photo(self):
        return {'en': 'Photo', 'ru': 'Фото'}

    @_Tr
    def tr_pitch(self):
        return {'en': 'Pitch', 'ru': 'Питч'}

    @_Tr
    def tr_port(self):
        return {'en': 'Port', 'ru': 'Порт'}

    @_Tr
    def tr_position(self):
        return {'en': 'Position', 'ru': 'Должность'}

    @_Tr
    def tr_prepare_scan_failed(self):
        return {'en': 'Scan setup failed', 'ru': 'Сбой подготовки к сканированию'}

    @_Tr
    def tr_prepare_scan_send_to_equipment(self):
        return {'en': 'Send to hardware', 'ru': 'Отправить\nоборудованию'}

    @_Tr
    def tr_prepare_scan_successfully(self):
        return {'en': 'Scan setup completed\nsuccessfully', 'ru': 'Подготовка сканирования\nпроведена успешно'}

    @_Tr
    def tr_pressure(self):
        return {'en': 'Pressure, Torr', 'ru': 'Давление, Torr'}

    @_Tr
    def tr_protocol(self):
        return {'en': 'Protocol', 'ru': 'Протокол'}

    @_Tr
    def tr_protocol_already_exists(self):
        return {'en': 'The protocol with the same name already exists.', 'ru': 'Протокол с таким именем уже существует'}

    @_Tr
    def tr_protocol_description(self):
        return {'en': 'Description', 'ru': 'Описание'}

    @_Tr
    def tr_protocol_designer(self):
        return {'en': 'Protocol\nDesigner', 'ru': 'Конструктор\nпротоколов'}

    @_Tr
    def tr_protocol_examples(self):
        return {'en': 'Protocol examples', 'ru': 'Примеры протоколов'}

    @_Tr
    def tr_protocol_filter(self):
        return {'en': 'Protocol filter:', 'ru': 'Фильтр протоколов:'}

    @_Tr
    def tr_protocol_name(self):
        return {'en': 'Protocol name', 'ru': 'Название протокола'}

    @_Tr
    def tr_protocol_select_step_instruction_1(self):
        return {'en': 'Step {step_number}. Select a study protocol.',
                'ru': 'Шаг {step_number}. Выберите протокол исследования.'}

    @_Tr
    def tr_protocol_selection(self):
        return {'en': 'Protocol selection', 'ru': 'Выбор протокола'}

    @_Tr
    def tr_protocol_step(self):
        return {'en': 'Protocol step', 'ru': 'Шаг протокола'}

    @_Tr
    def tr_protocol_step_name(self):
        return {'en': 'Protocol step name', 'ru': 'Название шага'}

    @_Tr
    def tr_protocol_step_name_2_rows(self):
        return {'en': 'Protocol step\nname', 'ru': 'Название шага'}

    @_Tr
    def tr_protocol_user(self):
        return {'en': 'Custom protocols', 'ru': 'Протоколы'}

    @_Tr
    def tr_command(self):
        return {'en': 'Command', 'ru': 'Команда'}

    @_Tr
    def tr_command_in_type(self):
        return {'en': 'In type', 'ru': 'In type'}

    @_Tr
    def tr_command_out_type(self):
        return {'en': 'Out type', 'ru': 'Out type'}

    @_Tr
    def tr_run_command(self):
        return {'en': 'Run', 'ru': 'Выполнить'}

    @_Tr
    def tr_quality(self):
        return {'en': 'Quality', 'ru': 'Качество'}

    @_Tr
    def tr_reason(self):
        return {'en': 'Reason', 'ru': 'Основание'}

    @_Tr
    def tr_reboot(self):
        return {'en': 'Reboot', 'ru': 'Перезагрузка'}

    @_Tr
    def tr_reconstruction_options(self):
        return {'en': 'Reconstruction options', 'ru': 'Реконструкция'}

    @_Tr
    def tr_reconstruction_options_header(self):
        return {'en': 'Reconstruction', 'ru': 'Реконструкция'}

    @_Tr
    def tr_refresh(self):
        return {'en': 'Refresh', 'ru': 'Обновить'}

    @_Tr
    def tr_rentgen(self):
        return {'en': 'Rentgen', 'ru': 'Рентген'}

    @_Tr
    def tr_reports_and_scan_results(self):
        return {'en': 'Reports and\nscan results', 'ru': 'Отчеты и результаты\nсканирований'}

    @_Tr
    def tr_resolution(self):
        return {'en': 'Resolution, px', 'ru': 'Разрешение, пикс'}

    @_Tr
    def tr_responsible_person(self):
        return {'en': 'Responsible person', 'ru': 'Ответственное лицо'}

    @_Tr
    def tr_retry(self):
        return {'en': 'Retry', 'ru': 'Повторить'}

    @_Tr
    def tr_retry_scan_request(self):
        return {'en': 'Retry scan?', 'ru': 'Повторить сканирование?'}

    @_Tr
    def tr_set_patient_position_to_all_steps_request(self):
        return {'en': 'Set new position to all steps?', 'ru': 'Установить новое положение на все шаги?'}

    @_Tr
    def tr_close_scan_tab_request(self):
        return {'en': 'Close tab?', 'ru': 'Закрыть вкладку?'}

    @_Tr
    def tr_roi_analysis(self):
        return {'en': 'Roi analysis', 'ru': 'Анализ ROI'}

    @_Tr
    def tr_roi_analysis_step(self):
        return {'en': 'ROI', 'ru': 'ROI'}

    @_Tr
    def tr_roi_step_instruction_1(self):
        return {'en': 'Step {step_number}. Trigger start. Set ROI for analyzing of concentration',
                'ru': 'Шаг {step_number}. Trigger start. Анализ плотности. Выберите на снимке область для измерения плотности и "Старт"\n3D сканирование будет запущено автоматически, при достижении нужной плотности'}

    @_Tr
    def tr_rotation_direction_clockwise(self):
        return {'en': 'Clockwise', 'ru': 'По часовой'}

    @_Tr
    def tr_rotation_direction_counter_clockwise(self):
        return {'en': 'Counter clockwise', 'ru': 'Против часовой'}

    @_Tr
    def tr_rotation_speed(self):
        return {'en': 'Rotation speed, Hz', 'ru': 'Скорость вращения, Гц'}

    @_Tr
    def tr_russian(self):
        return {'en': 'Русский', 'ru': 'Русский'}

    @_Tr
    def tr_save(self):
        return {'en': 'Save', 'ru': 'Сохранить'}

    @_Tr
    def tr_save_as(self):
        return {'en': 'Save as...', 'ru': 'Сохранить как...'}

    @_Tr
    def tr_save_image(self):
        return {'en': 'Save image', 'ru': 'Сохранить снимок'}

    @_Tr
    def tr_save_report(self):
        return {'en': 'Save report', 'ru': 'Сохранить'}

    @_Tr
    def tr_edit(self):
        return {'en': 'Edit', 'ru': 'Правка'}

    @_Tr
    def tr_scan_area(self):
        return {'en': 'Scan area', 'ru': 'Область сканирования'}

    @_Tr
    def tr_scan_area_mm(self):
        return {'en': 'Scan area, mm', 'ru': 'Область скан-я, мм'}

    @_Tr
    def tr_scan_area_mm_short(self):
        return {'en': 'mm', 'ru': 'мм'}

    @_Tr
    def tr_scan_body_area(self):
        return {'en': 'Organ', 'ru': 'Орган'}

    @_Tr
    def tr_scan_date(self):
        return {'en': 'Date', 'ru': 'Дата'}

    @_Tr
    def tr_scan_mode(self):
        return {'en': 'Mode', 'ru': 'Режим'}

    @_Tr
    def tr_scan_options(self):
        return {'en': 'Scan options', 'ru': 'Сканирование'}

    @_Tr
    def tr_planing(self):
        return {'en': 'Planing', 'ru': 'Планирование'}

    @_Tr
    def tr_scan_options_header(self):
        return {'en': 'Scan', 'ru': 'Сканирование'}

    @_Tr
    def tr_scan_protocol(self):
        return {'en': 'Scan protocol', 'ru': 'Протокол сканирования'}

    @_Tr
    def tr_scan_protocols(self):
        return {'en': 'Scan protocols', 'ru': 'Протоколы сканирования'}

    @_Tr
    def tr_scan_report_title(self):
        return {'en': 'Scan report', 'ru': 'Отчёт о сканировании'}

    @_Tr
    def tr_scan_reports(self):
        return {'en': 'Scan reports', 'ru': 'Отчёты о сканировании'}

    @_Tr
    def tr_scan_scout_side(self):
        return {'en': 'Scout sagittal plane', 'ru': 'Обзорный сагиттальная пл.'}

    @_Tr
    def tr_scan_scout_two_side(self):
        return {'en': 'Scout two plane', 'ru': 'Обзорный две пл.'}

    @_Tr
    def tr_table_motion(self):
        return {'en': 'Table motion', 'ru': 'Движение стола'}

    @_Tr
    def tr_number_of_volume_images(self):
        return {'en': 'Number of Volume Images', 'ru': 'Число объёмных изображений'}

    @_Tr
    def tr_table_motions_moving(self):
        return {'en': 'Moving', 'ru': 'С движением'}

    @_Tr
    def tr_table_motions_stationary(self):
        return {'en': 'Stationary', 'ru': 'Стационарный'}

    @_Tr
    def tr_scan_selected_protocol(self):
        return {'en': 'Scans of selected protocol:', 'ru': 'Сканирования выбранного протокола:'}

    @_Tr
    def tr_scan_time(self):
        return {'en': 'Scan time, s', 'ru': 'Время сканирования, сек'}

    @_Tr
    def tr_scan_trigger_start(self):
        return {'en': 'Trigger start', 'ru': 'Trigger start'}

    @_Tr
    def tr_scan_type(self):
        return {'en': 'Scan type', 'ru': 'Тип скан-я'}

    @_Tr
    def tr_scanned_area(self):
        return {'en': 'Scanned area', 'ru': 'Отсканированная область'}

    @_Tr
    def tr_scanning(self):
        return {'en': 'Scan', 'ru': 'Сканирование'}

    @_Tr
    def tr_scout_front(self):
        return {'en': 'Scout front plane', 'ru': 'Обзорный фронтальная пл.'}

    @_Tr
    def tr_scout_prepare(self):
        return {'en': 'Setup.', 'ru': 'Подготовка.'}

    @_Tr
    def tr_scout_prepare_step_instruction_1(self):
        return {
            'en': 'Step {step_number}. Scout scan setup. Indicate the position of the patient. Check scan parameters\nand reconstruction parameters. Click the "Send to Hardware" button, then the "Next>" button.',
            'ru': 'Шаг {step_number}. Scout сканирование. Подготовка. Укажите позицию пациента. Проверьте параметры сканирования\nи параметры реконструкции. Нажмите кнопку "Отправить оборудованию", затем кнопку "Далее >".'}

    @_Tr
    def tr_scout_scan(self):
        return {'en': '2D', 'ru': '2D'}

    @_Tr
    def tr_scout_scan_required(self):
        return {'en': 'Scout scan required', 'ru': 'Необходимо обзорное сканирование'}

    @_Tr
    def tr_scout_scan_step(self):
        return {'en': 'Scanning.', 'ru': 'Сканирование.'}

    @_Tr
    def tr_scout_scan_step_instruction_1(self):
        return {
            'en': 'Step {step_number}. Scout scan. To start scanning, press "Scan start" and confirm by pushing the red button\non the operator’s desk.',
            'ru': 'Шаг {step_number}. Scout сканирование.\nДля запуска нажмите "Запуск сканирования" и подтвердите нажатием на красную кнопку на столе оператора.'}

    @_Tr
    def tr_sec(self):
        return {'en': 's', 'ru': 'с'}

    @_Tr
    def tr_select(self):
        return {'en': 'Select', 'ru': 'Выбрать'}

    @_Tr
    def tr_select_scan_area(self):
        return {'en': 'Select scan area', 'ru': 'Выберите область сканирования'}

    @_Tr
    def tr_select_step(self):
        return {'en': 'Select step', 'ru': 'Выберете шаг'}

    @_Tr
    def tr_sensors(self):
        return {'en': 'Sensors', 'ru': 'Датчики'}

    @_Tr
    def tr_session_report(self):
        return {'en': 'Report', 'ru': 'Отчет'}

    @_Tr
    def tr_set_new_protocol_request(self):
        return {'en': 'Change protocol?', 'ru': 'Сменить протокол?'}

    @_Tr
    def tr_set_saved_parameters_q(self):
        return {'en': 'Set saved parameters?', 'ru': 'Установить сохранённые параметры?'}

    @_Tr
    def tr_settings(self):
        return {'en': 'Settings', 'ru': 'Настройки'}

    @_Tr
    def tr_show_image(self):
        return {'en': 'Show image', 'ru': 'Изображение'}

    @_Tr
    def tr_shutdown(self):
        return {'en': 'Shutdown', 'ru': 'Выключение'}

    @_Tr
    def tr_sievert_factor(self):
        return {'en': 'Sievert factor', 'ru': 'Коэффициент зиверт'}

    @_Tr
    def tr_sign_out(self):
        return {'en': 'Sign out', 'ru': 'Выход'}

    @_Tr
    def tr_skip(self):
        return {'en': 'Skip', 'ru': 'Пропустить'}

    @_Tr
    def tr_slice_select_step(self):
        return {'en': 'Slice.', 'ru': 'Срез'}

    @_Tr
    def tr_slice_select_step_instruction_1(self):
        return {'en': 'Step {step_number}. Trigger start. Select scan slice. Select scan area.',
                'ru': 'Шаг {step_number}. Trigger start. Выбор среза сканирования. Установите на снимке границы'}

    @_Tr
    def tr_slice_size(self):
        return {'en': 'Slice size, mm', 'ru': 'Размер среза, мм'}

    @_Tr
    def tr_solvent_flow_speed(self):
        return {'en': 'Solvent flow', 'ru': 'Поток растворителя'}

    @_Tr
    def tr_solvent_volume(self):
        return {'en': 'Solvent volume, ml', 'ru': 'Объем раст-ля, мл'}

    @_Tr
    def tr_spent_s(self):
        return {'en': 'Spent, s', 'ru': 'Затрачено, с'}

    @_Tr
    def tr_start(self):
        return {'en': 'Start', 'ru': 'Старт'}

    @_Tr
    def tr_start_check(self):
        return {'en': 'Start test', 'ru': 'Запустить проверку'}

    @_Tr
    def tr_start_delay(self):
        return {'en': 'Start delay, s', 'ru': 'Задержка старта, сек'}

    @_Tr
    def tr_start_position(self):
        return {'en': 'Start position, mm', 'ru': 'Позиция старта, мм'}

    @_Tr
    def tr_start_position_short(self):
        return {'en': 'Start', 'ru': 'Старт'}

    @_Tr
    def tr_start_voice_command(self):
        return {'en': 'Start voice cmd', 'ru': 'Старт голос зап.'}

    @_Tr
    def tr_state(self):
        return {'en': 'State', 'ru': 'Состояние'}

    @_Tr
    def tr_status(self):
        return {'en': 'Status', 'ru': 'Статус'}

    @_Tr
    def tr_step(self):
        return {'en': 'Step', 'ru': 'Шаг'}

    @_Tr
    def tr_step_is_not_completed(self):
        return {'en': 'Step is not completed', 'ru': 'Шаг не завершён'}

    @_Tr
    def tr_stop(self):
        return {'en': 'Stop', 'ru': 'Стоп'}

    @_Tr
    def tr_go(self):
        return {'en': 'Go', 'ru': 'Go'}

    @_Tr
    def tr_stop_position(self):
        return {'en': 'Stop position, mm', 'ru': 'Позиция стопа, мм'}

    @_Tr
    def tr_stop_position_short(self):
        return {'en': 'Stop', 'ru': 'Стоп'}

    @_Tr
    def tr_stop_work(self):
        return {'en': 'Stop work', 'ru': 'Остановить работу'}

    @_Tr
    def tr_study_protocol(self):
        return {'en': 'Study protocol:', 'ru': 'Протокол исследования:'}

    @_Tr
    def tr_study_protocol_name(self):
        return {'en': 'Study protocol Name', 'ru': 'Название протокола исследовани'}

    @_Tr
    def tr_subsystems_state(self):
        return {'en': 'Subsystems state', 'ru': 'Состояние подсистем'}

    @_Tr
    def tr_sum_dose(self):
        return {'en': 'Total dose', 'ru': 'Общая доза'}

    @_Tr
    def tr_survey_plan(self):
        return {'en': 'Survey plan', 'ru': 'План обследования'}

    @_Tr
    def tr_survey_plan_step_instruction_1(self):
        return {'en': 'Step {step_number}. Survey plan. Check the list of scheduled scans.',
                'ru': 'Шаг {step_number}. План обследования. Проверьте список запланированных сканирований.'}

    @_Tr
    def tr_survey_report_step_instruction_1(self):
        return {'en': 'Step {step_number}. Session Report.', 'ru': 'Шаг {step_number}. Отчет за сеанс.'}

    @_Tr
    def tr_table(self):
        return {'en': 'Table', 'ru': 'Стол'}

    @_Tr
    def tr_GAS(self):
        return {'en': 'Gas Feed System', 'ru': 'Gas Feed System'}

    @_Tr
    def tr_PES(self):
        return {'en': 'PES', 'ru': 'PES'}

    @_Tr
    def tr_Scanner(self):
        return {'en': 'Scanner', 'ru': 'Сканер'}

    @_Tr
    def tr_table_height(self):
        return {'en': 'Table height, mm', 'ru': 'Высота стола, мм'}

    @_Tr
    def tr_table_horizontal_pos(self):
        return {'en': 'Table horizontal position', 'ru': 'Горизонтальная позиция стола'}

    @_Tr
    def tr_table_speed(self):
        return {'en': 'Table speed, mm/s', 'ru': 'Скорость стола, мм/с'}

    @_Tr
    def tr_table_speed_short(self):
        return {'en': 'Table speed', 'ru': 'Скорость стола'}

    @_Tr
    def tr_table_speed_short_min(self):
        return {'en': 'Table speed min', 'ru': 'Скорость стола мин'}

    @_Tr
    def tr_table_speed_short_max(self):
        return {'en': 'Table speed max', 'ru': 'Скорость стола макс'}

    @_Tr
    def tr_table_vertical_pos(self):
        return {'en': 'Table vertical position', 'ru': 'Вертикальная позиция стола'}

    @_Tr
    def tr_target_position(self):
        return {'en': 'Target position', 'ru': 'Целевая позиция'}

    @_Tr
    def tr_technical_service(self):
        return {'en': 'Technical service', 'ru': 'Техобслуживание'}

    @_Tr
    def tr_technical_service_password_message(self):
        return {'en': 'Enter the password to get the technical service access',
                'ru': 'Введите пароль для входа в режим техобслуживания'}

    @_Tr
    def tr_temperature(self):
        return {'en': 'Temperature, °C', 'ru': 'Температура, °C'}

    @_Tr
    def tr_time(self):
        return {'en': 'Time', 'ru': 'Время'}

    @_Tr
    def tr_title(self):
        return {'en': 'Name', 'ru': 'Название'}

    @_Tr
    def tr_to_plan(self):
        return {'en': 'To plan', 'ru': 'Запланировать'}

    @_Tr
    def tr_to_planned_scan(self):
        return {'en': 'Scheduled scan', 'ru': 'Запланированное сканирование'}

    @_Tr
    def tr_to_start_position(self):
        return {'en': 'To start position', 'ru': 'На позицию старта'}

    @_Tr
    def tr_tomograph_scheme(self):
        return {'en': 'CT Operation', 'ru': 'Операции с\nтомографом'}

    @_Tr
    def tr_tomograph_state(self):
        return {'en': 'Tomograph state', 'ru': 'Состояние томографа'}

    @_Tr
    def tr_tools(self):
        return {'en': 'Tools', 'ru': 'Инструменты'}

    @_Tr
    def tr_total(self):
        return {'en': 'Total', 'ru': 'Всего'}

    @_Tr
    def tr_total_dose(self):
        return {'en': 'The total dose of radiation per session:', 'ru': 'Суммарная доза облучения за сеанс:'}

    @_Tr
    def tr_total_s(self):
        return {'en': 'Total, s', 'ru': 'Всего, с'}

    @_Tr
    def tr_diameter_roi_circle(self):
        return {'en': 'Zone diameter, mm', 'ru': 'Диаметр зоны, мм'}

    @_Tr
    def tr_early_start(self):
        return {'en': 'Manual start', 'ru': 'Ручной старт'}

    @_Tr
    def tr_pes_battery_title(self):
        return {'en': 'Battery', 'ru': 'Батарея'}

    @_Tr
    def tr_pes_open(self):
        return {'en': 'Idle', 'ru': 'Бездействует'}

    @_Tr
    def tr_pes_running(self):
        return {'en': 'Drive', 'ru': 'Работает'}

    @_Tr
    def tr_pes_standby(self):
        return {'en': 'Charging', 'ru': 'Заряжается'}

    @_Tr
    def tr_plane_select(self):
        return {'en': 'Slice selection', 'ru': 'Выбор среза'}

    @_Tr
    def tr_point_select(self):
        return {'en': 'Area select', 'ru': 'Выбор области'}

    @_Tr
    def tr_set_point(self):
        return {'en': 'Set point', 'ru': 'Установить точку'}

    @_Tr
    def tr_start_scan(self):
        return {'en': 'Scan start', 'ru': 'Запуск\nсканирования'}

    @_Tr
    def tr_start_test_error(self):
        return {'en': 'Failed to start hardware test', 'ru': 'Не удалось запустить проверку оборудования'}

    @_Tr
    def tr_trigger_start_concentration(self):
        return {'en': 'Concentration, HU', 'ru': 'Концентрация, HU'}

    @_Tr
    def tr_zero_plane(self):
        return {'en': 'Point zero', 'ru': 'Точка ноль'}

    @_Tr
    def tr_trauma(self):
        return {'en': 'Trauma', 'ru': 'Травма'}

    @_Tr
    def tr_trigger_options(self):
        return {'en': 'Trigger options', 'ru': 'Параметры триггера'}

    @_Tr
    def tr_trigger_start_check_count(self):
        return {'en': 'Number of checks', 'ru': 'Число проверок'}

    @_Tr
    def tr_trigger_start_concentration_threshold(self):
        return {'en': 'Target ROI, HU', 'ru': 'Целевой ROI, HU'}

    @_Tr
    def tr_trigger_start_concentration_threshold_short(self):
        return {'en': 'HU', 'ru': 'HU'}

    @_Tr
    def tr_trigger_start_max_check_count(self):
        return {'en': 'Max. checks', 'ru': 'Макс. проверок'}

    @_Tr
    def tr_trigger_start_max_wait(self):
        return {'en': 'Max. trigger wait', 'ru': 'Макс. ожидание'}

    @_Tr
    def tr_trigger_start_only_manual(self):
        return {'en': 'Manual start', 'ru': 'Ручной старт'}

    @_Tr
    def tr_trigger_start_step_delay(self):
        return {'en': 'Trigger start step delay', 'ru': 'Trigger start: время шага'}

    @_Tr
    def tr_trigger_start_step_delay_short(self):
        return {'en': 'Step delay', 'ru': 'Время шага'}

    @_Tr
    def tr_trigger_start_wait(self):
        return {'en': 'Trigger wait', 'ru': 'Ожидание триггера'}

    @_Tr
    def tr_tube_is_warming_up(self):
        return {'en': 'X-ray tube is warming up', 'ru': 'Идёт прогрев рентгеновской трубки'}

    @_Tr
    def tr_tube_warm_up(self):
        return {'en': 'X-ray tube warm up', 'ru': 'Прогрев рентгеновской трубки'}

    @_Tr
    def tr_tube_warm_up_required(self):
        return {'en': 'X-ray tube warm up required', 'ru': 'Требуется прогрев рентгеновской трубки'}

    @_Tr
    def tr_turbomolecular_pump(self):
        return {'en': 'Turbomolecular pump', 'ru': 'Турбомолекулярный насос'}

    @_Tr
    def tr_up(self):
        return {'en': 'Up', 'ru': 'Вверх'}

    @_Tr
    def tr_update(self):
        return {'en': 'Update', 'ru': 'Обновить'}

    @_Tr
    def tr_morning_startup(self):
        return {'en': 'Morning Startup', 'ru': 'Утренний запуск'}

    @_Tr
    def tr_evening_shutdown(self):
        return {'en': 'Evening Shutdown', 'ru': 'Вечернее отключение'}

    @_Tr
    def tr_update_ecg(self):
        return {'en': 'Update ECG', 'ru': 'Обновить ЭКГ'}

    @_Tr
    def tr_user(self):
        return {'en': 'User', 'ru': 'Пользовательский'}

    @_Tr
    def tr_vacuum(self):
        return {'en': 'Vacuum', 'ru': 'Вакуум'}

    @_Tr
    def tr_NEGs(self):
        return {'en': 'NEGs', 'ru': 'NEGs'}

    @_Tr
    def tr_e_sources(self):
        return {'en': 'E-Sources', 'ru': 'E-Sources'}

    @_Tr
    def tr_cooling(self):
        return {'en': 'Cooling', 'ru': 'Cooling'}

    @_Tr
    def tr_value(self):
        return {'en': 'Value', 'ru': 'Значение'}

    @_Tr
    def tr_edit(self):
        return {'en': 'Edit', 'ru': 'Редактировать'}

    @_Tr
    def tr_value_should_be_between(self):
        return {'en': 'Value {} should be between {} and {}.', 'ru': 'Значение {} должно быть между {} и {}.'}

    @_Tr
    def tr_volt(self):
        return {'en': 'V', 'ru': 'В'}

    @_Tr
    def tr_weekdays(self):
        return {'en': ('Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'), 'ru': ('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс')}

    @_Tr
    def tr_months(self):
        return {'en':
                    ('January', 'February', 'March',
                     'April', 'May', 'June',
                     'July', 'August', 'September',
                     'October', 'November', 'December'),
                'ru':
                    ('Январь', 'Февраль', 'Март',
                     'Апрель', 'Май', 'Июнь',
                     'Июль', 'Август', 'Сентябрь',
                     'Октябрь', 'Ноябрь', 'Декабрь',
                     )}

    @_Tr
    def tr_weight(self):
        return {'en': 'Weight, kg', 'ru': 'Вес, кг'}

    @_Tr
    def tr_width_mm(self):
        return {'en': 'Width, mm', 'ru': 'Ширина, мм'}

    @_Tr
    def tr_windows(self):
        return {'en': 'Windows', 'ru': 'Окна'}

    @_Tr
    def tr_worklist(self):
        return {'en': 'Worklist', 'ru': 'Worklist'}

    @_Tr
    def tr_wwires(self):
        return {'en': 'W-wire acquisition', 'ru': 'W-wire acquisition'}

    @_Tr
    def tr_yes(self):
        return {'en': 'Yes', 'ru': 'Да'}

    @_Tr
    def tr_dsebct_no_connection(self):
        return {'en': 'No connection', 'ru': 'Нет связи'}

    @_Tr
    def tr_dsebct_fault(self):
        return {'en': 'Fault', 'ru': 'Сбой'}

    @_Tr
    def tr_dsebct_init(self):
        return {'en': 'Init', 'ru': 'Инициализация'}

    @_Tr
    def tr_dsebct_on(self):
        return {'en': 'On', 'ru': 'Вкл'}

    @_Tr
    def tr_dsebct_off(self):
        return {'en': 'Off', 'ru': 'Выкл'}

    @_Tr
    def tr_dsebct_running(self):
        return {'en': 'Running', 'ru': 'Работа'}

    @_Tr
    def tr_dsebct_moving(self):
        return {'en': 'Moving', 'ru': 'Движение'}

    @_Tr
    def tr_dsebct_alarm(self):
        return {'en': 'Alarm', 'ru': 'Внимание'}

    @_Tr
    def tr_dsebct_standby(self):
        return {'en': 'Standby', 'ru': 'Ожидание'}

    @_Tr
    def tr_dsebct_unknown(self):
        return {'en': 'Unknown', 'ru': 'Неизвестно'}

    @_Tr
    def tr_voice_cmds(self):
        return {'en': 'Voice commands', 
                'ru': 'Голосовые команды'}

    @_Tr
    def tr_header_voice_cmd_ru_filename(self):
        return {'en': 'RU file',
                'ru': 'RU-файл'}

    @_Tr
    def tr_header_voice_cmd_en_filename(self):
        return {'en': 'EN file',
                'ru': 'EN-файл'}

    @_Tr
    def tr_header_voice_cmd_ru_duration(self):
        return {'en': 'RU duration',
                'ru': 'RU-длительность'}
    
    @_Tr
    def tr_header_voice_cmd_en_duration(self):
        return {'en': 'EN duration',
                'ru': 'EN-длительность'}
    
    @_Tr
    def tr_voice_cmd_duration(self):
        return {'en': 'Duration',
                'ru': 'Длительность'}
    
    @_Tr
    def tr_voice_cmd_file_is_not_assigned(self):
        return {'en': 'File not assigned',
                'ru': 'Файл не назначен'}

    @_Tr
    def tr_voice_cmd_is_not_avaibled(self):
        return {'en': 'Voice command not avaibled',
                'ru': 'Голосовая команда недоступна'}

    @_Tr
    def tr_delete_voice_cmd_q(self):
        return {'en': 'Delete the voice command?',
                'ru': 'Удалить голосовую команду?'}
    
    @_Tr
    def tr_add_voice_cmd(self):
        return {'en': 'Add voice command',
                'ru': 'Добавить голосовую команду'}

    @_Tr
    def tr_ru_audio_file(self):
        return {'en': 'RU audiofile', 
                'ru': 'RU-аудиофайл'}

    @_Tr
    def tr_en_audio_file(self):
        return {'en': 'EN audiofile',
                'ru': 'EN-аудиофайл'}

    @_Tr 
    def tr_select_audio_file(self):
        return {'en': 'Select audiofile',
                'ru': 'Выбрать аудиофайл'}

    @_Tr
    def tr_ru_audio_duration(self):
        return {'en': '[RU] Duration of the voice command: {d}s',
                'ru': '[RU] Длительность голосовой команды: {d}с'}
    
    @_Tr
    def tr_en_audio_duration(self):
        return {'en': '[EN] Duration of the voice command: {d}s',
                'ru': '[EN] Длительность голосовой команды: {d}с'}

    @_Tr
    def tr_title_select_audio_file(self):
        return {'en': 'Select audiofile',
                'ru': 'Выберите аудиофайл'}

    @_Tr
    def tr_ru_name(self):
        return {'en': 'RU name',
                'ru': 'RU-имя'}
    
    @_Tr
    def tr_en_name(self):
        return {'en': 'EN name',
                'ru': 'EN-имя'}

    @_Tr
    def tr_playback_lang(self):
        return {'en': 'Playback language',
                'ru': 'Язык воспроизведения'}
    
    @_Tr
    def tr_header_air_calib(self):
        return {'en': 'Air', 'ru': 'Воздушная'}

    @_Tr
    def tr_header_water_calib(self):
        return {'en': 'Water', 'ru': 'Водная'}

    @_Tr
    def tr_header_multi_pin_calib(self):
        return {'en': 'Multipin', 'ru': 'Мультипиновая'}
    
    @_Tr
    def tr_header_dark_calib(self):
        return {'en': 'Dark', 'ru': 'По "чёрному"'}
    
    @_Tr
    def tr_title_label_air_calib(self):
        return {'en': 'Air calibration', 'ru': 'Воздушная калибровка'}

    @_Tr
    def tr_title_label_multi_pin_calib(self):
        return {'en': 'Multipin calibration', 'ru': 'Мультипиновая калибровка'}

    @_Tr
    def tr_title_label_water_calib(self):
        return {'en': 'Water calibration', 'ru': 'Водная калибровка'}

    @_Tr
    def tr_title_label_dark_calib(self):
        return {'en': 'Dark calibration', 'ru': 'Калибровка по "чёрному"'}

    @_Tr
    def tr_title_label_calib_type(self):
        return {'en': 'Type of calibration', 'ru': 'Тип калибровки'}

    @_Tr 
    def tr_calib_datetime_format(self):
        return {'en': '%Y-%m-%d %H:%M', 'ru': '%Y-%m-%d %H:%M'}

    @_Tr
    def tr_title_label_op_dt(self):
        return {'en': 'Last performed on {dt}\nby {op}', 
                'ru': 'Последний раз выполнено {dt}\nоператором {op}'}

    @_Tr
    def tr_title_label_calib_has_not_been_performed(self):
        return {'en': 'Calibration has not yet been performed\n',
                'ru': 'Калибровка еще не выполнялась\n'}

    @_Tr 
    def tr_minititle_calib_has_prepared(self):
        return {'en': 'The task was sent to the equipment',
                'ru': 'Задача отправлена на оборудование'}

    @_Tr
    def tr_related_reports_info_label(self):
        """ TODO: Нужно изменить наименование метода... """
        return {'en': 'The change will affect {N} more report(s)', 
                'ru': 'Изменение коснётся еще {N} репорта(ов)'}

    @_Tr
    def tr_tooltip_filter_by_date(self):
        return {'en': ('Filtering by date (first filtering by date, and then by time).'
                    '\n'
                    'Definitions:\n'
                    '   {op} - conditional operator (">", "<", ">=", "<=", "==")\n'
                    '   {date} - Date in DD.MM.YYYYY format\n'
                    '   {date_s} - any character that can be in a string with the date\n'
                    '\n'
                    'Don\'t use extra characters or spaces!\n'
                    'The "_" prefix is used for clarity, it\'s optional.\n'
                    '\n'
                    'Date filter options and examples:\n'
                    '   1) Search for a substring in a date string:\n'
                    '       - "_ {date_s}" -> "_ 21"; "_ 21.01"\n'
                    '       - "{date_s}" -> "21"; "21.01"\n'
                    '\n'
                    '   2) Search via date comparison:\n'
                    '       - "{op} {date}" -> "> 24.03.2022"\n'
                    '\n'
                    '   3) Search by specifying the period:\n'
                    '       - "{op} {date} | {op} {date}" -> "> 24.03.2022 | < 27.03.2022"\n'
                    '       - "{op} {date} | {op} {date}" -> "< 27.03.2022 | > 24.03.2022"\n'
                    '\n'
                    '   4) This writing is not excluded, either, '
                          ' but a non-obvious filtering result is possible:\n'
                    '       - "{date_s} | {op} {date}" -> ".25. | < 28.03.2022" \n'
                    '       - "_ {date_s} | {op} {date}" -> "_ .25. | < 28.03.2022" \n'
                    '       - "{op} {date} | {date_s}" -> "< 28.03.2022 | .25."\n'
                    '       - "{op} {date} | _ {date_s}" -> "< 28.03.2022 | _ .25."\n'
                ),
                'ru': (
                    'Фильтрация по дате (сначала идёт фильтрация по дате, '
                                         'а потом уже по времени).\n'
                    '\n'
                    'Определения:\n'
                    '   {оп} - условный оператор (">", "<", ">=", "<=", "==")\n'
                    '   {дата} - дата в формате ДД.ММ.ГГГГ\n'
                    '   {дата_с} - любой символ, который может быть в строке с датой\n'
                    '\n'
                    'Не используйте лишние символы или пробелы!\n'
                    'Префикс "_ " используется для наглядности, он необязателен\n'
                    '\n'
                    'Варианты фильтров даты и их примеры:\n'
                    '   1) Поиск подстроки в строке даты:\n'
                    '       - "_ {дата_с}" -> "_ 21"; "_ 21.01"\n'
                    '       - "{дата_с}"  -> "21"; "21.01"\n'
                    '\n'
                    '   2) Поиск через сравнение даты:\n'
                    '       - "{оп} {дата}" -> "> 24.03.2022"\n'
                    '\n'
                    '   3) Поиск через указание периода:\n'
                    '       - "{оп} {дата} | {оп} {дата}" -> "> 24.03.2022 | < 27.03.2022"\n'
                    '       - "{оп} {дата} | {оп} {дата}" -> "< 27.03.2022 | > 24.03.2022"\n'
                    '\n'
                    '   4) Не исключается и такой вариант записи, '
                          'но возможен неочевидный результат фильтрации:\n'
                    '       - "{дата_с} | {оп} {дата}"   -> ".25. | < 28.03.2022" \n'
                    '       - "_ {дата_с} | {оп} {дата}" -> "_ .25. | < 28.03.2022" \n'
                    '       - "{оп} {дата} | {дата_с}"   -> "< 28.03.2022 | .25."\n'
                    '       - "{оп} {дата} | _ {дата_с}" -> "< 28.03.2022 | _ .25."\n'
                )}

    @_Tr 
    def tr_tooltip_filter_by_time(self):
        return {'en': ('Filtering by time (first filtering by date, and then by time).'
                    '\n'
                    'Definitions:\n'
                    '   {op} - conditional operator (">", "<", ">=", "<=", "==")\n'
                    '   {time} - Time in HH:MM:SS format\n'
                    '   {time_s} - any character that can be in a string with the time\n'
                    '\n'
                    'Don\'t use extra characters or spaces!\n'
                    'The "_" prefix is used for clarity, it\'s optional.\n'
                    '\n'
                    'Time filter options and examples:\n'
                    '   1) Search for a substring in a time string:\n'
                    '       - "_ {time_s}" -> "_ 21"; "_ 21:01"\n'
                    '       - "{time_s}" -> "21"; "21:01"\n'
                    '\n'
                    '   2) Search via time comparison:\n'
                    '       - "{op} {time}" -> "> 21:00:00"\n'
                    '\n'
                    '   3) Search by specifying the period:\n'
                    '       - "{op} {time} | {op} {time}" -> "> 21:00:00 | < 22:00:00"\n'
                    '       - "{op} {time} | {op} {time}" -> "< 22:00:00 | > 21:00:00"\n'
                    '\n'
                    '   4) This writing is not excluded, either, '
                          ' but a non-obvious filtering result is possible:\n'
                    '       - "{time_s} | {op} {time}" -> ":25: | < 22:00:00" \n'
                    '       - "_ {time_s} | {op} {time}" -> "_ :25: | < 22:00:00" \n'
                    '       - "{op} {time} | {time_s}" -> "< 22:00:00  | :25:"\n'
                    '       - "{op} {time} | _ {time_s}" -> "< 22:00:00  | _ :25:"\n'
                ),
                'ru': (
                    'Фильтрация по времени (сначала идёт фильтрация по дате, '
                                            'а потом уже по времени).\n'
                    '\n'
                    'Определения:\n'
                    '   {оп} - условный оператор (">", "<", ">=", "<=", "==")\n'
                    '   {время} - Время в формате ЧЧ:ММ:СС\n'
                    '   {время_с} - любой символ, который может быть в строке со временем\n'
                    '\n'
                    'Не используйте лишние символы или пробелы!\n'
                    'Префикс "_ " используется для наглядности, он необязателен\n'
                    '\n'
                    'Варианты фильтров времени и их примеры:\n'
                    '   1) Поиск подстроки в строке времени:\n'
                    '       - "_ {время_с}" -> "_ 21"; "_ 21:01"\n'
                    '       - "{время_с}"  -> "21"; "21:01"\n'
                    '\n'
                    '   2) Поиск через сравнение времени:\n'
                    '       - "{оп} {время}" -> "> 21:00:00"\n'
                    '\n'
                    '   3) Поиск через указание периода:\n'
                    '       - "{оп} {время} | {оп} {время}" -> "> 21:00:00 | < 22:00:00"\n'
                    '       - "{оп} {время} | {оп} {время}" -> "< 22:00:00 | > 21:00:00"\n'
                    '\n'
                    '   4) Не исключается и такой вариант записи, '
                          'но возможен неочевидный результат фильтрации:\n'
                    '       - "{время_с} | {оп} {время}"   -> ":25: | < 22:00:00" \n'
                    '       - "_ {время_с} | {оп} {время}" -> "_ :25: | < 22:00:00" \n'
                    '       - "{оп} {время} | {время_с}"   -> "< 22:00:00  | :25:"\n'
                    '       - "{оп} {время} | _ {время_с}" -> "< 22:00:00  | _ :25:"\n'
                )}

    @_Tr
    def tr_horizontal_short(self):
        return {'en': 'H',
                'ru': 'Г'}

    @_Tr
    def tr_vertical_short(self):
        return {'en': 'V',
                'ru': 'В'}
