from datetime import timedelta, datetime

from dateutil.relativedelta import relativedelta


def text_to_int(text):
    text = text.strip()
    if text == '':
        return 0
    else:
        return int(float(text))


def text_to_float(text):
    text = text.strip().replace(',', '.')
    if text in ('', None, 'None'):
        return 0
    else:
        return float(text)


def float_or_none_to_str(value):
    if value is None:
        return ''
    return str(value)


def age_parts(dob) -> relativedelta:
    return relativedelta(datetime.now().date(), dob)


def age(dob) -> int:
    import datetime
    today = datetime.date.today()

    if today.month < dob.month or \
            (today.month == dob.month and today.day < dob.day):
        return today.year - dob.year - 1
    else:
        return today.year - dob.year


def age_in_months(dob) -> int:
    r = relativedelta(datetime.now().date(), dob)
    return r.months + (r.years + 1) * 12


def scale(min_value, max_value, value, start, end):
    if max_value == min_value:
        return 0
    return (end - start) * (value - min_value) / (max_value - min_value) + start


def un_scale(start, end, value, min_value, max_value):
    return (end - start) * (value - min_value) / (max_value - min_value) + start


def time_delta_format(td: timedelta) -> str:
    seconds = td.total_seconds()
    if seconds == 0:
        return ''
    if seconds < 60:
        return str(int(seconds))
    elif seconds < 60 * 60:
        mm, ss = divmod(seconds, 60)
        mm, ss = int(mm), int(ss)
        return f'{mm}:{ss:02d}'
    else:
        mm, ss = divmod(seconds, 60)
        hh, mm = divmod(mm, 60)
        hh, mm, ss = int(hh), int(mm), int(ss)
        return f'{hh}:{mm}:{ss:02d}'
