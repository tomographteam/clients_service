import configparser
import json
import os
import shutil
from datetime import datetime
from functools import lru_cache
from logging import getLogger
from pathlib import Path
from typing import Optional

from PyQt5.QtGui import QFontDatabase, QFont


class Settings:
    __inst: Optional['Settings'] = None

    @classmethod
    def inst(cls) -> 'Settings':
        if not cls.__inst:
            cls.__inst = Settings()
        return cls.__inst

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.android = os.environ.get('ANDROID_ADD_DATA_PATH', '') != ''
        self._app_data_path = ''
        self._device_name_config = None
        self._tomograph_config = None
        self._path_config = None

        # project_path = os.getcwd()
        # app_data_path = os.path.join(project_path, 'app_data')
        self._app_data_path = self._get_app_data_path()
        self._app_data_path = self._app_data_path.replace("/tests/model/equipment/subsystem_device_manager/peripheral", "")
        # log = getLogger('bootstrap')
        # log.info(f'self._app_data_path={self._app_data_path}')
        config_path = os.path.join(self._app_data_path, 'app_data/config')
        # print(f'config_path={config_path}')
        # os.path.isfile()

        # config.read_file(open(project_path + '/app_data/conf.ini'))
        conf_path_main = os.path.join(config_path, 'conf.ini')
        if not os.path.isfile(conf_path_main):
            e = FileExistsError(f'Config file not found at {conf_path_main}')
            raise e
        self.config.read(
            [
                conf_path_main,
                os.path.join(config_path, 'version.ini'),
                os.path.join(config_path, 'conf.override.ini'),
            ])
        self._init_app_data()
        #self.protocol = self._load_protocol_settings()
        # self._init_font()

    def _load_protocol_settings(self):
        json_path_default = os.path.join(self.resources_path, 'resources/protocol.json')
        json_path_override = os.path.join(self.resources_path, 'app_data/protocol.json')
        if os.path.isfile(json_path_override):
            json_path = json_path_override
        else:
            json_path = json_path_default
        return json.load(open(json_path, 'r'))

    def _init_font(self):
        font_id = QFontDatabase.addApplicationFont(os.path.join(self.get_path_config()['fonts'], 'liberation-mono.ttf'))
        font_family = QFontDatabase.applicationFontFamilies(font_id).at(0)
        self._font = QFont(font_family)
        self._font.setPointSize(18)

    def get_font(self) -> QFont:
        return self._font

    def _get_app_data_path(self) -> str:
        snap = os.environ.get('SNAP_USER_COMMON', '')
        android = os.environ.get('ANDROID_ADD_DATA_PATH', '')
        # print(f'snap={snap}')
        if False and snap:
            app_data_path = snap
            # print(os.environ['SNAP_USER_COMMON'])
            # print(os.environ['SNAP'])
            # print(os.listdir('/'))
            self.resources_path = os.environ['SNAP']
            default_config_path = os.path.join(self.resources_path, 'resources/conf.default.ini')
            # print(resources_path)
            config_path = os.path.join(app_data_path, 'app_data/config')
            self._install_config_to_snap(default_config_path, config_path)
        elif android:
            self.resources_path = android
            app_data_path = android

        else:
            project_path = os.getcwd()
            # app_data_path = os.path.join(project_path, 'app_data')
            self.resources_path = project_path
            app_data_path = project_path
        return app_data_path

    def _install_config_to_snap(self, default_config_path, config_path):
        if not os.path.isfile(os.path.join(config_path, 'conf.ini')):
            path = Path(config_path)
            path.mkdir(parents=True)
            shutil.copy(default_config_path, os.path.join(config_path, 'conf.ini'))

    def _init_app_data(self):
        for p in self.get_path_config().values():
            if not os.path.exists(p):
                os.mkdir(p)

    def get_app_data_root(self):
        return os.path.join(self._app_data_path, 'app_data')

    def get_app_data_path(self, name: str) -> str:
        return os.path.join(self._app_data_path, self.config.get('PATH', name))

    def get_resources_path(self, name: str) -> str:
        return os.path.join(self.resources_path, self.config.get('PATH', name))

    def get_path_config(self):

        if not self._path_config:
            self._path_config = {
                'protocols': self.get_app_data_path('protocols'),
                'operators': self.get_app_data_path('operators'),
                'patients': self.get_app_data_path('patients'),
                'scan_results': self.get_app_data_path('scan_results'),
                'recon_mnt_path': self.get_app_data_path('recon_mnt_path'),
                'settings': self.get_app_data_path('settings'),
                'logs': self.get_app_data_path('logs'),

                'resources': self.get_resources_path('resources'),
                'images': self.get_resources_path('images'),
                'images_tomograph': self.get_resources_path('images_tomograph'),
                'images_scan_areas': self.get_resources_path('images_scan_areas'),
                'patient_position': self.get_resources_path('patient_position'),
                'protocols_examples': self.get_resources_path('protocols_examples'),
                'protocol_icons': self.get_resources_path('protocol_icons'),
                'ui_icons': self.get_resources_path('ui_icons'),
                'operators_photos': self.get_resources_path('operators_photos'),
                'sounds': self.get_resources_path('sounds'),
                'voice_commands': self.get_resources_path('voice_commands'),
                'dicom_template': self.get_resources_path('dicom_template'),
                'fonts': self.get_resources_path('fonts'),
            }
        return self._path_config

    def get_tomograph_config(self):
        if not self._tomograph_config:
            config = self.config
            self._tomograph_config = {
                'manufacturer': str(config.get('TOMOGRAPH', 'manufacturer')),
                'manufacturer_model_name': str(config.get('TOMOGRAPH', 'manufacturer_model_name')),
                'device_serial_number': str(config.get('TOMOGRAPH', 'device_serial_number')),
                'last_calibration': datetime.strptime(config.get('TOMOGRAPH', 'last_calibration'), '%d.%m.%Y %H:%M:%S'),
                'patient_table_width': int(config.get('TOMOGRAPH', 'patient_table_width')),
                'patient_table_len': int(config.get('TOMOGRAPH', 'patient_table_len')),
                'patient_table_height_min': int(config.get('TOMOGRAPH', 'patient_table_height_min')),
                'patient_table_height_max': int(config.get('TOMOGRAPH', 'patient_table_height_max')),
                'scan_area_max_z': int(config.get('TOMOGRAPH', 'scan_area_max_z')),
                'scan_plane_offset': int(config.get('TOMOGRAPH', 'scan_plane_offset')),
                'max_table_speed': int(config.get('TOMOGRAPH', 'max_table_speed')),
                'min_collimator_width': float(config.get('TOMOGRAPH', 'min_collimator_width')),
                'max_collimator_width': float(config.get('TOMOGRAPH', 'max_collimator_width')),
            }
        return self._tomograph_config

    def get_device_name_config(self):
        if not self._device_name_config:
            config = self.config
            self._device_name_config = {
                'DSEBCT': str(config.get('DEVICE_NAME', 'DSEBCT')),
                'SCANNER': str(config.get('DEVICE_NAME', 'SCANNER')),
                'PES': str(config.get('DEVICE_NAME', 'PES')),
                'HVPS_NWL': str(config.get('DEVICE_NAME', 'HVPS_NWL')),
                'HVPS_CT': str(config.get('DEVICE_NAME', 'HVPS_CT')),
                'HVPS_USED': str(config.get('DEVICE_NAME', 'HVPS_USED')),
                'BCE': str(config.get('DEVICE_NAME', 'BCE')),
                'WWIRES': str(config.get('DEVICE_NAME', 'WWIRES')),
                'PTABLE': str(config.get('DEVICE_NAME', 'PTABLE')),
                'CHILLER': str(config.get('DEVICE_NAME', 'CHILLER')),
                'AGILENT': str(config.get('DEVICE_NAME', 'AGILENT')),
                'COLLIMATOR': str(config.get('DEVICE_NAME', 'COLLIMATOR')),
                'NEGPOWER': str(config.get('DEVICE_NAME', 'NEGPOWER')),
                'NEGPOWERMINI_1': str(config.get('DEVICE_NAME', 'NegPowerMini_1')),
                'NEGPOWERMINI_2': str(config.get('DEVICE_NAME', 'NegPowerMini_2')),
                'DAS_1': str(config.get('DEVICE_NAME', 'DAS_1')),
                'DAS_2': str(config.get('DEVICE_NAME', 'DAS_2')),
                'RECON': str(config.get('DEVICE_NAME', 'RECON')),
                'ECG': str(config.get('DEVICE_NAME', 'ECG')),
                'ELECTRODES': str(config.get('DEVICE_NAME', 'ELECTRODES')),
                'COLLIMATOR_M_1': str(config.get('DEVICE_NAME', 'COLLIMATOR_M_1')),
                'COLLIMATOR_M_2': str(config.get('DEVICE_NAME', 'COLLIMATOR_M_2')),
                'COLLIMATOR_M_3': str(config.get('DEVICE_NAME', 'COLLIMATOR_M_3')),
            }
        return self._device_name_config


@lru_cache(maxsize=None)
def get_view_conf():
    config = Settings.inst().config
    if Settings.inst().android:
        style_path = os.path.join(Settings.inst().resources_path, config.get('view', 'style_android'))
    else:
        style_path = os.path.join(Settings.inst().resources_path, config.get('view', 'style_desktop'))
    is_win = os.name == 'nt'
    image_viewer_cmd = config.get('view', 'image_viewer_cmd_win') if is_win else config.get('view', 'image_viewer_cmd')
    shutdown_cmd = config.get('view', 'shutdown_cmd_win') if is_win else config.get('view', 'shutdown_cmd')
    reboot_cmd = config.get('view', 'reboot_cmd_win') if is_win else config.get('view', 'reboot_cmd')
    software_version = config.get('view', 'software_version')
    window_title = config.get('view', 'window_title').format(software_version)
    return {
        'window_title': window_title,
        'software_version': software_version,
        # 'style': style_path,
        'style': style_path,
        'image_viewer_cmd': image_viewer_cmd,
        'shutdown_cmd': shutdown_cmd,
        'reboot_cmd': reboot_cmd,
        'open_help_cmd': config.get('view', 'open_help_cmd'),
        'open_beam_adjustment_cmd': config.get('view', 'open_beam_adjustment_cmd'),
        'expert_password': config.get('view', 'expert_password'),
        'technical_service_password': config.get('view', 'technical_service_password'),
        'background_color': config.get('view', 'background_color'),
        'select_operator_on_start': True if config.get('view', 'select_operator_on_start') == 'True' else False,
        'show_sensors_on_start': True if config.get('view', 'show_sensors_on_start') == 'True' else False,
        'allow_create_operator_on_start': True if config.get('view',
                                                             'allow_create_operator_on_start') == 'True' else False,
        'enable_media': True if config.get('view', 'enable_media') == 'True' else False,
        'has_worklist': True if config.get('view', 'has_worklist') == 'True' else False,
        'show_debug_options': config.getboolean('view', 'show_debug_options', fallback=False)
    }


def get_station_conf():
    config = Settings.inst().config
    return {
        'station_name': config.get('station', 'station_name'),
    }


def get_server_config():
    config = Settings.inst().config
    return {
        'tango_rest_server': config.get('SCSC', 'tango_rest_server'),
        'tango_rest_port': config.get('SCSC', 'tango_rest_port'),
        'tango_server': config.get('SCSC', 'tango_server'),
        'tango_port': config.get('SCSC', 'tango_port'),
        'request_timeout': config.getint('SCSC', 'request_timeout'),
        'user': config.get('SCSC', 'user'),
        'password': config.get('SCSC', 'password'),
    }


def get_reconstruction_server_config():
    config = Settings.inst().config
    return {
        'host': config.get('RECONSTRUCTION', 'host'),
        'port': config.get('RECONSTRUCTION', 'port'),
    }


def get_ecg_server_config():
    config = Settings.inst().config
    return {
        'host': config.get('PERIPHERAL', 'ecg_host_binary'),
        'port': config.get('PERIPHERAL', 'ecg_port_binary'),
    }


def get_peripheral_server_config():
    config = Settings.inst().config
    return {
        'contrast_injector_host': config.get('PERIPHERAL', 'contrast_injector_host'),
        'contrast_injector_port': config.get('PERIPHERAL', 'contrast_injector_port'),
        'ecg_host': config.get('PERIPHERAL', 'ecg_host'),
        'ecg_port': config.get('PERIPHERAL', 'ecg_port'),
        'blood_pressure_host': config.get('PERIPHERAL', 'blood_pressure_host'),
        'blood_pressure_port': config.get('PERIPHERAL', 'blood_pressure_port'),
        'breath_monitor_host': config.get('PERIPHERAL', 'breath_monitor_host'),
        'breath_monitor_port': config.get('PERIPHERAL', 'breath_monitor_port'),
        'oxygen_monitor_host': config.get('PERIPHERAL', 'oxygen_monitor_host'),
        'oxygen_monitor_port': config.get('PERIPHERAL', 'oxygen_monitor_port'),
    }


def get_ip_camera_config():
    config = Settings.inst().config
    return {
        'ip_address_1': config.get('IP_CAMERA', 'ip_address_1'),
        'ip_address_2': config.get('IP_CAMERA', 'ip_address_2'),
        'ip_address_3': config.get('IP_CAMERA', 'ip_address_3'),
        'ip_address_4': config.get('IP_CAMERA', 'ip_address_4'),
    }


# def get_pacs_config():
#     config = Settings.inst().config
#     return {
#         'ip_address': config.get('PACS', 'ip_address'),
#         'port': config.get('PACS', 'port'),
#     }


def get_path_config():
    settings = Settings.inst()
    return settings.get_path_config()
    # config = settings.config


def get_protocol_config():
    config = Settings.inst().config
    return {
        'T': float(config.get('PROTOCOL', 'T')),
        'k': float(config.get('PROTOCOL', 'k')),
        'can_edit_examples': True if config.get('PROTOCOL', 'can_edit_examples') == 'True' else False,
    }


def get_tomograph_config():
    config = Settings.inst()
    return config.get_tomograph_config()


def get_sievert_factor_config():
    config = Settings.inst().config
    return {
        'head': float(config.get('SIEVERT_FACTOR', 'head')),
        'neck': float(config.get('SIEVERT_FACTOR', 'neck')),
        'chest': float(config.get('SIEVERT_FACTOR', 'chest')),
        'abdominal': float(config.get('SIEVERT_FACTOR', 'abdominal')),
        'pelvis': float(config.get('SIEVERT_FACTOR', 'pelvis')),
        'limbs': float(config.get('SIEVERT_FACTOR', 'limbs')),
    }

# _initconfig()
