import logging.config
import os
from logging import Logger
from logging.handlers import RotatingFileHandler
import datetime as dt
from tango_server.utils.settings import get_path_config

LOG_MAX_BYTES = 3 * 1024 * 1024
LOG_BACKUP_COUNT = 3


class LogsFileHandler(RotatingFileHandler):
    _log_dir = get_path_config()['logs']

    def __init__(self, filename, *args, **kwargs):
        filename = os.path.join(self._log_dir, filename)
        super().__init__(filename, *args, **kwargs)


class EveryRunFileHandler(LogsFileHandler):
    def __init__(self, filename, backupCount=3, *args, **kwargs):
        super().__init__(filename, *args, backupCount=backupCount, **kwargs)
        if os.path.isfile(self.baseFilename):
            self.doRollover()


formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
performance_formatter = logging.Formatter('%(asctime)s.%(msecs)03d - %(name)s - %(levelname)s - %(message)s',
                                          datefmt="%Y-%m-%d %H:%M:%S:")
exception_formatter = logging.Formatter('\n\n%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                        datefmt="%Y-%m-%d %H:%M:%S")


def create_logger(logger_name: str, all_handler: LogsFileHandler, lvl=logging.DEBUG, fmt=None) -> Logger:
    handler = EveryRunFileHandler(f'{logger_name}.log', maxBytes=LOG_MAX_BYTES, backupCount=LOG_BACKUP_COUNT)
    handler.setFormatter(fmt or formatter)
    logger = logging.getLogger(logger_name)
    logger.setLevel(lvl)
    logger.addHandler(handler)
    logger.addHandler(all_handler)
    logger.propagate = False
    return logger


def init_logging():
    exceptions_handler = LogsFileHandler('exceptions.log', maxBytes=LOG_MAX_BYTES, backupCount=LOG_BACKUP_COUNT)
    exceptions_handler.setLevel(logging.ERROR)
    exceptions_handler.setFormatter(exception_formatter)
    exceptions_logger = logging.getLogger('exceptions')
    exceptions_logger.addHandler(exceptions_handler)

    all_handler = LogsFileHandler('all.log', maxBytes=LOG_MAX_BYTES, backupCount=LOG_BACKUP_COUNT)
    all_handler.setLevel(logging.DEBUG)
    all_handler.setFormatter(formatter)

    create_logger('events', all_handler, logging.DEBUG)
    create_logger('scsc', all_handler, logging.DEBUG)
    create_logger('tango', all_handler, logging.DEBUG)
    create_logger('user', all_handler, logging.DEBUG)
    create_logger('recon', all_handler)
    create_logger('pacs', all_handler)
    create_logger('performance', all_handler, fmt=performance_formatter)
    create_logger('peripheral', all_handler)
