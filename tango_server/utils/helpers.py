from datetime import datetime
from typing import Iterable
import wave
import contextlib


def months_period(fod: datetime, tod: datetime) -> Iterable[datetime]:
    start_month = fod.month
    end_months = (tod.year - fod.year) * 12 + tod.month + 1
    return (datetime(year=yr, month=mn, day=1) for (yr, mn) in (
        (int((m - 1) / 12) + fod.year, (m - 1) % 12 + 1) for m in range(start_month, end_months)
    ))


def round_float(value: float, precision: int):
    value = str(f"{value:.{precision}f}")
    return value


def get_audio_duration(fp: str) -> str:
    try:
        with contextlib.closing(wave.open(fp, 'r')) as f:
            frames = f.getnframes()
            rate = f.getframerate()
            return str(round_float(frames / float(rate), 2))
    except Exception as e:
        print(e)
        return 'ERROR'
